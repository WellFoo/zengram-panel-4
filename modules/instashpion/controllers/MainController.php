<?php
namespace app\modules\instashpion\controllers;
use app\models\Account;
use app\models\AccountActionLog;
use app\models\InstashpionHint;
use app\models\ShpionBalance;
use app\models\Users;
use Yii;
use yii\db\Expression;
use yii\web\Controller;

class MainController extends Controller
{
	const BONUS_TIME = (60 * 60 * 24 * 30);
	const WORK_ACCOUNT_TIME = (60 * 60 * 12);

	public function actionIndex ()
	{
		$referrer = \Yii::$app->request->getReferrer();
		$hint = new InstashpionHint();
		$hint->date = time();

		if ($referrer) {
			$host = parse_url($referrer, PHP_URL_HOST);

			$hint->source = $host;
		} else {
			$hint->source = null;
		}

		$hint->save();

		$balance = 0;
		$is_payed = false;
		$from = (time() + self::BONUS_TIME);
		$timer = false;

		if (!\Yii::$app->user->isGuest)
		{
			$balances = ShpionBalance::find()
				->select(['user_id', 'id', 'date', new Expression('array_to_json(victims) as victims'), 'is_guest'])
				->where('user_id = :user_id and date > :interval', [
					':user_id' => \Yii::$app->user->id,
					':interval' => time() - self::BONUS_TIME
				])
				->orderBy('date')
				->all();

			$user = Users::findOne(['id' => \Yii::$app->user->id]);

			if ($user->is_payed) {
				$is_payed = true;
			}

			if ((!$balances || ($balances[0] && $balances[0]->date < (time() - self::BONUS_TIME))) && $user->is_payed) {
				Yii::warning('Баланс отсутствует');

				$accounts = Account::find()->where(['user_id' => \Yii::$app->user->id])->limit(5);

				if ($accounts->count() > 0) {
					foreach ($accounts->all() as $account)  {
						$time = AccountActionLog::getActivityTime($account->id, date('Y-m-d', time() - self::WORK_ACCOUNT_TIME));

						\Yii::info($time);

						if ($time['common'] >= (60 * 60 * 11)) {
							Yii::info('Добавляем служебный баланс');

							$model = new ShpionBalance();

							$model->date = time();
							$model->victims = '{}';
							$model->is_bonus = 1;
							$model->user_id = \Yii::$app->user->id;

							$model->save();

							$balance = 3;
							$from = (time()  + self::BONUS_TIME);

							$timer = true;
							break;
						} else {
							Yii::error('Аккаунт проработал только ' . $time['common'] . ' сек.');
						}
					}
				}
			}

			if ($balances) {
				Yii::warning('Баланс присутствует');
				$lastBalance = null;

				$guests_balances = array_filter((array) $balances, function ($balance) {
					return ($balance->is_guest == 1);
				});

				Yii::info($guests_balances);

				if (count($guests_balances) < count($balances)) {
					$is_payed = true;
				}

				foreach ($balances as $model) {
					$lastBalance = $model;

					\Yii::info(json_decode($model->victims));
					\Yii::info($model->victims);

					$victims = json_decode($model->victims);

					$victims = array_filter((array) $victims, function ($victim) {
						return ($victim > 0);
					});

					$balance = $balance + (3 - count($victims));
				}

				if ($lastBalance) {
					$from = ($lastBalance->date  + self::BONUS_TIME);
				}

				$timer = true;
			}
		} else {
			if (!\Yii::$app->request->cookies->has('shpion_balance')) {
				\Yii::$app->response->cookies->add(
					new \yii\web\Cookie([
						'name' => 'shpion_balance',
						'value' => 3
					]));

				$balance = 3;
			} else {
				$balance = \Yii::$app->request->cookies->get('shpion_balance')->value;
			}

			$from = (time()  + self::BONUS_TIME);
		}

		Yii::info('isPayed', $is_payed);


		return $this->render('index', [
			'balance' => intval($balance),
			'from' => $from,
			'timer' => $timer,
			'is_payed' => $is_payed
		]);
	}
}
