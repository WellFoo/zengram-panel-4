<?php
namespace app\modules\instashpion\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%victims}}".
 *
 * @property integer $id
 *
 * @property integer $follower_count
 * @property integer $following_count
 * @property integer $media_count
 *
 * @property string $username
 * @property string $name
 * @property string $avatar
 *
 * @property integer $instagram_id
 *
 * @property integer $updated_at
 */
class Victim extends ActiveRecord
{
	const UPDATE_TIMEOUT = 1 * 60;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%victims}}';
    }

	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['follower_count', 'following_count', 'media_count', 'instagram_id'], 'integer'],
            [['username', 'name', 'avatar'], 'string'],
            [['updated_at'], 'safe']
        ];
    }
    
    public function lastAnalize() {
        return Analize::find()
            ->where([
                'victim_id' => $this->id
            ])
        ->orderBy(['id' => SORT_DESC])
        ->one();
    }
/*

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'username' => 'Login',
            'instagram_id' => 'Instagram ID',
        ];
    }

	public static function checkUpdateDate($date)
	{
		return time() > $date + self::UPDATE_TIMEOUT;
	}

	public function getAccount()
	{
		return CacheInstagramAccount::getAccount($this->instagram_id);
	}

	public static function getVictims()
	{
		return self::findAll([
			'user_id' => Yii::$app->user->getId()
		]);
	}

    public function getVictimsLogs()
    {
        return $this->hasMany(VictimsLog::className(), ['victim_id' => 'id']);
    }

	public static function addVictim($id)
	{

		$api = new iApi();
		$user = $api->getUser($id);

		if ($api->error !== false) {

			if ($api->error === iApi::ERROR_PRIVATE) {
				PrivateAccounts::add($id);
			}

			return $api->error;
		}

		$victim = new Victims([
			'instagram_id'      => (string) $user['pk'],
			'count_media'       => intval($user['media_count']),
			'count_followed_by' => intval($user['follower_count']),
			'count_follows'     => intval($user['following_count']),
			'last_update'       => 0,
			'user_id' => 		Yii::$app->user->getId(),
		]);

		if ($victim->save()) {
			return true;
		}

		return false;
	}

	public function getUser()
	{
		return User::findOne($this->user_id);
	}

	public function getToken()
	{
		// TODO: проверка полученного токена
		return $this->getUser()->access_token;

	}

	public static function deleteVictim($id)
	{
		               //    Перамидко     \\
		             self::deleteAll(['id' => $id]);
		           Medias::deleteAll(['victim_id' => $id]);
		          Follows::deleteAll(['victim_id' => $id]);
		       VictimsLog::deleteAll(['victim_id' => $id]);
		    VictimsUpdate::deleteAll(['victim_id' => $id]);
		   VictimActivity::deleteAll(['victim_id' => $id]);
		VictimUpdateTasks::deleteAll(['victim_id' => $id]);
		//                 | Хеопз |                    \\


		$command = 'ps aux | grep \'/var/www/html/yii victim/analyze '.$id.'\' | grep -v grep | awk \'{print $2}\'';
		exec($command ,$op);
	}

	public function updateVictimAccount($api = null)
	{
		if (is_null($api)) {
			$api = new iApi();
		}

		$data = $api->getUser($this->instagram_id);

		$this->setAttributes([
			'count_media'       	=> intval($data['media_count']),
			'count_followed_by' 	=> intval($data['follower_count']),
			'count_follows'     	=> intval($data['following_count']),

			'count_new_media'       => intval($data['media_count']) - $this->count_media,
			'count_new_followed_by' => intval($data['follower_count']) - $this->count_followed_by,
			'count_new_follows'     => intval($data['following_count']) - $this->count_follows,

			'last_update' => time()
		]);
		$this->save();
	}

	public static function getLimits($id)
	{

		$victim = Victims::findOne($id);

		return [
			VictimsUpdate::STAGE_PROCESSING_FOLLOWS => $victim->count_follows,
			VictimsUpdate::STAGE_SEARCH_FOLLOWS => $victim->count_follows,
			VictimsUpdate::STAGE_SCAN_MUTUAL_LIKES => $victim->count_media,
			VictimsUpdate::STAGE_SCAN_TOP => 20 * 99, // TODO: говнокод
		];
	}
	*/
}
