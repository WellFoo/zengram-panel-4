var AppComponent = React.createClass({

    mixins: [
        Backbone.React.Component.mixin
    ],

    componentDidMount () {
        this.on(this, {
            models: {
                store: this.props.store,
                analize: this.props.store.get('analize')
            }
        });
    },

    componentWillUnmount () {
        this.off(this);
    },

    changeUser(user) {
        app.set({
            'user': user,
            'isBrowser': false
        });
        app.get('analize').flush();
    },

    addAccount(user, login, password) {
        this.props.store.set('handlerUser', new HandlerUser(user));
        this.props.store.get('handlerUser').set({
            'login': login,
            'password': password
        });

        this.props.store.get('user').set('is_private', false);
    },

    change() {
        app.set({
            user: null
        });

        app.get('analize').flush();
    },

    render() {
        var self = this;

        var store = this.state.store;

        return (
            <div className="common-form">
                <SearchForm searchResult = {this.props.store.get('searchResult')} store = {this.props.store} onSelectUser = {this.changeUser} />

                {(() => {
                    { /* Отображаем информацию об аккаунте */ }

                    if (app.get('user') !== null && app.get('user').get('instagram_id') > 0) {
                        return (
                            <AccountInfo
                                model = {app.get('user')}
                                store = {self.props.app}
                                process = {app.get('analize').get('process')}
                                onStartAnalize = {this.startAnalize}
                                onStartReAnalize = {this.startReanalize}
                                onAddAccount = {this.addAccount}
                                onRemove = {this.change}
                            />
                        );
                    } else {
                        /*
                         * Часть шаблона
                         */
                        return (
                            <div className="info__wrap">
                                <section className="info">
                                    <div className="info__advantages">
                                        <h2 className="info__column-title info__advantages-title">За кем мы рекомендуем следить</h2>
                                        <ul className="info__advantages-list">
                                            <li className="info__advantages-item _1">
                                                <h3 className="info__advantage-tile">Вторая половинка</h3>
                                                <p className="info__advantage-description">Хотите узнать, кому ставит лайки ваша половинка?</p>
                                            </li>
                                            <li className="info__advantages-item _6">
                                                <h3 className="info__advantage-tile">Подруга</h3>
                                                <p className="info__advantage-description">Вам и подруге нравится один и тот же парень? Проверьте наверняка!</p>
                                            </li>
                                            <li className="info__advantages-item _2">
                                                <h3 className="info__advantage-tile">Муж</h3>
                                                <p className="info__advantage-description">Не изменял ли вам муж, будучи в командировке?</p>
                                            </li>
                                            <li className="info__advantages-item _7">
                                                <h3 className="info__advantage-tile">Вам кто-то нравится</h3>
                                                <p className="info__advantage-description">Вы влюбились и хотите узнать о человеке как можно больше?</p>
                                            </li>
                                            <li className="info__advantages-item _3">
                                                <h3 className="info__advantage-tile">Жена</h3>
                                                <p className="info__advantage-description">Интересно, что делала жена, пока вас не было дома?</p>
                                            </li>
                                            <li className="info__advantages-item _8">
                                                <h3 className="info__advantage-tile">Знакомые люди</h3>
                                                <p className="info__advantage-description">Знакомый стучится к вам в друзья? Хотите узнать, что он за человек?</p>
                                            </li>
                                            <li className="info__advantages-item _5">
                                                <h3 className="info__advantage-tile">Свои дети</h3>
                                                <p className="info__advantage-description">Хотите получать отчет о действиях вашего ребенка в Instagram?</p>
                                            </li>
                                            <li className="info__advantages-item _9">
                                                <h3 className="info__advantage-tile">Бывшая любовь</h3>
                                                <p className="info__advantage-description">Нужна информация о том, с кем теперь встречается бывший?</p>
                                            </li>
                                        </ul>
                                    </div>
                                </section>
                            </div>
                        );
                    }
                })()}

                {(() => {
                    { /* Отображаем анализ */ }

                    if (app.get('analize').get('id') !== null || app.get('analize').get('reanalize') !== null) {
                        var id = app.get('analize').get('id');

                        if (app.get('analize').get('reanalize') !== null) {
                            id = app.get('analize').get('reanalize');
                        }

                        return (
                            <ProgressPanel store = {self.props.store} analize_id = {id} />
                        );
                    }
                })()}
            </div>
        );
    }
});
