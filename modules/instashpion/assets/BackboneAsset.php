<?php
namespace app\modules\instashpion\assets;

use yii\web\View;
use yii\web\AssetBundle;

class BackboneAsset extends AssetBundle
{
	public $sourcePath = '@bower';
	public $js = [
		YII_ENV_DEV ? 'underscore/underscore.js' : 'underscore/underscore-min.js',
		'backbone/backbone.js',
	];
	public $jsOptions = [
		'position' => View::POS_HEAD,
	];
}