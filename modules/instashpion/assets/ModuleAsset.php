<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\instashpion\assets;

use yii\web\View;
use yii\web\AssetBundle;

class ModuleAsset extends AssetBundle
{
	public $jsOptions = ['position' => View::POS_HEAD];
	public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
	    'css/bootstrap-toggle.min.css',
	    'css/site.css',
        'css/instaspion.css',
	    'css/helvetica.css',
	    'css/jquery.tagit.css',
	    'css/font-awesome.min.css',
    ];

    public $js = [
	    'js/zengram-i18n.js',
	    'js/bootstrap-toggle.min.js',
	    'js/jquery.dotdotdot.min.js',
	    'js/bootbox.min.js',
	    'js/tag-it.js',
	    'js/global.js',
	    'js/moment.js',
	    'js/moment-with-locales.js',
	    'js/share.js',
	    'js/debug.js',
	    'js/jquery.cookie.js'
    ];

    public $depends = [
	    'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
	    'yii\bootstrap\BootstrapPluginAsset',
	    'yii\web\YiiAsset',
	    'yii\jui\JuiAsset',
	    'app\modules\instashpion\assets\ReactBackboneComponentAsset',
    ];
}
