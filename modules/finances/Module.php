<?php

namespace app\modules\finances;

use app\models\Users;
use yii\web\ForbiddenHttpException;
use Yii;

class Module extends \yii\base\Module
{
	public $controllerNamespace = 'app\modules\finances\controllers';

//	public $defaultRoute = 'users';
	public $layout = 'main';

	public function init()
	{
		parent::init();
		// custom initialization code goes here

		$availableRoles = [Users::ROLE_FINANCIER, Users::ROLE_ADMIN];
		if (Yii::$app->user->isGuest || !in_array(Yii::$app->user->identity->role, $availableRoles)) {
			throw new ForbiddenHttpException('You are not allowed to access this page.');
		}
	}
}
