<?php namespace app\modules\partnership\forms;

use app\modules\partnership\models\Partner;
use app\modules\partnership\Module;
use Yii;
use yii\base\Model;

class PartnerForm extends Model
{
	/** @var Partner */
	private $_partner;

	public $name;
	public $walet;
	public $confirm_oferta;

	public function rules()
	{
		return [
			[['confirm_oferta', 'walet'], 'required'],
			[['name', 'walet'], 'filter', 'filter' => 'trim'],
			['walet', 'filter', 'filter' => 'strtoupper'],
			['walet', 'match', 'pattern' => '/^R\d{12}$/i'],
			['name', 'string'],
			['confirm_oferta', 'integer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'name'  => Module::t('module', 'Name'),
			'walet' => Module::t('module', 'Walet ({type})', ['type' => 'WMR']),
		];
	}

	/**
	 * @param Partner $partner
	 */
	public function setPartner($partner)
	{
		$this->_partner = $partner;
		$this->name = $partner->name;
		$this->walet = $partner->walet;
	}

	public function save($validate = true)
	{
		if ($validate && !$this->validate()) {
			return false;
		}

		$this->_partner->name = $this->name;
		$this->_partner->walet = $this->walet;
		return $this->_partner->save();
	}
}