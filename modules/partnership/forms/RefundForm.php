<?php namespace app\modules\partnership\forms;

use app\modules\partnership\models\Manager;
use app\modules\partnership\models\Partner;
use app\modules\partnership\models\Payment;
use app\modules\partnership\models\Refund;
use app\modules\partnership\Module;
use yii\base\Model;

/**
 * Class RefundForm
 * @package app\modules\partnership\forms
 * @property Partner $partner
 */
class RefundForm extends Model
{
	/** @var Partner */
	private $_partner;
	public $amount;
	public $target;
	public $walet;

	public function rules()
	{
		return [
			[['amount', 'target'], 'required'],
			['amount', 'number', 'min' => 1, 'max' => $this->_partner->balance],
			['target', 'in', 'range' => [Refund::TYPE_INNER, Refund::TYPE_WALLET]],
			['walet', 'match', 'pattern' => '/^R\d{12}$/i'],
			['walet', 'required',
				'whenClient' => "function(attribute, value) {
					return $('#refundform-target option:selected').val() == 'wallet';
				}",
				'when' => function($model) {
				return $model->target === Refund::TYPE_WALLET;
			}]
		];
	}

	public function attributeLabels()
	{
		return [
			'amount' => Module::t('module', 'Sum'),
			'target' => Module::t('module', 'Where'),
			'walet' => 'Кошелёк WMR'
		];
	}

	public function checkWallet($attribute, $params)
	{
		$this->addError('Вы не указали номер WMR кошелька');
		if ($this->target === Refund::TYPE_WALLET && empty($this->_partner->walet)) {
			$this->addError('Вы не указали номер WMR кошелька');
			$this->addError($attribute, Module::t('module', 'You must specify the number of the wallet in the Personal Area settings'));
		}
	}

	public function refund()
	{
		if (!$this->validate()) {
			return false;
		}

		$refund = new Refund();
		$refund->partner_id = $this->_partner->id;
		$refund->amount = $this->amount;
		$refund->target = $this->target;
		$refund->wallet = $this->_partner->walet;
		if ($this->_partner->walet == '' && $this->walet != 'R000000000000'){
			$partner = Partner::findOne($this->_partner->id);
			$partner->walet = $this->walet;
			$partner->save();
			$refund->wallet = $this->walet;
		}
		$refund->save();

		switch ($this->target) {
			case 'wallet':
				$this->toWalet($refund);
				break;

			case 'inner':
				$this->toInner($refund);
				break;
		}

		return true;
	}

	/**
	 * @param $refund Refund
	 */
	private function toWalet($refund)
	{
		if ($refund->amount < Module::$settings->min_refund) {
			return;
		}

		$subject = Module::t('module', 'New refund request');
		$body = Module::t('module', 'Received a new request for withdrawal of funds from the user {user} with amount {amount}', [
			'user' => $this->_partner->user->emailAddress,
			'amount' => \Yii::$app->formatter->asCurrency($refund->amount)
		]);

		/** @var Manager[] $managers */
		$managers = Manager::find()->with(['user'])->all();
		foreach ($managers as $manager) {
			$this->sendNotification(
				$manager->user->emailAddress,
				$subject,
				$body
			);
		}
	}

	/**
	 * @param $refund Refund
	 */
	private function toInner($refund)
	{
		$this->_partner->user->addBalance($refund->amount);
		$refund->close();
	}

	/**
	 * @param string $address
	 * @param string $subject
	 * @param string $body
	 * @return bool
	 */
	private function sendNotification($address, $subject, $body)
	{
		try {
			\Yii::$app->mailer->compose()
				->setFrom(\Yii::$app->params['adminEmail'])
				->setTo($address)
				->setSubject($subject)
				->setTextBody($body)
				->setHtmlBody($body)
				->send();
		} catch (\Exception $e) {
			return false;
		}
		return true;
	}

	/**
	 * @param $partner Partner
	 */
	public function setPartner($partner)
	{
		$this->_partner = $partner;
		if (!is_null($partner)) {
			$this->amount = $partner->balance;
		}
	}
}