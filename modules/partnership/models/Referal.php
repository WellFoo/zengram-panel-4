<?php namespace app\modules\partnership\models;

use app\modules\partnership\Module;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pfx_partnership_referals".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $partner_id
 * @property string  $reg_date
 *
 * @property User    $user
 * @property Partner $partner
 */
class Referal extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%partnership_referals}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'partner_id'], 'required'],
			[['user_id', 'partner_id'], 'integer'],
			[['reg_date'], 'safe']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'         => Module::t('module', 'ID'),
			'user_id'    => Module::t('module', 'User ID'),
			'partner_id' => Module::t('module', 'Partner ID'),
			'reg_date'   => Module::t('module', 'Reg Date'),
		];
	}

	public function getUser()
	{
		return self::hasOne(User::className(), ['id' => 'user_id']);
	}

	public function getPartner()
	{
		return self::hasOne(Partner::className(), ['id' => 'partner_id']);
	}
}
