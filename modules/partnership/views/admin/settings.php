<?php
/**
 * @var \yii\web\View $this
 */

use app\modules\partnership\Module;
use yii\helpers\Html;

$this->title = Module::t('module', 'Settings');
?>

<h1><?= $this->title ?></h1>

<?php
$form = \yii\widgets\ActiveForm::begin();

echo $form->field(Module::$settings, 'percent_1lvl');
echo $form->field(Module::$settings, 'percent_2lvl');
echo $form->field(Module::$settings, 'min_refund');

echo Html::submitButton(Module::t('module', 'Save'), ['class' => 'btn btn-success']);
echo Html::a(Module::t('module', 'Cancel'), ['index'], ['class' => 'btn btn-default']);

$form::end();
?>
