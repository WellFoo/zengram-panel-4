<?php

use yii\helpers\Url;

$this->title = 'Статистика выплат';

?>

<ul class="nav nav-tabs">
	<li><a href="<?= Url::to(['manager/index'])?>">Выплаты</a></li>
	<li class="active"><a href="<?= Url::to(['manager/statistic'])?>">Статистика</a></li>
</ul>

<?= $this->render('statistic.int.php') ?>