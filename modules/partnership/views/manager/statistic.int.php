<?php

use app\modules\partnership\models\Partner;
use app\modules\partnership\models\Refund;
use app\modules\partnership\models\User;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

$date_to = Yii::$app->request->get('date_to');
$date_from = Yii::$app->request->get('date_from');

if (empty($date_from) || empty($date_to)) {
	$now = new \DateTime();
	$now->setDate($now->format('Y'), (int) $now->format('m') + 1, 1);
	$date_to = $now->format('Y-m-d');
	$now->sub(new \DateInterval('P1M'));
	$date_from = $now->format('Y-m-d');
}

$query = Partner::find()->select([
	Partner::tableName().'.*',
	User::tableName().'.mail',
]);

$query->leftJoin(User::tableName(),
	User::tableName().'.id = '.Partner::tableName().'.user_id'
);

$query->addSelect(['wait_to_refund' => sprintf(
	"(SELECT SUM(amount) FROM %s WHERE partner_id = %s AND is_refunded = 0)",
	Refund::tableName(),
	Partner::tableName().'.id'
)]);

$query->addSelect(['refunded' => sprintf(
	"(SELECT SUM(amount) FROM %s WHERE partner_id = %s AND is_refunded = 1)",
	Refund::tableName(),
	Partner::tableName().'.id'
)]);

$dataProvider = new ActiveDataProvider([
	'query' => $query->asArray(),
	/*'sort'  => [
		'defaultOrder' => [
			'date' => SORT_ASC
		]
	]*/
]);

?>

<!-- Статистика -->
<div class="modal fade" id="show-statistic">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<h4 class="title"></h4>

				<div class="stat-content">

				</div>

			</div>
			<div class="modal-footer clearfix">
				<a href="#" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</a>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="show-referal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<h4 class="title"></h4>
				<a class="ref-content" href="#"></a>
			</div>
			<div class="modal-footer clearfix">
				<a href="#" class="btn btn-default pull-right" data-dismiss="modal">Закрыть</a>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<style>

	.refunds .form-control {
		height: 34px !important;
		border-radius: 0 !important;
		border: 1px solid #ccc !important;
	}

	.refunds [type=submit] {
		border-radius: 0 4px 4px 0 !important;
	}

	thead .form-control {
		cursor: pointer;
	}

	thead .form-control:hover {
		border-color: #006600;
	}

	.nav-tabs {
		margin-bottom: 35px;
	}
</style>

<form class="form-horizontal refunds">
	<div class="form-group">
		<label class="control-label col-xs-5">Детальная статистика за период</label>
		<div class="col-xs-5">
			<div class="input-group">
				<span class="input-group-addon">С</span>
				<?= Html::input('date', 'date_from', $date_from, ['class' => 'form-control']) ?>
				<span class="input-group-addon">По</span>
				<?= Html::input('date', 'date_to', $date_to, ['class' => 'form-control']) ?>
				<!--
				<span class="input-group-btn">
					<button type="submit" class="btn btn-success">Применить</button>
				</span>
				-->
			</div>
		</div>
	</div>
</form>


<?= \yii\grid\GridView::widget([
	'dataProvider' => $dataProvider,
	'columns' => [
		[
			'attribute' => 'id',
			'label' => 'Партнёр',
			'format' => 'raw',
			'value' => function($item){
				return $item['mail'].'<br/>Текущий баланс: <strong>'.$item['balance'].'</strong> руб.';
			}
		], [
			'attribute' => 'reg_date',
			'label' => 'Дата регистрации',
			'value' => function($item) {
				return date('d-m-Y', strtotime($item['reg_date']));
			}
		], [
			'attribute' => 'refunded',
			'format' => 'currency',
			'label' => 'Выведено',
			'value' => function($item) {
				if (!$item['refunded']) {
					return 0;
				}

				return $item['refunded'];
			}
		], [
			'attribute' => 'wait_to_refund',
			'format' => 'currency',
			'label' => 'Требуется вывести',
			'value' => function($item) {
				if (!$item['wait_to_refund']) {
					return 0;
				}

				return $item['wait_to_refund'];
			}
		], [
			'format' => 'raw',
			'label' => '',
			'value' => function($item) {
				/* @var $partner Partner */
				$partner = Partner::findOne($item['id']);
				return Html::a(
					'<span class="glyphicon glyphicon-list-alt"></span>',
					['#'],
					[
						'title' => 'Статистика рефералов',
						'class' => 'show-stat',
						'data' => [
							'partner-id' => $item['id']
						]
					]).' '.
					Html::a(
						'<span class="glyphicon glyphicon-link"></span>',
						['#'],
						[
							'title' => 'Реферальная ссылка',
							'class' => 'show-ref',
							'data' => [
								'ref_link' => $partner->referal_link,
								'email' => $item['mail'],
							]
						]
				);
			}
		]
	]
]) ?>

<script>

	var $statisticModal = $('#show-statistic'),
		$refModal = $('#show-referal'),
		partnerStatUrl = '/partnership/manager/get-partner-stat/',
		$refundForm = $('form.refunds');

	$('.show-stat').click(function(){
		$.post(
			partnerStatUrl,
			{
				id: $(this).attr('data-partner-id'),
				date_from: $refundForm.find('[name=date_from]').val(),
				date_to: $refundForm.find('[name=date_to]').val()
			},
			function(responce){

				if (responce.status === 'error') {
					return false;
				}

				$statisticModal.find('.title').html('Статистика по партнёру ' + responce.email);
				$statisticModal.find('.stat-content').html(responce.content);
				$statisticModal.modal('show');

			},
			'json'
		);
		return false;
	});

	$('.show-ref').click(function(){
		var link = $(this).data('ref_link');
		$refModal.find('.title').html('Реферальная ссылка партнёра ' + $(this).data('email'));
		$refModal.find('.ref-content').attr('href', link).html(link);
		$refModal.modal('show');
		return false;
	})


</script>