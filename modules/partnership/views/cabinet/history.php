<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $data_provider
 */

use app\modules\partnership\Module;
use app\modules\partnership\asset\ModuleAsset;

ModuleAsset::register($this);
$this->title = Module::t('module', 'Refunds histroy');
?>
<div class="link-f clearfix">
	<a class="i2" href="/partnership/cabinet/index/" style="background-color: #E8E8E8;">Рефералы</a>
	<a class="i1" href="/partnership/cabinet/banners/" style="background-color: #E8E8E8;">Рекламные материалы</a>
</div>
<p class="green-text" style="margin-bottom:15px;">История вывода</p>
<?= \yii\grid\GridView::widget([
	'dataProvider' => $data_provider,
	'columns' => [
		'date',
		'amount:currency',
		[
			'label' => Module::t('module', 'Destination'),
			'value' => function ($item) {
				/** @var \app\modules\partnership\models\Refund $item */
				if ($item->target === $item::TYPE_INNER) {
					return Module::t('module', 'Internal Zenlink balance');
				}
				return $item->wallet;
			}
		],
	]
]) ?>
<div class="view" style="display: none;">
	Отображать по:
	<a class="selected" href="#">10</a>
	<a onclick="change(20);" href="#">20</a>
	<a onclick="change(30);" href="#">30</a>
</div>
