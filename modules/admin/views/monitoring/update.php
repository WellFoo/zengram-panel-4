<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Monitoring */

$this->title = 'Обновить отслеживание: ' . ' ' . $model->category.' '.$model->title;
$this->params['breadcrumbs'][] = ['label' => 'Отслеживания', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="monitoring-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
