<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title                   = 'Страницы';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-admin">

    <?php if (Yii::$app->session->hasFlash('PageDeleted')): ?>
        <div class="alert alert-success">
            Страница успешно удалена
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-xs-12 col-md-2 col-sm-2">
            <a href="<?=Url::to(['edit']); ?>" class="btn btn-primary">Добавить страницу</a>
        </div>
        <div class="col-xs-12 col-md-10 col-sm-10">

        </div>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Заголовок</th>
            <th>Альяс</th>
            <th class="text-center">Вес</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($pages as $page): ?>
            <tr>
                <td><?php echo $page['id']; ?></td>
                <td><?php echo $page['title']; ?></td>
                <td><?php echo $page['alias']; ?></td>
                <td class="text-center"><?php echo $page['weight']; ?></td>
                <td><?php echo Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>',
                        ['edit', 'id' => $page['id']], array('class' => '')); ?></td>
                <td><?php echo Html::a('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>',
                        ['delete', 'id' => $page['id']], array('class' => '')); ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

</div>