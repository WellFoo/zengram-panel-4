<?php

use yii\helpers\Html;

/**
 * @var $this   yii\web\View
 * @var $place  app\models\Place
 * @var $points array
 * @var $type   integer
 */

$this->registerJsFile('//maps.google.com/maps/api/js', ['position' => $this::POS_HEAD]);

$this->title = 'Рабочие точки / ' . $place->name;
?>

<div class="site-admin">

	<h1><?= Html::a('Рабочие точки', ['/admin/geo-points']) . ' / ' . $place->name ?></h1>

	<div id="map_canvas" style="height: 600px;"></div>

	<?php \yii\widgets\ActiveForm::begin(['id' => 'type-form']); ?>
		<div>
			<?= Html::radioList('type', $type, ['рабочие', 'сетка'], [
				'itemOptions' => [
					'onChange' => '$("#type-form").submit();'
				]
			]) ?>
		</div>
	<?php \yii\widgets\ActiveForm::end(); ?>
</div>

<script type="text/javascript">
	var points = JSON.parse('<?= json_encode($points) ?>');

	function initialize()
	{
		/** @namespace google.maps */
		/** @namespace google.maps.LatLng */
		/** @namespace google.maps.LatLngBounds */
		/** @namespace google.maps.Marker */
		/** @namespace google.maps.Rectangle */
		/** @namespace google.maps.Circle */
		var center = new google.maps.LatLng('<?= $place->lat ?>', '<?= $place->lng ?>');

		var corner1 = new google.maps.LatLng(
			'<?= $place->northEastLat ?>',
			'<?= $place->northEastLng ?>'
		);
		var corner2 = new google.maps.LatLng(
			'<?= $place->southWestLat ?>',
			'<?= $place->southWestLng ?>'
		);

		var bounds = new google.maps.LatLngBounds(corner2, corner1);

		var map = new google.maps.Map(document.getElementById('map_canvas'), {
			zoom: 8,
			center: center
		});

		map.fitBounds(bounds);

		new google.maps.Marker({
			position: center,
			map: map,
			title: 'C'
		});

		new google.maps.Rectangle({
			bounds: bounds,
			map: map,
			fillColor: '#FFF',
			fillOpacity: 0.3,
			strokeColor: '#FFF',
			strokeOpacity: 0.7,
			zIndex: 0
		});

		for (var i = 0; i < points.length; i++) {
			new google.maps.Circle({
				center: new google.maps.LatLng(points[i].lat, points[i].lng),
				map: map,
				radius: 1000,
				fillColor: '#5CB85C',
				fillOpacity: 0.3,
				strokeColor: '#5CB85C',
				strokeOpacity: 0.7,
				zIndex: 1
			});
		}
	}
	google.maps.event.addDomListener(window, 'load', initialize);
</script>