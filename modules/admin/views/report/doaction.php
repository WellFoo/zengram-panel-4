<?php
use yii\grid\GridView;
use app\models\Users;
use app\modules\admin\models\Balance;
use yii\helpers\Html;

/* @var $this yii\web\View
 * @var integer $count
 * @var string $date
 * @var string $reportType
 * @var integer $dateFrom
 * @var integer $dateTo
 * @var integer $sum
 * @var integer $sumPaypal
 * @var integer $sumRobokassa
 * @var integer $sum2co
 * @var integer $countUsersFirst
 * @var integer $countUsersAfter
 * @var float $sumUsersFirst
 * @var float $sumUsersAfter
 *
 */


$this->title = 'Отчёты';
?>
<?= $this->render('_filters', ['date' => $date, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo, 'reportType' => $reportType]); ?>
<div class="row" style="padding-top:30px;">
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			//['class' => 'yii\grid\SerialColumn'],
			'id',
			'mail',
			[
				'label' => 'Платежи',
				'attribute' => 'invoices',
				'format' => 'raw',
				'value' => function ($item) {
					//$test = print_r($item->invoices,true);

					$result = '';
					if (count($item->invoices) == 0) {
						return 'Нет';
					}
					foreach ($item->invoices as $invoice) {
						$result .= '<strong>' . $invoice->sum . '</strong> | ' . $invoice->agent;
						$date = new DateTime($invoice->date);
						$result .= ' | ' . $date->format('Y-m-d H:i:s') . '<br>';

					}
					return $result;
					//return(count($item->invoices));
				}
			],
			[
				'label' => 'Баланс',
				'attribute' => 'balance',
				'format' => 'raw',
				'contentOptions' => ['align' => 'center', 'style' => 'vertical-align:middle'],
				'value' => function ($item) {
					/** @var Users $item */
					//balance is in seconds
					return Html::a(Balance::formatBalance($item->balance),
						'javascript:void(0)', ['id' => 'balance_' . $item->id,
							'onclick' => 'showData(' . $item->id . ', \'balance-log\')']);
				}
			],
			[
				'label' => 'Дата регистрации',
				'attribute' => 'regdate',
				'format' => 'raw',
				'value' => function ($item) {
					$date = new DateTime($item->regdate);
					$result = $date->format('Y-m-d H:i:s');
					return $result;
				}
			]
		],
	]);
	?>

</div>
