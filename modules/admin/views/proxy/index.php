<?php

use app\models\Proxy;
use app\models\ProxyCat;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View
 * @var $currentCatId integer
 * @var $cats array[ProxyCat]
 * @var $proxies array[Proxy]
 */

$this->registerJsFile('@web/js/lib/can.custom.js', [
	'position' => \yii\web\View::POS_HEAD,
	'depends' => ['\yii\web\JqueryAsset']
]);

$cat_list = [];
foreach ($cats as $item) {
	$cat_list[$item['id']] = $item['name'];
	if ($item['default']) {
		$cat_list[$item['id']] .= ' (по умолчанию)';
	}
}

$this->title = 'Прокси-серверы';
?>

<style>
	.proxy-state {
		border-radius: 50%;
		cursor: pointer;
		height: 15px;
		width: 15px;
		margin-top: 3px;
	}
	.proxy-state.status-undefined {
		background: #BBB;
	}
	.proxy-state.status-error {
		background: #B00;
	}
	.proxy-state.status-warning {
		background: #BB0;
	}
	.proxy-state.status-ok {
		background: #0B0;
	}

	.loader.progress {
		height: 4px;
		border-radius: 2px;
		margin-bottom: 5px;
		opacity: 0;
	}
	.loader.progress .progress-bar {
		width: 100%;
	}
</style>

<?php
$cat_model = new ProxyCat();
$form = \yii\bootstrap\ActiveForm::begin([
	'action' => ['/admin/proxy-cat/create'],
	'layout' => 'horizontal',
	'enableAjaxValidation' => true,
	'validationUrl' => ['/admin/proxy-cat/validate']
]);

\yii\bootstrap\Modal::begin([
	'id' => 'proxy-cat-modal',
	'header' => '<h2>Добавить новую категорию</h2>',
	'footer' => Html::submitButton('Готово', ['class' => 'btn btn-primary']),
]);

echo $form->field($cat_model, 'name');

echo $form->field($cat_model, 'default')->checkBox();

\yii\bootstrap\Modal::end();

\yii\bootstrap\ActiveForm::end();
?>

<div class="site-admin">

	<div class="row form-horizontal">

		<div class="form-group" style="margin-right: 0; margin-left: 0;">

			<label for="proxy-cat" class="control-label pull-left">Категория:</label>

			<div class="col-sm-3">
				<?= Html::dropDownList(
					'proxy-cat',
					$currentCatId,
					$cat_list,
					[
						'id' => 'proxy-cat',
						'class' => 'form-control',
						'prompt' => 'Все'
					]
				) ?>
			</div>

			<div class="pull-left">
				<div class="dropdown">
					<button id="proxy-cat-dropdown" class="btn btn-primary" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<span class="glyphicon glyphicon-menu-hamburger"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="proxy-cat-dropdown">
						<li class="dropdown-header">Действия</li>
						<li><a href="#" data-toggle="modal" data-target="#proxy-cat-modal">Добавить категорию</a></li>
						<li role="separator" class="divider"></li>
						<li class="dropdown-header">Текщая категория</li>
						<li><a href="#" id="set-proxy-default">Назначить по умолчанию</a></li>
						<li><a href="#" id="remove-proxy">Удалить</a></li>
					</ul>
				</div>
			</div>

			<div class="pull-left" style="margin-left: 15px;">
				<?php

				$proxy_model = new Proxy();
				$form = ActiveForm::begin([
					'action' => \yii\helpers\Url::to(['create']),
					'layout' => 'horizontal'
				]);

				\yii\bootstrap\Modal::begin([
					'header' => '<h2>Добавить новый прокси-сервер</h2>',
					'footer' => Html::submitButton(
						'Готово',
						[
							'class' => 'btn btn-primary',
						]),
					'toggleButton' => [
						'label' => 'Добавить прокси-сервер',
						'class' => 'btn btn-primary'
					],
				]);
				echo $form->field($proxy_model, 'cat_id')->dropDownList(
					$cat_list,
					[
						'id' => 'new-proxy-cat',
						'prompt' => 'Выберите'
					]
				);
				echo $form->field($proxy_model, 'proxy');

				\yii\bootstrap\Modal::end();

				\yii\bootstrap\ActiveForm::end();

				?>
			</div>
		</div>

	</div>

	<div class="row">
		<div id="ajax-loader" class="progress loader">
			<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"></div>
		</div>
	</div>

	<div class="row">
		<table class="table table-striped">
			<thead>
			<tr>
				<th width="1%">#</th>
				<th width="1%"></th>
				<th>Прокси</th>
				<th width="5%">Пользователей онлайн</th>
				<th width="1%"></th>
			</tr>
			</thead>
			<tbody id="proxy-list">
			<tr>
				<th></th>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
<script type="text/mustache" id="proxy-list-template">
	{{#each items}}
		<tr {{data 'item'}}>
			<th>{{id}}</th>
			<td><div class="proxy-state status-{{status}}"></div></td>
			<td>{{proxy}}</td>
			<td>{{online}}</td>
			<td>
				<a class="btn btn-link btn-xs delete" title="Удалить">
					<span class="glyphicon glyphicon-trash"></span>
				</a>
			</td>
		</tr>
	{{/each}}
</script>

<script type="text/javascript">
	jQuery(function($) { 'use strict';
		var controls = {
			catSelect: $('#proxy-cat'),
			setDefaultProxy: $('#set-proxy-default'),
			removeProxy: $('#remove-proxy')
		};
		var currentCat = controls.catSelect.val();

		var proxyData = JSON.parse('<?= json_encode($proxies) ?>');

		var ProxyList = can.Control.extend({
			init: function(el, op) {
				this.options.items = new can.List(op.items);
				this.element.html(can.view('proxy-list-template', {
					items: this.options.items
				}));
			},
			'.delete click': deleteClick,
			'.proxy-state click': checkProxy
		});

		var proxyList = new ProxyList('#proxy-list', {items: proxyData});

		var $loader = $('#ajax-loader');

		function deleteClick(el/*, event*/)
		{
			if (!confirm('Вы уверены, что хотите удалить выбранный прокси-сервер?')) {
				return;
			}
			var items = this.options.items;
			var item  = el.parents('tr').data('item');
			var index = items.indexOf(item);
			$.ajax({
				url: '<?= \yii\helpers\Url::to(['remove']) ?>',
				type: 'POST',
				dataType: 'json',
				data: {
					_csrf: yii.getCsrfToken(),
					id: item.id
				},
				success: function(response) {
					if (response.status === 'ok') {
						items.splice(index, 1);
					}
				},
				beforeSend: ajaxStart,
				complete: ajaxStop
			});
		}

		function checkProxy(el/*, event*/)
		{
			var item  = el.parents('tr').data('item');
			$.ajax({
				url: '<?= Url::to(['/admin/proxy/check']) ?>',
				type: 'GET',
				dataType: 'json',
				data: {
					id: item.id
				},
				success: checkCallback
			});
		}

		function checkCallback(response)
		{
			proxyList.options.items.each(function(item/*, index*/){
				if (item.id == response.id) {
					item.attr('status', response.status);
				}
			});
		}

		function ajaxStart()
		{
			$loader.css('opacity', 1);
		}

		function ajaxStop(xhr, status)
		{
			$loader.css('opacity', 0);
			if (status !== 'success') {
				// TODO add alert
			}
		}

		controls.catSelect.on('change', function(){
			var data = {};

			currentCat = this.value;
			if (currentCat !== '') {
				data.cat = currentCat;
			}

			$.ajax({
				url: '<?= Url::to(['/admin/proxy']) ?>',
				method: 'GET',
				dataType: 'json',
				data: data,
				success: function(response) {
					proxyList.options.items.replace(response);
				},
				beforeSend: ajaxStart,
				complete: ajaxStop
			});
			$('#new-proxy-cat').val(currentCat);
		});

		controls.setDefaultProxy.on('click', function(){
			window.location = '<?= Url::to(['/admin/proxy-cat/set-default']) ?>?id=' + currentCat;
		});

		controls.removeProxy.on('click', function(){
			if (confirm('Вы уверены что хотите удалить эту категорию и все связанные прокси?')) {
				window.location = '<?= Url::to(['/admin/proxy-cat/remove']) ?>?id=' + currentCat;
			}
		});

	});
</script>