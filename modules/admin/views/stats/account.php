<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \yii\data\ActiveDataProvider $dataProviderSummary
 * @var app\models\AccountStats[] $summary
 * @var string $date
 * @var integer $dateFrom
 * @var integer $dateTo
 * @var \app\modules\admin\models\AccountStatsSearch $filterModel
 */

use app\models\Account;
use app\models\AccountStatsSearch;
use MongoDB\Client;
use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Статистика действий';
?>
<div class="accounts-actions-index">

	<h1><?= $this->title ?></h1>

	<?php
	Pjax::begin();

	ActiveForm::begin([
		'method' => 'get',
		'options' => [
			'class' => 'form-horizontal',
			'style' => 'margin: 20px 0;'
		]
	]); ?>
		<div class="row">
			<div class="col-xs-3">
				<div class="btn-group" role="group" aria-label="...">
					<button type="submit" name="date" value="day"
					        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
					</button>
					<button type="submit" name="date" value="week"
					        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
					</button>
					<button type="submit" name="date" value="month"
					        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
					</button>
				</div>

			</div>

			<div class="col-xs-9">
				<label class="control-label col-xs-2">Диапазон</label>

				<div class="input-group col-xs-6 col-xs-offset-1">
					<label for="date-from" class="input-group-addon">с</label>
					<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
					       class="form-control date-picker" data-date-format="yyyy-mm-dd">
					<label for="date-to" class="input-group-addon">по</label>
					<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
					       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
				</div>
			</div>
		</div>
	<?php
	ActiveForm::end();

	$models = $dataProvider->getModels();
	$dates = [];

	foreach ($models as $model) {
		$dates[] = $model->date;
	}

	$mongo = new Client(\Yii::$app->params['mongo_dsn']);

	$cursor = $mongo->zengram->out->aggregate([
			['$match' => [
				'$and' => [
					['value.instagram_id' => $models[0]->instagram_id],
					['value.date' => [
						'$gte' => (new \MongoDB\BSON\Timestamp(0, $dateFrom - (60 * 60 * 3 + 20))),
						'$lte' => (new \MongoDB\BSON\Timestamp(0, $dateTo + (60 * 60 * 3 - 20)))
					]]
				],
			]],
			['$project' => [
				'instagram_id' => '$value.instagram_id',
				'date' => '$value.date',
				'problem' => '$value.followersCountProblem',
				'came' => [ '$size' => '$value.camefollowers' ],
				'gone' => [ '$size' => '$value.gonefollowers' ]
			]],
		]
	);

	$documents = [];
	$total_came = 0;
	$total_gone = 0;

	foreach ($cursor as $document)
	{
		$date_parts = explode(':', $document['date']);
		$document_date = date('Y-m-d', intval(end($date_parts)));

		$documents[$document_date] = $document;

		if (in_array($document_date, $dates)) {
			$total_came += $document['came'];
			$total_gone += $document['gone'];
		}
	}

	echo \yii\grid\GridView::widget([
		'dataProvider' => $dataProviderSummary,
		'columns'      => [
			'account_id',
			[
				'attribute' => 'mail',
				'format'    => 'raw',
				'filter'    => false,
				'value'     => function ($item) {
					/** @var app\models\AccountStats $item */
					if (!empty($item->user->mail)) {
						return \yii\helpers\Html::a($item->user->mail, ['/admin/users', 'UsersSearch[id]' => $item->user->id]);
					}

					return null;
				}
			],
			[
				'attribute' => 'login',
				'format'    => 'raw',
				'filter'    => false,
				'value'     => function ($item) use ($documents) {
					/** @var app\models\AccountStats $item */
					if (empty($item->account->login)) {
						if (empty($item->instagram_id)) {
							return null;
						} else {
							$link = \yii\helpers\Html::a($item->instagram_id, 'http://' . Yii::$app->params['serverIP'] . '/userLog.php?id=' . $item->instagram_id);
						}
					} else {
						$link = \yii\helpers\Html::a($item->account->login, 'http://' . ($item->account->server ? $item->account->server : Yii::$app->params['serverIP'])
							. '/userLog.php?id=' . $item->account->instagram_id, ['target' => '_blank']);
					}


					return $link;
				}
			],
			'likes',
			'comments',
			'follows',
			[
				'attribute' => 'mongo_came',
				'label'     => 'Пришли',
				'format'    => 'raw',
				'value'     => function ($model) use($total_came) {
					if ($total_came > 0) {
						return (
							'<span title = "Старый формат: '. $model->came .'">'.
							intval($total_came) .
							'</span>'
						);
					}

					return (
						'<span title = "Старый формат: '. $model->came .'">'.
						intval($model->mongo_came) .
						'</span>'
					);
				}
			],
			[
				'attribute' => 'mongo_gone',
				'label'     => 'Ушли',
				'format'    => 'raw',
				'value'     => function ($model) use($total_gone){
					if ($total_gone > 0) {
						return (
							'<span title = "Старый формат: '. $model->came .'">'.
								intval($total_gone) .
							'</span>'
						);
					}

					return (
						'<span title = "Старый формат: '. $model->gone .'">'.
						intval($model->mongo_gone).
						'</span>'
					);
				}
			],
			'unfollows',
			'photos',
			'manual_comments',
			'password_resets'
		],
		'summary' => "Summary:",
	]);

	echo \yii\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		'columns'      => [
			'account_id',
			[
				'attribute' => 'mail',
				'format'    => 'raw',
				'filter'    => false,
				'value'     => function ($item) {
					/** @var app\models\AccountStats $item */
					if (!empty($item->user->mail)) {
						return \yii\helpers\Html::a($item->user->mail, ['/admin/users', 'UsersSearch[id]' => $item->user->id]);
					}
					return null;
				}
			],
			[
				'attribute' => 'login',
				'format'    => 'raw',
				'filter'    => false,
				'value'     => function ($item) use($documents) {
					/** @var app\models\AccountStats $item */
					if (empty($item->account->login)) {
						if (empty($item->instagram_id)) {
							return null;
						} else {
							$link = \yii\helpers\Html::a($item->instagram_id, 'http://' . Yii::$app->params['serverIP'] . '/userLog.php?id=' . $item->instagram_id);
						}
					} else {
						$link = \yii\helpers\Html::a($item->account->login, 'http://' . ($item->account->server ? $item->account->server : Yii::$app->params['serverIP'])
							. '/userLog.php?id=' . $item->account->instagram_id, ['target' => '_blank']);
					}
					$problem = '';

					if (!empty($documents[$item->date]) && !empty($documents[$item->date]['problem']) && $documents[$item->date]['problem']) {
						$problem = '<b color = "red">!!!</b>';
					}

					return $link . ' ' . $problem;
				}
			],
			[
				'attribute' => 'date',
				'filter'    => false,
			],
			'likes',
			'comments',
			'follows',
			[
				'attribute' => 'mongo_came',
				'label'     => 'Пришли',
				'format'    => 'raw',
				'value'     => function ($model) use ($documents) {
					if (!empty($documents[$model->date])) {
						return (
							'<span title = "Старый формат: '. $model->came .'">'.
								intval($documents[$model->date]['came']) .
							'</span>'
						);
					}

					return (
						'<span title = "Старый формат: '. $model->came .'">'.
							intval($model->mongo_came) .
						'</span>'
					);
				}
			],
			[
				'attribute' => 'mongo_gone',
				'label'     => 'Ушли',
				'format'    => 'raw',
				'value'     => function ($model) use ($documents) {
					if (!empty($documents[$model->date])) {
						return (
							'<span title = "Старый формат: '. $model->gone .'">'.
								intval($documents[$model->date]['gone']) .
							'</span>'
						);
					}
					return (
						'<span title = "Старый формат: '. $model->gone .'">'.
							intval($model->mongo_gone) .
						'</span>'
					);
				}
			],
			'unfollows',
			'photos',
			'manual_comments',
			'password_resets'
		],
	]);

	Pjax::end();
	?>
</div>