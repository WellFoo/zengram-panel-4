<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var string $date
 * @var string $dateFrom
 * @var string $dateTo
 * @var integer $count
 * @var integer $cost
 */

use app\models\Account;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Статистика действий';
?>
<div class="accounts-actions-index">

	<h1><?= $this->title ?></h1>

	<?php
	Pjax::begin();
	ActiveForm::begin([
		'method' => 'get',
		'options' => [
			'class' => 'form-horizontal',
			'style' => 'margin: 20px 0;'
		]
	]); ?>
		<div class="row">
			<div class="col-xs-3">
				<div class="btn-group" role="group" aria-label="...">
					<button type="submit" name="date" value="day"
					        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
					</button>
					<button type="submit" name="date" value="week"
					        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
					</button>
					<button type="submit" name="date" value="month"
					        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
					</button>
				</div>

			</div>

			<div class="col-xs-9">
				<label class="control-label col-xs-2">Диапазон</label>

				<div class="input-group col-xs-6 col-xs-offset-1">
					<label for="date-from" class="input-group-addon">с</label>
					<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
					       class="form-control date-picker" data-date-format="yyyy-mm-dd">
					<label for="date-to" class="input-group-addon">по</label>
					<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
					       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
				</div>
			</div>
		</div>
	<?php
	ActiveForm::end();
	?>
	<div class="clearfix"></div>
	<div class="text-center">
		<table class="table table-bordered col-xs-3" style="width: auto;">
			<thead>
			<tr>
				<th width="33%" style="text-align: center;">Количество размещений</th>
				<th width="33%" style="text-align: center;">Всего начислено</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td><?= $count ?></td>
				<td><?= $cost ? Account::getStaticTimerText($cost, true) : 0;?></td>
			</tr>
			</tbody>
		</table>
	</div>
	<div class="clearfix"></div>
	<?php
	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'tableOptions' => [
			'class' => 'table table-bordered table-hover'
		],
		'columns'      => [
			[
				'attribute' => 'mail',
				'label' => 'Пользователь',
				'format'    => 'raw',
				'value'     => function ($item) {
					/** @var app\models\ActionsLog $item */
					if (!empty($item->user->mail)) {
						return $item->user->mail;
					}
					return null;
				}
			],
			'added',
			[
				'attribute' => 'login',
				'label' => 'Аккаунт',
				'format'    => 'raw',
				'value'     => function ($item) {
					/** @var app\models\AccountStats $item */
					if (empty($item->account->login)) {
						if (empty($item->instagram_id)) {
							return null;
						} else {
							return $item->instagram_id;
						}
					} else {
						return \yii\helpers\Html::a($item->account->login, 'https://instagram.com/'.$item->account->login, ['target' => '_blank']);
					}
				}
			],
			[
				'attribute' => 'image_id',
				'label' => 'Отправленное изображение',
				'format'    => 'raw',
				'value'     => function ($item) {
					/** @var app\models\ActionsLog $item */
					if (!empty($item->image->path)) {
						return '<img width=200 height=200 src="'. $item->image->path .'"/>';
					}
					return null;
				}
			],
			[
				'attribute' => 'cost',
				'label' => 'Начислено',
				'format'    => 'raw',
				'value'     => function ($item) {
					/** @var app\models\ActionsLog $item */
					return $item->cost ? Account::getStaticTimerText($item->cost, true) : 0;
				}
			],
			[
				'attribute' => 'deleted',
				'label' => 'Удален?',
				'format'    => 'raw',
				'value'     => function ($item) {
					/** @var app\models\ActionsLog $item */
					return $item->deleted ? 'Да' : 'Нет';
				}
			],
		],
	]);
	Pjax::end();
	?>
</div>