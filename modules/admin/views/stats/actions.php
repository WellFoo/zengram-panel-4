<?php
use app\models\Users;

/** @var $actions array */
?>
<div class="site-admin">
	<table class="table">
		<thead>
		<tr>
			<th>ID</th>
			<th>Клиент</th>
			<th>E-mail</th>
			<th>Дата</th>
			<th>Действие</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($actions as $action): ?>
			<tr>
				<td><?= $action['id']; ?></td>
				<td><?php
					$user = Users::find()->where('id = '.$action['user_id'])->one();
					if ($user) {
						echo '<a href="/admin/projects/'.$user['id'].'">'.$user['mail'].'</a>';
					} else {
						echo 'Удалён';
					} ?></td>
				<td><?= $action['mail']; ?></td>
				<td><?= $action['datetime']; ?></td>
				<td><?php
					if ($action['data'] != 'close') {
						echo '<a href="/admin/stats/actions/?type='.$action['data'].'">'.$action['data'].'</a>';
					} else {
						echo $action['data'];
					} ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>