<?php

/* @var $this yii\web\View
 * @var integer $count
 * @var string $date
 * @var integer $dateFrom
 * @var integer $dateTo
 * @var integer $sum
 * @var float   $sumPaypal
 * @var float   $sumRobokassa
 * @var float   $sum2co
 * @var float   $sumYad
 * @var integer $countUsersFirst
 * @var integer $countUsersAfter
 * @var float   $sumUsersFirst
 * @var float   $sumUsersAfter
 */

$this->title = 'Статистика платежей';
?>
<h1><?= $this->title ?></h1>
<form method="get" class="form-horizontal" style="margin: 20px 0;">
	<div class="row">
		<div class="col-xs-3">
			<div class="btn-group" role="group" aria-label="...">
				<button type="submit" name="date" value="day"
				        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
				</button>
				<button type="submit" name="date" value="week"
				        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
				</button>
				<button type="submit" name="date" value="month"
				        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
				</button>
			</div>

		</div>

		<div class="col-xs-9">
			<label class="control-label col-xs-2">Диапазон</label>

			<div class="input-group col-xs-6 col-xs-offset-1">
				<label for="date-from" class="input-group-addon">с</label>
				<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">
				<label for="date-to" class="input-group-addon">по</label>
				<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">

				<span class="input-group-btn">
					<button type="submit" name="date" value="range"
					        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
					</button>
				</span>
			</div>
		</div>
	</div>
</form>
<table class="table table-striped table-bordered table-hover">
	<tr>
		<td colspan="4">Общее количество платежей</td>
		<td><?= $count ? $count : '0' ?></td>
	</tr>
	<tr>
		<td colspan="4">Общая сумма платежей</td>
		<td><?= $sum ? number_format($sum, 2) : '0' ?></td>
	</tr>
	<?php if (Yii::$app->language === 'ru') { ?>
		<tr>
			<td></td>
			<td colspan="3">В том числе Yandex</td>
			<td><?= $sumYad ? number_format($sumYad, 2) : '0' ?></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="3">В том числе Interkassa</td>
			<td><?= $sumRobokassa ? number_format($sumRobokassa, 2) : '0' ?></td>
		</tr>
	<?php } else { ?>
		<tr>
			<td></td>
			<td colspan="3">В том числе 2CO</td>
			<td><?= $sum2co ? number_format($sum2co, 2) : '0' ?></td>
		</tr>
	<?php } ?>
	<tr>
		<td></td>
		<td colspan="3">В том числе Paypal</td>
		<td><?= $sumPaypal ? number_format($sumPaypal, 2) : '0' ?></td>
	</tr>
	<tr>
		<td colspan="5">Количество и сумма платежей по пакетам</td>
	</tr>
	<tr>
		<td></td>
		<td>Цена</td>
		<td>Кол-во</td>
		<td>Сумма</td>
		<td>Доля</td>
	</tr>
	<?php

	/** @var object[] $colPackets */
	foreach ($colPackets as $colPacket): ?>
		<tr>
			<td></td>
			<td> <?= number_format($colPacket['price'], 2) ?></td>
			<td> <?= $colPacket['count_price'] ? $colPacket['count_price'] : 0 ?></td>
			<td> <?= $colPacket['sum_price'] ? $colPacket['sum_price'] : 0 ?></td>
			<td> <?= round(($colPacket['sum_price'] / $sum) * 100, 0)  ?>%</td>
		</tr>
	<?php endforeach ?>
	<tr>
		<td colspan="5">Суммы по клиентам</td>
	</tr>
	<tr>
		<td colspan="3">Первичные платежи</td>
		<td><?= $countUsersFirst ?></td>
		<td><?= number_format($sumUsersFirst, 2) ?></td>
	</tr>
	<tr>
		<td colspan="3">Второй и более платежи</td>
		<td><?= $countUsersAfter ?></td>
		<td><?= number_format($sumUsersAfter, 2) ?></td>
	</tr>
</table>