<?php

use app\models\Users;
use app\modules\admin\models\OverallStatistics;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View
 * @var $count integer
 * @var string $date
 * @var integer $dateFrom
 * @var integer $dateTo
 * @var ActiveDataProvider $dataprovider
 */

$this->title = 'Статистика предложений';
//$this->params['breadcrumbs'][] = $this->title;
?>
	<div class="site-admin">
		<h1><?= $this->title ?></h1>

		<?php
		Pjax::begin();
		ActiveForm::begin([
			'method' => 'get',
			'options' => [
				'class' => 'form-horizontal',
				'style' => 'margin: 20px 0;'
			]
		]); ?>
		<div class="row">
			<div class="col-xs-3">
				<div class="btn-group" role="group" aria-label="...">
					<button type="submit" name="date" value="day"
					        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
					</button>
					<button type="submit" name="date" value="week"
					        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
					</button>
					<button type="submit" name="date" value="month"
					        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
					</button>
				</div>

			</div>

			<div class="col-xs-9">
				<label class="control-label col-xs-2">Диапазон</label>

				<div class="input-group col-xs-6 col-xs-offset-1">
					<label for="date-from" class="input-group-addon">с</label>
					<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
					       class="form-control date-picker" data-date-format="yyyy-mm-dd">
					<label for="date-to" class="input-group-addon">по</label>
					<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
					       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
				</div>
			</div>
		</div>
		<?php
		ActiveForm::end();
		echo GridView::widget([
			'dataProvider' => $dataprovider,
			'tableOptions' => [
				'class' => 'table table-bordered table-hover'
			],
			'columns'      => [
				[
					'attribute' => 'user_id',
					'label' => 'Email',
					'format'    => 'raw',
					'value'     => function ($item) {
						$user = Users::find()->where(['id' => $item->user_id])->one();
						if ($user){
							return $user->mail;
						}
						return null;
					}
				],
				'text',
				'added'
			],
		]);
		Pjax::end();
		?>
	</div>