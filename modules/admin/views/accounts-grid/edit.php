<?php

use app\models\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model app\modules\admin\models\AccountsGridForm
 */

$this->title = 'Сетка аккаунтов';
?>
<style>
.form-sub-header {
	font-weight: bold;
	padding: 0 15px 15px;
}
</style>
<div class="site-admin">
	<h1>Новый аккаунт</h1>

	<?php $form = ActiveForm::begin([
		'id' => 'account-grid-form',
		'layout' => 'horizontal',
		'fieldConfig' => [
			'horizontalCssClasses' => [
				'label' => 'col-sm-3',
				'offset' => 'col-sm-offset-3',
				'wrapper' => 'col-sm-9'
			]
		],
		'options' => ['class' => 'col-sm-8']
	]); ?>

		<div class="form-sub-header col-sm-offset-3">Instagram</div>
		<?= $form->field($model, 'account_login')->textInput(['readonly' => $model->scenario !== 'create']) ?>
		<?= $form->field($model, 'account_password')->textInput(['readonly' => $model->scenario !== 'create']) ?>

		<hr>
		<div class="form-sub-header col-sm-offset-3">E-Mail</div>
		<?= $form->field($model, 'email') ?>
		<?= $form->field($model, 'email_password') ?>

		<hr>
		<div class="form-sub-header col-sm-offset-3">Zengram</div>
		<?= $form->field($model, 'user_id')->dropDownList(
			ArrayHelper::map(
				Users::findAll(['is_service' => true]),
				'id',
				'mail'
			),
			[
				'prompt' => '',
				'readonly' => $model->scenario !== 'create'
			]
		) ?>

		<?= $form->field($model, 'donor_url') ?>

		<?= $form->field($model, 'sign') ?>
		<?= $form->field($model, 'sign_url') ?>

		<?= $form->field($model, 'mark_number') ?>

		<?= $form->field($model, 'description')->textarea() ?>

		<div class="col-sm-offset-3">
			<div class="form-group">
				<?= Html::a('Отмена', ['/admin/accounts-grid'], ['class' => 'btn btn-default']) ?>
				<?= Html::submitButton('Готово', ['class' => 'btn btn-success', 'name' => 'submit-button']) ?>
			</div>
		</div>
	<?php $form->end(); ?>

</div>