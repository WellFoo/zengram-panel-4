<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\admin\models\PopularAccounts $popular
 * @var \yii\data\ActiveDataProvider $data_provider
 * @var string  $date
 * @var integer $dateFrom
 * @var integer $dateTo
 */
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Статистика по популярным (@' . $popular->username . ')';
?>

<h2><?= $this->title ?></h2>
<div>
<?php
Pjax::begin();

ActiveForm::begin([
	'method' => 'get',
	'action' => [''],
	'options' => [
		'class' => 'form-horizontal',
		'style' => 'margin: 20px 0;'
	]
]); ?>
	<div class="row">
		<div class="col-xs-3">
			<div class="btn-group" role="group" aria-label="...">
				<button type="submit" name="date" value="day"
				        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
				</button>
				<button type="submit" name="date" value="week"
				        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
				</button>
				<button type="submit" name="date" value="month"
				        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
				</button>
			</div>

		</div>

		<div class="col-xs-9">
			<label class="control-label col-xs-2">Диапазон</label>

			<div class="input-group col-xs-6 col-xs-offset-1">
				<label for="date-from" class="input-group-addon">с</label>
				<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">
				<label for="date-to" class="input-group-addon">по</label>
				<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
			</div>
		</div>
	</div>
<?php ActiveForm::end(); ?>

<?php
echo GridView::widget([
	'dataProvider' => $data_provider,
	'tableOptions' => [
		'class' => 'table table-bordered table-hover'
	],
	'columns' => [
		'date',
		[
			'attribute' => 'commenter.login',
			'label' => 'Коментатор'
		],
		'media:url',
		'text:text',
	]
]);

Pjax::end();
?>
</div>
