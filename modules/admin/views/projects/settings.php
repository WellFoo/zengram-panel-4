<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\AccountSettings;

/**
 * @var $account app\models\Account
 * @var $settings app\models\Settings
 * @var $settings_pause_0 app\models\Settings
 * @var $settings_pause_1 app\models\Settings
 * @var $settings_pause_2 app\models\Settings
 */

$this->title                   = 'Settings';
//$this->params['breadcrumbs'][] = $this->title;
$on_change = '$.post("'.Url::to(['/admin/projects/options', 'id' => $account->id]).'", {option: this.name, state: this.checked ? 1 : 0});';
$int_change = '$.post("'.Url::to(['/admin/projects/options', 'id' => $account->id]).'", {option: this.name, state: $(this).val()});';
?>
<div class="site-admin">

	<h1>Персональные настройки <?php echo $account->login; ?></h1>

	<p>
		<label class="control-label">
			<?= Html::checkbox('experimental', $account->options->experimental, [
				'onChange' => $on_change
			]) ?>
			Использовать экспериментальные функции
		</label>
	</p>

	<p>
		<label class="control-label">
			<?= Html::checkbox('skipCommercial', $account->options->skipCommercial, [
				'onChange' => $on_change
			]) ?>
			<?= $account->options->getAttributeLabel('skipCommercial') ?>
		</label>
	</p>

	<p>
		<label class="control-label">
			<?= Html::checkbox('filter_by_follows', $account->options->filter_by_follows, [
				'onChange' => $on_change
			]) ?>
			<?= $account->options->getAttributeLabel('filter_by_follows') ?>
		</label>
	</p>

	<p>
		<label class="control-label">
			<?= Html::checkbox('skip_popular_update', $account->options->skip_popular_update, [
				'onChange' => $on_change
			]) ?>
			<?= $account->options->getAttributeLabel('skip_popular_update') ?>
		</label>
	</p>

	<p>
		<label class="control-label">
			<?= Html::checkbox('comment_popular', $account->options->comment_popular, [
				'onChange' => $on_change
			]) ?>
			<?= $account->options->getAttributeLabel('comment_popular') ?>
		</label>
	</p>

	<p>
		<label class="control-label">
			<?= Html::checkbox('account_antispam', $account->options->account_antispam, [
				'onChange' => $on_change
			]) ?>
			<?= $account->options->getAttributeLabel('account_antispam') ?>
		</label>
	</p>

	<p>
		<label class="control-label">
			<?= Html::checkbox('use_whitelist', $account->options->use_whitelist, [
				'onChange' => $on_change
			]) ?>
			<?= $account->options->getAttributeLabel('use_whitelist') ?>
		</label>
	</p>

	<p>
		<label class="control-label">
			<?= Html::checkbox('parallel_unfollows', $account->options->parallel_unfollows, [
				'onChange' => $on_change
			]) ?>
			<?= $account->options->getAttributeLabel('parallel_unfollows') ?>
		</label>
	</p>
	<p>
		<label class="control-label">
			<?= Html::input('text', 'follows_limit_to_unfollow', $account->options->follows_limit_to_unfollow, [
				'onChange' => $int_change
			]) ?>
			<?= $account->options->getAttributeLabel('follows_limit_to_unfollow') ?>
		</label>
	</p>
	<p>
		<label class="control-label">
			<?= Html::checkbox('auto_fill', $account->options->auto_fill, [
				'onChange' => $on_change
			]) ?>
			<?= $account->options->getAttributeLabel('auto_fill') ?>
		</label>
	</p>


	<table class="table">
		<thead>
		<tr>
			<th>Ключ</th>
			<th class="text-center">Значение</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td colspan="3"><strong style="font-weight: bold">Настройки вне зависимости от скорости</strong></td>
		</tr>
		<?php foreach ($settings as $setting): ?>
			<tr>
				<td><?php echo $setting['description']; ?></td>
				<td class="text-center">
					<?php
					/** @var AccountSettings $customValue */
					$customValue = AccountSettings::find()->where(['account_id' => $account->instagram_id, 'setting' => $setting['setting'], 'pause' => null])->one();
					if (!empty($customValue) && $customValue->value != $setting['value']) {
						echo '<span style="color: #090">'.$customValue->value.'</span>';
					} else {
						echo $setting['value'];
					}
					?>
				</td>
				<td>
					<?= Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', [
						'/admin/projects/setting/',
						'id'      => $account->instagram_id,
						'setting' => $setting['setting'],
						'pause'   => $setting['pause']
					]); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="3"><strong style="font-weight: bold">Настройки для медленной скорости</strong></td>
		</tr>
		<?php foreach ($settings_pause_0 as $setting): ?>
			<tr>
				<td><?php echo $setting['description']; ?></td>
				<td class="text-center"><?php
					$customValue = AccountSettings::find()->where(['account_id' => $account->instagram_id, 'setting' => $setting['setting'], 'pause' => 0])->one();
					if(!empty($customValue) && $customValue->value!=$setting['value'])
						echo '<span style="color: #090">'.$customValue->value.'</span>';
					else
						echo $setting['value']; ?></td>
				<td>
					<?= Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', [
						'/admin/projects/setting/',
						'id'      => $account->instagram_id,
						'setting' => $setting['setting'],
						'pause'   => $setting['pause']
					]); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="3"><strong style="font-weight: bold">Настройки для средней скорости</strong></td>
		</tr>
		<?php foreach ($settings_pause_1 as $setting): ?>
			<tr>
				<td><?php echo $setting['description']; ?></td>
				<td class="text-center"><?php
					$customValue = AccountSettings::find()->where(['account_id' => $account->instagram_id, 'setting' => $setting['setting'], 'pause' => 1])->one();
					if(!empty($customValue) && $customValue->value!=$setting['value'])
						echo '<span style="color: #090">'.$customValue->value.'</span>';
					else
						echo $setting['value']; ?></td>
				<td>
					<?= Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', [
						'/admin/projects/setting/',
						'id'      => $account->instagram_id,
						'setting' => $setting['setting'],
						'pause'   => $setting['pause']
					]); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="3"><strong style="font-weight: bold">Настройки для быстрой скорости</strong></td>
		</tr>
		<?php foreach ($settings_pause_2 as $setting): ?>
			<tr>
				<td><?php echo $setting['description']; ?></td>
				<td class="text-center"><?php
					$customValue = AccountSettings::find()->where(['account_id' => $account->instagram_id, 'setting' => $setting['setting'], 'pause' => 2])->one();
					if(!empty($customValue) && $customValue->value!=$setting['value'])
						echo '<span style="color: #090">'.$customValue->value.'</span>';
					else
						echo $setting['value']; ?></td>
				<td>
					<?= Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', [
						'/admin/projects/setting/',
						'id'      => $account->instagram_id,
						'setting' => $setting['setting'],
						'pause'   => $setting['pause']
					]); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

</div>