<?php
/**
 * @var $this \yii\web\View
 */

use app\models\SnifferGroups;
use app\models\SnifferServers;
use app\models\SnifferUsers;
use yii\bootstrap\Html;

$this->title = 'Состояние серверов';

$servers = SnifferGroups::find()->all();

?>

<div>
	<?= Html::a(
		'Пользователи',
		['/admin/servers/users/'],
		[
			'class' => 'btn btn-success server-add margin-btn',
			'title' => 'Пользователи',
		]
	); ?>
</div>

<div>
	<?= Html::a(
		'Сводная по серверам',
		'#',
		[
			'class' => 'btn btn-warning server-summary margin-btn',
			'title' => 'Сводная по серверам',
		]
	); ?>
</div>

<?php

/** @var SnifferGroups $server */
foreach ($servers as $server) {

	echo Html::a(
			$server->name,
			'#',
			[
				'class' => 'server-group btn btn-primary',
				'title' => 'Управление группой',
				'data' => [
					'id' => $server->id
				]
			]
		);

	echo Html::a(
		'<span class="glyphicon glyphicon-plus"></span>',
		['/admin/servers/add-server/' . $server->id],
		[
			'class' => 'btn btn-success server-add margin-btn',
			'title' => 'Добавить сервер в группу',
		]
	);

	echo $this->render('monitoring.group.php', [
		'server' => $server
	]);

}

echo Html::a(
	'Добавить группу',
	'#',
	[
		'class' => 'server-add btn btn-success',
		'title' => 'Добавить группу',
		'data' => [
			'id' => $server->id
		]
	]
);

?>

<!-- Модальное окно добавления пользователя -->
<div class="modal fade" id="add-user-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">

				<h3>
					Добавить пользователя для мониторинга
				</h3>

				<input type="hidden" name="server" class="server-id">

				<p class="body">

					<?php

					$users = SnifferUsers::getUsers();

					echo Html::dropDownList('user', '', $users, [
						'class' => 'form-control'
					]);

					?>

				</p>

			</div>
			<div class="modal-footer">
				<button id="add-user-btn" type="button" class="btn btn-success" data-dismiss="modal">Назначить</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Модальное окно параметров -->
<div class="modal fade" id="user-settings-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">

				<h3>
					Управление оповещением
				</h3>

				<p class="body">

				</p>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" data-dismiss="modal">Ок</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Модальное окно сводной по серверу -->
<div class="modal fade" id="server-summary">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">

				<h3>
					Сводная неполадок по серверам
				</h3>

				<div class="body">

				</div>

			</div>

			<div class="modal-footer">
				<div class="row">
					<div class="col-xs-10">
						<div class="input-group">
							<label for="from" class="input-group-addon">Сервер</label>
							<select style="width: 230px;" name="server" id="summary-server" class="form-control">
								<?php

								echo '<option value="0">Не выбрано</option>';

								/** @var SnifferGroups $group */
								foreach ($servers as $group) {

									echo '<optgroup label="' . $group->name . '">';

									$servers_in_group = SnifferServers::find()->where([
										'parent' => $group->id
									])->orderBy(['type' => SORT_ASC, 'id' => SORT_ASC]);

									/** @var SnifferServers $server */
									foreach ($servers_in_group->each() as $server) { ?>

										<option value="<?= $server->id ?>"><?= long2ip($server->ip) . ' - ' . $server->name ?></option>

									<?php }

									echo '</optgroup>';
								}

								?>
							</select>
							<label for="from" class="input-group-addon">с</label>
							<input name="from" value="" id="date-summary-from"
							       class="form-control date-picker" data-date-format="yyyy-mm-dd">
							<label for="to" class="input-group-addon">по</label>
							<input name="to" value="" id="date-summary-to"
							       class="form-control date-picker" data-date-format="yyyy-mm-dd">

							<span class="input-group-btn">
								<span class="btn btn-success  server-summary">
									Сформировать
								</span>
							</span>
						</div>
					</div>
					<div class="col-xs-2 pull-right">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
					</div>
				</div>

			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Модальное окно истории -->
<div class="modal fade" id="parameter-history">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">

				<h3>
					История изменения параметра "<span class="title"></span>"
				</h3>

				<div class="body">

				</div>

			</div>

			<div class="modal-footer">
				<div class="row">
					<div class="col-xs-8">
						<div class="input-group">
							<label for="from" class="input-group-addon">с</label>
							<input name="from" value="" id="date-from"
							       class="form-control date-picker" data-date-format="yyyy-mm-dd">
							<label for="to" class="input-group-addon">по</label>
							<input name="to" value="" id="date-to"
							       class="form-control date-picker" data-date-format="yyyy-mm-dd">

							<span class="input-group-btn">
								<span class="btn btn-success history" id="history-submit">
									Сформировать
								</span>
							</span>
						</div>
					</div>
					<div class="col-xs-2 pull-right">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
					</div>
				</div>

			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<style>

	.server-group {
		padding-left: 30px;
		margin-bottom: 10px;
		margin-right: 10px;
		width: 300px;
	}

	.margin-btn {
		margin-bottom: 10px;
		margin-right: 10px;
	}

	.server-status {
		display: inline-block;
		border-radius: 100px;
		width: 10px;
		height: 10px;
		margin-right: 8px;
		cursor: pointer;
	}

	.soft-link {
		border-bottom: 1px dotted #999;
		cursor: pointer;
	}

	.server-status.status-1 {
		background-color: #2cbc3a;
	}

	.server-status.status-0 {
		background-color: #ff5c6c;
	}

	.server-status.status-2 {
		background-color: #ffc55a;
	}

	.server-1 {

	}

	.server-0 {
		background-color: rgba(255, 92, 108, 0.18) !important;
	}

	.server-2 {
		background-color: rgba(255, 197, 90, 0.2) !important;
	}


	.server-info {
		margin-bottom: 30px;
	}

	.error-message {
		padding: 6px;
		font-size: .85em;
		cursor: pointer;
		margin-top: -6px;
		margin-bottom: -6px;
		margin-left: -6px;
	}

	#user-settings-modal table {
		width: 100%;
		margin-top: 15px;
		border: 1px solid #ddd;
		color: #333;
	}

	#user-settings-modal table th {
		color: #337ab7;
		padding: 5px 10px;
		border-bottom-width: 2px;
	}

	#user-settings-modal table th,
	#user-settings-modal table td {
		border: 1px solid #ddd;
	}

	#user-settings-modal table td {
		padding: 5px;
	}

	#user-settings-modal table tbody tr:nth-of-type(odd) {
		background-color: #f9f9f9;
	}

	.notification {
		width: 0;
		text-align: center;
	}

	.notification input[type=checkbox] {
		width: 16px;
		height: 16px;
		cursor: pointer;
	}

	.notification img {
		width: 20px;
		height: 20px;
		vertical-align: text-bottom;
	}

	.datepicker{z-index:1151 !important;}

	.loading {
		position: relative;
		margin-left: 22px;
	}

	.loading-btn {
		padding-left: 22px;
		background-image: url(/images/main-square.gif);
		background-size: 20px 20px;
		background-repeat: no-repeat;
		background-position: left center;
	}

	.loading:before {
		display: block;
		position: absolute;
		left: -22px;
		top: -2px;
		content: '';
		width: 20px;
		height: 20px;
		background: url(/images/main-square.gif) no-repeat left center;
		background-size: 20px 20px;
	}

	.history-body {
		position: relative;
		min-height: 200px;
		width: 100%;
		padding: 0 15px;
		overflow: hidden;
	}

	.history-body .log-wrapper {
		position: absolute;
		bottom: 0;
		left: 20px;
		right: 20px;
		overflow-x: auto;
		overflow-y: hidden;
	}

	.history-body .log-slider {
		white-space: nowrap;
		position: relative;
	}

	.history-body .limit {
		position: absolute;
		left: 0;
		right: 0;
		cursor: pointer;
		padding: 3px 0;
		margin-bottom: 20px;
	}

	.history-body .limit .line {
		border-bottom: 1px dashed #333;
	}

	.history-body .limit:hover .line {
		border-color: orange;
	}

	.history-body .limit .value {
		position: absolute;
		left: 0;
		top: -25px;
	}

	.history-body .limit:before {
		left: 0;
	}

	.history-body .limit:after {
		right: 0;
	}

	.history-body .limit:before,
	.history-body .limit:after {
		position: absolute;
		top: 1px;
		content: '';
		width: 5px;
		height: 5px;
		border-radius: 50px;
		background-color: #333;
		display: block;
	}

	.history-body .log {
		width: 5px;
		background-color: darkgray;
		cursor: pointer;
		display: inline-block
	}

	.history-body .log.bad {
		background-color: red;
	}

	.history-body .log:hover {
		background-color: orange;
	}

	.clr:after {
		content: "";
		clear: both;
		display: block;
	}

	#server-summary .body {
		height: calc(100vh - 430px);
		overflow-y: auto;
	}

	.parameter-icon {
		display: inline-block;
		border-radius: 100px;
		width: 14px;
		height: 14px;
		margin-left: 5px;
		cursor: pointer;
		vertical-align: -2px;
	}

	.parameter-icon.icon-rabbit {
		 background: url(/images/icons/icon-rabbit.png) center no-repeat;
		background-size: 14px;
	 }
	.parameter-icon.icon-pgsql {
		background: url(/images/icons/icon-pgsql.png) center no-repeat;
		background-size: 14px;
	}
	.parameter-icon.icon-mysql {
		background: url(/images/icons/icon-mysql.png) center no-repeat;
		background-size: 14px;
	}
	.parameter-icon.icon-backup {
		background: url(/images/icons/icon-backup.png) center no-repeat;
		background-size: 12px;
	}
</style>

<script>

	var
		$userSettingsModal = $('#user-settings-modal'),
		$parameterHistory = $('#parameter-history'),
		$historyDateFrom = $('#date-from'),
		$historyDateTo = $('#date-to'),
		$historySubmit = $('#history-submit'),

		$serverSummary = $('#server-summary'),
		$serverSummaryDateFrom = $('#date-summary-from'),
		$serverSummaryDateTo = $('#date-summary-to'),
		$serverSummaryServer = $('#summary-server'),
		$addUserModal = $('#add-user-modal');

	$(document).on('click', '.server-summary', function(e){

		var $this = $(this);

		$this.addClass('loading-btn');

		$.post(
			'get-server-summary',
			{
				server: $serverSummaryServer.val(),
				from: $serverSummaryDateFrom.val(),
				to: $serverSummaryDateTo.val()
			},
			function(response){

				if (response.status != 'ok') {
					return false;
				}

				$serverSummary.find('.body').html(response.data);
				$serverSummaryDateFrom.val(response.dateFrom);
				$serverSummaryDateTo.val(response.dateTo);
				$serverSummary.modal('show');

			},
			'json'
		).always(function(){
			$this.removeClass('loading-btn');
		});

		e.preventDefault();
		return false;
	});

	$(document).on('click', '.history', function(e){

		var $this = $(this),
			server_id = $this.attr('data-server'),
			parameter = $this.attr('data-parameter');

		if (!server_id) {
			server_id = $this.closest('tr').data('id');
		}

		$this.addClass('loading');

		$.post(
			'get-parameter-history',
			{
				parameter: parameter,
				server: server_id,
				from: $historyDateFrom.val(),
				to: $historyDateTo.val()
			},
			function(response){

				if (response.status != 'ok') {
					return false;
				}

				$historySubmit.attr('data-server', server_id);
				$historySubmit.attr('data-parameter', parameter);
				$parameterHistory.find('.title').html(response.parameter);
				$parameterHistory.find('.body').html(response.data);
				$historyDateFrom.val(response.dateFrom);
				$historyDateTo.val(response.dateTo);
				$parameterHistory.modal('show');

				$parameterHistory.find('[data-toggle=popover]').popover({
					placement: 'top',
					container: 'body',
					html: true,
					trigger: 'hover'
				});
			},
			'json'
		).always(function(){
			$this.removeClass('loading');
		});

		e.preventDefault();
		return false;
	});

	$(document).on('click', '.edit-monitoring', function(e){

		var $this = $(this);

		$this.addClass('loading');

		$.post(
			'get-server-monitoring',
			{
				server: $this.data('id')
			},
			function(response){

				if (response.status != 'ok') {
					return false;
				}

				$userSettingsModal.find('.body').html(response.data);
				$userSettingsModal.modal('show');
			},
			'json'
		).always(function(){
			$this.removeClass('loading');
		});

		e.preventDefault();
		return false;
	});

	$('.add-user-btn').click(function(){
		$addUserModal.find('input[name=server]').val($(this).data('id'));
	});

	$('#add-user-btn').click(function(){

		var server = $addUserModal.find('input[name=server]').val(),
			user = $addUserModal.find('select[name=user]').val();

		$.post(
			'add-user-to-server',
			{
				server: server,
				user: user
			},
			function(response){

				if (response.status == 'ok') {
					$('.add-user-btn[data-id = ' + server + ']').parent().html(response.data);
				}
			},
			'json'
		);

	});

	$(document).ready(function(){
		setTimeout(function(){
			$('[data-toggle=popover]').popover({
				placement: 'left',
				container: 'body',
				html: true,
				trigger: 'hover'
			});
		}, 100);
	});


	$(document).on('change', '#server-user', function(e){
		var $this = $(this),
			id = $this.data('id'),
			user = $this.val();

		$.post(
			'change-user',
			{
				server_id: id,
				user_id: user
			},
			function(response){

				if (response.status != 'ok') {
					return false;
				}

				$('.user-info[data-id = ' + id + ']').parent().html(response.data);
			},
			'json'
		);

		e.preventDefault();
		return false;
	});


	$(document).on('click', '.remove-parameter', function(e){

		var $this = $(this),
			id = $this.data('id');

		$.post(
			'delete-parameter',
			{
				parameter_id: id
			}
		);

		$userSettingsModal.find('tr[data-id = ' + id + ']').remove();

		e.preventDefault();
		return false;
	});

	$(document).on('change', '.existing-parameters input, .existing-parameters select', function(e){

		var $this = $(this),
			parameter = $this.attr('name'),
			parameter_id = $this.closest('tr').data('id'),
			value;

		if ($this.attr('type') == 'checkbox') {
			value = $this.prop('checked');
		} else {
			value = $this.val();
		}

		$.post(
			'set-parameter',
			{
				parameter: parameter,
				parameter_id: parameter_id,
				value: value
			}
		);

		e.preventDefault();
		return false;
	});

	$(document).on('click', '.add-user-param', function(e){

		var id = $(this).data('id'),
			data = {server_id: id};

		//$this.addClass('loading');

		$('#parameter-form input, #parameter-form select').each(function(){

			var $this = $(this),
				value;

			if ($this.attr('type') == 'checkbox') {
				value = $this.prop('checked');
			} else {
				value = $this.val();
			}

			data[$this.attr('name')] = value;

		});

		$.post(
			'add-parameter',
			data,
			function(response){

				if (response.status != 'ok') {
					return false;
				}

				$userSettingsModal.find('.body').html(response.data);
			},
			'json'
		).always(function(){
			//$this.removeClass('loading');
		});

		e.preventDefault();
		return false;
	});

</script>
