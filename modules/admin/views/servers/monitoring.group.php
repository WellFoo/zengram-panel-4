<?php
/**
 * @var $this \yii\web\View
 * @var $server \app\models\SnifferGroups
 */

use app\models\SnifferServers;
use app\models\SnifferUsers;
use yii\bootstrap\Html;

$query = SnifferServers::find()->where([
	'parent' => $server->id
])->orderBy(['type' => SORT_ASC, 'id' => SORT_ASC]);

$dataProvider = new \yii\data\ActiveDataProvider([
	'query' => $query
]);

echo \yii\grid\GridView::widget([
	'dataProvider' => $dataProvider,
	'layout' => '
		{items}'
	,
	'rowOptions' => function ($server) {
		/** @var SnifferServers $server */
		return [
			'class' => 'server-' . $server->status,
			'data' => [
				'id' => $server->id
			],
		];
	},
	'columns'      => [
		[
			'attribute' => 'ip',
			'label' => 'IP',
			'format' => 'raw',
			'value' => function ($server) {
				/** @var $server SnifferServers */

				$parameters = $server->getDecodeParameters();
				$data = '';

				foreach ($parameters as $command => $parameter) {

					switch ($command) {
						case 'rabbit':
							$icon = 'icon-rabbit parameter-icon';
							$title = 'RabbitMQ';

							if (is_null($parameter)) {
								$popup = 'Данные отсутствуют';
							} else {
								$popup = '';
								foreach ($parameter as $name => $value) {
									if (is_array($value)) continue;
									$popup .= $name . ': ' . $value . '<br>';
								}
							}

							break;

						case 'mysql':
							$icon = 'icon-mysql parameter-icon';
							$title = 'MySQL';

							if (is_null($parameter)) {
								$popup = 'Данные отсутствуют';
							} else {
								$popup = 'Статус: ' . $parameter['status'] . '<br>';
								$popup .= $parameter['status'] == 'ok' ?
									'Соединений: ' .  $parameter['count'] :
									'Причина: ' .  $parameter['description'];

							}

							break;
						case 'pgsql':
							$icon = 'icon-pgsql parameter-icon';
							$title = 'PostgreSQL';

							if (is_null($parameter)) {
								$popup = 'Данные отсутствуют';
							} else {
								$popup = 'Статус: ' . $parameter['status'] . '<br>';
								$popup .= $parameter['status'] == 'ok' ?
									'Соединений: ' .  $parameter['count'] :
									'Причина: ' .  $parameter['description'];

							}

							break;

						case 'backup':
							$config = $server->getConfiguration()['add']['backup'];

							if (is_null($parameter)) {
								$popup = 'Данные отсутствуют';
							} else {
								$size = round($parameter['size'] / 1024 / 1024 / 1024, 2);

								$popup = 'Последняя запись: ' . date('Y-m-d', $parameter['time']) . '<br>';
								$popup .= 'Занято: ' .  $size . ' из ' . $config['free'] . ' GB';

							}
							$icon = 'icon-backup parameter-icon';
							$title = 'BackUp';
							break;
						default:
							$icon = 'glyphicon glyphicon-info-sign';
							$title = 'ТАЙТЛ';
							$popup = 'ПОПАП';
							break;
					}

					$data .= Html::tag('span',
						'',
						[
							'class' => ' ' . $icon,
							'title' => $title,
							'data' => [
								'toggle' => 'popover',
								'content' => $popup
							]
						]
					);

				}

				return Html::tag('span', '', [
					'class' => 'server-status status-' . $server->status,
					'title' => '№' . $server->id . ' - ' . $server->name,
					'data' => [
						'toggle' => 'popover',
						'content' => implode('<br/>', $server->getDescriptions())
					]
				]) . Html::tag('span',
					long2ip($server->ip),
					[
						'class' => 'soft-link',
						'title' => '№' . $server->id . ' - ' . $server->name,
						'data' => [
							'toggle' => 'popover',
							'content' => implode('<br/>', $server->getDescriptions())
						]
					]
				) . $data;
			}
		],
		[
			'attribute' => 'name',
			'label' => 'Название',
		],
		[
			'attribute' => 'time',
			'label' => 'Время',
			'value' => function($server) {
				/** @var $server SnifferServers */
				$days = floor($server->time / 86400);
				return ($days > 0 ? $days . ' ' : '') . gmdate('H:i:s',  $server->time);
			}
		],
		[
			'attribute' => 'last_check',
			'label' => 'Прошло',
			'format' => 'raw',
			'value' => function($server) {
				/** @var $server SnifferServers */
				$time = time() - $server->last_check;

				if ($time > 900) {
					return ' > 15 минут';
				}

				return (
				empty($server->message) ? '' :
					Html::tag(
						'span',
						'',
						[
							'class' => 'glyphicon glyphicon-info-sign error-message',
							'title' => 'Ошибка при проверке!',
							'data' => [
								'toggle' => 'popover',
								'content' => $server->message,
							]
						])
				) . gmdate('H:i:s', $time);
			}
		],
		[
			'attribute' => 'ping',
			'label' => 'Пинг',
			'format' => 'raw',
			'value' => function($server) {
				/** @var $server SnifferServers */
				return Html::tag('span',
					$server->ping == 0 ? ' - ' : $server->ping . ' мс',
					[
						'class' => 'soft-link history',
						'data' => [
							'parameter' => 'ping',
						]
					]
				);
			}
		],
		[
			'attribute' => 'disk_drive',
			'label' => 'Диск',
			'format' => 'raw',
			'value' => function($server) {
				/** @var $server SnifferServers */
				$param = $server->getDescription('disk');
				return Html::tag('span',
					$server->disk_drive == 0 ? ' - ' : $server->disk_drive . '%',
					[
						'class' => 'soft-link history',
						'data' => [
							'parameter' => 'disk_drive',
							'toggle' => 'popover',
							'content' => 'Занято: ' . round($param * $server->disk_drive / 100, 2) . ' из ' . $param . ' GB'
						]
					]
				);
			}
		],
		[
			'attribute' => 'memory',
			'label' => 'Память',
			'format' => 'raw',
			'value' => function($server) {
				/** @var $server SnifferServers */
				$param = $server->getDescription('memory');
				return Html::tag('span',
					$server->memory == 0 ? ' - ' : $server->memory . '%',
					[
						'class' => 'soft-link history',
						'data' => [
							'parameter' => 'memory',
							'toggle' => 'popover',
							'content' => 'Используется: ' . round($param * $server->memory / 100, 2) . ' из ' . $param . ' GB'
						]
					]
				);
			}
		],
		[
			'attribute' => 'processor',
			'label' => 'Процессор',
			'format' => 'raw',
			'value' => function($server) {
				/** @var $server SnifferServers */
				return Html::tag('span',
					$server->processor == 0 ? ' - ' : $server->processor . '%',
					[
						'class' => 'soft-link history',
						'data' => [
							'parameter' => 'processor',
							'toggle' => 'popover',
							'content' => 'Ядер: ' . $server->getDescription('core')
						]
					]
				);
			}
		],
		[
			'attribute' => 'user_id',
			'format' => 'raw',
			'label' => 'Пользователь',
			'value' => function($server) {
				/** @var $server SnifferServers */
				$user = SnifferUsers::findOne($server->user_id);

				if (is_null($user)) {
					return Html::a(
						'<span class="glyphicon glyphicon-plus"></span>&nbsp;Добавить',
						'#',
						[
							'class' => 'btn btn-success add-user-btn user-info',
							'title' => 'Добавить пользователя',
							'data' => [
								'target' => '#add-user-modal',
								'toggle' => 'modal',
								'id' => $server->id,
							],
						]
					);
				}

				return Html::a(
					$user->name,
					'#',
					[
						'class' => 'edit-monitoring user-info',
						'data' => [
							/*'target' => '#user-settings-modal',
							'toggle' => 'modal',*/
							'id' => $server->id,
						],
					]
				);
			}
		],
	],
]);