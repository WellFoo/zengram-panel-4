<?php
/**
 * @var $this \yii\web\View
 * @var $data array
 * @var $started_count int
 * @var $not_working array
 */
$this->title = 'Нагрузка серверов';
?>
<div class="stats-servers">
	<h1><?= $this->title ?></h1>

	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Сервер</th>
				<th>Память</th>
				<th>Процессор</th>
				<th>Диск (свободно)</th>
				<th>Проекты</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$sum = 0;
			foreach ($data as $worker => $load) {
				$sum += $load['projects'];
				?>
				<tr>
					<td><?= $worker ?></td>
					<?php if ($load === false) { ?>
						<td colspan="4">ОШИБКА ПОЛУЧЕНИЯ ИНФОРМАЦИИ</td>
					<?php } else { ?>
						<td><?= Yii::$app->formatter->asDecimal(100 - $load['mem'], 2) ?>%</td>
						<td><?= Yii::$app->formatter->asDecimal(100 - $load['cpu'], 2) ?>%</td>
						<td><?= $load['disk'] ?></td>
						<td><?= $load['projects'] ?></td>
					<?php } ?>
				</tr>
			<?php } ?>
			<tr>
				<td colspan="4" class="text-right">Запущеных по базе: <?= $started_count ?></td>
				<td><?= $sum ?></td>
			</tr>
		</tbody>
	</table>
	<h2>Неподнявшиеся после падения проекты</h2>
	<?php
		foreach($not_working as $nwAccount){
			echo '<p><a href="/admin/projects?login='.$nwAccount['login'].'" target="_blank" title="Перейти к настройкам аккаунта">' . $nwAccount['login'] . '</a></p>';
		}
	?>
</div>
