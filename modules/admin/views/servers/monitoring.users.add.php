<?php
/**
 * @var $this \yii\web\View
 * @var $user \app\models\SnifferUsers
 */

use app\models\SnifferServers;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

$this->title = 'Добавление пользователя';

echo '<h2>' . $this->title . '</h2>';

echo $this->render('monitoring.user.form.php', [
	'user' => $user
]);