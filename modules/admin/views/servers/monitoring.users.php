<?php
/**
 * @var $this \yii\web\View
 * @var $server \app\models\SnifferGroups
 */

use app\models\SnifferServers;
use yii\bootstrap\Html;

$this->title = 'Пользователи мониторинга';

$query = \app\models\SnifferUsers::find()
	//->all()
;

$dataProvider = new \yii\data\ActiveDataProvider([
	'query' => $query
]);

echo '<h2>' . $this->title . '</h2>';

echo Html::a(
	'Добавить пользователя',
	['/admin/servers/add-user/'],
	[
		'class' => 'btn btn-success',
	]);

echo '<br><br>';

echo \yii\grid\GridView::widget([
	'dataProvider' => $dataProvider,
	'layout' => '
		{items}'
	,
	'columns'      => [
		'id',
		'name',
		'email',
		'phone',
		'skype',
		[
			'label' => '',
			'format' => 'raw',
			'value' => function($user){
				/** @var $user \app\models\SnifferUsers */
				return Html::a(
					'<span class="glyphicon glyphicon-pencil"></span>',
					['/admin/servers/edit-user/' . $user->id]
				) . '&emsp;' . Html::a(
					'<span class="glyphicon glyphicon-remove"></span>',
					['/admin/servers/delete-user/' . $user->id],
					['class' => 'action-delete']
				);
			}
		],
	],
]);

?>

<script>
	$('.action-delete').click(function(e){
		if (!confirm('Удалить пользователя?')) {
			e.preventDefault();
			return false;
		}
	});
</script>
