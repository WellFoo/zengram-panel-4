<?php

use yii\helpers\Html;

/**
 * @var array $settings
 * @var array $settings_pause_0
 * @var array $settings_pause_1
 * @var array $settings_pause_2
 */

$this->title = 'Settings';
?>
<div class="site-admin">

	<table class="table">
		<thead>
		<tr>
			<th>Ключ</th>
			<th class="text-center">Значение</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td colspan="3"><strong style="font-weight: bold">Настройки вне зависимости от скорости</strong></td>
		</tr>
		<?php foreach ($settings as $setting): ?>
			<tr>
				<td><?= $setting['description']; ?></td>
				<td class="text-center"><?= $setting['value']; ?></td>
				<td><?= Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', [
						'/admin/settings/edit', 'id' => $setting['id']
					]); ?></td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="3"><strong style="font-weight: bold">Настройки для медленной скорости</strong></td>
		</tr>
		<?php foreach ($settings_pause_0 as $setting): ?>
			<tr>
				<td><?= $setting['description']; ?></td>
				<td class="text-center"><?= $setting['value']; ?></td>
				<td><?= Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', [
						'/admin/settings/edit', 'id' => $setting['id']
					]); ?></td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="3"><strong style="font-weight: bold">Настройки для средней скорости</strong></td>
		</tr>
		<?php foreach ($settings_pause_1 as $setting): ?>
			<tr>
				<td><?= $setting['description']; ?></td>
				<td class="text-center"><?= $setting['value']; ?></td>
				<td><?= Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', [
						'/admin/settings/edit', 'id' => $setting['id']
					]); ?></td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="3"><strong style="font-weight: bold">Настройки для быстрой скорости</strong></td>
		</tr>
		<?php foreach ($settings_pause_2 as $setting): ?>
			<tr>
				<td><?= $setting['description']; ?></td>
				<td class="text-center"><?= $setting['value']; ?></td>
				<td><?= Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', [
						'/admin/settings/edit', 'id' => $setting['id']
					]); ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

</div>