<form method = "POST">
    <label>
        <input type = "checkbox" name = "delivery_stats" <?=($settings['delivery_stats'])? 'checked' : ''?> /> Включить рассылку
    </label> <br />

    <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />

    <label>
        <input type = "checkbox" name = "delivery_stats_test" <?=($settings['delivery_stats_test'])? 'checked' : ''?> /> Тестовый режим
    </label>
    <br />

    <input type = "submit" value = "Изменить настройки" class = "btn btn-primary" />
</form> <br /><br />

<?php
use yii\grid\GridView;
use yii\widgets\Pjax;

Pjax::begin();
echo GridView::widget([
    'id'             => 'delivery-stats',
    'dataProvider'   => $dataProvider,
    'filterSelector' => '[name="num_users"]',
    'pager'          => [
        'firstPageLabel' => 'Перв',
        'lastPageLabel'  => 'Посл'
    ],
    'columns'        => [
        [
            'label'         => 'id',
            'attribute'     => 'id',
            'headerOptions' => ['style' => 'width: 7%;'],
            'format'        => 'raw',
            'value'         => function ($item) {
                return $item->id;
            }
        ], [
            'label'     => 'Пользователь',
            'attribute' => 'user_id',
            'format'    => 'raw',
            'value'     => function ($item) {
                return $item->user->mail;
            }
        ], [
            'label'     => 'Аккаунт',
            'attribute' => 'instagram_id',
            'format'    => 'raw'
        ], [
            'label'     => 'Действия',
            'attribute' => 'id',
            'headerOptions' => ['style' => 'width: 10%; text-align: right;'],
            'format'    => 'raw',
            'value'     => function ($item) {
                return '<a href = "'.\yii\helpers\Url::to(['/admin/utility/delivery-stats-remove', 'id' => $item->id]).'" class = "btn btn-danger btn-sm">Удалить</a>';
            }
        ]
    ]
]);
?>
<?php Pjax::end(); ?>

<a href = "<?=\yii\helpers\Url::to('/admin/utility/delivery-stats-add')?>" class = "btn btn-primary">Добавить</a>
