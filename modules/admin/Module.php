<?php

namespace app\modules\admin;

use app\models\Users;
use yii\web\ForbiddenHttpException;
use Yii;

class Module extends \yii\base\Module
{
	public $controllerNamespace = 'app\modules\admin\controllers';

	public $defaultRoute = 'users';
	public $layout = 'main';

	public function init()
	{
		parent::init();
		// custom initialization code goes here

		if (Yii::$app->user->isGuest) {
			throw new ForbiddenHttpException('You are not allowed to access this page.');
		}

		/** @var Users $user */
		$user = Yii::$app->user->identity;
		$availableRoles = [
			Users::ROLE_ADMIN,
			Users::ROLE_MANAGER
		];
		if (!in_array($user->role, $availableRoles)) {
			throw new ForbiddenHttpException('You are not allowed to access this page.');
		}

		Yii::$app->language = 'ru';
	}
}
