<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Account;
use app\models\AccountProcessLog;
use app\models\Invoice;
use app\models\TestDeliveryAccount;
use app\models\Whitelist;
use core\logger\Logger;
use DOMDocument;
use DOMXPath;
use Yii;
use yii\base\ErrorException;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UtilityController extends Controller
{
	public function actionIdByLogin()
	{
		return $this->render('id-by-login');
	}

	public function actionDeliveryStats()
	{
		$this->enableCsrfValidation = false;
		$query = \app\models\TestDeliveryAccount::find();

		$settings = json_decode(file_get_contents(__DIR__ . '/../config/settings.json'), true);

		if (Yii::$app->request->isPost) {
			$settings['delivery_stats'] = Yii::$app->request->post('delivery_stats');
			$settings['delivery_stats_test'] = Yii::$app->request->post('delivery_stats_test');

			file_put_contents(__DIR__ . '/../config/settings.json', json_encode($settings));
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		return $this->render('delivery-stats', compact('query', 'dataProvider', 'settings'));
	}

	public function actionDeliveryStatsAdd()
	{
		$model = new \app\models\TestDeliveryAccount();

		if (Yii::$app->request->isPost) {
			$model->load(Yii::$app->request->post());

			$model->user_id = Yii::$app->user->id;

			if ($model->save()) {
				Yii::$app->response->redirect(['/admin/utility/delivery-stats']);
			}
		}

		return $this->render('delivery-stats-add', compact('model'));
	}

	public function actionDeliveryStatsRemove($id)
	{
		if (!$id) {
			Yii::$app->getResponse()->redirect(['/admin/utility/delivery-stats']);
			return;
		}

		/* @type Post $post */
		$account = TestDeliveryAccount::findOne(['id' => $id]);

		$account->delete();
		Yii::$app->response->redirect(['/admin/utility/delivery-stats']);
	}

	public function actionNopayedInvoices() {
		$query = Invoice::find()
			->select([
				'DISTINCT ON(user_id) *'
			])
			->where("agent = 'yandex_card' and status = 0");

		$date = Yii::$app->request->get('date');

		if (!empty($date)) {
			$query->andWhere("(date > ('".date('Y.m.d', strtotime($date))."'::timestamp) and date < ('".date('Y.m.d', strtotime($date))."'::timestamp + '1 days'::interval)::timestamp)");
		} else {
			$query->andWhere("date >= ('".date('Y.m.d')."'::timestamp)");
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		return $this->render('nopayed-invoices', compact('query', 'dataProvider'));
	}

	public function actionIdByLoginAction()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$login = Yii::$app->request->post('login', '');

		if (empty($login)) {
			return [
				'data' => 'Пустой логин',
			];
		}

		try {
			$html = file_get_contents('http://instagram.com/'.$login);
		} catch (ErrorException $e) {
			return [
				'data' => 'Не удалось найти такого пользователя',
			];
		}

		$doc = new DOMDocument();
		$doc->loadHTML($html);

		$xpath = new DOMXPath($doc);
		$js = $xpath->query('//body/script[@type="text/javascript"]')->item(0)->nodeValue;

		$start = strpos($js, '{');
		$end = strrpos($js, ';');
		$json = substr($js, $start, $end - $start);

		$data = json_decode($json, true);
		$data = $data['entry_data']['ProfilePage'][0];

		if (!empty($data['user']['id'])) {
			return [
				'data' => $data['user']['id'],
			];
		}

		return [
			'data' => 'ID не найден',
		];
	}

	public function actionAccountLog($id)
	{
		if (empty($id)) {
			throw new NotFoundHttpException();
		}
		
		$account = Account::findOne([
			'instagram_id' => $id
		]);

		if (is_null($account)) {
			throw new NotFoundHttpException();
		}

		return $this->render('account-log', $this->buildLog($account));
	}

	private function buildLog($account, $max_id = 0)
	{
		$hoursFrom = (int) Yii::$app->request->get('hoursFrom', 0);
		$hoursTo = (int) Yii::$app->request->get('hoursTo', 23);

		$secondsTo = $hoursTo * 60 * 60;
		$secondsFrom = $hoursFrom * 60 * 60;

		$dateTo = strtotime(date('Y-m-d')) + $secondsTo;

		$date = Yii::$app->request->get('date', 'day');
		$worker = (int) Yii::$app->request->get('worker', 0);
		$group = (int) Yii::$app->request->get('group', -1);
		$envinroment = (int) Yii::$app->request->get('envinroment', -1);
		$text_search = Yii::$app->request->get('text-search', null);

		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 7;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 30;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from')) + $secondsFrom;
				$dateTo = strtotime(Yii::$app->request->get('to')) + $secondsTo;
				break;
		}

		$logs = AccountProcessLog::find()
			->where([
				'instagram_id' => $account->instagram_id
			])
			->andWhere(['>=', 'date', $dateFrom])
			->andWhere(['<', 'date', $dateTo])
			->groupBy('id')
			->orderBy([
				'id' => SORT_ASC
			]);

		if ($worker !== 0) {
			$logs->andWhere([
				'worker_ip' => sprintf(
					'%u',
					ip2long(
						Yii::$app->params['workers'][$worker - 1])
				)
			]);
		}

		if ($envinroment === -1) {
			$group = -1;
		} else {
			$code_croups = Logger::getGroupBranch($envinroment);

			if (empty($code_croups[$group])) {
				$group = -1;
			}
		}

		if ($group !== -1 && !empty($text_search)) {

			$group_codes = Logger::getCodesInGroup($group);
			$text_codes =  Logger::getCodesByText(trim($text_search));

			$logs->andWhere([
				'code' => array_intersect($group_codes, $text_codes)
			]);

		} else if ($group !== -1) {
			$logs->andWhere([
				'code' => Logger::getCodesInGroup($group)
			]);
		} else if (!empty($text_search)) {
			$logs->andWhere([
				'code' => Logger::getCodesByText(trim($text_search))
			]);
		}

		if ($envinroment !== -1) {
			$logs->andWhere([
				'environment' => $envinroment
			]);
		}

		if ($max_id !== 0) {
			$logs->andWhere([
				'>', 'id', $max_id
			]);
		}



		$dataProvider = new ActiveDataProvider([
			'query' => $logs,
			'pagination'    => [
				'pageSize'  => 500,
			]
		]);

		if (empty(Yii::$app->request->get('page', ''))) {
			$dataProvider->pagination->page = ceil($logs->count() / 500) - 1;
		}

		return [
			'account' => $account,
			'count' => $logs->count(),
			'date' => $date,
			'dateTo' => $dateTo,
			'dateFrom' => $dateFrom,
			'hoursFrom' => $hoursFrom,
			'hoursTo' => $hoursTo,
			'worker' => $worker,
			'group' => $group,
			'text_search' => $text_search,
			'envinroment' => $envinroment,
			'dataProvider' => $dataProvider,
		];
	}

	public function actionAccountLogUpdate()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$id = Yii::$app->request->get('id', 0);

		if (empty($id)) {
			throw new NotFoundHttpException();
		}

		$account = Account::findOne([
			'instagram_id' => $id
		]);

		if (is_null($account)) {
			throw new NotFoundHttpException();
		}

		$max_id = (int) Yii::$app->request->post('max_id', 0);

		$data = $this->buildLog($account, $max_id);

		if ($data['count'] == 0) {
			return false;
		}

		return $this->renderAjax('account-log-table', $data);
	}

	public function actionWhitelistLog()
	{
		$accounts = Account::find()->where(['or',
			['whitelist_added' => Whitelist::WHITELIST_DELAY],
			['whitelist_added' => Whitelist::WHITELIST_ERROR],
		])->orderBy(['id' => SORT_DESC]);

		return $this->render('whitelist-log', [
			'accounts' => $accounts
		]);
	}
}