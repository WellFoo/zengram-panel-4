<?php

namespace app\modules\admin\controllers;

use app\models\Invoice;
use app\models\MonitoringLog;
use app\modules\admin\models\MonitoringWorkers;
use Yii;
use app\models\Monitoring;
use app\models\MonitoringWorkersOffline;
use app\modules\admin\models\MonitoringSearch;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MonitoringController implements the CRUD actions for Monitoring model.
 */
class MonitoringController extends Controller
{
	public function behaviors()
	{
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all Monitoring models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new MonitoringSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		$dataProviderCategory = $searchModel->search(Yii::$app->request->queryParams, false);
		$category_query = clone $dataProviderCategory->query;
		

		/** @var ActiveQuery $category_query */
		$category_query->select([new Expression('COUNT(DISTINCT '.Monitoring::tableName().'.id) AS counts, category')])->asArray()->groupBy('category')->all();

		$searchModel->categoriesCount = ArrayHelper::map($category_query->all(), 'category', 'counts');

		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'date'          => $searchModel->date,
			'dateFrom'      => $searchModel->dateFrom,
			'dateTo'        => $searchModel->dateTo,
		]);
	}

	/**
	 * Creates a new Monitoring model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Monitoring();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Monitoring model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		
		$load_result = $model->load(Yii::$app->request->post());
		
		$model->sms = str_replace([' ', '(', ')', '-'], '', $model->sms);

		if ($load_result && $model->save()) {
			return $this->redirect(['index']);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	public function actionLog($id)
	{
		$model = $this->findModel($id);
		return $this->render('log', [
			'title' => $model->title,
			'dataProvider' => new ActiveDataProvider([
				'query' => MonitoringLog::find()->where(['monitoring_id' => $id]),
				'sort'=> ['defaultOrder' => ['time'=>SORT_DESC]]
			])
		]);
	}

	public function actionServersLog()
	{
		$query = MonitoringWorkersOffline::find();
		return $this->render('servers', [
			'dataProvider' => new ActiveDataProvider([
				'query' => $query,
				'sort'=> ['defaultOrder' => ['starttime'=>SORT_DESC]]
			])
		]);
	}

	public function actionBadInvoices()
	{
		$query = Invoice::find()->where(['!=', 'status', Invoice::STATUS_SUCCESS])->andWhere(['redirected' => 1]);
		return $this->render('badInvoices', [
			'dataProvider' => new ActiveDataProvider([
				'query' => $query,
				'sort'=> ['defaultOrder' => ['date'=>SORT_DESC]]
			])
		]);
	}

	/**
	 * Deletes an existing Monitoring model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Monitoring model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Monitoring the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Monitoring::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
	public function actionOverloadLog(){
		$query = MonitoringWorkers::find()->where(['type_event' => MonitoringWorkers::MEMORY_OVERLOAD]);
		return $this->render('overload', [
			'dataProvider' => new ActiveDataProvider([
				'query' => $query,
				'sort'=> ['defaultOrder' => ['starttime'=>SORT_DESC]]
			])
		]);
	}
}
