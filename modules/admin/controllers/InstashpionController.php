<?php
namespace app\modules\admin\controllers;

use app\models\InstashpionHint;
use app\models\Invoice;
use app\models\ShpionBalance;
use app\models\UserHints;
use app\modules\instashpion\models\Analize;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class InstashpionController extends Controller
{
	public function init()
	{
		parent::init();

		if (!Yii::$app->user->identity->isAdmin) {
			throw new ForbiddenHttpException('У вас нет прав для просмотра этой страницы.');
		}
	}

	public function actionIndex()
	{
		$dateTo = strtotime(date('Y-m-d'));
		$date = Yii::$app->request->get('date', 'day');

		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 7;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 30;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from'));
				$dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}

		$paymentsQuery = Invoice::find()
			->where([
				'price_id' => 9, 
				'status' => Invoice::STATUS_SUCCESS
			])
			->andWhere(['>=', 'date', date('Y-m-d', $dateFrom)])
			->andWhere(['<', 'date', date('Y-m-d', $dateTo)]);


		$hitsQuery = InstashpionHint::find()
			->andWhere(['>=', 'date', $dateFrom])
			->andWhere(['<', 'date', $dateTo]);


		$tHQ = clone $hitsQuery;
		$yHQ = clone $hitsQuery;
		$gHQ = clone $hitsQuery;
		$nHQ = clone $hitsQuery;
		$zHQ = clone $hitsQuery;

		$totalHits = $tHQ->count();

		$yandexHits = $yHQ->andWhere(['source' => 'yandex.ru'])->orWhere(['source' => 'yandex.com'])->count();
		$googleHits = $gHQ->andWhere(['source' => 'google.ru'])->orWhere(['source' => 'google.com'])->count();
		$zengramHits = $zHQ->andWhere(['source' => 'zengram.ru'])->count();

		$normalHits = $nHQ->andWhere(['source' => null])->orWhere(['source' => ''])->count();

		$etcHits = $totalHits - $yandexHits - $googleHits - $zengramHits - $normalHits;

		$yPQ = (clone $paymentsQuery);
		$iPQ = (clone $paymentsQuery);
		$pPQ = (clone $paymentsQuery);

		$yandexPayments = $yPQ->andWhere(['agent' => 'yandex'])->count();
		$interkassPayments = $iPQ->andWhere(['agent' => 'interkassa'])->count();
		$paypalPayments = $pPQ->andWhere(['agent' => 'paypal'])->count();

		$analizeQuery = Analize::find()
			->andWhere(['>=', 'updated_at', date('Y-m-d', $dateFrom)])
			->andWhere(['<', 'updated_at', date('Y-m-d', $dateTo)]);


		$uAC = clone $analizeQuery;
		$gAC = clone $analizeQuery;

		$usersAnalizeCount = $uAC->andWhere('user_id > :null', [':null' => 0])->count();
		$payAnalizeCount = ShpionBalance::find()->where('is_bonus != 1')->count();
		$guestsAnalizeCount = $gAC->andWhere('user_id is null')->count();

		$dataProvider = new ActiveDataProvider([
			'query' => $analizeQuery,
		]);

		return $this->render('index', [
			'yandexPayments' => $yandexPayments,
			'interkassaPayments' => $interkassPayments,
			'paypalPayments' => $paypalPayments,

			'usersAnalizeCount' => $usersAnalizeCount,
			'guestsAnalizeCount' => $guestsAnalizeCount,
			'payAnalizeCount' => $payAnalizeCount * 3,

			'totalHits' => $totalHits,
			'yandexHits' => $yandexHits,
			'googleHits' => $googleHits,
			'zengramHits' => $zengramHits,
			'normalHits' => $normalHits,
			'etcHits' => $etcHits,

			'dataProvider' => $dataProvider,

			'date' => $date,
			'dateTo' => $dateTo,
			'dateFrom' => $dateFrom
		]);
	}

}