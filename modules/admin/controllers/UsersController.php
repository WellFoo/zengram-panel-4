<?php

namespace app\modules\admin\controllers;

use app\models\Account;
use app\models\AccountActionLog;
use app\models\AccountProcessLog;
use app\models\BalanceFlow;
use app\models\Mail;
use app\models\ProxyCat;
use app\models\Users;
use app\models\UsersLog;
use app\modules\admin\models\AccountBlocks;
use app\modules\admin\models\Balance;
use app\modules\admin\models\UsersSearch;
use app\components\Controller;
use app\models\UsersInfo;
use core\logger\Logger;
use Yii;
use yii\bootstrap\Html;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\Response;

class UsersController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'except' => [
					'index',
					'log',
					'balance-log',
					'info'
				],
				'rules' => [
					[
						'allow' => true,
						'matchCallback' => function ($rule, $action) {
							return Yii::$app->user->identity->isAdmin;
						}
					]
				]
			]
		];
	}

	public function actionIndex()
	{
		$searchModel = new UsersSearch();
		$dataProvider = $searchModel->search();

		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider
		]);
	}

	public function actionBalanceFlowLog()
	{
		$user_id = (int) Yii::$app->request->get('user_id', 0);
		$user = Users::findOne($user_id);

		if (is_null($user)) {
			throw new NotFoundHttpException();
		}

		$accounts = Account::findAll([
			'user_id' => $user->id
		]);

		$dateFrom = Yii::$app->request->get('from', null);
		$dateTo = Yii::$app->request->get('to', null);

		if (is_null($dateFrom)) {
			$dateFrom = date('Y-m-d');
		}

		if (is_null($dateTo)) {
			$dateTo = date('Y-m-d', time() + 86399);
		}

		$result = [];

		$interval = strtotime($dateTo) - strtotime($dateFrom);

		/** @var Account $account */
		foreach ($accounts as $account) {
			$actions = AccountActionLog::getActivityTime($account->id, $dateFrom, $dateTo);

			$result[$account->id] = [
				'account' => $account
			];

			if (!is_array($actions)) {
				$result[$account->id]['balance'] = '-';
				continue;
			}

			$result[$account->id]['balance'] = $actions['common'];

			// Старт-стоп
			$start = [
				Logger::Z011_ACCOUNT_START_BY_USER,
				Logger::Z011_ACCOUNT_START_BY_ADMIN,
				Logger::Z011_ACCOUNT_START_BY_CRON,
				Logger::Z011_ACCOUNT_START_BY_SPREAD_PROXY,
			];

			$stop = [
				Logger::Z012_ACCOUNT_STOP_BY_USER,
				Logger::Z012_ACCOUNT_STOP_BY_ADMIN,
				Logger::Z012_ACCOUNT_STOP_BY_SPREAD_PROXY,
				Logger::Z012_ACCOUNT_STOP_BY_BALANCE,
				Logger::Z012_ACCOUNT_STOP_BY_ERROR,
				Logger::Z012_ACCOUNT_STOP_BY_COMMENTS_ERROR,
				Logger::Z012_ACCOUNT_STOP_BY_GEO_ERROR,
				Logger::Z012_ACCOUNT_STOP_BY_TIMER,
			];

			$result[$account->id]['actions'] = [];
			$actions = AccountProcessLog::find()
				->where(['>=', 'date', strtotime($dateFrom)])
				->andWhere(['<', 'date', strtotime($dateTo)])
				->andWhere(['instagram_id' => $account->instagram_id])
				->andWhere(
					ArrayHelper::merge(
						['or'],
						$this->generateLogCondition($start, $stop)
					)
				)
				->orderBy(['date' => SORT_ASC])
				->all();

			/** @var AccountProcessLog $action */
			foreach ($actions as $action) {
				$result[$account->id]['actions'][] =
					(in_array($action->code, $start) ? '+ ' : '- ') .
					date('Y-m-d H:i', $action->date)
				;
			}

			// Блокировки
			$blocks = AccountBlocks::find()
				->where(['>=', 'block_date', $dateFrom])
				->andWhere(['account_id' => $account->id])
				->andWhere(['<=', 'block_date', $dateTo])
				->orderBy(['block_date' => SORT_ASC])
				->all();

			$result[$account->id]['blocks'] = [];
			$result[$account->id]['blocked_time'] = 0;

			/** @var AccountBlocks $block */
			foreach ($blocks as $block) {
				$result[$account->id]['blocks'][] = $block->action;
				$result[$account->id]['blocked_time'] += strtotime($block->block_expire) - strtotime($block->block_date);
			}

		}

		return $this->render('balance-flow-log', [
			'data' => $result,
			'user_id' => $user_id,
			'interval' => $interval,
			'date_from' => $dateFrom,
			'date_to' => $dateTo,
			'email' => $user->mail
		]);
	}

	private function generateLogCondition($start, $stop)
	{
		$result = [];
		$codes = ArrayHelper::merge($start, $stop);
		foreach ($codes as $code) {
			$result[] = [
				'code' => $code
			];
		}
		return $result;
	}

	public function actionMailing()
	{
		return $this->render('mailing');
	}

	public function actionSwitch($id)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		/** @var Users $user */
		$user = Users::findOne($id);
		if ($user === null) {
			throw new NotFoundHttpException();
		}

		$availableAttributes = [
			'sameMade',
			'sameBlack',
			'allowImages',
			'is_service'
		];
		$attr = Yii::$app->request->post('attrName', null);
		$value = Yii::$app->request->post('value', 0);
		if ($attr === null || !in_array($attr, $availableAttributes)) {
			throw new BadRequestHttpException();
		}

		$user->setAttribute($attr, $value ? 1 : 0);
		if ($user->validate()) {
			$user->save();
		}

		return [
			'status' => 'ok',
			'value'  => $user->getAttribute($attr)
		];
	}

	public function actionEdit($id)
	{
		/* @var $model Users */
		$model = Users::findOne($id);
		if ($model === null) {
			throw new HttpException(404, 'User not found.');
		}

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->session->setFlash('success', 'User has been saved');
			Yii::$app->response->redirect(['/admin/users']);
		} else {
			Yii::$app->session->setFlash('error', 'Model could not be saved');
		}

		$cats = ProxyCat::find()
			->orderBy(['default' => SORT_DESC, 'name' => SORT_ASC])
			->all();

		return $this->render('edit', [
			'model' => $model,
			'cats'  => $cats
		]);
	}

	public function actionDelete($id)
	{
		if (!$id) {
			Yii::$app->session->setFlash('User Deleted Error');
			Yii::$app->getResponse()->redirect(['admin/users']);
			throw new BadRequestHttpException();
		}
		/** @type Users $user */
		$user = Users::findOne(['id' => $id]);

		if ($user == null || !$user->id) {
			throw new ForbiddenHttpException(Yii::t('app', 'You cannot view that resource'));
		}
		$user->delete();

		Yii::$app->session->setFlash('UserDeleted');
		return $this->redirect(['/admin/users']);
	}

	public function actionMassDelete()
	{
		$list = Yii::$app->request->post('delete-user', []);
		if (is_array($list) && count($list)) {
			Users::deleteAll(['in', 'id', $list]);
		}
		return $this->redirect(['/admin/users']);
	}

	public function actionInfo($id)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		$model = UsersInfo::findOne(['user_id' => $id]);
		if ($model === null) {
			$model = new UsersInfo(['user_id' => $id]);
			$model->save();
		}
		$data = ['<div class="row"><div class="col-md-10 col-sm-10 col-xs-10">Описание</div>
				<div class="col-md-2 col-sm-2 col-xs-2">Значение</div></div>'];
		foreach ($model->attributeLabels() as $attributeId => $attribute) {
			$data[] = '<div class="row"><div class="col-md-10 col-sm-10 col-xs-10">' . $attribute . '</div>
			<div class="col-md-2 col-sm-2 col-xs-2">' .
				(is_int($model->$attributeId) && in_array($model->$attributeId, [0, 1]) ? ($model->$attributeId ? 'да' : 'нет') :
					$model->$attributeId) . '</div></div>';
		}
		return ['data' => $data, 'status' => 'ok'];
	}

	public function actionLog($id, $page = 0)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$log_page_size = 100; // TODO: константа? параметр?

		$logs = UsersLog::find()
			->where(['user_id' => $id]);

		$pages = ceil($logs->count() / $log_page_size);

		$logs
			->orderBy(['date' => SORT_DESC])
			->limit($log_page_size)
			->offset((int) $page * $log_page_size);

		$data = ['<div class="row"><div class="col-md-4 col-sm-4 col-xs-4">Дата</div>
					<div class="col-md-8 col-sm-8 col-xs-8">Текст</div></div>'];
		foreach ($logs->each() as $log) {
			$data[] = '<div class="row"><div class="col-md-4 col-sm-4 col-xs-4">' .
				$log->date .
				' МСК</div><div class="col-md-8 col-sm-8 col-xs-8">' . $log->text . '</div></div>';
		}

		return [
			'data' => $data,
			'pagination' => $this->generagePagination('log', $id, $pages, $page),
			'status' => 'ok'
		];
	}

	public function actionBalanceLog($id, $page = 0)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$log_page_size = 100; // TODO: константа? параметр?

		$logs = BalanceFlow::find()
			->where(['user_id' => $id]);

		$pages = ceil($logs->count() / $log_page_size);

		$logs
			->orderBy(['date' => SORT_DESC])
			->limit($log_page_size)
			->offset((int) $page * $log_page_size);

		$data = ['<div class="row"><div class="col-md-3 col-sm-3 col-xs-3">Дата</div>
				<div class="col-md-1 col-sm-1 col-xs-1">Тип</div>
				<div class="col-md-2 col-sm-2 col-xs-2">Кол-во (дни:часы:мин)</div>
				<div class="col-md-6 col-sm-6 col-xs-6">Описание</div></div>'];

		foreach ($logs->each() as $log)
		{
			//balance in seconds
			$data[] = '<div class="row"><div class="col-md-3 col-sm-3 col-xs-3">'. $log->date. ' МСК</div>
			<div class="col-md-1 col-sm-1 col-xs-1">'.($log->type == 'sub' ? '-' : '+').'</div>
			<div class="col-md-2 col-sm-2 col-xs-2">'.Balance::formatBalance($log->value).'</div>
			<div class="col-md-6 col-sm-6 col-xs-6">'.$log->description.'</div></div>';
		}
		return [
			'data' => $data,
			'pagination' => $this->generagePagination('balance-log', $id, $pages, $page),
			'status' => 'ok'
		];
	}

	private function generagePagination($action, $user_id, $pages_count, $page)
	{
		$pages = [];

		for ($i = 0; $i < $pages_count; $i++) {
			$pages[$i] = $i;
		}

		return Html::dropDownList(
			'pagination',
			$page,
			$pages, [
				'onchange' => 'showData('.$user_id.', "'.$action.'", $(this).val())',
				'class' => 'form-control'
			]
		);
	}

	public function actionBalance($id)
	{
		if ($id == null) {
			throw new HttpException(404, 'User not found.');
		}

		/** @var Users $model */
		$model = Users::find()->where('id = ' . $id)->one();
		$model->balance = $model->balance + ((int)$_POST['format'] * (int)$_POST['balance']);
		$model->is_payed = true;

		Mail::deleteAll([
			'mail' => $model->mail,
			'type' => 'lowbalance'
		]);

		if ($model->save()) {
			$bf = new BalanceFlow([
				'user_id'     => $id,
				'value'       => ((int)$_POST['format'] * (int)$_POST['balance']),
				'description' => 'Добавление из админки',
				'type'        => 'add'
			]);
			$bf->save();
			return Balance::formatBalance($model->balance);
		}

		return 'error';
	}
}