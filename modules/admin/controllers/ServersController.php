<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\components\RPCHelper;
use app\models\Account;
use app\models\SnifferGroups;
use app\models\SnifferLog;
use app\models\SnifferMonitoringParameters;
use app\models\SnifferServers;
use app\models\SnifferUsers;
use UnitedPrototype\GoogleAnalytics\Exception;
use Yii;
use yii\bootstrap\Html;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ServersController extends Controller
{
	public $defaultAction = 'load';

	/**
	 * Initializes the object.
	 * This method is invoked at the end of the constructor after the object is initialized with the
	 * given configuration.
	 */
	public function init()
	{
		parent::init();

		if (!Yii::$app->user->identity->isAdmin) {
			throw new ForbiddenHttpException('У вас нет прав для просмотра этой страницы.');
		}
	}

	public function actionIndex()
	{
		$projects   = Account::find()->all();
		$connection = Yii::$app->db;
		$model      = $connection->createCommand('SELECT server, COUNT(*) as count FROM accounts GROUP BY server');
		$servers    = $model->queryAll();
		return $this->render('index', [
			'projects' => $projects,
			'servers'  => $servers,
		]);
	}

	public function actionLoad()
	{
		$load = [];
		foreach (Yii::$app->params['workers'] as $worker) {
			$result = RPCHelper::getServerLoad($worker);
			$load[$worker] = $result['status'] === 'ok' ? $result['data'] : false;
		}
		$notWorking = $this->searchNotWorking();
		return $this->render('load', [
			'data'          => $load,
			'started_count' => Account::find()->where(['monitoring_status' => 'work'])->count(),
			'not_working' => $notWorking
		]);
	}

	public function actionMonitoring()
	{
		return $this->render('monitoring');
	}

	public function actionUsers()
	{
		return $this->render('monitoring.users.php');
	}

	public function actionEditUser($id)
	{
		if (empty($id)) {
			throw new NotFoundHttpException();
		}

		$user = SnifferUsers::findOne($id);

		if (is_null($user)) {
			throw new NotFoundHttpException();
		}

		if ($user->load(Yii::$app->request->post()) && $user->save()) {
			return $this->redirect(['users']);
		}

		return $this->render('monitoring.users.edit.php', [
			'user' => $user
		]);
	}

	public function actionAddUser()
	{
		$user = new SnifferUsers();

		if ($user->load(Yii::$app->request->post()) && $user->save()) {
			return $this->redirect(['users']);
		}

		return $this->render('monitoring.users.add.php', [
			'user' => $user
		]);
	}

	public function actionGetServerSummary()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$server_id = (int) Yii::$app->request->post('server');
		$dateFrom =  Yii::$app->request->post('from');
		$dateTo = Yii::$app->request->post('to');

		if (empty($dateTo) && empty($dateFrom)) {
			$dateTo = strtotime(date('Y-m-d 23:59:59'));
			$dateFrom = $dateTo - 86400;
		} else {
			$dateTo = strtotime($dateTo) + 86399;
			$dateFrom =  strtotime($dateFrom);
		}

		$logs = SnifferLog::find()
			->andWhere(['>=', 'time', $dateFrom])
			->andWhere(['<', 'time', $dateTo])
			->orderBy(['id' => SORT_ASC]);

		if ($server_id) {
			$logs->andWhere([
				'server_id' => $server_id,
			]);
		}

		//TODO: притормаживает, т.к. в кеш пишутся модели по одной. Сделать пачкой, тормоза исчезнут
		$parameters_cache = [];
		$servers_cache = [];
		$groups_cache = [];
		$user_cache = [];
		$result = [];

		/** @var SnifferLog $log */
		foreach ($logs->each() as $log) {

			if (empty($parameters_cache[$log->server_id])) {
				$parameters_cache[$log->server_id] = SnifferMonitoringParameters::findAll([
					'server_id' => $log->server_id
				]);
			}

			$parameters = $parameters_cache[$log->server_id];

			/** @var SnifferMonitoringParameters $parameter */
			foreach ($parameters as $parameter) {

				$name = $parameter->parameter;
				$value = $log->$name;

				if (!$parameter->compare($value)) {
					continue;
				}

				if (empty($user_cache[$parameter->user_id])) {
					$user_cache[$parameter->user_id] = SnifferUsers::findOne($parameter->user_id);
					if (is_null($user_cache[$parameter->user_id])) {
						continue;
					}
				}

				$user = $user_cache[$parameter->user_id];

				if (empty($servers_cache[$log->server_id])) {
					$servers_cache[$log->server_id] =  SnifferServers::findOne($log->server_id);
					if (is_null($servers_cache[$log->server_id])) {
						continue;
					}
				}

				/** @var SnifferServers $server */
				$server = $servers_cache[$log->server_id];

				if (empty($groups_cache[$server->parent])) {
					$groups_cache[$server->parent] = SnifferGroups::findOne($server->parent);
					if (is_null($groups_cache[$server->parent])) {
						continue;
					}
				}

				/** @var SnifferGroups $group */
				$group = $groups_cache[$server->parent];

				$result[] = [
					'date' => date('Y-m-d H:i:s', $log->time),
					'server' => long2ip($server->ip) . ' - ' . $group->name . ' (' . $server->name . ')',
					'description' =>
						$parameter->getParameterDescription() . ' ' .
						$parameter->getOperatorDescription() . ' ' .
						$parameter->value . ' (' . $value . ')',
					'user' => $user->name
				];
			}
		}

		$dataProvider = new ArrayDataProvider([
			'allModels' => $result
		]);

		$data = GridView::widget([
			'dataProvider' => $dataProvider,
			'layout' => '{items}',
			'columns'      => [
				[
					'attribute' => 'date',
					'label' => 'Дата',
				],
				[
					'attribute' => 'description',
					'label' => 'Описание',
				],
				[
					'attribute' => 'server',
					'label' => 'Сервер',
				],
				[
					'attribute' => 'user',
					'label' => 'Наблюдатель',
				],
			],
		]);

		return [
			'status' => 'ok',
			'dateFrom' =>  date('Y-m-d', $dateFrom),
			'dateTo' => date('Y-m-d', $dateTo),
			'data' => $data
		];
	}

	public function actionDeleteUser($id)
	{
		if (empty($id)) {
			throw new NotFoundHttpException();
		}

		$user = SnifferUsers::findOne($id);

		if (is_null($user)) {
			throw new NotFoundHttpException();
		}

		SnifferMonitoringParameters::deleteAll([
			'user_id' => $user->id
		]);

		$user->delete();

		return $this->redirect(['users']);
	}

	public function actionEditMonitoring($id)
	{
		if (empty($id)) {
			throw new NotFoundHttpException();
		}

		$monitoring = SnifferMonitoringParameters::findOne($id);

		if (is_null($monitoring)) {
			throw new NotFoundHttpException();
		}

		$user = SnifferUsers::findOne($monitoring->user_id);

		if (is_null($user)) {
			throw new NotFoundHttpException();
		}

		if ($monitoring->load(Yii::$app->request->post()) && $monitoring->save()) {
			return $this->redirect(['servers/edit-user/' . $user->id]);
		}

		return $this->render('monitoring.monitoring.edit.php', [
			'user' => $user,
			'monitoring' => $monitoring,
		]);
	}

	public function actionAddMonitoring($id)
	{
		if (empty($id)) {
			throw new NotFoundHttpException();
		}

		$user = SnifferUsers::findOne($id);

		if (is_null($user)) {
			throw new NotFoundHttpException();
		}

		$monitoring = new SnifferMonitoringParameters([
			'user_id' => $user->id
		]);

		if ($monitoring->load(Yii::$app->request->post()) && $monitoring->save()) {
			return $this->redirect(['servers/edit-user/' . $user->id]);
		}

		return $this->render('monitoring.monitoring.add.php', [
			'user' => $user,
			'monitoring' => $monitoring,
		]);
	}

	public function actionDeleteMonitoring($id)
	{
		if (empty($id)) {
			throw new NotFoundHttpException();
		}

		$monitoring = SnifferMonitoringParameters::findOne($id);

		if (is_null($monitoring)) {
			throw new NotFoundHttpException();
		}

		$user_id = $monitoring->user_id;

		$monitoring->delete();

		return $this->redirect(['servers/edit-user/' . $user_id]);
	}

	public function actionGetParameterHistory()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$server_id = (int) Yii::$app->request->post('server');
		$parameter_name = Yii::$app->request->post('parameter');

		$dateFrom =  Yii::$app->request->post('from');
		$dateTo = Yii::$app->request->post('to');

		if (empty($dateTo) && empty($dateFrom)) {
			$dateTo = strtotime(date('Y-m-d 23:59:59'));
			$dateFrom = $dateTo - 86400;
		} else {
			$dateTo = strtotime($dateTo) + 86399;
			$dateFrom =  strtotime($dateFrom);
		}

		$server = SnifferServers::findOne($server_id);

		if (is_null($server) || empty($server->$parameter_name)) {
			throw new ForbiddenHttpException();
		}

		$limits = SnifferMonitoringParameters::find()
			->select([
				SnifferMonitoringParameters::tableName().'.value',
				SnifferMonitoringParameters::tableName().'.operator',
				SnifferMonitoringParameters::tableName().'.id',
				SnifferUsers::tableName().'.name',
			])
			->where([
				'parameter' => $parameter_name,
				'server_id' => $server_id,
			])
			->leftJoin(SnifferUsers::tableName(), SnifferMonitoringParameters::tableName().'.user_id = '.SnifferUsers::tableName().'.id')
			->asArray();

		$logs = SnifferLog::find()
			->where([
				'server_id' => $server_id,
			])
			->andWhere(['>=', 'time', $dateFrom])
			->andWhere(['<', 'time', $dateTo])
			->orderBy(['id' => SORT_ASC]);

		return [
			'status' => 'ok',
			'parameter' => SnifferMonitoringParameters::getParametersList()[$parameter_name],
			'dateFrom' =>  date('Y-m-d', $dateFrom),
			'dateTo' => date('Y-m-d', $dateTo),
			'data' => $this->renderAjax('monitoring.history.php', [
				'server' => $server,
				'parameter' => $parameter_name,
				'limits' => $limits->all(),
				'logs'   => $logs->all(),
				'dateFrom' => $dateFrom,
				'dateTo' => $dateTo,
			])
		];
	}

	public function actionAddUserToServer()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$server_id = (int) Yii::$app->request->post('server');
		$user_id   = (int) Yii::$app->request->post('user');

		$server = SnifferServers::findOne($server_id);
		$user = SnifferUsers::findOne($user_id);

		if (is_null($server) || is_null($user)) {
			throw new ForbiddenHttpException();
		}

		$server->user_id = $user_id;
		$server->save();

		// Генерация параметров
		$parametres = $server->getParams();

		foreach ($parametres as $name) {

			$parameter = new SnifferMonitoringParameters([
				'server_id' => $server_id,
				'user_id' => $user_id,
				'parameter' => $name
			]);

			$parameter->setDefaultValues($server->$name);
			$parameter->save();
		}

		return [
			'status' => 'ok',
			'data' => Html::a(
				$user->name,
				'#',
				[
					'class' => 'edit-monitoring user-info',
					'data' => [
						'id' => $server->id
					],
				]
			)
		];
	}

	public function actionGetServerMonitoring()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$server_id = (int) Yii::$app->request->post('server');

		$server = SnifferServers::findOne($server_id);

		if (is_null($server) ) {
			throw new ForbiddenHttpException();
		}

		return [
			'status' => 'ok',
			'data' => $this->renderAjax('monitoing.server.user.form.php', [
				'server' => $server
			])
		];
	}

	public function actionSetParameter()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$parameter_id = (int) Yii::$app->request->post('parameter_id');
		$new_parameter = Yii::$app->request->post('parameter');
		$new_value = Yii::$app->request->post('value');

		$parameter = SnifferMonitoringParameters::findOne($parameter_id);

		if (is_null($parameter) ) {
			throw new ForbiddenHttpException();
		}

		$parameter->$new_parameter = $new_value;
		$parameter->save();

		return [
			'status' => 'error'
		];
	}

	public function actionChangeUser()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$server_id = (int) Yii::$app->request->post('server_id');
		$user_id = (int) Yii::$app->request->post('user_id');

		$server = SnifferServers::findOne($server_id);
		$user = SnifferUsers::findOne($user_id);

		if (is_null($server) || is_null($user)) {
			throw new ForbiddenHttpException();
		}

		$parameters = SnifferMonitoringParameters::findAll([
			'server_id' => $server_id,
			'user_id' => $server->user_id
		]);

		foreach ($parameters as $parameter) {
			$parameter->user_id = $user_id;
			$parameter->save();
		}

		$server->user_id = $user_id;
		$server->save();

		return [
			'status' => 'ok',
			'data' => Html::a(
				$user->name,
				'#',
				[
					'class' => 'edit-monitoring user-info',
					'data' => [
						/*'target' => '#user-settings-modal',
						'toggle' => 'modal',*/
						'id' => $server->id
					],
				]
			)
		];
	}

	public function actionDeleteParameter()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$parameter_id = (int) Yii::$app->request->post('parameter_id');

		$parameter = SnifferMonitoringParameters::findOne($parameter_id);

		if (is_null($parameter) ) {
			throw new ForbiddenHttpException();
		}

		$parameter->delete();

		return [
			'status' => 'ok'
		];
	}

	public function actionAddParameter()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$server_id = (int) Yii::$app->request->post('server_id');

		$server = SnifferServers::findOne($server_id);

		if (is_null($server) ) {
			throw new ForbiddenHttpException();
		}

		$monitoring = new SnifferMonitoringParameters([
			'user_id' => $server->user_id,
			'server_id' => $server_id
		]);

		$monitoring->load([
			'SnifferMonitoringParameters' => Yii::$app->request->post()
		]);

		if ($monitoring->save()) {
			return [
				'status' => 'ok',
				'data' => $this->renderAjax('monitoing.server.user.form.php', [
					'server' => $server
				])
			];
		}

		return [
			'status' => 'error'
		];
	}
	private function searchNotWorking()
	{

		$notWorking = [];
		$insta_ids = [];
		foreach (Yii::$app->params['workers'] as $ip) {
			$context = stream_context_create(['http' => ['timeout' => 5]]);
			$content = @file_get_contents('http://' . $ip . '/zengramManage.php?action=active-projects', false, $context);
			$data = json_decode($content, true);

			if (!is_array($data)) {
				$data = [];
				Yii::error('Ошибка при получении данных');
			}
			foreach ($data as $item) {
				$insta_ids[] = $item['insta_id'];
			}
		}

		$accounts = Account::find()->asArray()->where(['monitoring_status' => 'work'])->all();
		foreach ($accounts as $account) {
			/** @var Account $account */
			// Ищем отвалившиеся аккаунты
			if (in_array(strval($account['instagram_id']), $insta_ids)) {
				continue;
			}

			// Пишем в лог информацию, если процесс работы с аккаунтом отвалился
			//echo 'Аккаунт ' . $account->instagram_id . ' был перезапущен после сбоя' . PHP_EOL;
			$arrIndex = count($notWorking);
			$notWorking[$arrIndex]['login'] = $account['login'];
			$notWorking[$arrIndex]['table_id'] = $account['id'];
		}
		return $notWorking;
	}
}