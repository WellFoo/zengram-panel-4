<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Place;
use Yii;
use yii\db\Query;
use yii\web\ForbiddenHttpException;

class GeoPointsController extends Controller
{
	/**
	 * Initializes the object.
	 * This method is invoked at the end of the constructor after the object is initialized with the
	 * given configuration.
	 */
	public function init()
	{
		parent::init();

		if (!Yii::$app->user->identity->isAdmin) {
			throw new ForbiddenHttpException('У вас нет прав для просмотра этой страницы.');
		}
	}

	public function actionIndex($id = null)
	{
		if ($id !== null) {
			$place = Place::findOne($id);
			$type = Yii::$app->request->post('type', 0);
			$query = (new Query())
				->select('lat, lng')
				->where(['place_id' => $place->id]);

			if ($type == 0) {
				$query->from('const_workpoints');
			} else {
				$query->from('cover');
			}

			return $this->render('detail', [
				'place' => $place,
				'type'  => $type,
				'points' => $query->all()
			]);
		}

		$places = (new Query())
			->select([
				'p.id',
				'p.name',
				'COUNT(c.id) AS covers',
				'(SELECT COUNT(id) FROM const_workpoints WHERE place_id = p.id) AS points'
			])->from(['p' => 'place'])
			->innerJoin(['c' => 'cover'], 'c.place_id = p.id')
			->groupBy('p.id')
			->orderBy('p.name')
			->all();

		return $this->render('index', [
			'places' => $places
		]);
	}

	public function actionRemove()
	{
		$id = empty($_GET['id']) ? Yii::$app->request->post('id') : intval($_GET['id']);
		if (empty($id)) {
			return $this->redirect(['admin/geo-points']);
		}

		Yii::$app->db->createCommand()
			->delete('cover', ['place_id' => $id])
			->execute();
		Yii::$app->db->createCommand()
			->delete('const_workpoints', ['place_id' => $id])
			->execute();
		return $this->redirect(['admin/geo-points']);
	}

}