<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "hashtags_relations".
 *
 * @property integer $id
 * @property integer $place_id
 * @property string $hashtags_id
 * @property integer $counter
 */
class HashtagsRelations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hashtags_relations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['place_id'], 'required'],
            [['place_id', 'counter'], 'integer'],
            [['hashtags_ids'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'place_id' => 'Place ID',
            'hashtags_ids' => 'Hashtags ID',
            'counter' => 'Counter',
        ];
    }
}
