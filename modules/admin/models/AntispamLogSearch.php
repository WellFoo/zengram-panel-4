<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\AntispamLog;

/**
 * AntispamLogSearch represents the model behind the search form about `app\modules\admin\models\AntispamLog`.
 */
class AntispamLogSearch extends AntispamLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'account_id', 'commentator_id'], 'integer'],
            [['media_id', 'comment_id', 'text', /*'time_deleted',*/ 'commentator_login', 'media_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AntispamLog::find();


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'time_deleted' => $this->time_deleted,
            'user_id' => $this->user_id,
            'account_id' => $this->account_id,
            'commentator_id' => $this->commentator_id,
        ]);

        $query->andFilterWhere(['like', 'media_id', $this->media_id])
            ->andFilterWhere(['like', 'comment_id', $this->comment_id])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'commentator_login', $this->commentator_login])
            ->andFilterWhere(['like', 'media_code', $this->media_code]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'time_deleted' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }
}
