<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "popular_accounts".
 *
 * @property integer $id
 * @property integer $instagram_id
 * @property string  $username
 * @property string  $avatar
 * @property string  $full_name
 * @property string  $biography
 * @property string  $external_url
 * @property integer $follower_count
 * @property integer $media_count
 */
class PopularAccounts extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'popular_accounts';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['instagram_id', 'username', 'avatar', 'full_name', 'biography', 'external_url', 'follower_count', 'media_count'], 'required'],
			[['instagram_id', 'follower_count', 'media_count'], 'integer'],
			[['biography'], 'string'],
			[['username', 'avatar', 'full_name', 'external_url'], 'string', 'max' => 255],
			[['instagram_id'], 'unique']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'             => 'ID',
			'instagram_id'   => 'Instagram ID',
			'username'       => 'Логин',
			'avatar'         => 'Аватар',
			'full_name'      => 'Полное имя',
			'biography'      => 'Биография',
			'external_url'   => 'Внешняя ссылка',
			'follower_count' => 'Кол-во подписчика',
			'media_count'    => 'Кол-во медиа',
		];
	}
}
