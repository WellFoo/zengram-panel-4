<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "donor_descriptions".
 *
 * @property integer $id
 * @property integer $donor_id
 * @property string  $language
 * @property string  $text
 *
 * @property Donor $donor
 */
class DonorDescription extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'donor_descriptions';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['donor_id', 'language', 'text'], 'required'],
			[['donor_id'], 'integer'],
			[['language', 'text'], 'string']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'donor_id' => 'Donor ID',
			'language' => 'Language',
			'text' => 'Text',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDonor()
	{
		return $this->hasOne(Donor::className(), ['id' => 'donor_id']);
	}
}
