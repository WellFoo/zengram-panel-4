<?php namespace app\modules\management\controllers;

use app\components\PechkinApi;
use app\models\Users;
use app\models\UserTokens;
use app\modules\management\models\UniSenderList;
use app\modules\management\models\UsersSearch;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\QueryInterface;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use Yii;

class UsersController extends Controller
{
	public function actionIndex()
	{
		$searchModel = new UsersSearch();
		$users = $searchModel->search(Yii::$app->request->get());
		$users->query->with('lastAction');

		if (Yii::$app->request->isAjax) {
			return $this->exportUsers($users->query);
		}

		return $this->render('index', [
			'users' => $users,
			'searchModel' => $searchModel
		]);
	}

	/**
	 * @param QueryInterface $query
	 * @return string
	 * @throws BadRequestHttpException
	 * @throws Exception
	 */
	private function exportUsers($query)
	{
		set_time_limit(0);
		ob_implicit_flush(true);
		ob_end_flush();

		$new_list_title = Yii::$app->request->post('list_title', false);
		$complete = 0;
		/** @var ActiveQuery $query */
		$usersCount = $query->count(UsersSearch::tableName().'.id');

		$generateTokens = !!Yii::$app->request->post('tokens', false);

		switch(Yii::$app->request->post('service_type', false)){
			case 'pechkin':
				$api = new PechkinApi(Yii::$app->params['pechkin_login'], Yii::$app->params['pechkin_password']);
				$list_result = $api->lists_add($new_list_title);
				if (empty($list_result['list_id'])){
					throw new Exception('No list created');
				}
				$list_id = $list_result['list_id'];
				if ($generateTokens) {
					$api->lists_add_merge($list_id, 'text', ['req' => 'on', 'var' => 'ATOKEN', 'title' => 'accesskey']);
				}
				foreach ($query->each(20) as $user) {
					/** @var Users $user */
					$data = [];
					if ($generateTokens) {
						$token = new UserTokens();
						$token->generate($user->id);
						$token->save();
						//Имя
						$data['merge_1'] = '';
						//Фамилия
						$data['merge_2'] = '';
						$data['merge_3'] = $token->token;
					}
					try {
						$api->lists_add_member($list_id, $user->mail, $data);
					} catch (Exception $e){}
					echo json_encode([
						'count' => $usersCount,
						'complete' => ++$complete
					]);
				}
				break;
			case 'unisender':
				
				$list_id = Yii::$app->request->post('uni_list_id', false);
				if ($list_id) {
					$list = new UniSenderList(['id' => $list_id]);
				} elseif ($new_list_title) {
					$list = UniSenderList::create($new_list_title);
				} else {
					throw new BadRequestHttpException();
				}
				if ($generateTokens) {
					$list->addField('ACCESS_TOKEN');
				}

				foreach ($query->each() as $user) {
					/** @var Users $user */
					$data = [
						'email' => $user->mail,
						'email_request_ip' => $user->registerIP
					];
					if ($generateTokens) {
						$token = new UserTokens();
						$token->generate($user->id);
						$token->save();
						$data['ACCESS_TOKEN'] = $token->token;
					}
					$list->addUser($data);
					echo json_encode([
						'count' => $usersCount,
						'complete' => ++$complete
					]);
				}
				$list->importUsers();

				break;
			default:
				throw new BadRequestHttpException();
		}

		return json_encode([
			'count' => $usersCount,
			'complete' => $usersCount
		]);
	}
}