<?php
/**
 * @var \yii\web\View $this
 * @var array   $projects
 * @var string  $date_from
 * @var string  $date_to
 * @var integer $user
 */

use yii\helpers\Html;

$this->title = 'Статистика по комментированию';

?>

<h2><?= $this->title ?></h2>

<form class="form-horizontal refunds">
	<div class="row">
		<div class="col-xs-3">
			<div class="btn-group" role="group" aria-label="...">
				<button type="submit" name="date" value="day"
				        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
				</button>
				<button type="submit" name="date" value="week"
				        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
				</button>
				<button type="submit" name="date" value="month"
				        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
				</button>
			</div>

		</div>

		<div class="col-xs-5">
			<label class="control-label col-xs-3">Диапазон</label>

			<div class="input-group col-xs-9 col-xs-offset-1">
				<label for="from" class="input-group-addon">с</label>
				<input name="from" value="<?= $date_from ? date('Y-m-d', $date_from) : '' ?>" id="date-from"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">
				<label for="to" class="input-group-addon">по</label>
				<input name="to" value="<?= $date_to ? date('Y-m-d', $date_to) : '' ?>" id="date-to"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
			</div>
		</div>
		<div class="col-xs-4">
			<label class="control-label col-xs-5">Пользователь</label>

			<div class="input-group col-xs-7">

				<span class="input-group-btn">
					<?=Html::dropDownList(
						'user',
						$user,
						[
							0 => 'Не выбрано',

						] + $projects, [
						'class' => 'form-control user-filter'
					])
					?>
				</span>

			</div>
		</div>
	</div>
</form>


	<br>

<?= $this->render('index.int.php', [
	'date_from' => $date_from,
	'date_to'   => $date_to,
	'user'      => $user,
]) ?>