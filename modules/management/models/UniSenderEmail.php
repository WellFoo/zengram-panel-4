<?php

namespace app\modules\management\models;

use app\components\UniSenderApiTrait;
use yii\base\Model;

class UniSenderEmail extends Model
{
	use UniSenderApiTrait;

	public $id;
	public $list_id;
	public $from_name;
	public $from_address;
	public $subject;
	public $html_body;
	public $text_body;
	public $lang;

	public function rules()
	{
		return [
			[['list_id', 'from_name', 'from_address', 'subject', 'html_body'], 'required'],
			[['id', 'list_id'], 'integer'],
			[['from_name', 'subject', 'html_body', 'text_body', 'lang'], 'string'],
			['from_address', 'email'],
		];
	}

	public function save()
	{
		if (!$this->validate()) {
			return false;
		}

		$this->id = self::getApi()->createEmailMessage(
			$this->list_id,
			$this->from_name,
			$this->from_address,
			$this->subject,
			$this->html_body,
			$this->text_body
		);

		return true;
	}
}