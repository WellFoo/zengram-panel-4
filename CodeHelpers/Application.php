<?php  namespace yii\web;

/**
 * @property \yii\db\Connection $postdb
 * @property \Firstgoer\CurlComponent $curl
 * @property \yii\caching\Cache $cache_mongo
 */
class Application extends \yii\base\Application
{
	public function handleRequest($request) {}
}