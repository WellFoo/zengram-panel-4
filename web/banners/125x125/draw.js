(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 125,
	height: 125,
	fps: 30,
	color: "#FFFFFF",
	manifest: [
		{src:"/banners/125x125/logo.png", id:"logo"}
	]
};



// symbols:



(lib.logo = function() {
	this.initialize(img.logo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,150,35);


(lib.Symbol29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#92FFDD").s().p("AgPBjQgEAAgDgDQgCgCAAgEIAAgSIgBihIAAgCQABgCACgCQADgDADAAIAqAAIAADFg");
	this.shape.setTransform(-129.7,120.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#86E4C2").s().p("AgGBjIAAjFIAAAAIANAAIAADFg");
	this.shape_1.setTransform(-126.3,120.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFB095").s().p("AgpBuIgCAAQgEgBgDgCQgEgDgCgEIAAgBIAAAAIBRABQAEgBAEgDQAEgEAAgEIAAgLQAAgEgEgEQgEgDgEgBIhRAAIgCgDIAAgBIAAAAIgBgBIgDgBIgBgBIgCAAIgCgBIgBgEIABgDIB1AAQAHAAAEgEQAEgEAAgGIAAgPQAAgFgEgFQgEgEgHAAIh1AAIAAgDIgBgDIAAAAIgEgBQgBAAAAAAQgBAAgBAAQAAAAAAAAQgBAAAAAAIgEgEIAAgBICCAAQAHAAAEgEQAEgFAAgGIAAgKQAAgGgEgFQgEgDgHAAIiCAAIABgEIAAgBQACgEAFAAIABAAIADgBIABgEIAAgDIBtgBQAGAAADgDQAEgEAAgFIgBghIAGAAQAHgBAGgDIABAAIABAoQgBAFgDAFQgEAEAAABQAAACAHADQAHAFAAAHIAAAXQAAAGgEAEQgDAEgFACQgBAAAAAAQAAAAgBAAQAAAAAAAAQAAAAAAAAQAAAAAAAAQAAABABAAQAAAAAAAAQABABABAAQAEACAEAFQADAEAAAGIAAAcQAAAGgFAGQgFAFgHAAQgEAAgDACQgCADAAAFIAAAVQAAAIgFAEQgEAFgHAAg");
	this.shape_2.setTransform(-158.8,115);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFD2B1").s().p("AgiDbIgFgBIhegfIBSACQAHAAAEgGQAFgEAAgHIAAgWQAAgEACgDQADgDAEAAQAHAAAFgEQAFgGAAgGIAAgdQAAgFgDgFQgEgFgEgBQgBgBgBAAQAAAAAAgBQgBAAAAAAQAAgBAAAAQAAAAAAgBQAAAAAAAAQABAAAAgBQAAAAABAAQAFgCADgDQAEgFAAgGIAAgWQAAgIgHgFQgHgDAAgBQAAgCAEgEQADgFABgFIgBglIAEgEQAEgDACgEQACgFgCgEIgZhmQgCgHACgFIARgmQADgGAFgDQAFgEAEAAIAEAAQAJABAGAFQAGAGAAAJIAAAeQAAAFACAEIA+B0IAaBPQABADADACQACACADAAIAtAAIAAACIABCjIgmgBIgGABIgbAKIgFABgAiSC0IgCgEQACAFAEACQgCAAgCgDgAiUCvIAAgBIAAggIBTAAQAEAAAEADQAEAEAAAFIAAAKQAAAFgEAEQgEADgEAAgAiWCJIAAAAIAAABIAAgBgAifB/IAAgsIB3AAQAHAAAEAFQAEAEAAAGIAAAOQAAAHgEAEQgEAEgHAAgAisBGIAAgEIAAgkICEAAQAHAAAEAEQAEAEAAAHIAAAKQAAAGgEAEQgEAFgHAAgAifgPQAAgGAEgFQADgDAEgBIAEgBIBsAAIABAgQAAAFgEAEQgDACgGAAIhvABg");
	this.shape_3.setTransform(-149.6,107.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#517786").s().p("AjBB/QgHgHAAgJIAAjdQAAgJAEgGQAFgHAJAAIF/AAIAAEIIl7ABQgJAAgGgGg");
	this.shape_4.setTransform(-105.3,121.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-166.9,85.4,81.8,49.3);


(lib.Symbol28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAVApIAAgiIgPAAIgYAiIgVAAIAcgkQgIgCgEgDQgDgCgCgFQgBgFAAgFQAAgLAHgGQAHgGANAAIAqAAIAABRgAgIgXQgDACAAAGQAAAFADADQADADAFAAIAVAAIAAgWIgVAAQgFAAgDADg");
	this.shape.setTransform(-43.3,157.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AASApIAAg6IgBAAIgdA6IgXAAIAAhRIATAAIAAA6IAAAAIAdg6IAXAAIAABRg");
	this.shape_1.setTransform(-51.8,157.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgIApIAAhCIgbAAIAAgPIBHAAIAAAPIgbAAIAABCg");
	this.shape_2.setTransform(-60.1,157.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgTAmQgJgGgFgKQgFgJAAgMQAAgMAFgKQAFgKAJgGQAKgFAKAAQAQAAAKAIQAJAIACAOIgSAAQgCgHgEgFQgFgEgHAAQgJAAgGAIQgGAHAAANQAAAOAGAHQAFAHAJABQAIgBAFgEQAFgFACgJIASAAQgCAPgKAJQgKAJgQAAQgLAAgJgFg");
	this.shape_3.setTransform(-68.2,157.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUApQgEgDgBgFQgFAFgIADQgFACgJAAQgNAAgHgGQgHgHgBgLQABgIADgGQADgFAGAAIAMgEIANgBIAHgCIAGgBIADgDIABgCIAAgDQAAgHgEgCQgFgEgGAAQgJAAgEAEQgFADgBAIIgRAAQACgPAJgGQAKgHAPAAQAIAAAIACQAHADAFAFQAEAGAAAJIAAAnIABAGQAAAAABAAQAAAAAAABQABAAAAAAQABAAAAAAIACAAIADAAIAAAMIgHABIgFABQgGAAgDgCgAAFACIgIACIgIACQgEABgDADQgDADAAAFQAAAGAEADQAEADAHAAQAGAAADgBQAFgCADgEQADgDAAgDIABgHIAAgKQgFACgFAAg");
	this.shape_4.setTransform(-77,157.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AARApIAAgjIgIACIgJABIgIABQgNAAgGgGQgHgFAAgMIAAgbIASAAIAAAVQAAAIADAEQAFAEAHAAIAFgBIAHgCIAGgBIAAghIASAAIAABRg");
	this.shape_5.setTransform(-86,157.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgYA5IgIgBIAAgQIAHABIADAAQAFAAADgDQACgDABgEIADgIIgghPIAUAAIAVA7IAUg7IATAAIgeBPIgGAQQgEAIgDAEQgGAGgKAAIgFAAg");
	this.shape_6.setTransform(-94.3,159.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_7.setTransform(-107.7,157.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgbApIAAhRIA3AAIAAAPIglAAIAABCg");
	this.shape_8.setTransform(-115.4,157.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgTAmQgKgGgEgKQgFgJAAgNQAAgLAFgKQAFgKAKgFQAJgGAJAAQAMAAAJAFQAJAGAEAKQAFALAAAMIAAACIg7AAQAAAMAGAHQAHAHAIAAQAHAAAFgEQAFgDACgIIASAAQgCAJgFAGQgGAHgIADQgHADgKAAQgKAAgJgFgAAVgGQgBgLgFgGQgGgFgJgBQgHAAgFAGQgGAGgBALIAoAAIAAAAg");
	this.shape_9.setTransform(-123.6,157.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("Ag2ApIAAhRIATAAIAABCIAcAAIAAhCIAQAAIAABCIAcAAIAAhCIASAAIAABRg");
	this.shape_10.setTransform(-134.6,157.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAUApQgEgDgBgFQgFAFgIADQgFACgJAAQgNAAgHgGQgHgHgBgLQABgIADgGQADgFAGAAIAMgEIANgBIAHgCIAGgBIADgDIABgCIAAgDQAAgHgEgCQgFgEgGAAQgJAAgEAEQgFADgBAIIgRAAQACgPAJgGQAKgHAPAAQAIAAAIACQAHADAFAFQAEAGAAAJIAAAnIABAGQABAAAAAAQAAAAABABQAAAAAAAAQABAAAAAAIACAAIADAAIAAAMIgHABIgFABQgGAAgDgCgAAFACIgIACIgIACQgEABgDADQgDADAAAFQAAAGAEADQAEADAHAAQAGAAADgBQAFgCADgEQADgDAAgDIABgHIAAgKQgFACgFAAg");
	this.shape_11.setTransform(-145.4,157.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AguA5IAAhxIAzAAQAMAAAIAFQAIADAEAHQAEAGAAAIQAAAJgEAGQgFAGgHADQAKADAGAEQAFAIABALQAAAKgFAHQgFAIgHAEQgJAFgKAAgAgaAoIAcAAQAHAAAFgBQAGgBADgFQAEgDgBgIQABgIgEgDQgEgEgFgCIgMgBIgcAAgAgagIIAaAAQAKAAAGgDQAFgEABgIQAAgIgEgDQgDgDgEgCQgGgBgFABIgaAAg");
	this.shape_12.setTransform(-155.4,156.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAdApIAAg1IgUA1IgRAAIgTg1IgBAAIAAA1IgRAAIAAhRIAYAAIAVBAIAAAAIAWhAIAYAAIAABRg");
	this.shape_13.setTransform(-97.3,137.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgYA5IgHgBIAAgQIAGABIADAAQAFAAADgDQACgDABgEIADgIIgghPIAUAAIAVA7IAUg7IATAAIgeBPIgGAQQgEAIgDAEQgGAGgKAAIgFAAg");
	this.shape_14.setTransform(-107,139.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAdApIAAg1IgUA1IgRAAIgTg1IgBAAIAAA1IgRAAIAAhRIAYAAIAVBAIAAAAIAWhAIAYAAIAABRg");
	this.shape_15.setTransform(-116.8,137.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AASApIAAg6IgBAAIgdA6IgXAAIAAhRIATAAIAAA6IAAAAIAdg6IAXAAIAABRg");
	this.shape_16.setTransform(-127.1,137.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AASApIAAgkIgiAAIAAAkIgTAAIAAhRIATAAIAAAhIAiAAIAAghIASAAIAABRg");
	this.shape_17.setTransform(-136.3,137.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AASApIAAg6IgBAAIgdA6IgXAAIAAhRIATAAIAAA6IAAAAIAdg6IAXAAIAABRg");
	this.shape_18.setTransform(-145.5,137.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAdApIAAg1IgUA1IgRAAIgTg1IgBAAIAAA1IgRAAIAAhRIAYAAIAVBAIAAAAIAWhAIAYAAIAABRg");
	this.shape_19.setTransform(-155.7,137.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgWAIIAAgPIAtAAIAAAPg");
	this.shape_20.setTransform(-48.9,117.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgTAmQgKgGgEgKQgFgJAAgNQAAgLAFgKQAFgKAKgFQAJgGAJAAQAMAAAJAFQAIAGAGAKQAEALAAAMIAAACIg7AAQABAMAFAHQAHAHAIAAQAIAAAEgEQAFgDACgIIASAAQgCAJgGAGQgFAHgHADQgIADgKAAQgKAAgJgFgAAVgGQgBgLgFgGQgGgFgJgBQgHAAgGAGQgFAGgBALIAoAAIAAAAg");
	this.shape_21.setTransform(-60.8,118.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AASApIAAg6IgBAAIgdA6IgXAAIAAhRIATAAIAAA6IAAAAIAdg6IAXAAIAABRg");
	this.shape_22.setTransform(-70,118.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AASApIAAgkIgiAAIAAAkIgTAAIAAhRIATAAIAAAhIAiAAIAAghIASAAIAABRg");
	this.shape_23.setTransform(-79.2,118.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUAmQgIgGgFgKQgFgJAAgNQAAgLAFgKQAGgKAIgFQAJgGAKAAQAMAAAJAFQAJAGAEAKQAFALAAAMIAAACIg7AAQAAAMAHAHQAFAHAJAAQAHAAAFgEQAFgDADgIIARAAQgCAJgGAGQgEAHgIADQgIADgJAAQgLAAgKgFgAAVgGQgBgLgGgGQgFgFgJgBQgHAAgGAGQgFAGgCALIApAAIAAAAg");
	this.shape_24.setTransform(-88.1,118.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AApApIgZgkIgIAJIAAAbIgPAAIAAgbIgIgJIgZAkIgWAAIAjguIgfgjIAWAAIAcAjIABAAIAAgjIAPAAIAAAjIABAAIAcgjIAWAAIgfAjIAjAug");
	this.shape_25.setTransform(-98.9,118.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AASApIAAg6IgBAAIgdA6IgXAAIAAhRIATAAIAAA6IAAAAIAdg6IAXAAIAABRg");
	this.shape_26.setTransform(-109.7,118.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgjApIAAhRIArAAQAMAAAGAFQAHAGgBAJQAAAHgDAEQgCAEgGADIAAAAQAHACAEAEQAFAFAAAIQAAAMgJAGQgIAGgOAAgAgRAbIAXAAQAGAAADgDQADgDABgFQgBgGgDgDQgDgDgGAAIgXAAgAgRgHIAVAAQAFAAADgDQACgCAAgFQAAgFgCgCQgDgCgGAAIgUAAg");
	this.shape_27.setTransform(-118.5,118.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAdA2IAAgaIg5AAIAAAaIgRAAIAAgpIAKAAIAFgOQADgIABgLQABgKAAgOIAAgJIA8AAIAABCIALAAIAAApgAgHghIgCATIgDAPIgEAMIAhAAIAAg0IgYAAg");
	this.shape_28.setTransform(-128,119.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_29.setTransform(-137.6,118.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgnA5IAAhvIARAAIAAALQAEgHAHgDQAHgDAHAAQALAAAJAFQAIAGAFAKQAEAJAAAOQAAAMgEAJQgFAJgIAFQgIAGgLAAQgHAAgHgDQgGgDgFgHIAAApgAgQgiQgGAIAAAOQAAAMAGAHQAGAHAKAAQAKAAAGgHQAGgHAAgMQAAgOgGgIQgGgHgKAAQgKAAgGAHg");
	this.shape_30.setTransform(-147,119.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AASApIAAhCIgiAAIAABCIgTAAIAAhRIBHAAIAABRg");
	this.shape_31.setTransform(-156.7,118.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgUAmQgIgGgFgKQgFgJAAgNQAAgLAFgKQAGgKAIgFQAJgGAKAAQAMAAAJAFQAJAGAEAKQAFALAAAMIAAACIg7AAQAAAMAHAHQAFAHAJAAQAHAAAFgEQAFgDADgIIARAAQgCAJgGAGQgEAHgIADQgIADgJAAQgLAAgKgFgAAVgGQgBgLgGgGQgFgFgJgBQgHAAgGAGQgFAGgCALIApAAIAAAAg");
	this.shape_32.setTransform(-39.1,98.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_33.setTransform(-48.4,98.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AAQApIgYglIgKALIAAAaIgTAAIAAhRIATAAIAAAlIAeglIAXAAIghAhIAkAwg");
	this.shape_34.setTransform(-56.9,98.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgTAmQgJgGgFgKQgFgJAAgMQAAgMAFgKQAFgKAJgGQAKgFAKAAQAQAAAKAIQAJAIACAOIgSAAQgCgHgEgFQgFgEgHAAQgJAAgGAIQgGAHAAANQAAAOAGAHQAFAHAJABQAIgBAFgEQAFgFACgJIASAAQgCAPgKAJQgKAJgQAAQgLAAgJgFg");
	this.shape_35.setTransform(-66.1,98.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgUAmQgIgGgFgKQgFgJAAgNQAAgLAFgKQAGgKAIgFQAJgGAKAAQAMAAAJAFQAIAGAGAKQAEALAAAMIAAACIg7AAQABAMAFAHQAHAHAIAAQAHAAAFgEQAFgDACgIIASAAQgCAJgGAGQgFAHgHADQgIADgJAAQgLAAgKgFgAAVgGQgBgLgGgGQgFgFgJgBQgHAAgGAGQgFAGgCALIApAAIAAAAg");
	this.shape_36.setTransform(-75,98.3);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AARApIAAgjIgIACIgJABIgIABQgMAAgIgGQgGgFAAgMIAAgbIASAAIAAAVQAAAIAEAEQAEAEAHAAIAFgBIAHgCIAGgBIAAghIASAAIAABRg");
	this.shape_37.setTransform(-83.9,98.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AASApIAAg6IgBAAIgdA6IgXAAIAAhRIATAAIAAA6IAAAAIAdg6IAXAAIAABRg");
	this.shape_38.setTransform(-92.7,98.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgIApIAAhCIgbAAIAAgPIBHAAIAAAPIgbAAIAABCg");
	this.shape_39.setTransform(-101,98.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAUApQgEgDgBgFQgFAFgIADQgFACgJAAQgNAAgHgGQgHgHgBgLQABgIADgGQADgFAGAAIAMgEIANgBIAHgCIAGgBIADgDIABgCIAAgDQAAgHgEgCQgFgEgGAAQgJAAgEAEQgFADgBAIIgRAAQACgPAJgGQAKgHAPAAQAIAAAIACQAHADAFAFQAEAGAAAJIAAAnIABAGQABAAAAAAQAAAAABABQAAAAAAAAQABAAAAAAIACAAIADAAIAAAMIgHABIgFABQgGAAgDgCgAAFACIgIACIgIACQgEABgDADQgDADAAAFQAAAGAEADQAEADAHAAQAGAAADgBQAFgCADgEQADgDAAgDIABgHIAAgKQgFACgFAAg");
	this.shape_40.setTransform(-109,98.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AAdApIAAg1IgUA1IgRAAIgTg1IgBAAIAAA1IgRAAIAAhRIAYAAIAVBAIAAAAIAWhAIAYAAIAABRg");
	this.shape_41.setTransform(-119.2,98.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_42.setTransform(-129.6,98.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgIApIAAhCIgbAAIAAgPIBHAAIAAAPIgbAAIAABCg");
	this.shape_43.setTransform(-138,98.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgjApIAAhRIArAAQAMAAAGAFQAHAGgBAJQAAAHgDAEQgCAEgGADIAAAAQAHACAEAEQAFAFAAAIQAAAMgJAGQgIAGgOAAgAgRAbIAXAAQAGAAADgDQADgDABgFQgBgGgDgDQgDgDgGAAIgXAAgAgRgHIAVAAQAFAAADgDQACgCAAgFQAAgFgCgCQgDgCgGAAIgUAAg");
	this.shape_44.setTransform(-146,98.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AAhA5IgLgfIgrAAIgMAfIgUAAIArhxIAUAAIAsBxgAAQALIgQguIgRAuIAhAAg");
	this.shape_45.setTransform(-155.9,96.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-163.3,86,135.3,81.2);


(lib.Symbol27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1C1C1B").s().p("AgKAzIAAgGIgGAAIAAgGIAGAAIAAAGIAoAAIAAAGgAAeAtIAAgGIASAAIAAAGgAAxAnIAAgGIALAAIAAgvIgMAAIAAgGIATAAIAAA7gAgXAnIAAgMIgGAAIAAgSIAYAAIAAAGIgSAAIAAAMIAXAAIAAAGIgRAAIAAAGgAgjAJIAAgLIgZAAIAAgGIgGAAIAAgMIAGAAIAAAMIA3AAIAAAGIgYAAIAAALgAAkgUIAAgGIgMAAIAAgGIAMAAIAAAGIAMAAIAAAGgAg8gUIAAgGIA4AAIAAgGIAEAAIAAAGIASAAIAAAGgAAMggIAAgGIgGAAIAAgGIAGAAIAAAGIAMAAIAAAGgAgKggIAAgSIAQAAIAAAGIgKAAIAAAMg");
	this.shape.setTransform(18.2,23.6,2.422,2.422,90);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1 copy
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhdCiIAAgrIABAAIgDgBIgOAAIAAgtIgPAAIAAhkIAPAAIAAgOIAOAAIACgCIAAgOIAbAAIAAgOIAtAAIAAgQIAdAAIAAg8IAOAAIAAgOIAdAAIAAANIAQAAIAACIIANAAIAAgPIAtAAIAAAqIgQAAIAAAPIgPAAIAAAcIgOAAIAAAeIgNAAIAAAdIgPAAIgBABIAAAsg");
	this.shape_1.setTransform(18.2,23.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(5.7,7.3,25,32.6);


(lib.Symbol23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2382AC").s().p("AhkCNIAAkYIBGAAQA/gBAiAlQAiAkAABDQAABCgiAlQgiAmg9AAgAhABvIAfAAQAvAAAageQAYgcAAg1QAAg1gYgcQgZgdgwAAIgfAAg");
	this.shape.setTransform(1.6,22.2,0.364,0.364);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2382AC").s().p("AgbBnQAggQAAgeIgEAAQgHAAgHgGQgGgFAAgLQAAgJAHgGQAGgHAIABQAMgBAHALQAHAJAAARQAAAYgNATQgNARgVALgAgOhPQgGgHAAgKQAAgKAGgHQAHgGAHAAQAKAAAGAGQAHAHAAAKQAAAKgHAHQgGAGgKAAQgHAAgHgGg");
	this.shape_1.setTransform(-5.2,22.9,0.364,0.364);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ah/EpQh7g1gxh9Qgyh7A1h7QA1h7B9gyQB6gxB7A1QB8A1AxB9QAyB6g1B8Qg1B7h9AxQg8AYg7AAQg/AAhAgbg");
	this.shape_2.setTransform(-0.7,22.1,0.364,0.364);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E7E7E7").s().p("AhhiiIEKDnIlRBeg");
	this.shape_3.setTransform(10,23.9,0.364,0.364);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FDE4D3").s().p("Ah7A4QgXgBgRgQQgQgRAAgWQAAgWAQgQQARgQAXAAID3AAQAXAAARAQQAQAQAAAWQAAAWgQARQgRAQgXABg");
	this.shape_4.setTransform(65.7,77.8,0.24,0.24);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FDE4D3").s().p("Ah7A4QgXgBgRgQQgQgRAAgWQAAgWAQgQQARgQAXAAID3AAQAXAAARAQQAQAQAAAWQAAAWgQARQgRAQgXABg");
	this.shape_5.setTransform(13.4,77.8,0.24,0.24);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#ACEEEB").s().p("AiyAwIAAhgIFlAAIAABgg");
	this.shape_6.setTransform(52.6,74.6,0.24,0.24);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FEFFFF").p("AwoIxIAAxhMAhRAAAIAARhg");
	this.shape_7.setTransform(39.2,64.6,0.24,0.24);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#ACEEEB").s().p("AhgATIAAglIDBAAIAAAlg");
	this.shape_8.setTransform(34.8,61.9,0.24,0.24);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#ACEEEB").s().p("AiHAbIAAg1IEOAAIAAA1g");
	this.shape_9.setTransform(24.4,61.9,0.24,0.24);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#ACEEEB").s().p("AhgATIAAglIDBAAIAAAlg");
	this.shape_10.setTransform(54.7,61.9,0.24,0.24);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#ACEEEB").s().p("AhhATIAAglIDDAAIAAAlg");
	this.shape_11.setTransform(44.3,61.9,0.24,0.24);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#ACEEEB").s().p("AiHCIIAAkOIEPAAIAAEOg");
	this.shape_12.setTransform(34.8,66.7,0.24,0.24);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#ACEEEB").s().p("AiHCIIAAkOIEOAAIAAEOg");
	this.shape_13.setTransform(24.4,66.7,0.24,0.24);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#ACEEEB").s().p("AiGCIIAAkOIEOAAIAAEOg");
	this.shape_14.setTransform(54.8,66.7,0.24,0.24);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#ACEEEB").s().p("AiGCIIAAkOIENAAIAAEOg");
	this.shape_15.setTransform(44.3,66.7,0.24,0.24);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#6EC5C5").s().p("AtvCMIAAkXIbfAAIAAEXg");
	this.shape_16.setTransform(39.3,74.6,0.24,0.24);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#6EC5C5").s().p("AifAuIAAhbIE/AAIAABbg");
	this.shape_17.setTransform(39.6,58,0.24,0.24);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#2382AC").s().p("AhbIxIAAxhIC3AAIAARhg");
	this.shape_18.setTransform(62.7,64.6,0.24,0.24);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#2382AC").s().p("AhcIxIAAxhIC5AAIAARhg");
	this.shape_19.setTransform(15.9,64.6,0.24,0.24);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#6EC5C5").s().p("AhbBCIAAiDIC3AAIAACDg");
	this.shape_20.setTransform(58.4,53.6,0.24,0.24);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#ACEEEB").s().p("AtXBCIAAiDIavAAIAACDg");
	this.shape_21.setTransform(35.7,53.6,0.24,0.24);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CECECE").s().p("AxjA7QgyAAgigjQgkghAAgxMAm3AAAQAAAxgkAhQgiAjgyAAg");
	this.shape_22.setTransform(38.9,83.6,0.24,0.24);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#ADADAD").s().p("AzbAkIAAhHMAm3AAAIAABHg");
	this.shape_23.setTransform(38.9,81.3,0.24,0.24);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FEFFFF").s().p("AwoIxIAAxhMAhRAAAIAARhg");
	this.shape_24.setTransform(39.2,64.6,0.24,0.24);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#CECECE").s().p("AxAKTQgfAAgVgVQgXgWAAgfIAAyRQAAgfAXgWQAVgVAfAAMAiCAAAQAeAAAWAVQAVAWABAfIAASRQgBAfgVAWQgWAVgeAAg");
	this.shape_25.setTransform(39.2,64.6,0.24,0.24);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FDE4D3").s().p("Ai3C4QhNhMAAhsQAAhrBNhMQBMhNBrAAQBsAABMBNQBNBMAABrQAABshNBMQhMBNhsAAQhrAAhMhNg");
	this.shape_26.setTransform(67.2,80.4,0.24,0.24);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FDE4D3").s().p("Ai3C4QhNhMAAhsQAAhrBNhMQBMhNBrAAQBsAABMBNQBNBMAABrQAABshNBMQhMBNhsAAQhrAAhMhNg");
	this.shape_27.setTransform(11.9,80.4,0.24,0.24);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AiCAoQg3gwAAgvQAAglA3AAQAhgBBhAHQAzAABQgGQA3AAAAAlQAAAvg3AwQg7A1hIAAQhHAAg7g1g");
	this.shape_28.setTransform(39.4,34.9,0.24,0.24);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#D45954").s().p("AgTFGQi6gPiYhcQiehghJiUQAdhwBThRQBXhVB1gUQB1gUChBBQCfBBBKBcQAQAUAbAoQAbApAPATQA3BEBCAPQBvAcAhghQhdCBilBDQiJA4iWAAQggAAgfgDg");
	this.shape_29.setTransform(48,7.9,0.24,0.24);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#D45954").s().p("AgPCdQgOhCghhtQgjhSgRgvQgfhXAYgvQAfg9A+AUQAxAQArA3QAsA4AQBMQAPBGgMBIQgMBPgqBTQgiBDg3BMQAZgmgYiFg");
	this.shape_30.setTransform(53.6,18.8,0.24,0.24);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#D45954").s().p("AmyCMQAGjJBjipQA2heBMhDQBShJBfgfQBjgiBoAVQBtAVBHBIQAPAQATAEQAcAGAMAGQgOBShFBHQg6A7haAtQgZANiMA7QhjAqg6AmQh9BWhACAQhCCGAVCPQhYivAGjKg");
	this.shape_31.setTransform(33.1,15,0.24,0.24);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FDE4D3").s().p("AhcBdQgngmAAg3QAAg1AngnQAngnA1AAQA2AAAnAnQAnAnAAA1QAAA2gnAnQgnAng2AAQg1AAgngng");
	this.shape_32.setTransform(54.2,26.6,0.24,0.24);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FDE4D3").s().p("AhcBdQgngnAAg2QAAg1AngnQAngnA1AAQA2AAAnAnQAnAnAAA1QAAA3gnAmQgmAng3AAQg1AAgngng");
	this.shape_33.setTransform(24.6,26.6,0.24,0.24);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgYAZQgKgLAAgOQAAgNAKgLQALgKANAAQAOAAALAKQAKALAAANQAAAOgKALQgLAKgOAAQgNAAgLgKg");
	this.shape_34.setTransform(46.6,23.7,0.24,0.24);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#3D3D3D").s().p("AgPAlQgPgQAQgjIgPgCQAhAAAZgXQAFAQgUAiQgOAdgJAAQgDAAgDgDg");
	this.shape_35.setTransform(48.1,23.6,0.24,0.24);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#D45954").s().p("AAfgcQhFgdh0AqQAfgqAtgQQAtgQArAOQAuAPAnArQAmApAVA7QhGhZg1gWg");
	this.shape_36.setTransform(47.7,18.6,0.24,0.24);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#3D3D3D").s().p("AgNALQgUgiAFgQQAZAXAhAAIgQACQARAkgQAPQgCADgDAAQgKAAgNgdg");
	this.shape_37.setTransform(30.6,23.6,0.24,0.24);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#D45954").s().p("AhdgRQAmgrAugPQArgOAtAQQAsAQAgAqQh0gqhEAdQg2AWhHBZQAWg7Angpg");
	this.shape_38.setTransform(31,18.6,0.24,0.24);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FCD3B7").s().p("AhKBLQgfgfAAgsQAAgqAfggQAfgfArAAQAsAAAfAfQAfAgAAAqQAAAsgfAfQggAfgrAAQgrAAgfgfg");
	this.shape_39.setTransform(49.3,29.8,0.24,0.24);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FCD3B7").s().p("AhKBLQgfgfAAgsQAAgqAfggQAggfAqAAQAsAAAfAfQAfAgAAAqQAAAsgfAfQgfAfgsAAQgqAAgggfg");
	this.shape_40.setTransform(29.6,29.8,0.24,0.24);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#3D3D3D").s().p("AgzBFQgWgdAAgoQAAgnAWgdQAWgdAdAAQAeAAAWAdQAWAdAAAnQAAAogWAdQgWAdgeAAQgdAAgWgdg");
	this.shape_41.setTransform(46.2,24.6,0.24,0.24);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#3D3D3D").s().p("AAoAcQgSgPgWAAQgUAAgTAPQgSAQgJAaQgHgUAAgWQAAgnAWgcQAWgdAdgBQAfABAVAdQAWAcAAAnQAAAWgHAUQgJgagSgQg");
	this.shape_42.setTransform(32.5,23.9,0.24,0.24);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FDE4D3").s().p("AmFKbQgfgXgcgaQhFhCgbhfQhYkqgIijQgNkeBajBQCXlFGcAQQGdgQCXFFQBaDBgNEeQgICjhYEqQgbBfhFBCQgcAagfAXQiuB/jYAAQjXAAiuh/g");
	this.shape_43.setTransform(39.4,23,0.24,0.24);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FCC6A4").s().p("AjXBqQADiYgChiQAAgIAGAAIGhAAQAGAAAAAIQgCBjADCXQhpAvhvgBQhuABhpgvg");
	this.shape_44.setTransform(39.4,42.2,0.24,0.24);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FDE4D3").s().p("AraD/IGyiHQAZgIARgWQAOgVAFgcQALhCADijQAChdgBhaQAAgGAGAAIGtAAQAHAAgBAGIABC5QADCkALA/QAFAcAPAVQAQAWAZAIIGyCHQlcB7l/AAQl+AAlch7g");
	this.shape_45.setTransform(39.4,46.7,0.24,0.24);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#ACEEEB").s().p("AABI/IoCgCQoFgDAAgDQACgIgHg5QgIhAACgvQAIijB7haQA1gnBKgXIHpiXQAZgIARgWQAOgUAFgdQALhEADijQAChdgBhaQAAgGAGAAIGtAAQAHAAgBAGIABC6QADCkALBAQAFAdAPAUQAQAWAZAIIHoCWQBSAaA+AyQBzBbgECcQgBAsgMA/QgKA1ABAJIACAGQAAAAAAABQAAAAAAAAQAAABgBAAQAAAAgBAAg");
	this.shape_46.setTransform(39.4,51.4,0.24,0.24);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FDE4D3").s().p("AwDIuQgBAAAAAAQgBAAAAAAQAAgBAAAAQAAAAAAgBIACgFQABgFgCg6QgCg9AHgvQAWieB5hYQAzgmBHgWIHbiTQAZgHAPgVQAPgVAEgbQALhCADieQAChegBhUQAAgBAAAAQAAgBAAAAQAAgBABAAQAAgBABAAQAAAAABgBQAAAAABAAQAAgBABAAQAAAAABAAIGgAAQABAAAAAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQgBBZACBbQADCfALA/QAFAcAOAUQAPAVAZAHIHaCTQBOAXA+AyQBvBYALCXQADAtgGA7QgFA2ACAHQAAAEv7ADg");
	this.shape_47.setTransform(39.3,51.4,0.24,0.24);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#D45954").s().p("AqANoIAA7PIUBAAIAAbPg");
	this.shape_48.setTransform(39.4,36,0.24,0.24);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.6,0,86.1,86.7);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D9B293").s().p("Ag+gVQAbAXAjAAQAiAAAcgXQgRAsgtAAQgrgBgTgrg");
	this.shape.setTransform(85.4,86.9,0.94,0.94);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9F6D5C").s().p("ACzARQgUgRgZgDQgYgCgXAJQgNAGgDgHQgDgEANgJQAVgPAhAHQAfAHAUAUQAIAKgEACIgCAAQgEAAgFgEgAi9AVQgEgCAIgKQAUgUAfgHQAhgHAVAPQANAJgEAEQgDAHgMgGQgXgJgXACQgaADgUARQgFAEgEAAIgCAAg");
	this.shape_1.setTransform(84.1,63.6,0.94,0.94);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3D494E").s().p("ABwAoQgJgRAAgXQAAgWAJgRQAIgRANAAQANAAAKARQAIARABAWQgBAXgIARQgKARgNAAQgNAAgIgRgAiaAoQgKgRABgXQgBgWAKgRQAJgRAMAAQANAAAKARQAIARABAWQgBAXgIARQgKARgNAAQgMAAgJgRg");
	this.shape_2.setTransform(85.4,73.4,0.94,0.94);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#A0B6C1").s().p("AkKDsIAAgBIAwhaIgeksIAUgTIA5BIIA8iFIB5AWQB/AaAVAXQAUAUApCmIAMA0IAkCigAiWCFICcAAIAAgkIicAAg");
	this.shape_3.setTransform(111.7,146.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#2DA192").s().p("AguCLIAdksIgTgTIAkgwIAlAwIgTATIAdEsIgvBag");
	this.shape_4.setTransform(85,147);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#778E9A").s().p("ABzARIAAghICfAAIAAAhgAkRARIAAghICfAAIAAAhg");
	this.shape_5.setTransform(85,157.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BACBD3").s().p("AkKDsIAkiiIAMg0QApimATgUQAWgXB/gaQA+gNA7gJIA8CFIA5hIIAUATIgeEsIAwBaIAAABgAgFCFICcAAIAAgkIicAAg");
	this.shape_6.setTransform(58.3,146.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#D19E84").s().p("Ah7AfIAAhvQBQAkArAAQAsAABQgkIAABvIh8Ayg");
	this.shape_7.setTransform(85,115.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAlCNIglgvIB8gzIACgBIAfAgIgDAGIg8CFgAiZBQIgDgGIAfggIACABIB7AzIgkAvIg5BIgAAABegAhHi3QgegMAAgRIDLAAQAAARgfAMQgdAMgqAAQgoAAgfgMg");
	this.shape_8.setTransform(85,114.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#EED2B6").s().p("Ah7E4IgDgCQhNgjgVgZQgcglgZhMQgKghgHgiIgDAAQgaAEgWgZQgWgYgEglQgDgmAPgfQARgeAagDIAKAAQAQACAKAIQAMAJABAuQAAAWgCAVIAWAAQgDgVABgZQADgxAYgRIAAg2QgDAAgEgJQgIgRAAgnQAAgyA6gdQA+geBzAAIACAAQBzAAA9AeQA7AdAAAyQAAAngHARQgEAJgEAAIAAA2QAYARADAxQACAZgDAVIAVAAQgCgVAAgWQACguAKgJQALgIAPgCIAIAAIAGAAQAaADAQAeQARAfgEAmQgFAlgVAYQgWAZgagEIgDAAQgHAigKAhQgYBMgeAlQgTAZhOAjIgDACQhQAjgsAAQgrAAhQgjgAhHDEQAfANAoAAQAqAAAdgNQAfgMAAgRIjLAAQAAARAeAMg");
	this.shape_9.setTransform(85,76.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EF961C").s().p("AHmLMQgpiogTgUQgWgXh/gaIh7gWIADgGIgfggIgCABIAAhxIADgBQBOgjATgZQAegmAYhMQAKghAHgiIADAAQAaAEAWgZQAVgYAFglQAEgmgRgeQgQgegagEIgGAAIAHAAIAAigQgBiSighLQgygYg8gOIgvgIIAAgBIAAAAIgCAAIAAABQhOAIhPAmQihBLABCSIAACgIADAAQgaAEgRAeQgPAeADAmQAEAlAWAYQAWAZAagEIADAAQAHAiAKAhQAZBMAcAmQAVAZBNAjIADABIAABxIgCgBIgfAgIADAGQg7AJhAANQh/AagWAXQgUAUgoCoIgMA1Qg2gogwgwQh2h1hBiZQhDieAAitQAAirBDieQBBiYB2h2QB1h1CZhBQCehDCrAAQCtAACdBDQCZBBB1B1QB2B2BACYQBDCeABCrQgBCthDCeQhACZh2B1QgwAwg2AoIgMg1g");
	this.shape_10.setTransform(85,76.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#7F5538").s().p("AD3ELQAEgVgDgYQgDgxgYgRIAAg2QAFAAADgJQAHgRAAgnQAAgwg6gdQg9geh0gBIgBAAQh0ABg9AeQg7AdAAAwQAAAnAIARQAEAJADAAIAAA2QgYARgDAxQgBAYADAVIgVAAQACgUgBgXQAAgtgMgJQgLgJgPgBIgKAAIgEAAIAAigQAAiQCghMQBPglBPgJIAAAAIAAAAIABAAIAAAAIAvAJQA9AOAxAXQChBMAACQIAACgIgGgBIgIABQgPABgLAJQgLAJgBAtQAAAXACAUg");
	this.shape_11.setTransform(84.9,48.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,170,170);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape.setTransform(247.6,22.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAXA2IAAgvIgtAAIAAAvIgYAAIAAhrIAYAAIAAArIAtAAIAAgrIAYAAIAABrg");
	this.shape_1.setTransform(235.4,22.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_2.setTransform(224.5,22.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_3.setTransform(214,22.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgtA2IgGgBIAAgTIAEABIAEAAQAEAAADgCQADgCADgGQACgGAAgLIACg+IBOAAIAABrIgYAAIAAhYIgfAAIgCAtQAAAWgJAMQgIALgPAAIgIgBg");
	this.shape_4.setTransform(201.5,22.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAXA2IAAhYIgtAAIAABYIgYAAIAAhrIBdAAIAABrg");
	this.shape_5.setTransform(190,22.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgZAyQgNgIgGgMQgGgNAAgQQAAgQAGgNQAHgOAMgGQAMgIAPAAQAUAAAOALQAMAKACATIgYAAQgCgKgGgGQgGgFgKAAQgMAAgIAKQgIAKAAARQAAASAIAJQAIAKALABQALAAAGgGQAIgHABgMIAYAAQgDAVgNALQgOAMgUAAQgPgBgLgGg");
	this.shape_6.setTransform(178.2,22.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaAxQgLgHgHgNQgGgMAAgQQAAgPAGgNQAHgOAMgIQAMgHANAAQAQAAAMAHQALAIAHAOQAFANAAAQIAAADIhOAAQABARAIAIQAHAJANAAQAJAAAGgEQAHgFADgLIAXAAQgDAMgGAJQgIAIgKAFQgKAEgMAAQgPAAgMgIgAAcgJQgCgNgHgJQgHgHgMAAQgKAAgHAHQgIAIgBAOIA2AAIAAAAg");
	this.shape_7.setTransform(166.5,22.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgbBGQgNgHgGgPQgDgHgCgJIgBgSIABgVQACgLADgLQADgLAFgIQAHgLAKgFQALgGANgBIAPgBQAFgBACgEIAYAAQgCAKgEAFQgEAFgHADQgGACgIAAIgRACQgJABgHAFQgIAGgFAJQgFAIgCAMIAAAAQAHgLAJgGQAJgGAOAAQAOAAALAHQALAHAGAMQAHALAAARQAAARgHAMQgGANgMAHQgLAHgQAAQgQAAgMgIgAgUgDQgIAIAAARQAAARAIAKQAIAKAMAAQANAAAIgKQAIgKAAgRQAAgRgIgIQgIgKgNAAQgMAAgIAKg");
	this.shape_8.setTransform(154.5,20.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AguA2IAAhrIAYAAIAAAoIAgAAQASAAAJAJQAKAHAAAQQAAAQgKAJQgJAKgSAAgAgWAjIAdAAQAIAAAEgEQAEgFgBgGQAAgIgDgEQgFgFgIAAIgcAAg");
	this.shape_9.setTransform(137,22.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_10.setTransform(126.1,22.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_11.setTransform(115.6,22.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgvA2IAAhrIA6AAQAPAAAJAHQAIAHAAANQAAAJgEAFQgDAFgJAEIAAAAQALADAFAFQAFAHAAALQAAAPgLAIQgKAIgTAAgAgXAkIAfAAQAIAAAEgEQAEgEAAgHQAAgHgEgEQgEgEgIAAIgfAAgAgXgKIAdAAQAGAAAEgDQADgDAAgHQABgGgEgDQgEgDgHAAIgcAAg");
	this.shape_12.setTransform(104,22.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_13.setTransform(91.7,22.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgbBGQgNgHgGgPQgDgHgCgJIgBgSIABgVQACgLADgLQADgLAFgIQAHgLAKgFQALgGANgBIAPgBQAFgBACgEIAYAAQgCAKgEAFQgEAFgHADQgGACgIAAIgRACQgJABgHAFQgIAGgFAJQgFAIgCAMIAAAAQAHgLAJgGQAJgGAOAAQAOAAALAHQALAHAGAMQAHALAAARQAAARgHAMQgGANgMAHQgLAHgQAAQgQAAgMgIgAgUgDQgIAIAAARQAAARAIAKQAIAKAMAAQANAAAIgKQAIgKAAgRQAAgRgIgIQgIgKgNAAQgMAAgIAKg");
	this.shape_14.setTransform(79.4,20.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_15.setTransform(66.9,22.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("Ag0BLIAAiSIAXAAIAAAOQAGgJAIgEQAKgEAJAAQAPAAALAHQALAHAGANQAGANAAASQAAAQgGALQgGANgLAHQgKAHgPAAQgJAAgJgEQgJgEgGgJIAAA2gAgVgtQgIAKAAASQAAARAIAJQAIAKANAAQANAAAIgKQAIgJAAgQQAAgSgIgLQgIgKgNAAQgNAAgIAKg");
	this.shape_16.setTransform(54.5,24.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAXA2IAAhYIgtAAIAABYIgYAAIAAhrIBdAAIAABrg");
	this.shape_17.setTransform(41.8,22.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_18.setTransform(29.6,22.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAiBKIAAh9IhDAAIAAB9IgaAAIAAiTIB3AAIAACTg");
	this.shape_19.setTransform(15.9,20.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#55B553").ss(6.2,1,1).p("A0KlJMAoVAAAQByAAAAByIAAGvQAAByhyAAMgoVAAAQhyAAAAhyIAAmvQAAhyByAAg");
	this.shape_20.setTransform(132.1,21.5,0.94,0.65);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#55B553").s().p("A0KFKQhyAAABhyIAAmvQgBhxBygBMAoVAAAQBxABABBxIAAGvQgBByhxAAg");
	this.shape_21.setTransform(132.1,21.5,0.94,0.65);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.1,-3.1,270.3,49.1);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_44 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(44).call(this.frame_44).wait(1));

	// 8629
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#55B553").s().p("AhOB2QgbgoAAhOQAAhMAbgoQAbgoAzgBQA0AAAbAoQAbAoAABOQAABNgbAoQgbAog0AAQgzAAgbgogAgfhnQgLAMgFASQgFASgBAVQgCAUABARQgBA7AOAbQAOAbAbAAQAUgBAMgMQALgMAFgSQAFgTABgTQACgTgBgPQABgOgCgTQgBgUgFgTQgEgSgMgNQgMgMgUAAQgTAAgMALg");
	this.shape.setTransform(170.1,20.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#55B553").s().p("AALCaIAAjTIhLAAIAAglQARABATgEQATgEANgNQAPgNAFgaIApAAIAAEzg");
	this.shape_1.setTransform(143.5,20.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#55B553").s().p("AhOB2QgbgoAAhOQAAhMAbgoQAbgoAzgBQA0AAAbAoQAbAoAABOQAABNgbAoQgbAog0AAQgzAAgbgogAgfhnQgLAMgFASQgFASgBAVQgCAUABARQgBA7AOAbQAOAbAbAAQAUgBAMgMQALgMAFgSQAFgTABgTQACgTgBgPQABgOgCgTQgBgUgFgTQgEgSgMgNQgMgMgUAAQgTAAgMALg");
	this.shape_2.setTransform(121.2,20.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#55B553").s().p("AhOB2QgbgoAAhOQAAhMAbgoQAbgoAzgBQA0AAAbAoQAbAoAABOQAABNgbAoQgbAog0AAQgzAAgbgogAgfhnQgLAMgFASQgFASgBAVQgCAUABARQgBA7AOAbQAOAbAbAAQAUgBAMgMQALgMAFgSQAFgTABgTQACgTgBgPQABgOgCgTQgBgUgFgTQgEgSgMgNQgMgMgUAAQgTAAgMALg");
	this.shape_3.setTransform(96.7,20.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#55B553").s().p("AAVCbIAAhJIiBAAIAAgyICCi6IAwAAIAADBIAnAAIAAArIgnAAIAABJgAhCAnIBYAAIAAiDg");
	this.shape_4.setTransform(169.9,20.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#55B553").s().p("Ag8CZQABghAJgjQAKglAQgjQAQgiATgfQAVggAXgVIifAAIAAguIDQAAIAAApQgXAZgVAfQgTAfgPAhQgPAkgIAkQgIAjAAAkg");
	this.shape_5.setTransform(145.5,20.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#55B553").s().p("Ag4CSQgYgNgNgXQgNgWAAgfIAAgHIAwAAQABAhAPAQQAPAQAbAAQAaAAAOgPQAPgOAAgaQAAgTgIgLQgJgLgOgFQgOgGgOABIgIAAIgIAAIAAgkIAEAAQAQAAAMgDQAPgCAJgKQAJgJABgUQgBgUgMgMQgNgMgUgBQgYAAgNAQQgNAPgBAeIgwAAQACgwAagbQAagbAtAAQAcAAAVAKQAWALAMASQAMATAAAZQAAAWgMARQgLAQgUAHQAaAHAPARQAOATAAAcQAAAdgNAXQgOAWgZANQgYAMgfAAQggAAgYgMg");
	this.shape_6.setTransform(96.5,20.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#55B553").s().p("Ag3CQQgXgMgNgUQgNgVgCgdIAyAAQADAYAPAOQAOAOAYAAQAaAAAPgSQAOgRAAggQAAgegPgRQgPgPgaAAQgRAAgMAGQgMAHgKANIgsgCIAdijICdAAIAAArIh5AAIgOBPQAMgMAPgFQAOgFAPAAQAdABAWAMQAVAMAMAXQAMAVAAAeQAAAhgOAYQgOAZgZAOQgZAOgeAAQgfAAgXgLg");
	this.shape_7.setTransform(169.8,20.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#55B553").s().p("Ag3CSQgYgMgOgUQgOgWAAgcQAAgdAPgTQAPgTAbgIQgVgHgMgRQgLgRAAgXQAAgYAMgTQAMgTAWgJQAWgKAbgBQAcABAVAKQAVAJAMATQAMASAAAXQAAAYgLARQgLAQgWAJQAbAJAPAQQAPARAAAfQAAAcgOAXQgOAVgYAMQgYAMggAAQgfAAgYgMgAgoAWQgQAPAAAZQAAAaAQAPQAQAPAZAAQAaAAAPgOQAPgOAAgbQAAgagQgPQgPgPgaAAQgZAAgPAPgAgihpQgNANAAAUQAAAVANALQAOAMAUABQAWgBANgLQANgMAAgVQAAgUgNgNQgNgMgWABQgVgBgNAMg");
	this.shape_8.setTransform(145.6,20.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#55B553").s().p("AhoCbQABgjAMgXQAMgXAUgSQAVgRAagRIAdgVQAPgJALgPQALgPAAgVQgBgYgNgNQgMgPgYAAQgUAAgLAMQgMAKgEARQgEAQAAASIgxAAIAAgHQABgiALgYQAOgYAWgOQAXgNAfAAQAdAAAXAMQAWALAMAUQAMAWABAcQgBAcgMAUQgMASgSAOQgUAPgWAOIgXARQgNAKgLALQgLAMgFAMICUAAIAAAtg");
	this.shape_9.setTransform(120.5,20.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#55B553").s().p("AhNB2QgcgnAAhLQAAgyANgkQAOgkAagUQAZgTAiAAQAoAAAXAVQAZAWAFAnIgxAAQgEgWgLgKQgMgLgUAAQgSABgLALQgNALgGARQgGARgDASIgEAgQAMgSASgKQARgKAWAAQAcAAAVANQAWANALAWQAMAVAAAeQAAAggMAYQgOAZgXANQgXAOgfAAQg0AAgcgogAgkAGQgOAQAAAdQAAAeAPASQAPARAXAAQAYAAAOgRQAPgRABgeQgBgdgPgRQgOgPgZAAQgXAAgPAPg");
	this.shape_10.setTransform(170.2,20.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#55B553").s().p("Ag0CUQgVgLgMgSQgNgTgBgYIAwAAQACATAMALQANAMAVABQARgBAMgLQAMgLAGgRQAHgRACgSQADgSAAgPQgMAUgRAJQgQAKgVAAQgcAAgWgNQgVgNgMgWQgMgVgBgeQABggANgYQANgZAYgOQAXgNAfAAQAfAAAVANQAVAOAMAWQANAXAFAaQAFAbgBAbQABAagGAdQgFAdgNAYQgNAZgWAPQgWAPgfAAQgbAAgUgKgAgphhQgOARAAAeQAAAcAOARQAPAPAYAAQAXgBAOgOQAPgRAAgcQAAgegPgRQgOgSgYAAQgYAAgOASg");
	this.shape_11.setTransform(145.4,20.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#55B553").s().p("Ag9CZQACghAJgjQAKglAQgjQAQgiATgfQAVggAXgVIieAAIAAguIDPAAIAAApQgXAZgVAfQgTAfgQAhQgOAkgIAkQgHAjgBAkg");
	this.shape_12.setTransform(170,20.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#55B553").s().p("AhNB2QgcgnAAhLQAAgyANgkQAOgkAagUQAZgTAiAAQAoAAAXAVQAZAWAFAnIgxAAQgEgWgLgKQgMgLgUAAQgSABgLALQgNALgGARQgGARgDASIgEAgQAMgSASgKQARgKAWAAQAcAAAVANQAWANALAWQAMAVAAAeQAAAggNAYQgNAZgXANQgXAOgfAAQg0AAgcgogAgkAGQgOAQAAAdQAAAeAPASQAPARAXAAQAYAAAOgRQAPgRABgeQgBgdgPgRQgOgPgZAAQgXAAgPAPg");
	this.shape_13.setTransform(96.8,20.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#55B553").s().p("AhoCbQABgjAMgXQAMgXAVgSQATgRAbgRIAdgVQAPgJALgPQALgPAAgVQAAgYgOgNQgNgPgXAAQgUAAgMAMQgLAKgEARQgEAQAAASIgxAAIAAgHQAAgiAMgYQANgYAXgOQAYgNAdAAQAeAAAXAMQAWALANAUQAMAWAAAcQAAAcgMAUQgMASgUAOQgSAPgWAOIgYARQgMAKgMALQgLAMgFAMICTAAIAAAtg");
	this.shape_14.setTransform(145,20.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#55B553").s().p("AhNB2QgdgnAAhLQAAgyAOgkQANgkAagUQAagTAiAAQAnAAAYAVQAZAWAEAnIgwAAQgEgWgMgKQgLgLgTAAQgTABgMALQgMALgGARQgHARgCASIgEAgQAMgSASgKQARgKAWAAQAcAAAVANQAWANAMAWQAMAVgBAeQABAggNAYQgOAZgXANQgYAOgeAAQg0AAgcgogAgkAGQgOAQAAAdQABAeAOASQAOARAYAAQAZAAAOgRQAOgRABgeQAAgdgPgRQgPgPgaAAQgWAAgPAPg");
	this.shape_15.setTransform(121.3,20.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1,p:{x:143.5}},{t:this.shape,p:{x:170.1}}]}).to({state:[{t:this.shape_6,p:{x:96.5}},{t:this.shape_1,p:{x:119}},{t:this.shape_5},{t:this.shape_4,p:{x:169.9}}]},14).to({state:[{t:this.shape_4,p:{x:96.5}},{t:this.shape_9},{t:this.shape_8,p:{x:145.6}},{t:this.shape_7,p:{x:169.8}}]},5).to({state:[{t:this.shape_7,p:{x:96.4}},{t:this.shape_6,p:{x:120.9}},{t:this.shape_11,p:{x:145.4}},{t:this.shape_10}]},5).to({state:[{t:this.shape_13},{t:this.shape_4,p:{x:121}},{t:this.shape,p:{x:145.6}},{t:this.shape_12,p:{x:170}}]},5).to({state:[{t:this.shape_12,p:{x:96.6}},{t:this.shape_7,p:{x:120.9}},{t:this.shape_1,p:{x:143.5}},{t:this.shape_8,p:{x:170.1}}]},5).to({state:[{t:this.shape_8,p:{x:96.7}},{t:this.shape_15},{t:this.shape_14},{t:this.shape_11,p:{x:169.9}}]},5).to({state:[{t:this.shape_8,p:{x:96.7}},{t:this.shape_15},{t:this.shape_14},{t:this.shape_11,p:{x:169.9}}]},5).wait(1));

	// клиентов уже пользуются сервисом
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#55B553").s().p("AAVAeIAAgmIgPAmIgLAAIgPgmIAAAmIgOAAIAAg8IASAAIAQAwIARgwIASAAIAAA8g");
	this.shape_16.setTransform(236.7,48.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#55B553").s().p("AgPAcQgHgEgEgHQgEgIAAgJQAAgJAEgHQAEgHAHgEQAHgEAIAAQAJAAAHAEQAHAEAEAHQAEAHAAAJQAAAJgEAIQgEAHgHAEQgHAEgJAAQgIAAgHgEgAgLgPQgFAGAAAJQAAAKAFAGQAEAFAHAAQAIAAAEgFQAFgGAAgKQAAgJgFgGQgEgGgIAAQgHAAgEAGg");
	this.shape_17.setTransform(228.9,48.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#55B553").s().p("AgOAcQgHgEgDgHQgEgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGABALIgNAAQgBgGgEgDQgDgEgGAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAEAFAHAAQAFAAAEgDQAEgEACgHIANAAQgCAMgHAHQgIAGgLAAQgIAAgHgEg");
	this.shape_18.setTransform(222.1,48.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#55B553").s().p("AANAeIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_19.setTransform(215.2,48.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#55B553").s().p("AgaAeIAAg8IAgAAQAJABAFAEQAEAEABAGQAAAFgDAEQgCADgEACQAFABADACQADAFAAAGQAAAJgGAEQgGAFgLgBgAgMAUIAQAAQAFAAACgCQADgCAAgEQAAgFgDgBQgCgDgFAAIgQAAgAgMgFIAPAAQAEAAACgCQACgBAAgFQAAgDgCgBQgCgCgEAAIgPAAg");
	this.shape_20.setTransform(208.6,48.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAFgDQAGgCAEAAQAIAAAHAEQAGAEAEAIQADAHAAAKQAAAJgDAGQgDAHgHAEQgGAEgJAAQgEAAgFgCQgFgCgEgFIAAAfgAgLgZQgFAGAAAKQAAAJAFAFQAEAFAHAAQAIAAAEgFQAEgFABgJQAAgKgGgGQgEgGgHAAQgHAAgEAGg");
	this.shape_21.setTransform(201.6,49.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgIAAgJQAAgIAEgHQAEgIAGgEQAHgEAHAAQAJAAAGAEQAHAEAEAIQADAHAAAJIAAACIgrAAQAAAJAEAFQAFAFAGAAQAFAAAEgCQADgDACgGIANAAQgBAHgEAEQgEAFgGADQgGACgHAAQgHAAgHgEgAAQgEQgBgIgEgFQgEgEgHAAQgFAAgEAEQgEAEgBAJIAeAAIAAAAg");
	this.shape_22.setTransform(194.5,48.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#55B553").s().p("AgOAcQgHgEgDgHQgEgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGACALIgOAAQgBgGgEgDQgDgEgGAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAEAFAHAAQAFAAAFgDQADgEACgHIANAAQgCAMgHAHQgIAGgLAAQgIAAgHgEg");
	this.shape_23.setTransform(187.9,48.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#55B553").s().p("AAQAeIAAgYIgMAAIgRAYIgQAAIAVgaIgIgEQgDgBgBgEIgBgIQAAgHAFgFQAFgFAKAAIAfAAIAAA8gAgFgRQgDACAAAEQAAAEADACQACACADAAIAQAAIAAgQIgQAAQgDAAgCACg");
	this.shape_24.setTransform(177.4,48.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#55B553").s().p("AgOAcQgHgEgDgHQgEgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGABALIgNAAQgBgGgEgDQgDgEgGAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAEAFAHAAQAFAAAEgDQAEgEACgHIANAAQgCAMgHAHQgIAGgLAAQgIAAgHgEg");
	this.shape_25.setTransform(171.3,48.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#55B553").s().p("AgFAeIAAgwIgVAAIAAgMIA1AAIAAAMIgVAAIAAAwg");
	this.shape_26.setTransform(165.1,48.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#55B553").s().p("AgHAYQgIgHgBgOIgLAAIAAAbIgOAAIAAg8IAOAAIAAAZIAMAAQABgMAIgHQAGgHAMAAQAJAAAHAEQAHAEADAHQAEAHAAAJQAAAJgEAIQgDAHgHAEQgHAEgJAAQgMAAgHgIgAAAgPQgCAGgBAJQABAKACAGQAEAFAIAAQAIAAAEgFQAEgGAAgKQAAgJgEgGQgEgGgIAAQgIAAgEAGg");
	this.shape_27.setTransform(157.7,48.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#55B553").s().p("AgSAqIgFAAIAAgMIAEABIADAAQADAAACgCIADgGIACgGIgXg6IAPAAIAOArIAQgrIAOAAIgXA7IgEALQgDAGgCAEQgEADgIAAIgEAAg");
	this.shape_28.setTransform(149.6,49.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#55B553").s().p("AgSAbQgHgGgBgKIAOAAQABAFACACQACACADABIAEAAQAGAAADgCQAEgCAAgFQAAgEgDgCQgCgDgHAAIgGAAIAAgIIAFAAQAEAAAEgBQADgDAAgEQAAgDgDgCQgCgCgGgBQgEABgDACQgCACgBAEIgOAAQABgIAEgEQAEgEAGgCQAGgBAEAAQAGAAAGABQAFACADAEQADAEABAFIgCAHIgEAEQgCACgCABIAAAAIAFABIAFAEQACADAAAFQgBAHgDAFQgEAEgGACQgGACgHAAQgLAAgHgFg");
	this.shape_29.setTransform(143.4,48.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#55B553").s().p("AgZAeIAAg8IAOAAIAAAXIAQAAQAKAAAGAGQAFACAAAJQAAAKgFAFQgGAGgJgBgAgLAUIAPAAQAEAAACgCQACgDAAgEQAAgEgCgDQgCgDgFAAIgOAAg");
	this.shape_30.setTransform(137.3,48.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#55B553").s().p("AgZAfIgEgBIAAgLIADABIACAAIAEgBQACgCABgDQACgDAAgHIABgiIAsAAIAAA8IgOAAIAAgxIgRAAIgBAZQAAAMgFAGQgFAHgIAAIgFAAg");
	this.shape_31.setTransform(130.1,48.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#55B553").s().p("AgPAcQgHgEgEgHQgEgIAAgJQAAgJAEgHQAEgHAHgEQAHgEAIAAQAJAAAHAEQAHAEAEAHQAEAHAAAJQAAAJgEAIQgEAHgHAEQgHAEgJAAQgIAAgHgEgAgLgPQgFAGAAAJQAAAKAFAGQAEAFAHAAQAIAAAEgFQAFgGAAgKQAAgJgFgGQgEgGgIAAQgHAAgEAGg");
	this.shape_32.setTransform(123.4,48.5);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#55B553").s().p("AANAeIAAgwIgZAAIAAAwIgOAAIAAg8IA1AAIAAA8g");
	this.shape_33.setTransform(116.4,48.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgIAAgJQAAgIAEgHQAEgIAGgEQAHgEAHAAQAJAAAGAEQAHAEAEAIQADAHAAAJIAAACIgrAAQAAAJAEAFQAFAFAGAAQAFAAAEgCQADgDACgGIANAAQgBAHgEAEQgEAFgGADQgGACgHAAQgHAAgHgEgAAQgEQgBgIgEgFQgEgEgHAAQgFAAgEAEQgEAEgBAJIAeAAIAAAAg");
	this.shape_34.setTransform(106.3,48.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#55B553").s().p("AAfAeIgTgbIgGAHIAAAUIgLAAIAAgUIgGgHIgTAbIgQAAIAaghIgXgbIAQAAIAVAbIABAAIAAgbIALAAIAAAbIAWgbIAQAAIgXAbIAaAhg");
	this.shape_35.setTransform(98.3,48.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#55B553").s().p("AgSAqIgFAAIAAgMIAEABIADAAQADAAACgCIADgGIACgGIgXg6IAPAAIAOArIAQgrIAOAAIgXA7IgEALQgDAGgCAEQgEADgIAAIgEAAg");
	this.shape_36.setTransform(90.5,49.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#55B553").s().p("AgaAfIAAg8IAgAAQAJAAAFADQAEAFABAGQAAAGgDADQgCADgEACQAFABADADQADAEAAAGQAAAIgGAFQgGAFgLAAgAgMAUIAQAAQAFAAACgCQADgCAAgEQAAgEgDgCQgCgDgFAAIgQAAgAgMgFIAPAAQAEAAACgCQACgBAAgEQAAgEgCgCQgCgBgEAAIgPAAg");
	this.shape_37.setTransform(237.1,30.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#55B553").s().p("AgPAcQgHgEgEgHQgEgIAAgJQAAgJAEgHQAEgHAHgEQAHgEAIAAQAJAAAHAEQAHAEAEAHQAEAHAAAJQAAAJgEAIQgEAHgHAEQgHAEgJAAQgIAAgHgEgAgLgPQgFAGAAAJQAAAKAFAGQAEAFAHAAQAIAAAEgFQAFgGAAgKQAAgJgFgGQgEgGgIAAQgHAAgEAGg");
	this.shape_38.setTransform(230.1,30.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#55B553").s().p("AgFAfIAAgxIgVAAIAAgLIA1AAIAAALIgVAAIAAAxg");
	this.shape_39.setTransform(223.7,30.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#55B553").s().p("AANAfIAAgbIgZAAIAAAbIgOAAIAAg8IAOAAIAAAYIAZAAIAAgYIAOAAIAAA8g");
	this.shape_40.setTransform(217.5,30.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgIAAgJQAAgIAEgHQAEgIAGgEQAHgEAHAAQAJAAAGAEQAHAEAEAIQADAHAAAJIAAACIgrAAQAAAJAEAFQAFAFAGAAQAFAAAEgCQADgDACgGIANAAQgBAHgEAEQgEAFgGADQgGACgHAAQgHAAgHgEgAAQgEQgBgIgEgFQgEgEgHAAQgFAAgEAEQgEAEgBAJIAeAAIAAAAg");
	this.shape_41.setTransform(210.8,30.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#55B553").s().p("AANAfIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_42.setTransform(203.9,30.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#55B553").s().p("AgZAfIgEgBIAAgLIADABIACAAIAEgBQACgCABgDQACgDAAgHIABgiIAsAAIAAA8IgOAAIAAgxIgRAAIgBAZQAAAMgFAGQgFAHgIAAIgFAAg");
	this.shape_43.setTransform(196.8,30.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#55B553").s().p("AAMAfIgRgcIgIAJIAAATIgPAAIAAg8IAPAAIAAAbIAWgbIAQAAIgXAZIAbAjg");
	this.shape_44.setTransform(190.8,30.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]}).wait(45));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(82.5,-6,161.6,62.2);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#55B553").s().p("AAVAeIAAgnIgPAnIgLAAIgPgnIAAAnIgOAAIAAg8IASAAIAQAwIARgwIASAAIAAA8g");
	this.shape.setTransform(62.3,54.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#55B553").s().p("AAVAgQgEAAgCgBQgDgCgBgEQgEAEgGACQgDABgGAAQgKAAgGgFQgFgFAAgIQAAgGACgEQADgEAEAAIAJgCIAJgCIAGgBIAEgBIACgBIABgCIAAgDQAAgFgDgCQgDgCgFAAQgGAAgEACQgDADgBAGIgNAAQACgLAHgFQAHgFALAAQAGAAAGACQAFACAEAEQADAEAAAHIAAAdIABAEIACABIABAAIACgBIAAAKIgFABIgEAAgAAEACIgGABIgGABIgFADQgCACAAAEQAAAFADACQADADAFAAQAEAAACgCIAGgDQACgDAAgCIABgGIAAgHIgHACg");
	this.shape_1.setTransform(54.9,54);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAFgDQAGgCAEAAQAJAAAGAEQAGAEADAIQAEAHAAAKQAAAJgEAGQgDAHgGAEQgGAEgIAAQgEAAgGgCQgFgCgEgFIAAAfgAgLgZQgFAGAAAKQAAAJAFAFQAEAFAHAAQAIAAAEgFQAEgFAAgJQAAgKgEgGQgFgGgHAAQgHAAgEAGg");
	this.shape_2.setTransform(47.9,55.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#55B553").s().p("AgUAeIAAg8IApAAIAAALIgbAAIAAAxg");
	this.shape_3.setTransform(41.9,54.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#55B553").s().p("AAVAgQgEAAgCgBQgDgCgBgEQgEAEgGACQgDABgGAAQgKAAgGgFQgFgFAAgIQAAgGACgEQADgEAEAAIAJgCIAJgCIAGgBIAEgBIACgBIABgCIAAgDQAAgFgDgCQgDgCgFAAQgGAAgEACQgDADgBAGIgNAAQACgLAHgFQAHgFALAAQAGAAAGACQAFACAEAEQADAEAAAHIAAAdIABAEIACABIABAAIACgBIAAAKIgFABIgEAAgAAEACIgGABIgGABIgFADQgCACAAAEQAAAFADACQADADAFAAQAEAAACgCIAGgDQACgDAAgCIABgGIAAgHIgHACg");
	this.shape_4.setTransform(35.8,54);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#55B553").s().p("AgFAeIAAgxIgVAAIAAgLIA1AAIAAALIgVAAIAAAxg");
	this.shape_5.setTransform(29.6,54.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#55B553").s().p("AgOAcQgGgEgEgHQgEgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGACALIgOAAQgBgGgEgDQgDgEgGAAQgGABgFAFQgEAGAAAJQAAAKAEAGQAEAFAHAAQAGAAAEgDQADgEABgHIAOAAQgCAMgHAHQgIAGgLAAQgIAAgHgEg");
	this.shape_6.setTransform(23.6,54);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#55B553").s().p("AANAeIAAgaIgZAAIAAAaIgOAAIAAg8IAOAAIAAAZIAZAAIAAgZIAOAAIAAA8g");
	this.shape_7.setTransform(16.7,54.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#55B553").s().p("AANAeIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_8.setTransform(9.8,54.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#55B553").s().p("AgaAeIAAg8IAgAAQAJAAAFAFQAEADABAIQAAAEgDAEQgCADgEACQAFABADADQADADAAAHQAAAIgGAFQgGAEgLAAgAgMAUIAQAAQAFAAACgCQADgCAAgEQAAgFgDgCQgCgCgFAAIgQAAgAgMgFIAPAAQAEAAACgCQACgCAAgEQAAgDgCgCQgCgBgEAAIgPAAg");
	this.shape_9.setTransform(-0.2,54.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#55B553").s().p("AANAeIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_10.setTransform(-10.5,54.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#55B553").s().p("AAMAeIgSgbIgIAJIAAASIgNAAIAAg8IANAAIAAAcIAXgcIARAAIgZAaIAbAig");
	this.shape_11.setTransform(-16.7,54.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#55B553").s().p("AgFAeIAAgxIgVAAIAAgLIA1AAIAAALIgVAAIAAAxg");
	this.shape_12.setTransform(-23.1,54.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#55B553").s().p("AgSAqIgFAAIAAgMIAEABIADAAQADAAACgCIADgGIACgGIgXg6IAPAAIAOArIAQgrIAOAAIgXA7IgEALQgDAGgCAEQgEADgIAAIgEAAg");
	this.shape_13.setTransform(-29,55.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAGgDQAFgCAEAAQAIAAAHAEQAGAEAEAIQADAHAAAKQAAAJgDAGQgDAHgHAEQgGAEgJAAQgEAAgFgCQgFgCgEgFIAAAfgAgMgZQgEAGAAAKQAAAJAEAFQAFAFAHAAQAHAAAFgFQAEgFABgJQAAgKgGgGQgEgGgHAAQgHAAgFAGg");
	this.shape_14.setTransform(-35.6,55.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#55B553").s().p("AAMAeIgSgbIgIAJIAAASIgNAAIAAg8IANAAIAAAcIAXgcIARAAIgZAaIAbAig");
	this.shape_15.setTransform(-42.3,54.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGABALIgNAAQgBgGgEgDQgEgEgFAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAEAFAGAAQAGAAAEgDQAEgEACgHIANAAQgCAMgHAHQgIAGgMAAQgHAAgHgEg");
	this.shape_16.setTransform(-49.1,54);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#55B553").s().p("AAVAgQgEAAgCgBQgDgCgBgEQgEAEgGACQgDABgGAAQgKAAgGgFQgFgFAAgIQAAgGACgEQADgEAEAAIAJgCIAJgCIAGgBIAEgBIACgBIABgCIAAgDQAAgFgDgCQgDgCgFAAQgGAAgEACQgDADgBAGIgNAAQACgLAHgFQAHgFALAAQAGAAAGACQAFACAEAEQADAEAAAHIAAAdIABAEIACABIABAAIACgBIAAAKIgFABIgEAAgAAEACIgGABIgGABIgFADQgCACAAAEQAAAFADACQADADAFAAQAEAAACgCIAGgDQACgDAAgCIABgGIAAgHIgHACg");
	this.shape_17.setTransform(-55.7,54);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAGgDQAFgCAEAAQAIAAAHAEQAGAEAEAIQADAHAAAKQAAAJgDAGQgDAHgHAEQgGAEgJAAQgDAAgFgCQgGgCgDgFIAAAfgAgMgZQgEAGAAAKQAAAJAEAFQAFAFAHAAQAHAAAFgFQAFgFAAgJQgBgKgFgGQgEgGgHAAQgHAAgFAGg");
	this.shape_18.setTransform(-62.7,55.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#55B553").s().p("AgOAcQgGgEgFgHQgDgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGABALIgNAAQgCgGgDgDQgDgEgGAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAFAFAFAAQAHAAADgDQAFgEABgHIANAAQgCAMgHAHQgIAGgMAAQgHAAgHgEg");
	this.shape_19.setTransform(66.3,40.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#55B553").s().p("AANAfIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_20.setTransform(59.4,40.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#55B553").s().p("AgaAfIAAg8IAgAAQAJAAAFADQAEAFABAGQAAAGgDADQgCADgEACQAFABADADQADAEAAAGQAAAIgGAFQgGAFgLAAgAgMAUIAQAAQAFAAACgCQADgCAAgEQAAgEgDgCQgCgDgFAAIgQAAgAgMgFIAPAAQAEAAACgCQACgBAAgEQAAgEgCgCQgCgBgEAAIgPAAg");
	this.shape_21.setTransform(52.8,40.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAFgDQAGgCAEAAQAIAAAHAEQAGAEADAIQAEAHAAAKQAAAJgEAGQgDAHgGAEQgGAEgIAAQgFAAgFgCQgFgCgEgFIAAAfgAgLgZQgFAGAAAKQAAAJAFAFQAEAFAHAAQAIAAAEgFQAFgFgBgJQAAgKgEgGQgFgGgHAAQgHAAgEAGg");
	this.shape_22.setTransform(45.8,41.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgIAAgJQAAgIAEgHQAEgIAGgEQAHgEAHAAQAJAAAGAEQAHAEAEAIQADAHAAAJIAAACIgrAAQAAAJAEAFQAFAFAGAAQAFAAAEgCQADgDACgGIANAAQgBAHgEAEQgEAFgGADQgGACgHAAQgHAAgHgEgAAQgEQgBgIgEgFQgEgEgHAAQgFAAgEAEQgEAEgBAJIAeAAIAAAAg");
	this.shape_23.setTransform(38.7,40.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#55B553").s().p("AgOAcQgGgEgFgHQgDgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGABALIgNAAQgCgGgDgDQgDgEgGAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAFAFAFAAQAHAAADgDQAFgEABgHIANAAQgCAMgHAHQgIAGgMAAQgHAAgHgEg");
	this.shape_24.setTransform(32.1,40.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#55B553").s().p("AANArIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8gAgMgdQgFgFgBgIIAHAAQABAEADADQADACAEAAQAFAAADgCQADgDABgEIAHAAQgBAIgEAFQgFAEgJAAQgHAAgFgEg");
	this.shape_25.setTransform(21.8,39.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#55B553").s().p("AANAfIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_26.setTransform(14.9,40.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#55B553").s().p("AAMAfIgSgcIgIAJIAAATIgNAAIAAg8IANAAIAAAbIAXgbIAQAAIgYAZIAbAjg");
	this.shape_27.setTransform(8.7,40.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGABALIgNAAQgCgGgDgDQgEgEgFAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAFAFAFAAQAHAAADgDQAFgEABgHIANAAQgCAMgHAHQgIAGgMAAQgHAAgHgEg");
	this.shape_28.setTransform(1.8,40.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgIAAgJQAAgIAEgHQAEgIAGgEQAHgEAHAAQAJAAAGAEQAHAEAEAIQADAHAAAJIAAACIgrAAQAAAJAEAFQAFAFAGAAQAFAAAEgCQADgDACgGIANAAQgBAHgEAEQgEAFgGADQgGACgHAAQgHAAgHgEgAAQgEQgBgIgEgFQgEgEgHAAQgFAAgEAEQgEAEgBAJIAeAAIAAAAg");
	this.shape_29.setTransform(-4.9,40.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#55B553").s().p("AANAfIAAgaIgGABIgHABIgGAAQgJAAgFgEQgFgDAAgJIAAgUIAOAAIAAAPQAAAGACADQADADAGAAIADAAIAFgBIAFgCIAAgYIANAAIAAA8g");
	this.shape_30.setTransform(-11.6,40.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#55B553").s().p("AANAfIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_31.setTransform(-18.2,40.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#55B553").s().p("AgFAfIAAgxIgVAAIAAgLIA1AAIAAALIgVAAIAAAxg");
	this.shape_32.setTransform(-24.4,40.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#55B553").s().p("AAVAgQgEAAgCgBQgDgCgBgEQgEAEgGACQgDABgGAAQgKAAgGgFQgFgFAAgIQAAgGACgEQADgEAEAAIAJgCIAJgCIAGgBIAEgBIACgBIABgCIAAgDQAAgFgDgCQgDgCgFAAQgGAAgEACQgDADgBAGIgNAAQACgLAHgFQAHgFALAAQAGAAAGACQAFACAEAEQADAEAAAHIAAAdIABAEIACABIABAAIACgBIAAAKIgFABIgEAAgAAEACIgGABIgGABIgFADQgCACAAAEQAAAFADACQADADAFAAQAEAAACgCIAGgDQACgDAAgCIABgGIAAgHIgHACg");
	this.shape_33.setTransform(-30.3,40.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#55B553").s().p("AAVAfIAAgoIgPAoIgLAAIgPgoIAAAoIgOAAIAAg8IASAAIAQAvIARgvIASAAIAAA8g");
	this.shape_34.setTransform(-38,40.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#55B553").s().p("AgPAcQgHgEgEgHQgEgIAAgJQAAgJAEgHQAEgHAHgEQAHgEAIAAQAJAAAHAEQAHAEAEAHQAEAHAAAJQAAAJgEAIQgEAHgHAEQgHAEgJAAQgIAAgHgEgAgLgPQgFAGAAAJQAAAKAFAGQAEAFAHAAQAIAAAEgFQAFgGAAgKQAAgJgFgGQgEgGgIAAQgHAAgEAGg");
	this.shape_35.setTransform(-45.8,40.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#55B553").s().p("AgFAfIAAgxIgVAAIAAgLIA1AAIAAALIgVAAIAAAxg");
	this.shape_36.setTransform(-52.1,40.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#55B553").s().p("AgaAfIAAg8IAgAAQAJAAAFADQAEAFABAGQAAAGgDADQgCADgEACQAFABADADQADAEAAAGQAAAIgGAFQgGAFgLAAgAgMAUIAQAAQAFAAACgCQADgCAAgEQAAgEgDgCQgCgDgFAAIgQAAgAgMgFIAPAAQAEAAACgCQACgBAAgEQAAgEgCgCQgCgBgEAAIgPAAg");
	this.shape_37.setTransform(-58.1,40.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#55B553").s().p("AAYAqIgHgWIggAAIgJAWIgQAAIAihTIAOAAIAgBTgAAMAIIgMgiIgMAiIAYAAg");
	this.shape_38.setTransform(-65.5,39.5);

	this.instance = new lib.logo();
	this.instance.setTransform(-75,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-75,-6,150,67.7);


(lib.Symbol5copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgkApIAAhRIAsAAQAMAAAGAFQAGAGAAAJQAAAHgCAEQgDAEgGADIAAAAQAIACADAEQAFAFAAAIQgBAMgHAGQgJAGgOAAgAgRAbIAXAAQAGAAADgDQAEgDAAgFQAAgGgEgDQgDgDgGAAIgXAAgAgRgHIAVAAQAFAAADgDQADgCgBgFQABgFgDgCQgDgCgGAAIgUAAg");
	this.shape.setTransform(-35.5,130.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_1.setTransform(-44.9,130.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgIApIAAhCIgbAAIAAgPIBHAAIAAAPIgbAAIAABCg");
	this.shape_2.setTransform(-53.3,130.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AASApIAAgkIgiAAIAAAkIgTAAIAAhRIATAAIAAAhIAiAAIAAghIASAAIAABRg");
	this.shape_3.setTransform(-61.7,130.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgYA4IgIAAIAAgQIAHABIADAAQAFAAACgDQADgDABgFIADgIIgghOIAUAAIAVA7IAUg7IATAAIgeBPIgGAQQgDAHgEAFQgGAGgKgBIgFAAg");
	this.shape_4.setTransform(-70.4,132.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAUApQgEgCgBgGQgFAGgIACQgFACgJAAQgNAAgHgGQgHgHgBgLQABgJADgEQADgGAGAAIAMgEIANgCIAHgBIAGgBIADgCIABgDIAAgDQAAgHgEgCQgFgEgGAAQgJAAgEAEQgFADgBAIIgRAAQACgPAJgGQAKgHAPAAQAIAAAIACQAHADAFAFQAEAGAAAJIAAAnIABAFQABABAAAAQAAAAABABQAAAAAAAAQABAAAAAAIACgBIADAAIAAANIgHACIgFAAQgGAAgDgCgAAFACIgIACIgIACQgEABgDACQgDADAAAGQAAAGAEADQAEADAHAAQAGAAADgCQAFgBADgDQADgDAAgEIABgIIAAgJQgFACgFAAg");
	this.shape_5.setTransform(-78.8,130.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAQApIgYglIgLALIAAAaIgSAAIAAhRIASAAIAAAlIAfglIAWAAIgfAhIAjAwg");
	this.shape_6.setTransform(-87.2,130.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAQApIgYglIgKALIAAAaIgTAAIAAhRIATAAIAAAlIAeglIAXAAIghAhIAkAwg");
	this.shape_7.setTransform(-95.8,130.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAUApQgEgCgBgGQgFAGgIACQgFACgJAAQgNAAgHgGQgHgHgBgLQABgJADgEQADgGAGAAIAMgEIANgCIAHgBIAGgBIADgCIABgDIAAgDQAAgHgEgCQgFgEgGAAQgJAAgEAEQgFADgBAIIgRAAQACgPAJgGQAKgHAPAAQAIAAAIACQAHADAFAFQAEAGAAAJIAAAnIABAFQABABAAAAQAAAAABABQAAAAAAAAQABAAAAAAIACgBIADAAIAAANIgHACIgFAAQgGAAgDgCgAAFACIgIACIgIACQgEABgDACQgDADAAAGQAAAGAEADQAEADAHAAQAGAAADgCQAFgBADgDQADgDAAgEIABgIIAAgJQgFACgFAAg");
	this.shape_8.setTransform(-104.9,130.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAUApIgUgfIgTAfIgWAAIAfgqIgcgnIAWAAIAQAaIARgaIAVAAIgcAmIAgArg");
	this.shape_9.setTransform(38.3,111.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAeApIAAhRIASAAIAABRgAgvApIAAhRIASAAIAAAeIAWAAQALAAAIAHQAHAFAAAMQAAANgHAHQgIAHgLAAgAgdAbIAUAAQAGgBADgDQABgDAAgFQAAgGgBgDQgEgEgGAAIgTAAg");
	this.shape_10.setTransform(28.3,111.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AASApIAAgkIgiAAIAAAkIgTAAIAAhRIATAAIAAAhIAiAAIAAghIASAAIAABRg");
	this.shape_11.setTransform(17.9,111.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AASApIAAgkIgiAAIAAAkIgTAAIAAhRIATAAIAAAhIAiAAIAAghIASAAIAABRg");
	this.shape_12.setTransform(8.7,111.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAUApQgEgCgBgGQgFAGgIACQgFACgJAAQgNAAgHgHQgHgGgBgMQABgIADgEQADgGAGAAIAMgEIANgCIAHgBIAGgBIADgCIABgDIAAgDQAAgHgEgCQgFgEgGAAQgJAAgEAEQgFADgBAIIgRAAQACgPAJgGQAKgHAPAAQAIAAAIACQAHADAFAFQAEAGAAAJIAAAnIABAFQABABAAAAQAAAAABABQAAAAAAAAQABAAAAAAIACgBIADAAIAAANIgHACIgFAAQgGAAgDgCgAAFACIgIACIgIACQgEABgDACQgDADAAAGQAAAGAEADQAEADAHAAQAGAAADgCQAFgBADgDQADgDAAgEIABgIIAAgJQgFACgFAAg");
	this.shape_13.setTransform(-0.2,111.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgkApIAAhRIAsAAQAMAAAGAFQAGAGAAAJQAAAHgCAEQgDAEgGADIAAAAQAIACADAEQAEAFABAIQgBAMgHAGQgJAGgOAAgAgRAbIAXAAQAGAAADgDQAEgDAAgFQAAgGgEgDQgDgDgGAAIgXAAgAgRgHIAVAAQAFAAADgDQADgCgBgFQABgFgDgCQgDgCgFAAIgVAAg");
	this.shape_14.setTransform(-9,111.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_15.setTransform(-18.5,111.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgnA5IAAhvIARAAIAAALQAEgHAHgDQAHgDAHAAQALAAAJAFQAIAGAFAKQAEAJAAAOQAAAMgEAJQgFAJgIAFQgIAGgLAAQgHAAgHgDQgGgDgFgHIAAApgAgQgiQgGAIAAAOQAAAMAGAHQAGAHAKAAQAKAAAGgHQAGgHAAgMQAAgOgGgIQgGgHgKAAQgKAAgGAHg");
	this.shape_16.setTransform(-27.9,112.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AASApIAAg6IgBAAIgdA6IgXAAIAAhRIATAAIAAA6IAAAAIAdg6IAXAAIAABRg");
	this.shape_17.setTransform(-37.6,111.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAQApIgYglIgKALIAAAaIgTAAIAAhRIATAAIAAAlIAeglIAXAAIghAhIAkAwg");
	this.shape_18.setTransform(-46,111.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_19.setTransform(-55.6,111.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgiApIgFgBIAAgOIADAAIAEAAQADAAACgBQADgCABgEQACgFAAgIIACgvIA7AAIAABRIgSAAIAAhCIgYAAIgBAiQAAAQgHAJQgGAJgLAAIgHgBg");
	this.shape_20.setTransform(-65.3,111.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgVA1QgJgFgFgMIgDgLIgBgPIABgPIADgQQACgJAEgGQAGgIAIgEQAIgFAJgBIAMAAQAEgBABgDIASAAQgBAHgDAFQgEAEgFABIgLADIgNABQgGAAgFAEQgGAFgEAGQgEAHgCAIIABAAQAFgHAHgFQAHgFAKAAQAKABAJAFQAIAFAFAKQAFAGAAAOQAAAMgFAKQgFAJgJAGQgJAFgMAAQgLAAgKgGgAgPgBQgGAFAAANQAAAMAGAIQAGAHAJABQAKgBAGgHQAGgIAAgMQAAgNgGgFQgGgIgKAAQgJAAgGAIg");
	this.shape_21.setTransform(-74.2,109.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAUApQgEgCgBgGQgFAGgIACQgFACgJAAQgNAAgHgHQgHgGgBgMQABgIADgEQADgGAGAAIAMgEIANgCIAHgBIAGgBIADgCIABgDIAAgDQAAgHgEgCQgFgEgGAAQgJAAgEAEQgFADgBAIIgRAAQACgPAJgGQAKgHAPAAQAIAAAIACQAHADAFAFQAEAGAAAJIAAAnIABAFQAAABABAAQAAAAAAABQABAAAAAAQABAAAAAAIACgBIADAAIAAANIgHACIgFAAQgGAAgDgCgAAFACIgIACIgIACQgEABgDACQgDADAAAGQAAAGAEADQAEADAHAAQAGAAADgCQAFgBADgDQADgDAAgEIABgIIAAgJQgFACgFAAg");
	this.shape_22.setTransform(-83.3,111.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgZAkQgJgIgBgNIATAAQABAGADADQADADADABIAHABQAHAAAFgDQAFgDAAgGQAAgFgEgEQgDgDgKgBIgIAAIAAgLIAHAAQAGAAAEgCQAFgDAAgFQAAgFgDgDQgEgDgIAAQgFAAgEADQgEAEgBAFIgSAAQABgKAFgGQAGgGAHgCQAIgCAHAAQAIAAAHACQAIACAEAFQAEAFAAAJQAAAEgCADQgCAFgDABQgDADgDAAIAAABQAEAAADACQAEACACAEQADAEAAAHQAAAIgFAGQgFAHgIACQgIADgJAAQgQAAgKgHg");
	this.shape_23.setTransform(-92.1,111.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgbAqQgKgOAAgcQAAgaAKgPQAJgPASAAQATAAAKAPQAJAOAAAbQAAAcgJAOQgKAPgTAAQgRAAgKgPgAgKgkQgEAEgCAGQgCAHAAAIIgBAMQAAAWAFAJQAFAKAJAAQAHAAAEgEQAFgFABgHIADgNIAAgNIAAgLIgCgOQgCgGgFgFQgEgFgHABQgGgBgEAFg");
	this.shape_24.setTransform(-105.1,109.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgWAIIAAgPIAtAAIAAAPg");
	this.shape_25.setTransform(71.1,91);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_26.setTransform(58.7,91.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AASApIAAgkIgiAAIAAAkIgTAAIAAhRIATAAIAAAhIAiAAIAAghIASAAIAABRg");
	this.shape_27.setTransform(49.4,91.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgTAlQgJgFgFgKQgFgJAAgNQAAgLAFgKQAFgKAJgFQAKgGAKAAQAQAAAKAIQAJAIACAOIgSAAQgCgIgEgEQgFgEgHAAQgJAAgGAIQgGAIAAAMQAAANAGAIQAFAHAJAAQAIAAAFgEQAFgFACgJIASAAQgCAPgKAJQgKAJgQAAQgLAAgJgGg");
	this.shape_28.setTransform(40.4,91.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAUApQgEgCgBgGQgFAGgIACQgFACgJAAQgNAAgHgHQgHgGgBgLQABgJADgEQADgGAGAAIAMgEIANgCIAHgBIAGgBIADgCIABgDIAAgDQAAgHgEgCQgFgEgGAAQgJAAgEAEQgFADgBAIIgRAAQACgPAJgGQAKgHAPAAQAIAAAIACQAHADAFAFQAEAGAAAJIAAAnIABAFQABABAAAAQAAAAAAABQABAAAAAAQABAAAAAAIACgBIADAAIAAANIgHACIgFAAQgGAAgDgCgAAFACIgIACIgIACQgEABgDADQgDACAAAGQAAAGAEADQAEADAHAAQAGAAADgCQAFgBADgDQADgDAAgEIABgHIAAgKQgFACgFAAg");
	this.shape_29.setTransform(31.6,91.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AASApIAAhCIgiAAIAABCIgTAAIAAhRIBHAAIAABRg");
	this.shape_30.setTransform(22.4,91.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_31.setTransform(13,91.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgZAkQgJgIgBgNIATAAQABAGADADQADADADABIAHABQAHAAAFgDQAFgDAAgGQAAgFgEgEQgDgDgKgBIgIAAIAAgLIAHAAQAGAAAEgCQAFgDAAgFQAAgFgDgDQgEgDgIAAQgFAAgEAEQgEADgBAFIgSAAQABgKAFgGQAGgGAHgCQAIgCAHAAQAIAAAHACQAIACAEAFQAEAFAAAJQAAAEgCADQgCAFgDABQgDADgDAAIAAABQAEAAADACQAEACACAEQADAEAAAHQAAAIgFAGQgFAHgIACQgIADgJAAQgQAAgKgHg");
	this.shape_32.setTransform(4.1,91.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgTAmQgKgGgEgKQgFgJAAgNQAAgLAFgKQAFgKAKgFQAJgGAJAAQAMAAAJAFQAJAGAEAKQAFALAAAMIAAACIg7AAQAAAMAGAHQAHAHAIAAQAHAAAFgEQAFgDACgIIASAAQgCAJgFAGQgGAHgIADQgHADgKAAQgKAAgJgFgAAVgGQgBgLgFgGQgGgFgJgBQgHAAgFAGQgGAGgBALIAoAAIAAAAg");
	this.shape_33.setTransform(-4.4,91.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgVA1QgJgFgFgMIgDgLIgBgPIABgPIADgQQACgJAEgGQAGgIAIgEQAIgFAJAAIAMgBQAEgBABgDIASAAQgBAHgDAFQgEAEgFABIgLADIgNABQgGAAgFAEQgGAFgEAGQgEAHgCAIIABAAQAFgHAHgFQAHgFAKAAQAKABAJAFQAIAFAFAKQAFAGAAANQAAANgFAKQgFAJgJAGQgJAFgMAAQgLAAgKgGgAgPgBQgGAFAAAMQAAANAGAIQAGAHAJABQAKgBAGgHQAGgIAAgNQAAgMgGgFQgGgIgKAAQgJAAgGAIg");
	this.shape_34.setTransform(-13.6,89.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_35.setTransform(-27.6,91.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AASApIAAgkIgiAAIAAAkIgTAAIAAhRIATAAIAAAhIAiAAIAAghIASAAIAABRg");
	this.shape_36.setTransform(-37,91.3);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgIApIAAhCIgbAAIAAgPIBHAAIAAAPIgbAAIAABCg");
	this.shape_37.setTransform(-45.2,91.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgKAhQgKgKgCgSIgPAAIAAAkIgSAAIAAhRIASAAIAAAhIAPAAQADgRALgJQAIgJARAAQALAAAKAFQAJAFAEAKQAFAKAAAMQAAANgFAKQgEAKgJAFQgKAFgLAAQgRAAgKgKgAAAgUQgDAIgBAMQABANADAIQAGAIAKAAQAKAAAGgIQAFgIABgNQgBgMgFgIQgGgIgKAAQgKAAgGAIg");
	this.shape_38.setTransform(-55.2,91.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgiApIgFgBIAAgOIADAAIAEAAQADAAACgBQADgCABgEQACgFAAgIIACgvIA7AAIAABRIgSAAIAAhCIgYAAIgBAiQAAAQgHAJQgGAJgLAAIgHgBg");
	this.shape_39.setTransform(-66.8,91.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_40.setTransform(-75.8,91.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgTAlQgJgFgFgKQgFgJAAgNQAAgLAFgKQAFgKAJgFQAKgGAKAAQAQAAAKAIQAJAIACAOIgSAAQgCgIgEgEQgFgEgHAAQgJAAgGAIQgGAIAAAMQAAANAGAIQAFAHAJAAQAIAAAFgEQAFgFACgJIASAAQgCAPgKAJQgKAJgQAAQgLAAgJgGg");
	this.shape_41.setTransform(-84.9,91.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgVA1QgJgFgFgMIgDgLIgBgPIABgPIADgQQACgJAEgGQAGgIAIgEQAIgFAJAAIAMgBQAEgBABgDIASAAQgBAHgDAFQgEAEgFABIgLADIgNABQgGAAgFAEQgGAFgEAGQgEAHgCAIIABAAQAFgHAHgFQAHgFAKAAQAKABAJAFQAIAFAFAKQAFAGAAANQAAANgFAKQgFAJgJAGQgJAFgMAAQgLAAgKgGgAgPgBQgGAFAAAMQAAANAGAIQAGAHAJABQAKgBAGgHQAGgIAAgNQAAgMgGgFQgGgIgKAAQgJAAgGAIg");
	this.shape_42.setTransform(-94.1,89.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AAhA5IgLgeIgrAAIgMAeIgUAAIArhxIAUAAIAsBxgAAQALIgQgtIgRAtIAhAAg");
	this.shape_43.setTransform(-104.2,89.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-111.5,79,192.4,61.4);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAUApQgEgDgBgFQgFAFgIADQgFACgJAAQgNAAgHgGQgHgHgBgLQABgJADgFQADgEAGgBIAMgEIANgBIAHgCIAGgCIADgCIABgBIAAgFQAAgFgEgEQgFgDgGAAQgJAAgEADQgFAEgBAIIgRAAQACgPAJgHQAKgGAPAAQAIAAAIADQAHACAFAGQAEAFAAAJIAAAnIABAGQAAAAABAAQAAAAABABQAAAAAAAAQABAAAAAAIACAAIADAAIAAAMIgHABIgFABQgGAAgDgCgAAFADIgIABIgIACQgEABgDADQgDADAAAFQAAAFAEAEQAEADAHAAQAGAAADgBQAFgCADgEQADgDAAgDIABgHIAAgKQgFACgFABg");
	this.shape.setTransform(68.5,45.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAdA2IAAgaIg5AAIAAAaIgRAAIAAgpIAKAAIAFgOQADgIABgLQABgLAAgNIAAgJIA8AAIAABCIALAAIAAApgAgHghIgCATIgDAPIgEAMIAhAAIAAg0IgYAAg");
	this.shape_1.setTransform(59,47.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_2.setTransform(49.5,45.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgnA5IAAhvIARAAIAAALQAEgHAHgDQAHgDAHAAQALAAAJAFQAIAGAFAKQAEAJAAAOQAAAMgEAJQgFAJgIAFQgIAGgLAAQgHAAgHgDQgGgDgFgHIAAApgAgQgiQgGAIAAAOQAAAMAGAHQAGAHAKAAQAKAAAGgHQAGgHAAgMQAAgOgGgIQgGgHgKAAQgKAAgGAHg");
	this.shape_3.setTransform(40,47.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_4.setTransform(30.2,45.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgbApIAAhRIA3AAIAAAPIglAAIAABCg");
	this.shape_5.setTransform(22.5,45.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_6.setTransform(9.4,45.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgcApIAAhRIA5AAIAAAPIgmAAIAABCg");
	this.shape_7.setTransform(1.8,45.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgUAmQgIgGgFgKQgFgJAAgNQAAgLAFgKQAGgKAJgFQAIgGAKAAQAMAAAJAFQAIAGAGAKQAEALAAAMIAAACIg7AAQABAMAFAHQAHAHAIAAQAHAAAFgEQAFgDACgIIASAAQgCAJgGAGQgFAHgHADQgIADgJAAQgLAAgKgFgAAVgGQgBgLgGgGQgFgFgJgBQgHAAgGAGQgFAGgCALIApAAIAAAAg");
	this.shape_8.setTransform(-6.5,45.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ag2ApIAAhRIASAAIAABCIAdAAIAAhCIAQAAIAABCIAcAAIAAhCIASAAIAABRg");
	this.shape_9.setTransform(-17.5,45.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAUApQgEgDgBgFQgFAFgIADQgFACgJAAQgNAAgHgGQgHgHgBgLQABgJADgFQADgEAGgBIAMgEIANgBIAHgCIAGgCIADgCIABgBIAAgFQAAgFgEgEQgFgDgGAAQgJAAgEADQgFAEgBAIIgRAAQACgPAJgHQAKgGAPAAQAIAAAIADQAHACAFAGQAEAFAAAJIAAAnIABAGQAAAAABAAQAAAAABABQAAAAAAAAQABAAAAAAIACAAIADAAIAAAMIgHABIgFABQgGAAgDgCgAAFADIgIABIgIACQgEABgDADQgDADAAAFQAAAFAEAEQAEADAHAAQAGAAADgBQAFgCADgEQADgDAAgDIABgHIAAgKQgFACgFABg");
	this.shape_10.setTransform(-28.3,45.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgtA5IAAhwIAyAAQALAAAJADQAIAEAEAGQAFAGAAAJQgBAJgEAGQgFAGgHADQAKACAGAGQAFAHAAALQABAKgFAIQgEAHgJAEQgIAEgLABgAgaAoIAcAAQAGAAAGgBQAGgCADgDQADgFAAgGQAAgJgDgDQgDgEgGgBIgNgBIgbAAgAgagIIAaAAQALAAAFgDQAGgEgBgJQAAgGgDgEQgCgDgGgBQgFgBgFAAIgaAAg");
	this.shape_11.setTransform(-38.2,44.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgTAmQgJgGgFgKQgFgJAAgMQAAgMAFgKQAFgKAJgGQAKgFAKAAQAQAAAKAIQAJAIACAOIgSAAQgCgIgEgEQgFgEgHAAQgJAAgGAIQgGAHAAANQAAANAGAIQAFAIAJAAQAIAAAFgFQAFgFACgJIASAAQgCAPgKAJQgKAJgQAAQgLAAgJgFg");
	this.shape_12.setTransform(-53,45.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AASApIAAg6IgBAAIgdA6IgXAAIAAhRIATAAIAAA6IAAAAIAdg6IAXAAIAABRg");
	this.shape_13.setTransform(29.1,26.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAQApIgYglIgKALIAAAaIgTAAIAAhRIATAAIAAAlIAeglIAXAAIghAhIAkAwg");
	this.shape_14.setTransform(20.7,26.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AASApIAAg6IgBAAIgdA6IgXAAIAAhRIATAAIAAA6IAAAAIAdg6IAXAAIAABRg");
	this.shape_15.setTransform(11.3,26.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AARApIAAgjIgIACIgJABIgIABQgMAAgHgGQgHgFAAgMIAAgbIASAAIAAAVQAAAIADAEQAFAEAHAAIAFgBIAHgCIAGgBIAAghIASAAIAABRg");
	this.shape_16.setTransform(2.3,26.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgTAmQgJgGgFgJQgFgKAAgMQAAgMAFgKQAFgKAJgGQAKgFAKAAQAQAAAKAIQAJAIACAOIgSAAQgCgIgEgEQgFgEgHAAQgJAAgGAIQgGAHAAANQAAANAGAIQAFAIAJAAQAIAAAFgFQAFgFACgJIASAAQgCAPgKAJQgKAJgQAAQgLAAgJgFg");
	this.shape_17.setTransform(-6.3,26.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AASApIAAg6IgBAAIgdA6IgXAAIAAhRIATAAIAAA6IAAAAIAdg6IAXAAIAABRg");
	this.shape_18.setTransform(-15.4,26.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AASApIAAhCIgiAAIAABCIgTAAIAAhRIBHAAIAABRg");
	this.shape_19.setTransform(-24.6,26.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAdA2IAAgaIg5AAIAAAaIgRAAIAAgpIAKAAIAFgOQADgIABgLQABgLAAgNIAAgJIA8AAIAABCIALAAIAAApgAgHghIgCATIgDAPIgEAMIAhAAIAAg0IgYAAg");
	this.shape_20.setTransform(-34,27.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_21.setTransform(-43.6,26.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AASApIAAhCIgiAAIAABCIgTAAIAAhRIBHAAIAABRg");
	this.shape_22.setTransform(-52.9,26.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgUAmQgJgGgEgKQgFgJAAgNQAAgLAFgKQAFgKAJgFQAKgGAJAAQAMAAAJAFQAJAGAEAKQAFALAAAMIAAACIg7AAQAAAMAHAHQAFAHAJAAQAIAAAEgEQAFgDADgIIARAAQgCAJgFAGQgGAHgIADQgHADgKAAQgKAAgKgFgAAVgGQgBgLgFgGQgGgFgJgBQgHAAgFAGQgGAGgCALIApAAIAAAAg");
	this.shape_23.setTransform(48.5,6.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAeApIAAhRIASAAIAABRgAgvApIAAhRIASAAIAAAeIAWAAQALAAAIAHQAHAFAAAMQAAANgHAHQgIAHgLAAgAgdAbIAUAAQAGgBADgDQABgDAAgFQAAgGgBgDQgEgEgGAAIgTAAg");
	this.shape_24.setTransform(38.2,6.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgjApIAAhRIArAAQAMAAAGAFQAHAGgBAJQAAAHgDAEQgCAEgGADIAAAAQAHACAEAEQAFAFAAAIQAAAMgJAGQgIAGgOAAgAgRAbIAXAAQAGAAADgDQADgDABgFQgBgGgDgDQgDgDgGAAIgXAAgAgRgHIAVAAQAFAAADgDQACgCAAgFQAAgFgCgCQgDgCgGAAIgUAAg");
	this.shape_25.setTransform(28.2,6.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AASApIAAg6IgBAAIgdA6IgXAAIAAhRIATAAIAAA6IAAAAIAdg6IAXAAIAABRg");
	this.shape_26.setTransform(18.9,6.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AApApIgZgkIgIAJIAAAbIgPAAIAAgbIgIgJIgZAkIgWAAIAjguIgfgjIAWAAIAcAjIABAAIAAgjIAPAAIAAAjIABAAIAcgjIAWAAIgfAjIAjAug");
	this.shape_27.setTransform(8.1,6.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_28.setTransform(-7.4,6.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAQApIgYglIgLALIAAAaIgSAAIAAhRIASAAIAAAlIAgglIAVAAIgfAhIAjAwg");
	this.shape_29.setTransform(-15.9,6.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgiApIAAhRIASAAIAAAeIAXAAQAOAAAHAHQAHAFAAAMQAAANgHAHQgHAHgOAAgAgQAbIAVAAQAHgBACgDQADgDAAgFQAAgGgDgDQgDgEgGAAIgVAAg");
	this.shape_30.setTransform(-24.8,6.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgiApIgFgBIAAgOIADAAIAEAAQADAAACgBQADgCABgEQACgFAAgIIACgvIA7AAIAABRIgSAAIAAhCIgYAAIgBAiQAAAQgHAJQgGAJgLAAIgHgBg");
	this.shape_31.setTransform(-34.3,6.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgUAmQgKgFgFgKQgFgKAAgNQAAgMAFgKQAFgJAKgGQAJgFALAAQANAAAJAFQAJAGAFAJQAFAKAAAMQAAANgFAKQgFAKgJAFQgJAFgNAAQgLAAgJgFgAgPgUQgHAIAAAMQAAANAHAIQAGAIAJAAQALAAAGgIQAGgIAAgNQAAgMgGgIQgGgIgLAAQgJAAgGAIg");
	this.shape_32.setTransform(-43.3,6.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgIA5IAAhfIglAAIAAgRIBbAAIAAARIglAAIAABfg");
	this.shape_33.setTransform(-52.7,4.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-59.5,-6,134.5,61.4);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#122A33").s().p("A/DHSMAnTgmFIW0XjMgnUAmEg");
	this.shape.setTransform(-5.5,188.5,0.188,0.188);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DBA577").s().p("At6OHIB4h0QAqgsAfg7QAvhaAEhmIgiu8QAAhMAXhNQAmh6BbhYIGbmOIAWgSQAdgUAjgRQBwg3CMACIIuAAIAZADQAdAGAVAPQBBAugxB0IgjBTQgtBahSAvQg3AghCAIImpAVQgnAGgaAaQgmAlgBBLIADBbIAQBFQAZBSAqBAIBgCTQAUAlAMAxQApCegrDaIgkDRQgaCghGCDQguBXg6A9In7Htg");
	this.shape_1.setTransform(22,137.2,0.188,0.188);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#DBA57D").s().p("AhEEkQhfAAhDhEQhChEADhhQADheBFhEIB+h4QBGhEBdAAQBgAABCBFQBCBEgCBgQgDBehGBDIh9B6QhFBDhdAAIgCAAg");
	this.shape_2.setTransform(64.4,151.5,0.188,0.188);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#DBA57D").s().p("AkdEUQhBhEAEhhQADhhBIhDIDijcQBGhFBigBQBgAABCBEQBCBEgDBhQgEBhhHBDIjkDcQhFBFhhABIgCAAQhgAAhChEg");
	this.shape_3.setTransform(63.2,140.2,0.188,0.188);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DBA57D").s().p("Ai4GVQhdgEhChEQhChEgBhdQgBhcBBg/IF2lqQBBg/BdAEQBcADBCBFQBCBEABBcQABBdhBA/Il2FqQg+A7hWAAIgJAAg");
	this.shape_4.setTransform(63.2,127.8,0.188,0.188);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#DBA57D").s().p("AiGFkQhgAAhChEQhChEAChgQADhgBGhCIECj6QBEhEBgABQBgAABCBEQBCBEgDBgQgCBghGBCIkDD6QhDBDhfAAIgBAAg");
	this.shape_5.setTransform(61.3,116.6,0.188,0.188);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#302825").s().p("AhYgUICxAAIixApg");
	this.shape_6.setTransform(46.3,122.6,0.188,0.188);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#4A403B").s().p("AhYgUICxAAIAAApg");
	this.shape_7.setTransform(40.4,122.6,0.188,0.188);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#C6C6C6").s().p("Ahvg2IAGAAQA3gEAyggQBUg4Ach5IAAAAQAAASgFAnQgJBDgSBEQg7DRiECGg");
	this.shape_8.setTransform(45.4,141.7,0.188,0.188);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E3E3E3").s().p("AhPhPQgThEgIhDIgGg1QAdB5BUA4QAzAgA2AEIAGAAIAAFCQiFiHg6jUg");
	this.shape_9.setTransform(41.1,141.7,0.188,0.188);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#302825").s().p("AB1BjQglishQgIQg1gGgvATQg6AXgwADIAAlaQBzAABUA2QBpBFBKClQAZA4AIBaQAEAtgCAjIgBAAQgSAKgOAoQgXBGAICGIgRAJQAAipgZh5g");
	this.shape_10.setTransform(47.2,119.4,0.188,0.188);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#4A403B").s().p("AieF8QAJiGgYhGQgOgpgSgJIgBAAQgBgjADgtQAIhaAZg4QBIijBxhHQBXg2BqAAIAAFaQgwgDg5gXQgwgTg2AGQhPAIglCsQgZB5AACpg");
	this.shape_11.setTransform(39.4,119.4,0.188,0.188);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#DDAE8A").s().p("Aj6G/IAAtVQAwgDA6gXQAvgTA3AGQBOAIAlCuQAZB5AACnIARgJQgIiEAXhGQAOgoASgKQA8gaAUAxQAQAmgKBKQgIA1gfApQgfAlgqANQgQBmgWBIQgbBUgeAaQgoAkhcApQhmArg4AAgAiyitICygqIiyAAg");
	this.shape_12.setTransform(48,126.3,0.188,0.188);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#ECC29E").s().p("AD7G/Qg5AAhlgrQhdgpgogkQgegagahUQgXhIgQhmQgqgNgfglQgfgpgHg1QgLhKAQgmQAUgxA8AaQATAJANApQAYBGgICEIARAJQAAinAZh5QAkiuBOgIQA3gGAwATQA5AXAxADIAANVgAC/itIAAgqIizAAg");
	this.shape_13.setTransform(38.5,126.3,0.188,0.188);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#454545").s().p("AscGAQANhMAXhqQAtjRAqiVIALgRIAKgMIAEgGIADgCIAJgJIATgQIAygiIB5g6IAhgMIABgBQBSgfBpgdIAGA2QAIBDATBDQA6DVCGCHQCFiGA8jSQAShDAJhDQAFgoAAgRIAAgBQBcAaBOAcIACAAIAOAGIAEABIB/A4IArAZIAsAgIAZAYIACADIAFAFIAIAMIAFAJQA2CIAoDWQAUBtAKBRg");
	this.shape_14.setTransform(43.3,143.9,0.188,0.188);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#CEA082").s().p("AhviJIABAAQA5AABjgsIAACGIBCARQgcB4hUA3QgyAhg3ADIgGABg");
	this.shape_15.setTransform(45.4,137.3,0.188,0.188);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E2BB9C").s().p("ABqC1Qg2gDgzghQhUg3gdh4IBEgRIAAiGQBiAsA5AAIABAAIAAE/g");
	this.shape_16.setTransform(41.1,137.3,0.188,0.188);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFCF8").s().p("AgDBgIAAi/IAHAAIAAC/g");
	this.shape_17.setTransform(28.9,97.3,0.188,0.188);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E3E3E3").s().p("AhbAbIAAg1IC3AAIAAA1g");
	this.shape_18.setTransform(43.8,103.8,0.188,0.188);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E3E3E3").s().p("AgzAbIAAg1IBnAAIAAA1g");
	this.shape_19.setTransform(40.1,103.8,0.188,0.188);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E3E3E3").s().p("AivAbIAAg1IFfAAIAAA1g");
	this.shape_20.setTransform(34.6,103.8,0.188,0.188);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFCF8").s().p("AhRAAIA3g1IATATIgVAVIBuAAIAAAaIhuAAIAVAWIgTAUg");
	this.shape_21.setTransform(25.6,97.3,0.188,0.188);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#454545").p("AAVAAQAAAIgGAGQgHAGgIAAQgHAAgHgGQgGgGAAgIQAAgHAGgHQAHgGAHAAQAIAAAHAGQAGAHAAAHg");
	this.shape_22.setTransform(54.8,155.3,0.188,0.188);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#454545").p("AAVAAQAAAIgGAHQgHAGgIAAQgHAAgHgGQgGgHAAgIQAAgHAGgHQAHgFAHAAQAIAAAHAFQAGAHAAAHg");
	this.shape_23.setTransform(54.8,154.2,0.188,0.188);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#454545").p("AAVAAQAAAIgGAGQgHAGgIAAQgHAAgHgGQgGgGAAgIQAAgHAGgHQAHgGAHAAQAIAAAHAGQAGAHAAAHg");
	this.shape_24.setTransform(54.8,153,0.188,0.188);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#F0F0F0").s().p("AgIAHIAAgNIARAAIAAANg");
	this.shape_25.setTransform(50.2,93.5,0.188,0.188);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#F0F0F0").s().p("AgIAOIAAgbIARAAIAAAbg");
	this.shape_26.setTransform(50.7,93.4,0.188,0.188);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#F0F0F0").s().p("AgIATIAAglIARAAIAAAlg");
	this.shape_27.setTransform(51.3,93.3,0.188,0.188);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#F0F0F0").s().p("AgIAZIAAgxIARAAIAAAxg");
	this.shape_28.setTransform(51.8,93.1,0.188,0.188);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#F0F0F0").s().p("AgIAfIAAg9IARAAIAAA9g");
	this.shape_29.setTransform(52.4,93,0.188,0.188);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#F0F0F0").s().p("AgoASIAAgjIBRAAIAAAjg");
	this.shape_30.setTransform(55.6,93.1,0.188,0.188);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#F0F0F0").ss(0.8).p("Ag4gZIAAANIgKAAIAAAZIAKAAIAAANIB7AAIAAgzg");
	this.shape_31.setTransform(55.2,93.1,0.188,0.188);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#F0F0F0").s().p("AgBAEIgBgEIABgCQAAgBABAAQAAAAAAAAQAAgBAAAAQAAAAAAAAQAAAAAAAAQABAAAAABQAAAAABAAQAAAAAAABIABACIgBAEIgCABIgBgBgAAAgBIAAABIAAADIAAABIABgBIABgDIgBgBIgBgBIAAABg");
	this.shape_32.setTransform(60.2,93);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#F0F0F0").s().p("AgCAEIAAgBIABAAIABABIABgBIAAgBIAAgBIgBgBIgBAAIAAAAIABAAQAAAAAAAAQAAAAAAAAQABAAAAAAQAAAAAAgBIAAgBIgBAAIAAAAIgBABIgBgCIACgBIACABIABACIgBABIgBAAIACAAIAAACIgBACIgCABIgCgBg");
	this.shape_33.setTransform(59.4,93);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#F0F0F0").s().p("AAAADIAAAAIAAgBIAAAAIAAgBIAAABIABAAIgBABgAAAAAIAAgBIAAgBIAAAAIABABIgBABIAAAAIAAAAg");
	this.shape_34.setTransform(58.8,93.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#F0F0F0").s().p("AAAAEIAAgEIAAgCIAAABIAAABIgBgBIABgDIACAAIAAAIg");
	this.shape_35.setTransform(58,93.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#F0F0F0").s().p("AAAAEIAAgEIAAgCIAAABIAAABIgBgBIABgDIACAAIAAAIg");
	this.shape_36.setTransform(57.2,93.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#5D5D5D").ss(4).p("AxmAAMAjNAAA");
	this.shape_37.setTransform(43.2,91.3,0.188,0.188);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#5D5D5D").ss(4).p("AxmAAMAjNAAA");
	this.shape_38.setTransform(43.2,157.2,0.188,0.188);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#EF6E4E").s().p("AhKBMQggggAAgsQAAgrAggfQAfggArAAQAsAAAgAgQAfAfAAArQAAAsgfAgQggAfgsAAQgrAAgfgfg");
	this.shape_39.setTransform(26.4,103.7,0.188,0.188);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#454545").p("ABYBUIgNgyQAHgPAAgTQAAgjgZgZQgagZgiAAQgjAAgaAZQgZAZAAAjQAAAjAZAaQAaAZAjAAQAZAAAWgPg");
	this.shape_40.setTransform(32.6,154,0.188,0.188,0,0,0,-0.1,0);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#454545").p("AAqhWQgZAAgRASQgPgSgZAAQgYAAgRARQgRARAAAXQAAAKAFAPIAEAJQAPAfAoAcQATAOAPAIQAQgIAUgOQAngcAPgfIAFgJQAEgNAAgMQAAgXgRgRQgRgRgXAAg");
	this.shape_41.setTransform(26.6,154.2,0.188,0.188);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#C1ECF7").s().p("AxmSSMAAAgkjMAjNAAAMAAAAkjg");
	this.shape_42.setTransform(43.2,129.1,0.188,0.188);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#71A1CC").s().p("AxmCYIAAkvMAjNAAAIAAEvg");
	this.shape_43.setTransform(43.2,97.3,0.188,0.188);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#5B87B9").s().p("AxmBdIAAi5MAjNAAAIAAC5g");
	this.shape_44.setTransform(43.2,93,0.188,0.188);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFCF8").s().p("AxmbTMAAAg2lMAjNAAAMAAAA2lg");
	this.shape_45.setTransform(43.2,124,0.188,0.188);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#E3E3E3").p("ACSAAQAAA8grArQgrArg8AAQg7AAgrgrQgrgqAAg9QAAg7ArgrQArgrA7AAQA8AAArArQArArAAA7g");
	this.shape_46.setTransform(43.2,163.1,0.188,0.188);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#E3E3E3").p("ACzAAQAABKg1A0Qg0A1hKAAQhIAAg1g1Qg1g0AAhKQAAhJA1g0QA0g1BJAAQBKAAA0A1QA1A0AABJg");
	this.shape_47.setTransform(43.2,163.1,0.188,0.188);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#1A1A1A").s().p("EgO/AkdQhFAAgzgyQgygyABhGMAAAhDlQgBhGAygyQAzgyBFAAId/AAQBFAAAyAyQAzAyAABGMAAABDlQAABGgzAyQgyAyhFAAg");
	this.shape_48.setTransform(43.2,124.4,0.188,0.188);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#BF8358").s().p("AymOCMAlNgr2MgAGAmfI10VKg");
	this.shape_49.setTransform(21.7,147.3,0.188,0.188);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.8,80.7,113.7,144.9);


(lib.Symbol2copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgPnAtKMAAAhaTIfOcIMAAAA+Lg");
	this.shape.setTransform(100,229);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-60,200,578);


(lib.Symbol2copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A1B400").s().p("EgPnAtUMAAAhanIfOcIMAAAA+fg");
	this.shape.setTransform(100,230);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-60,200,580);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#30AFB8").s().p("EgPnAtUMAAAhanIfOcIMAAAA+fg");
	this.shape.setTransform(100,230);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-60,200,580);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF5151").s().p("EgPnAxOMAAAhiaMAfOAjtMAAAA+tg");
	this.shape.setTransform(100,87);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-228,200,630);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgKA/IAAgXIAVAAIAAAXgAgEAcIgGg1IAAglIAUAAIAAAkIgFA2g");
	this.shape.setTransform(-39.8,35.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgYAqQgKgFgFgLQgHgMABgOQgBgOAHgLQAFgLAKgGQAMgGAMAAQAOAAALAGQAKAGAGALQAFALAAAOQAAAOgFAMQgGALgKAFQgLAGgOABQgNgBgLgGgAgSgXQgHAJAAAOQAAAPAHAJQAHAIALABQAMgBAGgIQAIgJgBgPQABgOgIgJQgGgIgMgBQgLABgHAIg");
	this.shape_1.setTransform(-47.6,37.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAUAuIAAgoIgnAAIAAAoIgUAAIAAhbIAUAAIAAAkIAnAAIAAgkIAUAAIAABbg");
	this.shape_2.setTransform(-58.1,37.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgmAuIAAhbIAUAAIAAAiIAaAAQAPAAAJAIQAHAFABAOQgBAOgHAIQgJAIgPAAgAgSAeIAYAAQAHAAADgEQAEgDgBgHQAAgGgDgEQgEgDgGAAIgYAAg");
	this.shape_3.setTransform(-67.8,37.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgmAvIgGgCIAAgQIAEABIAEAAQADAAACgCQADgCACgEQACgGAAgJIACg1IBDAAIAABbIgVAAIAAhLIgaAAIgCAnQAAASgHAKQgIAKgMAAIgHAAg");
	this.shape_4.setTransform(-78.5,37.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgWAqQgKgGgGgLQgFgLAAgOQAAgMAGgMQAGgLAKgGQAKgHALAAQAOAAAJAHQALAGAFALQAFAMAAAOIAAACIhCAAQAAAOAHAIQAGAIAKAAQAIAAAGgEQAFgEADgJIAUAAQgCAKgHAHQgFAHgJAEQgIAEgLAAQgNAAgKgHgAAXgHQAAgMgHgHQgGgGgKAAQgIAAgGAGQgHAGgBANIAtAAIAAAAg");
	this.shape_5.setTransform(-88.2,37.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgJAuIAAhLIgeAAIAAgQIBPAAIAAAQIgeAAIAABLg");
	this.shape_6.setTransform(-97.5,37.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAYAuIAAgmIgRAAIgbAmIgYAAIAfgoQgIgDgFgDQgEgDgBgFIgCgLQAAgMAIgHQAHgHAQAAIAvAAIAABbgAgJgbQgEADABAGQgBAHAEADQAEAEAFAAIAYAAIAAgZIgYAAQgFAAgEACg");
	this.shape_7.setTransform(-107.2,37.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgYAqQgKgFgFgLQgHgMABgOQgBgOAHgLQAFgLAKgGQAMgGAMAAQAOAAALAGQAKAGAGALQAFALAAAOQAAAOgFAMQgGALgKAFQgLAGgOABQgNgBgLgGgAgSgXQgHAJAAAOQAAAPAHAJQAHAIALABQAMgBAGgIQAIgJgBgPQABgOgIgJQgGgIgMgBQgLABgHAIg");
	this.shape_8.setTransform(-116.9,37.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgJAuIAAhLIgeAAIAAgQIBPAAIAAAQIgeAAIAABLg");
	this.shape_9.setTransform(-126.4,37.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgVAqQgKgGgGgLQgFgLgBgOQABgNAFgLQAGgMAKgFQALgHAMABQASgBAKAKQALAIACARIgUAAQgCgKgFgEQgGgFgIAAQgKAAgHAJQgHAJAAAOQAAAPAHAIQAGAJAKAAQAJAAAGgGQAGgFACgKIAUAAQgDARgLAKQgLAKgSABQgMgBgKgGg");
	this.shape_10.setTransform(-135.5,37.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgXAqQgLgFgFgLQgHgMAAgOQAAgOAHgLQAFgLALgGQAKgGANAAQAOAAAKAGQALAGAFALQAGALABAOQgBAOgGAMQgFALgLAFQgKAGgOABQgNgBgKgGgAgSgXQgHAJAAAOQAAAPAHAJQAHAIALABQAMgBAHgIQAGgJAAgPQAAgOgGgJQgIgIgLgBQgLABgHAIg");
	this.shape_11.setTransform(-145.8,37.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAhAuIAAg8IgBAAIgWA8IgTAAIgWg8IgBAAIAAA8IgTAAIAAhbIAbAAIAYBIIAAAAIAZhIIAbAAIAABbg");
	this.shape_12.setTransform(-157.5,37.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAWAuQgEgDgBgGQgGAGgJADQgGADgJAAQgPgBgIgHQgJgHAAgNQAAgJAEgGQAEgFAGgBQAGgDAHgBIAPgDIAJgBIAGgCIADgCIABgCIAAgFQAAgHgEgDQgFgEgIAAQgKAAgEAEQgFAEgBAJIgUAAQACgRALgHQALgIARABQAJAAAIACQAJADAFAGQAFAHAAAKIAAAtQAAAEABABQAAAAABABQAAAAABAAQAAAAABAAQAAABABAAIACgBIADAAIAAAPIgIABIgGABQgGAAgEgDgAAGADIgJACIgKACQgFABgDADQgDADAAAGQAAAHAFADQAEAEAIAAQAGAAAEgCQAGgCADgDQADgEABgEIAAgIIAAgLQgFACgFABg");
	this.shape_13.setTransform(-168.6,37.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgVAqQgKgGgGgLQgFgLgBgOQABgNAFgLQAGgMAKgFQALgHAMABQASgBAKAKQALAIACARIgUAAQgCgKgFgEQgGgFgIAAQgKAAgHAJQgHAJAAAOQAAAPAHAIQAGAJAKAAQAJAAAGgGQAGgFACgKIAUAAQgDARgLAKQgLAKgSABQgMgBgKgGg");
	this.shape_14.setTransform(-178.8,37.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAgAuIAAg8IAAAAIgWA8IgTAAIgWg8IgBAAIAAA8IgTAAIAAhbIAbAAIAYBIIAAAAIAZhIIAbAAIAABbg");
	this.shape_15.setTransform(-12.4,15.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAWAuQgEgDgBgGQgGAGgJADQgGADgJAAQgPgBgIgHQgJgHAAgNQAAgJAEgGQAEgFAGgBQAGgDAHgBIAPgDIAJgBIAGgCIADgCIABgCIAAgFQAAgHgEgDQgFgEgIAAQgKAAgEAEQgFAEgBAJIgUAAQACgRALgHQALgIARABQAJAAAIACQAJADAFAGQAFAHAAAKIAAAtQAAAEABABQAAAAABABQAAAAABAAQAAAAABAAQAAABABAAIACgBIADAAIAAAPIgIABIgGABQgGAAgEgDgAAGADIgJACIgKACQgFABgDADQgDADAAAGQAAAHAFADQAEAEAIAAQAGAAAEgCQAGgCADgDQADgEABgEIAAgIIAAgLQgFACgFABg");
	this.shape_16.setTransform(-23.6,15.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgtBAIAAh9IAUAAIAAANQAFgIAIgEQAIgDAHAAQANgBAJAHQAKAFAFAMQAGALgBAPQAAAOgEAJQgGALgJAGQgJAGgNABQgHgBgIgDQgHgDgGgJIAAAvgAgSgmQgGAIgBAPQABAPAGAIQAHAIALAAQALAAAHgIQAGgIABgOQgBgPgGgJQgHgJgLAAQgLAAgHAJg");
	this.shape_17.setTransform(-34.1,17.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgfAuIAAhbIA/AAIAAAQIgqAAIAABLg");
	this.shape_18.setTransform(-43.1,15.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAWAuQgEgDgBgGQgGAGgJADQgGADgJAAQgPgBgIgHQgJgHAAgNQAAgJAEgGQAEgFAGgBQAGgDAHgBIAPgDIAJgBIAGgCIADgCIABgCIAAgFQAAgHgEgDQgFgEgIAAQgKAAgEAEQgFAEgBAJIgUAAQACgRALgHQALgIARABQAJAAAIACQAJADAFAGQAFAHAAAKIAAAtQAAAEABABQAAAAABABQAAAAAAAAQABAAABAAQAAABABAAIACgBIADAAIAAAPIgIABIgGABQgGAAgEgDgAAGADIgJACIgKACQgFABgDADQgDADAAAGQAAAHAFADQAEAEAIAAQAGAAAEgCQAGgCADgDQADgEABgEIAAgIIAAgLQgFACgFABg");
	this.shape_19.setTransform(-52.2,15.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgJAuIAAhLIgeAAIAAgQIBPAAIAAAQIgeAAIAABLg");
	this.shape_20.setTransform(-61.6,15.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgVAqQgKgGgGgLQgFgLgBgOQABgNAFgLQAGgMAKgFQALgHAMABQASgBAKAKQALAIACARIgUAAQgCgKgFgEQgGgFgIAAQgKAAgHAJQgHAJAAAOQAAAPAHAJQAGAIAKAAQAJAAAGgGQAGgFACgKIAUAAQgDARgLAKQgLAKgSABQgMgBgKgGg");
	this.shape_21.setTransform(-70.7,15.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAUAuIAAgpIgnAAIAAApIgUAAIAAhbIAUAAIAAAkIAnAAIAAgkIAUAAIAABbg");
	this.shape_22.setTransform(-81,15.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAUAuIAAhBIgBAAIghBBIgZAAIAAhbIAUAAIAABCIABAAIAghCIAaAAIAABbg");
	this.shape_23.setTransform(-91.3,15.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgoAuIAAhbIAxAAQANAAAIAGQAGAGABALQgBAHgDAFQgDAFgHADIAAAAQAJACAFAFQAEAFAAAKQgBANgIAGQgJAHgRAAgAgUAfIAaAAQAIAAADgEQADgDABgGQgBgGgDgEQgDgDgIAAIgaAAgAgUgIIAYAAQAGAAADgDQAEgDgBgFQAAgGgDgCQgDgDgGABIgYAAg");
	this.shape_24.setTransform(-106.2,15.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgJAuIAAhLIgeAAIAAgQIBPAAIAAAQIgeAAIAABLg");
	this.shape_25.setTransform(-120.7,15.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAUAuIAAgpIgnAAIAAApIgUAAIAAhbIAUAAIAAAkIAnAAIAAgkIAUAAIAABbg");
	this.shape_26.setTransform(-130,15.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgcA/IgIAAIAAgSIAHABIAFABQAEgBAEgDQACgEACgEIADgJIgkhZIAXAAIAXBDIAXhDIAWAAIgiBZIgIASQgDAIgFAGQgGAFgMABIgGgBg");
	this.shape_27.setTransform(-139.9,17.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAWAuQgEgDgBgGQgGAGgJADQgGADgJAAQgPgBgIgHQgJgHAAgNQAAgJAEgGQAEgFAGgBQAGgDAHgBIAPgDIAJgBIAGgCIADgCIABgCIAAgFQAAgHgEgDQgFgEgIAAQgKAAgEAEQgFAEgBAJIgUAAQACgRALgHQALgIARABQAJAAAIACQAJADAFAGQAFAHAAAKIAAAtQAAAEABABQAAAAABABQAAAAAAAAQABAAABAAQAAABABAAIACgBIADAAIAAAPIgIABIgGABQgGAAgEgDgAAGADIgJACIgKACQgFABgDADQgDADAAAGQAAAHAFADQAEAEAIAAQAGAAAEgCQAGgCADgDQADgEABgEIAAgIIAAgLQgFACgFABg");
	this.shape_28.setTransform(-149.3,15.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AASAuIgbgpIgMAMIAAAdIgVAAIAAhbIAVAAIAAAqIAjgqIAZAAIglAlIApA2g");
	this.shape_29.setTransform(-158.8,15.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AASAuIgbgpIgMAMIAAAdIgVAAIAAhbIAVAAIAAAqIAjgqIAZAAIglAlIApA2g");
	this.shape_30.setTransform(-168.4,15.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AAWAuQgEgDgBgGQgGAGgJADQgGADgJAAQgPgBgIgHQgJgHAAgNQAAgJAEgGQAEgFAGgBQAGgDAHgBIAPgDIAJgBIAGgCIADgCIABgCIAAgFQAAgHgEgDQgFgEgIAAQgKAAgEAEQgFAEgBAJIgUAAQACgRALgHQALgIARABQAJAAAIACQAJADAFAGQAFAHAAAKIAAAtQAAAEABABQAAAAABABQAAAAABAAQAAAAABAAQAAABABAAIACgBIADAAIAAAPIgIABIgGABQgGAAgEgDgAAGADIgJACIgKACQgFABgDADQgDADAAAGQAAAHAFADQAEAEAIAAQAGAAAEgCQAGgCADgDQADgEABgEIAAgIIAAgLQgFACgFABg");
	this.shape_31.setTransform(-178.6,15.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAUBBIAAhCIgBAAIghBCIgZAAIAAhbIAUAAIAABCIABAAIAghCIAaAAIAABbgAgUgtQgHgGgBgNIAKAAQACAHAFADQAEADAHAAQAHAAAFgDQAFgDACgHIAKAAQgBANgIAGQgHAGgNAAQgMAAgIgGg");
	this.shape_32.setTransform(-62.4,-8.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgYAqQgKgFgFgLQgHgMABgOQgBgOAHgLQAFgLAKgGQAMgGAMAAQAOAAALAGQAKAGAGALQAFALAAAOQAAAOgFAMQgGALgKAFQgLAGgOABQgNgBgLgGgAgSgXQgHAJAAAOQAAAPAHAJQAHAIALABQAMgBAGgIQAIgJgBgPQABgOgIgJQgGgIgMgBQgLABgHAIg");
	this.shape_33.setTransform(-72.8,-6.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgoAuIAAhbIAxAAQANAAAHAGQAIAGgBALQAAAHgCAFQgEAFgGADIAAAAQAIACAEAFQAFAFAAAKQAAANgJAGQgJAHgQAAgAgTAfIAaAAQAGAAAEgEQAEgDgBgGQABgGgEgEQgEgDgGAAIgaAAgAgTgIIAXAAQAGAAADgDQADgDABgFQAAgGgEgCQgDgDgGABIgXAAg");
	this.shape_34.setTransform(-82.9,-6.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgVAqQgKgGgGgLQgFgLgBgOQABgNAFgLQAGgMAKgFQALgHAMABQASgBAKAKQALAIACARIgUAAQgCgKgFgEQgGgFgIAAQgKAAgHAJQgHAJAAAOQAAAPAHAIQAGAJAKAAQAJAAAGgGQAGgFACgKIAUAAQgDARgLAKQgLAKgSABQgMgBgKgGg");
	this.shape_35.setTransform(-93.1,-6.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAUAuIAAhCIgBAAIghBCIgZAAIAAhbIAUAAIAABCIABAAIAghCIAaAAIAABbg");
	this.shape_36.setTransform(-108.3,-6.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgJAuIAAhLIgeAAIAAgQIBPAAIAAAQIgeAAIAABLg");
	this.shape_37.setTransform(-117.7,-6.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgbA/IgIAAIAAgSIAGABIAEABQAFgBAEgDQADgEABgEIADgJIgjhZIAWAAIAXBDIAYhDIAUAAIgiBZIgHASQgDAIgFAGQgGAFgMABIgFgBg");
	this.shape_38.setTransform(-126.5,-4.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgtBAIAAh9IAUAAIAAANQAFgIAIgEQAIgDAHAAQANgBAJAHQAKAFAFAMQAGALgBAPQAAAOgEAJQgGALgJAGQgJAGgNABQgHgBgIgDQgIgDgFgJIAAAvgAgSgmQgHAIAAAPQAAAPAHAHQAHAJALAAQALAAAHgJQAGgHABgOQgBgPgGgJQgHgJgLAAQgLAAgHAJg");
	this.shape_39.setTransform(-136.5,-4.8);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AASAuIgbgpIgMAMIAAAdIgVAAIAAhbIAVAAIAAAqIAjgqIAZAAIglAlIApA2g");
	this.shape_40.setTransform(-146.4,-6.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgVAqQgKgGgGgLQgFgLgBgOQABgNAFgLQAGgMAKgFQALgHAMABQASgBAKAKQALAIACARIgUAAQgCgKgFgEQgGgFgIAAQgKAAgHAJQgHAJAAAOQAAAPAHAIQAGAJAKAAQAJAAAGgGQAGgFACgKIAUAAQgDARgLAKQgLAKgSABQgMgBgKgGg");
	this.shape_41.setTransform(-156.8,-6.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AAWAuQgEgDgBgGQgGAGgJADQgGADgJAAQgPgBgIgHQgJgHAAgNQAAgJAEgGQAEgFAGgBQAGgDAHgBIAPgDIAJgBIAGgCIADgCIABgCIAAgFQAAgHgEgDQgFgEgIAAQgKAAgEAEQgFAEgBAJIgUAAQACgRALgHQALgIARABQAJAAAIACQAJADAFAGQAFAHAAAKIAAAtQAAAEABABQAAAAABABQAAAAAAAAQABAAABAAQAAABABAAIACgBIADAAIAAAPIgIABIgGABQgGAAgEgDgAAGADIgJACIgKACQgFABgDADQgDADAAAGQAAAHAFADQAEAEAIAAQAGAAAEgCQAGgCADgDQADgEABgEIAAgIIAAgLQgFACgFABg");
	this.shape_42.setTransform(-166.6,-6.5);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgwA/IAAh9IA1AAQANAAALADQAJAFAGAJQAFAIABANQgBATgKAIQgMALgTAAIgiAAIAAAxgAgagCIAdAAQAMAAAGgFQAHgFgBgLQAAgIgDgFQgEgEgGgCQgFgCgIAAIgbAAg");
	this.shape_43.setTransform(-177.4,-8.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-185.8,-20,186.9,68.2);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_35 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(35).call(this.frame_35).wait(1));

	// Layer 5
	this.instance = new lib.Symbol18("synched",0);
	this.instance.setTransform(132.3,-80,0.497,0.497,0,0,0,85,85);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:10},9,cjs.Ease.get(1)).to({y:0},5,cjs.Ease.get(1)).wait(22));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(90,-122.2,84.6,84.6);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 15
	this.instance = new lib.Symbol15("synched",0);
	this.instance.setTransform(132.2,53.1,0.835,0.835,0,0,0,132.2,21.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Бесплатно 3 дня
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#55B553").s().p("AAWAqIAAgiIgQAAIgYAiIgWAAIAcgkQgHgDgEgDQgEgCgCgFQgBgFAAgFQABgLAGgHQAIgGANAAIArAAIAABTgAgIgYQgDADAAAFQAAAGADADQADADAFAAIAWAAIAAgXIgWAAQgFAAgDADg");
	this.shape.setTransform(192.4,22.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#55B553").s().p("AASAqIAAglIgjAAIAAAlIgSAAIAAhTIASAAIAAAhIAjAAIAAghIASAAIAABTg");
	this.shape_1.setTransform(183.7,22.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#55B553").s().p("AAeA3IAAgbIg6AAIAAAbIgSAAIAAgpIALAAIAFgOQACgJABgLQACgLAAgOIAAgJIA9AAIAABEIALAAIAAApgAgIgiIgBAUIgDAPIgEANIAiAAIAAg2IgaAAg");
	this.shape_2.setTransform(174.1,23.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#55B553").s().p("AgUA1QgJgEgFgIQgFgJAAgMIAAgCIASAAQAAANAGAFQAGAHAJAAQAJgBAFgFQAGgGAAgJQAAgHgDgFQgDgDgFgCQgGgCgEgBIgDAAIgDABIAAgMIACAAIAJgBQAFgCAEgDQADgDAAgIQAAgHgEgEQgFgFgHgBQgIAAgFAHQgFAFAAALIgSAAQABgSAJgJQAKgKAQAAQAKAAAIADQAIAEAEAHQAFAHAAAJQAAAIgFAHQgEAGgHADQAKACAFAFQAFAHABALQgBAKgFAJQgFAIgJAEQgJAFgLABQgLgBgJgFg");
	this.shape_3.setTransform(160,20.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#55B553").s().p("AgVAnQgKgGgFgKQgFgKAAgNQAAgMAFgKQAFgKAKgGQAKgFALAAQANAAAJAFQAKAGAFAKQAFAKAAAMQAAANgFAKQgFAKgKAGQgJAFgNAAQgMAAgJgFgAgQgUQgGAHgBANQABAOAGAIQAGAIAKAAQALAAAGgIQAGgIAAgOQAAgNgGgIQgGgHgLAAQgKAAgGAIg");
	this.shape_4.setTransform(146.1,22.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#55B553").s().p("AASAqIAAglIgjAAIAAAlIgSAAIAAhTIASAAIAAAhIAjAAIAAghIATAAIAABTg");
	this.shape_5.setTransform(136.6,22.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#55B553").s().p("AgIAqIAAhEIgbAAIAAgPIBIAAIAAAPIgcAAIAABEg");
	this.shape_6.setTransform(128.1,22.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#55B553").s().p("AAdAsQgFAAgEgCQgDgCgCgGQgFAGgIACQgGACgIAAQgNAAgIgGQgHgHAAgMQAAgIADgFQADgFAGgBIAMgEIANgCIAIgBIAGgBIADgCIAAgDIABgEQAAgGgEgDQgEgDgIAAQgIAAgFADQgFAEgBAIIgRAAQABgQALgGQAJgHAQAAQAIAAAIADQAIACAEAGQAFAGAAAJIAAApQAAAAAAABQAAABAAAAQAAABAAAAQAAABAAAAQABAAAAABQAAAAABAAQAAAAABAAQAAABABAAIACAAIACgBIAAANIgGACIgGAAgAAFADIgIABIgJACQgEABgDADQgDADAAAFQAAAGAFAEQAEADAHAAQAGAAADgCQAEgBAEgEQADgDAAgDIABgIIAAgKIgKADg");
	this.shape_7.setTransform(119.9,22.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#55B553").s().p("AgiAqIgGgBIAAgPIAEABIADAAQADAAACgCQADgBACgFQACgEAAgJIABgwIA9AAIAABTIgTAAIAAhEIgYAAIgBAjQgBARgGAJQgHAJgLAAIgGgBg");
	this.shape_8.setTransform(110.1,22.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#55B553").s().p("AASAqIAAhEIgjAAIAABEIgSAAIAAhTIBIAAIAABTg");
	this.shape_9.setTransform(101.1,22.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#55B553").s().p("AgTAnQgKgGgFgKQgFgJAAgNQAAgMAGgLQAFgKAJgGQAKgFAKAAQARAAAKAIQAJAIACAPIgSAAQgCgIgFgEQgFgFgHAAQgJABgGAHQgHAJAAAMQAAAOAGAHQAGAJAJgBQAIABAGgGQAFgEABgKIATAAQgCAQgLAJQgKAJgQAAQgLAAgJgFg");
	this.shape_10.setTransform(91.8,22.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#55B553").s().p("AgTAnQgKgGgFgKQgFgKAAgNQAAgLAFgKQAFgKAKgGQAJgGAKAAQANAAAIAGQAKAFAEALQAFAKAAANIAAACIg9AAQABANAGAHQAGAHAJAAQAHAAAFgEQAGgEACgIIASAAQgCAKgGAGQgFAHgIADQgIADgKAAQgLAAgIgFgAAVgGQAAgLgGgGQgGgGgJAAQgHAAgGAGQgGAFgBAMIApAAIAAAAg");
	this.shape_11.setTransform(82.7,22.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#55B553").s().p("AguA6IAAhzIBUAAIAAASIhAAAIAAAbIAhAAQATAAALAJQAKAHAAASQAAARgKAJQgLAKgTAAgAgaApIAhAAQAJAAAGgFQAEgEAAgKQAAgJgFgFQgFgGgKAAIggAAg");
	this.shape_12.setTransform(72.6,20.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(19.2,9.2,225.8,64.5);


// stage content:
(lib._image = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.instance = new lib.Symbol27("synched",0);
	this.instance.setTransform(62.6,149.1,0.627,0.627,0,0,0,18.2,23.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(546).to({_off:false},0).to({y:111.4},11,cjs.Ease.get(1)).to({regX:18.1,rotation:-27},6).to({regX:18.2,rotation:0},6).to({regX:18.1,rotation:27,y:111.5},6).to({regX:18.2,rotation:0,y:111.4},5).wait(1));

	// Бесплатно 3 дня
	this.instance_1 = new lib.Symbol16();
	this.instance_1.setTransform(62.7,137.4,0.515,0.515,0,0,0,132.1,33.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(420).to({_off:false},0).to({y:96.2},12,cjs.Ease.get(1)).to({regY:33.9,y:101.4},5,cjs.Ease.get(1)).wait(144));

	// Symbol 14
	this.instance_2 = new lib.Symbol14();
	this.instance_2.setTransform(43.9,116.1,0.627,0.627,0,0,0,133.4,35.7);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(403).to({_off:false},0).to({y:65.9},12,cjs.Ease.get(1)).to({y:72.2},5,cjs.Ease.get(1)).wait(161));

	// Symbol 13
	this.instance_3 = new lib.Symbol13("synched",0);
	this.instance_3.setTransform(150.3,72.2,0.627,0.627,0,0,0,139.8,35.7);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(386).to({_off:false},0).to({regY:35.6,y:22},12,cjs.Ease.get(1)).to({regY:35.7,y:28.3},5,cjs.Ease.get(1)).wait(178));

	// Layer 8
	this.instance_4 = new lib.Symbol2copy3("synched",0);
	this.instance_4.setTransform(330.9,381.9,0.627,0.627,0,0,0,527.6,157.6);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(366).to({_off:false},0).to({y:23.7},20,cjs.Ease.get(1)).wait(195));

	// Layer 4
	this.instance_5 = new lib.Symbol23("synched",0);
	this.instance_5.setTransform(45,94.5,0.627,0.627,0,0,0,36.6,43.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(280).to({_off:false},0).to({y:31.8},9,cjs.Ease.get(1)).to({regY:43.4,y:38.1},4,cjs.Ease.get(1)).wait(45).to({startPosition:0},0).to({regX:36.7,scaleX:0.82,scaleY:0.82},6,cjs.Ease.get(1)).to({regX:36.6,scaleX:0.63,scaleY:0.63},6,cjs.Ease.get(1)).to({regX:36.7,regY:43.2,scaleX:0.82,scaleY:0.82,y:38},6,cjs.Ease.get(1)).to({regX:36.6,regY:43.4,scaleX:0.63,scaleY:0.63,y:38.1},6,cjs.Ease.get(1)).to({_off:true},25).wait(194));

	// 4.Автоматическое продвижение - минимум Вашего участия
	this.instance_6 = new lib.Symbol28("synched",0);
	this.instance_6.setTransform(371.3,71.6,0.627,0.627,0,0,0,407.9,18.2);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(275).to({_off:false},0).to({y:27.6},11,cjs.Ease.get(1)).to({_off:true},101).wait(194));

	// Layer 5
	this.instance_7 = new lib.Symbol2copy2("synched",0);
	this.instance_7.setTransform(330.9,381.9,0.627,0.627,0,0,0,527.6,157.6);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(255).to({_off:false},0).to({y:23.7},20,cjs.Ease.get(1)).to({_off:true},112).wait(194));

	// Layer 3
	this.instance_8 = new lib.Symbol29("synched",0);
	this.instance_8.setTransform(194.3,75.6,0.627,0.627);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(136).to({_off:false},0).to({y:38},10,cjs.Ease.get(1)).to({_off:true},241).wait(194));

	// Layer 1
	this.instance_9 = new lib.Symbol17();
	this.instance_9.setTransform(109.5,49.4,0.351,0.351,0,0,0,389.4,90.5);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(129).to({_off:false},0).to({_off:true},257).wait(195));

	// 2.Только живые подписчики с Вашего города. copy
	this.instance_10 = new lib.Symbol5copy("synched",0);
	this.instance_10.setTransform(283.5,66,0.627,0.627,0,0,0,336.2,19.2);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(127).to({_off:false},0).to({y:31},11,cjs.Ease.get(1)).to({regY:19.3,y:28.3},4).to({_off:true},131).wait(308));

	// 2.Только живые подписчики с Вашего города.
	this.instance_11 = new lib.Symbol5("synched",0);
	this.instance_11.setTransform(283.5,66,0.627,0.627,0,0,0,336.2,19.2);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(122).to({_off:false},0).to({y:31},11,cjs.Ease.get(1)).to({regY:19.3,y:28.3},4).to({_off:true},136).wait(308));

	// Layer 2
	this.instance_12 = new lib.Symbol2copy("synched",0);
	this.instance_12.setTransform(330.9,381.9,0.627,0.627,0,0,0,527.6,157.6);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(107).to({_off:false},0).to({y:23.7},20,cjs.Ease.get(1)).to({_off:true},259).wait(195));

	// instagram.png
	this.instance_13 = new lib.Symbol3("synched",0);
	this.instance_13.setTransform(-29.4,-15,1.525,1.525,0,0,0,53,40.1);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(28).to({_off:false},0).to({regY:40,x:58.4,y:-8.8},10,cjs.Ease.get(1)).to({regX:52.9,scaleX:0.63,scaleY:0.63,x:66.6,y:28.3},7,cjs.Ease.get(1)).wait(42).to({startPosition:0},0).to({regX:53.1,scaleX:1.25,scaleY:1.25,x:79.2,y:-7.4},11,cjs.Ease.get(1)).to({regX:52.9,scaleX:0.63,scaleY:0.63,x:66.6,y:28.3},9,cjs.Ease.get(1)).to({_off:true},20).wait(454));

	// 1.Раскрути свой аккаунт в инстаграм самостоятельно!
	this.instance_14 = new lib.Symbol1("synched",0);
	this.instance_14.setTransform(371.5,72.3,0.627,0.627,0,0,0,399.4,19.3);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(16).to({_off:false},0).to({y:28.3},12,cjs.Ease.get(1)).to({_off:true},99).wait(454));

	// Layer 10
	this.instance_15 = new lib.Symbol2("synched",0);
	this.instance_15.setTransform(330.9,495.6,0.627,0.627,0,0,0,527.6,157.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).to({y:99},20,cjs.Ease.get(1)).to({_off:true},107).wait(454));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(62.3,316.1,125.5,395.3);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;