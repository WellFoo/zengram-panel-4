var Account = can.Map.extend({
    id: 1,
    login: 'demo',

    options: {
        likes: false,
        comment: false,
        follows: false,
        unfollows: false
    },

    avatar: '/img/demo.png',

    photos: 0,
    newComments: 0,

    likesOption: false,
    commentsOption: false,
    followsOption: false,
    unfollowsOption: false,

    disabledOptions: {
        likes: false,
        comments: false,
        follows: false,
        unfollows: false
    },

    isActive: false,
	hideTimer: true,
	timerText: '',

    info: {
        followers: 0,
        newFollowers: 0,
        followings: 0
    },
    counters: {
        likes: 0,
        comments: 0,
        follows: 0,
        unfollows: 0
    },
    speed: 0,
    disabled: false,

    errors: {
        common:     false,
        likes:      false,
        comments:   false,
        follows:    false,
        unfollows:  false
    },

    show: true,
    demo: false,

    getErrors: function() {
        return {
            common:     this.attr('errors').common,
            likes:      this.attr('errors').likes,
            comments:   this.attr('errors').comments,
            follows:    this.attr('errors').follows,
            unfollows:  this.attr('errors').unfollows
        };
    },

    addError: function(option, message) {
        //var errors = this.getErrors(),
        //    self = this;

        //errors[option] = message;

        this.attr('errors.' + option, message);

        reInitPopovers('[data-account-container="' + this.attr('id') + '"]');
    },

    clearErrors: function() {
        this.attr('errors', {
            common: false,
            likes: false,
            comments: false,
            unfollows: false
        });

        reInitPopovers('[data-account-container="' + this.attr('id') + '"]');
    },

    toggleOptions: function(option) {
        var $option = $(this.getOption(option));

        $option.get(0).checked = !($(this.getOption(option)).get(0).checked);
        $option.change();

        this.attr(option + 'Options', $option.get(0).checked);

        reInitPopovers('[data-account-container="' + this.attr('id') + '"]');
    },

    getSpeedItem: function () {
        var items = this.speed_items;
        for (var i = 0; i < items.length; i++) {
            if (items[i].value == this.attr('speed')) {
                return items[i];
            }
        }
        return items[0];
    },

    setSpeed: function (speed) {
        if (speed >= 0) {
            $.post("/site/speed/" + this.attr('id'), {
                'speed': speed
            }, function (data) {
                if (data == 'ok') {
                    return true;
                }
            });
        }
    },

    getOption: function(option) {
        return $(document).find('.projectOption[data-account='+this.attr('id')+'][data-option="' + option + '"]')
    },

    updateOptionState: function (option, value) {
        var $option = this.getOption(option);

        if ($option.get(0) && !(!value) !== $option.get(0).checked) {
            this.toggleOptions(option);
        }
    },

    /*
     * Управление опцией комментирования
     */

    toggleComments: function() {
        this.getOption('comment').parent('.toggle').click();
    },

    commentOn: function() { this.updateOptionState('comment', true); },
    commentOff: function() { this.updateOptionState('comment', false); },

    updateInfo: function (option, value) {
        var info = {
            followers: this.attr('info').followers || 0,
            newFollowers: this.attr('info').newFollowers || 0,
            followings: this.attr('info').followings || 0
        };

        info[option] = parseInt(value);

        this.attr('info', info);
    },

    getStateOptions: function () {
        return {
            likes:      this.attr('disabledOptions').likes || false,
            comments:   this.attr('disabledOptions').comments || false,
            follows:    this.attr('disabledOptions').follows || false,
            unfollows:  this.attr('disabledOptions').unfollows || false
        }
    },

    lockOption: function (option) {
        var states = this.getStateOptions();

        states[option] = true;

        $(document)
            .find('[data-account-container="'+this.attr('id')+'"] .' + option + '-option-toggle .toggle')
            .addClass('disabled');

        this.attr('disabledOptions', states);
    },

    unlockOption: function (option) {
        var states = this.getStateOptions();

        states[option] = false;

        $(document)
            .find('[data-account-container="'+this.attr('id')+'"] .' + option + '-option-toggle .toggle')
            .removeClass('disabled');

        this.attr('disabledOptions', states);
    },

    setCounter: function (option, value) {
        var counters = {
            likes:      this.attr('counters').likes || 0,
            comments:   this.attr('counters').comments || 0,
            follows:    this.attr('counters').follows || 0,
            unfollows:  this.attr('counters').unfollows || 0
        };

        counters[option] = parseInt(value);

        this.attr('counters', counters);
    },

    start: function() {
        this.lock();

        var self = this,
            url = "/site/start/" + this.attr('id') + '?' + jQuery.param(yii.getQueryParams(window.location.toString()));

        var result = this.checkValidTargeting();

        if (result)
        {
            $.get(url, function (response)
            {
                if (response == 'ok') {
                    self.attr('isActive', true);

                    setTimeout(function() {
                        self.checkStarted();
                        self.unlock();
                    }, 3000);
                } else {
                    self.unlock();
                }
            });
        } else { self.unlock(); }
    },

    stop: function() {
        this.lock();

        var self = this,
            url = "/site/stop/" + this.attr('id') + '?' + jQuery.param(yii.getQueryParams(window.location.toString()));

        $.get(url, function (response)
        {
            if (response == 'ok') {
                self.attr('isActive', false);
                self.unlock();
            } else {
                self.unlock();
            }
        });
    },

    lock: function() {
        this.attr('disabled', true);
    },

    unlock: function() {
        this.attr('disabled', false);
    },

    hide: function() {
        this.attr('show', false);
    },

    show: function() {
        this.attr('show', true);
    },

    reset: function() {
        var self = this,
            url = "/site/reset/" + this.attr('id') + '?' + jQuery.param(yii.getQueryParams(window.location.toString()));

        $.get(url, function (response)
        {
            if (response == 'ok') {
                self.setCounter('likes', 0);
                self.setCounter('comments', 0);
                self.setCounter('follows', 0);
                self.setCounter('unfollows', 0);
            }
        });
        return false;
    },

    checkStarted: function () {
        var self = this,
            url = "/site/stats/" + this.attr('id') + '?' + jQuery.param(yii.getQueryParams(window.location.toString()));

        $.get(url, function (response)
        {
            if (response === 'ok') {
                return true;
            }
            self.attr('isActive', false);
            self.showMessage(self.attr('id'), response, true);
        });
    },


});

/*
 * Сброс счетчика новых комментариев
 */
Account.prototype.clearNewComments = function () {
    var self = this;
    $.get("/account/clearcomments/" + this.attr('id'), function ()
    {
        self.attr('newComments', 0);
    });
    return true;
};

Account.prototype.checkValidTargeting = function () {
    var self = this;
    $.ajax({
        url: '/site/is-valid-targeting/?account_id=' + self.attr('id'),
        async: false,
        type: 'GET',
        success: function(data){
            error = data;
        }
    });

    var targeting = '';
    if (error != ''){
        switch (error){
            case 'no_places':
                var targeting = window.Zengram.t('Search by cities');
                break;
            case 'no_hashtags':
                var targeting = window.Zengram.t('Search by hashtags');
                break;
            case 'no_competitors':
                var targeting = window.Zengram.t('Search by accounts');
                break;
            case 'no_money':
                $("#low-balance").modal('show');
                return false;
        }
        var textNoTargeting = window.Zengram.t('Dear Customer! The tinctures of the project you have chosen to target {target}, but does not have data to work with. Enter information or select a different target and run the project', {target: targeting});

        var $modal = $("#emptyGeoModal");

        $modal.find('p').text(textNoTargeting);
        $modal.find('h2').text(window.Zengram.t('No points for work'));
        $modal.modal('show');

        return false;
    }

    return true;
};

Account.prototype.showMessage = function(accountId, error, showModal)
{
    console.log(error);

    var account = this.attr('login'),
        text = '',
        info = '';

    switch (error) {
        case 'passwordError':
        case 'password_error':
        case 'Invalid username/password':
        case 'The password you entered is incorrect. Please try again.':
            text = window.Zengram.t('Account @{account} have incorrect username or password.', {account: account});
            info = window.Zengram.t('Wrong username or password.');
            break;

        case 'checkpointError':
        case 'checkpoint_error':
        case 'checkpoint_required':
            text = '<div style="overflow: hidden; line-height: 30px; text-align: left; color: #333333;">' +
                '<img class="pull-right" src="' + window.Zengram.t('/img/telefon.jpg') + '" style="width: 250px;">' +
                window.Zengram.t('It is necessary to verify @{account} Instagram account. Make this action on <a target="_blank" href="https://instagram.com">Instagram.com</a> or in your Instagram app and we will start work.', {account: account}) +
                '</div>';
            info = window.Zengram.t('It is necessary to pass verification.');
            break;

        case 'followersLimitExceeded':
            text = window.Zengram.t('A limit of followings of the account @{account} is exceeded. It is necessary to unfollow.', {account: account});
            info = window.Zengram.t('The limit of followings is exceeded. It is necessary to unfollow.');
            break;

        case 'unfollowersLimitExceeded':
            text = window.Zengram.t('A limit of unfollowings of the account @{account} is exceeded. It is necessary to follow.', {account: account});
            info = window.Zengram.t('Work of unfollowings is complete');
            break;

        case 'emptyGeo':
            text = window.Zengram.t('The account @{account} has no points for work. It is necessary to adjust geography of spread.', {account: account});
            info = window.Zengram.t('There are no points for work. It is necessary to adjust geography of spread.');
            break;

        case 'badGeo':
            text = window.Zengram.t('The chosen points of the account @{account} contain few publications. It is necessary to choose other geopoint, or to expand the promoting area.', {account: account});
            info = window.Zengram.t('The chosen by you points contain few publications. It is necessary to choose other geopoint, or to expand the promoting area.');
            break;

        case 'emptyHashtags':
            text = window.Zengram.t('The account @{account} has no hashtags for work. It is necessary to add hashtags.', {account: account});
            info = window.Zengram.t('There are no hashtags for work. It is necessary to add hashtags.');
            break;

        case 'emptyCompetitors':
            text = window.Zengram.t('The account @{account} has no accounts for work. It is necessary to add accounts.', {account: account});
            info = window.Zengram.t('There are no accounts for work. It is necessary to add accounts.');
            break;

        case 'badHashtags':
            text = window.Zengram.t('The chosen hashtags of the account @{account} contain few publications. It is necessary to choose other hashtags.', {account: account});
            info = window.Zengram.t('The chosen by you hashtags contain few publications. It is necessary to choose other hashtags.');
            break;

        case 'emptyComments':
            text = window.Zengram.t('The account @{account} has no list of comments for adding. It is necessary to add comments, or to disconnect commenting.', {account: account});
            info = window.Zengram.t('There is no list of comments for additing. It is necessary to add comments, or to disconnect commenting.');
            break;

        case 'spamComments':
            text = window.Zengram.t('The account @{account} has all comments marked as Spam. It is necessary to add comments, or to disconnect commenting.', {account: account});
            info = window.Zengram.t('There is no list of comments for additing. All current comments are marked as Spam. It is necessary to add comments, or to disconnect commenting.');
            break;

        case 'instagramError':
            text = window.Zengram.t('There were error while uploading media to account @{account}. Try uploading another one after making some actions.', {account: account});
            break;

        case 'problemRequest':
            text = window.Zengram.t('On 10/07/2016 there are some problems of accounts authorization from instagram (5% of cases). Currently instagram denied authorization for your account {account}. This can happen on any device or service that you are using. We found out that the problem is solved by itself during the day. We offer you is to set the settings of your project and get familiar with our service. Once your account is authorized, the project will automatically start, which we will inform you by email.',
                {account: account}
            );
            break;
        default :
	        text = window.Zengram.t('An error occurred while processing your request. Please try again later.');
    }

    if (showModal && text.length) {
        alertModal(text);
    }

    if (info.length) {
        this.addError('common', info);
    }
};