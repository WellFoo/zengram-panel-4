var submitting = false;
$(document).ready(function ()
{
	$('a[href^="#"]').on('click', function (e)
	{
		e.preventDefault();

		var target = this.hash;
		var $target = $(target);

		$('html, body').stop().animate({
			'scrollTop': $target.offset().top - 10
		}, 900, 'swing', function ()
		{
			window.location.hash = target;
		});
	});
	$('#login-form').on('submit', function (e)
	{
		e.preventDefault();
		e.stopImmediatePropagation();
		var form = $('#login-form');

		form.find('button[type=submit]').addClass('loading');

		$.ajax({
			url: '/site/login',
			data: form.serialize(),
			type: 'post',
			beforeSend: function ()
			{
				submitting = true;
				form.find('.alert').hide();
				form.find('button[type=submit]').addClass('loading');
			},
			success: function (data)
			{
				if (data) {
					form.find('.alert').html('').show();
					form.find('button[type=submit]').removeClass('loading');
					for (var input in data) {
						if (data.hasOwnProperty(input)) {
							form.find('.alert').append(data[input] + '<br>');

						}
					}
				}
			},

			dataType: 'json'
		});
		return false;
	});

	$('#pay-reg-form').on('submit', function (e)
	{
		e.preventDefault();
		e.stopImmediatePropagation();
		if (submitting){
			return;
		}

		var form = $('#pay-reg-form');
		form.find('.alert').hide();
		form.find('button[type=submit]').addClass('loading');

		$.ajax({
			url: '/site/register',
			data: form.serialize(),
			type: 'post',
			ajaxBeforeSend: function ()
			{
				submitting = true;
			},
			success: function (data)
			{
				if (data && data.length > 0) {
					form.find('.alert').html('').show();
					for (var input in data) {
						if (data.hasOwnProperty(input)) {
							form.find('.alert').append(data[input] + '<br>');
						}
					}

					form.find('button[type=submit]').removeClass('loading')
				}
			},
			complete: function(){
				submitting = false;
				form.find('button[type=submit]').addClass('loading')
			},
			dataType: 'json'
		});
		return false;
	});

	$('#main-reg-form').on('submit', function (e)
	{
		e.preventDefault();
		e.stopImmediatePropagation();
		if (submitting){
			return;
		}

		var form = $('#main-reg-form');
		form.find('.alert').hide();
		form.find('button[type=submit]').addClass('loading');

		$.ajax({
			url: '/site/register',
			data: form.serialize(),
			type: 'post',
			ajaxBeforeSend: function ()
			{
				submitting = true;
			},
			success: function (data)
			{
				if (data && data.length > 0) {
					form.find('.alert').html('').show();
					for (var input in data) {
						if (data.hasOwnProperty(input)) {
							form.find('.alert').append(data[input] + '<br>');
						}
					}

					form.find('button[type=submit]').removeClass('loading')
				}
			},
			complete: function(){
				submitting = false;
				form.find('button[type=submit]').addClass('loading')
			},
			dataType: 'json'
		});
		return false;
	});

	$('#reg-form').on('submit', function (e)
	{
		e.preventDefault();
		e.stopImmediatePropagation();
		if (submitting){
			return;
		}

		var form = $('#reg-form');
		form.find('.alert').hide();
		form.find('button[type=submit]').addClass('loading');

		$.ajax({
			url: '/site/register',
			data: form.serialize(),
			type: 'post',
			ajaxBeforeSend: function ()
			{
				submitting = true;
			},
			success: function (data)
			{
				if (data && data.length > 0) {
					form.find('.alert').html('').show();
					for (var input in data) {
						if (data.hasOwnProperty(input)) {
							form.find('.alert').append(data[input] + '<br>');
						}
					}

					form.find('button[type=submit]').removeClass('loading')
				}
			},
			complete: function(){
				submitting = false;
				form.find('button[type=submit]').addClass('loading')
			},
			dataType: 'json'
		});
		return false;
	});
	$('#password-form').on('submit', function (e)
	{
		e.preventDefault();
		e.stopImmediatePropagation();
		var form = $('#password-form');
		console.log(form.find('button[type=submit]'));
		$.ajax({
			url: '/site/reset-password',
			data: form.serialize(),
			type: 'post',
			beforeSend: function ()
			{
				form.find('button[type=submit]').attr('disabled', 'disabled');
				form.find('.alert').hide();
			},
			success: function (data)
			{
				if (data && data.length > 0) {
					form.find('.alert').html('').show();
					form.find('.alert').append(data);
				} else {
					$('#passwordModal').modal('hide');
					$('#passwordSucessModal').find('.email-result').html(form.find('input[name=email]').val());
					$('#passwordSucessModal').modal('show');
				}
			},
			complete: function(){
				form.find('button[type=submit]').removeAttr('disabled');
			}
		});
		return false;
	});

	$(function ()
	{
		$("[data-toggle='tooltip']").tooltip();
	});

	$(function ()
	{
		$("[data-toggle='popover']").popover();
	});

});
function showPassword(){
	$('#loginModal').modal('hide');
	$('#passwordModal').find('input[name=email]').val($('#loginModal').find('input#loginform-mail').val());
}

function cancelRemind(){
	$('#loginModal').modal('show');
	$('#passwordModal').modal('hide');
}