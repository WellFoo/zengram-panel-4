function sendpubImage(id)
{
	var modal = $('#addQueue');
	var input = modal.find('#image-upload-input');
	var users = [];
	modal.find('input[name=imagesProfiles]:checked').each(function () {
		users.push($(this).data('id'));
	});
	$.ajax({
		url: "/site/addimages/",
		data: {
			image: $('#container_image').cropit('export'),
			caption: input.val(),
			users: users,
			pauseFrom: modal.find('#pauseFrom').val(),
			pauseTo: modal.find('#pauseTo').val()
		},
		method: 'post',
		beforeSend: function () {
			modal.find('#container_image button').hide();
			modal.find('#container_image .loading-send').show();
			modal.find('#upload-image-alert').hide().removeClass('alert-danger alert-success');
		},
		success: function (data) {
			if (data.indexOf(window.Zengram.t('Instagram error.')) === -1) {
				modal.find('#upload-image-alert').show().addClass('alert-success').html(window.Zengram.t('Image added to queue'));
				input.val('');
				setTimeout(function () {
					$('#addQueue').modal('hide')
				}, 5000);
			} else {
				if (data.indexOf('checkpoint_required') !== -1) {
					$("#checkpointError").modal('show');
				} else if (data.indexOf('Invalid username/password') !== -1) {
					$("#passwordError").modal('show');
				} else {
					modal.find('#upload-image-alert').show().addClass('alert-danger').html(data);
				}
			}
		},
		complete: function () {
			modal.find('#container_image button').show();
			modal.find('#container_image .loading-send').hide();
		}
	});
}