(function($){
	'use strict';

	if ($ === undefined) {
		console.error('Bs3Paginator reqires jQuery');
		return;
	} else if ($.fn.button === undefined) {
		console.error('Bs3Paginator reqires Bootstrap 3 Button.js');
		return;
	}

	$('head').append('<style type="text/css">.bs3paginator .btn {outline: none;}.bs3paginator .disabled{cursor: default;}</style>');

	var nameIterator = 0;

	var defaultSettings = {
		callback: function() {},
		arrows: {
			left: 'glyphicon glyphicon-chevron-left',
			right: 'glyphicon glyphicon-chevron-right',
			useIcons: true
		},
		classes: {
			arrowBtn: 'default',
			activeBtn: 'default',
			defaultBtn: 'default',
			disabledBtn: 'default'
		},
		name: null,
		pages: 1,
		current: 1,
		show: 5
	};

	function intval(input, defVal)
	{
		var out = parseInt(input);
		if (isNaN(out)) {
			out = defVal;
		}
		return out;
	}

	var Bs3Paginator = function(el, newSettings) {
		var pagesTotal = 1,
			currentPage = 1,
			showAround = 0,
			namePostfix = ++nameIterator,
			settings = {},
			arrowClassName = '',
			activeClassName = '',
			defaultClassName = '',
			disabledClassName = '',
			$container = $(el),
			$toolbar = $('<div class="btn-toolbar" role="toolbar">'),
			$leftGroup = $('<div class="btn-group" role="group">'),
			$rightGroup = $leftGroup.clone(),
			$radioGroup = $leftGroup.clone(),
			$pageButton = $('<label>'),
			$pageButtonInput = $('<input type="radio" autocomplete="off">');

		$.extend(true, settings, defaultSettings, newSettings);

		$container.addClass('bs3paginator')

		$toolbar
			.append($leftGroup)
			.append($radioGroup)
			.append($rightGroup)
			.appendTo($container);

		var $firstBtn = $('<button type="button">');
		$firstBtn
			.text(1)
			.on('click', function(){
				setCurrent(1, true);
				return false;
			})
			.appendTo($leftGroup);

		var $leftArrow = $('<button type="button">');
		$leftArrow
			.on('click', leftClick)
			.appendTo($leftGroup);

		var $rightArrow = $leftArrow.clone();
		$rightArrow
			.on('click', rightClick)
			.appendTo($rightGroup);

		var $lastBtn = $('<button type="button">');
		$lastBtn
			.text(pagesTotal)
			.on('click', function(){
				setCurrent(pagesTotal, true);
				return false;
			})
			.appendTo($rightGroup);

		$radioGroup
			.attr('data-toggle', 'buttons')
			.on('click', '.btn', pageClick);

		applySettings();

		function applySettings()
		{
			arrowClassName = 'btn btn-' + settings.classes.arrowBtn;
			activeClassName = 'btn btn-' + settings.classes.activeBtn + ' active';
			defaultClassName = 'btn btn-' + settings.classes.defaultBtn;
			disabledClassName = 'btn btn-' + settings.classes.disabledBtn + ' disabled';

			if (typeof settings.callback !== 'function') {
				settings.callback = defaultSettings.callback;
			}

			$firstBtn[0].className = arrowClassName;
			$leftArrow[0].className = arrowClassName;
			$leftArrow.html(settings.arrows.useIcons ? '<span class="' + settings.arrows.left + '"></span>' : settings.arrows.left);

			$lastBtn[0].className = arrowClassName;
			$rightArrow[0].className = arrowClassName;
			$rightArrow.html(settings.arrows.useIcons ? '<span class="' + settings.arrows.right + '"></span>' : settings.arrows.right);

			setPages(settings.pages, settings.current);
		}

		function setPages(pages, current)
		{
			var name = settings.name || 'bs3paginator_' + namePostfix;

			pagesTotal = pages;
			currentPage = current;

			$lastBtn.text(pagesTotal);

			showAround = pagesTotal;
			if (settings.show > 0) {
				showAround = Math.floor(settings.show / 2);
			}

			$radioGroup.html('');
			for (var i = 0; i < pages;) {
				++i;

				var $input = $pageButtonInput.clone();
				$input.attr({
					name: name,
					value: i
				});

				var $btn = $pageButton.clone();
				$btn.append($input)
					.append(i)
					.appendTo($radioGroup);
			}

			setCurrent(current);
		}

		function leftClick()
		{
			if ($leftArrow.hasClass('disabled')) {
				return;
			}
			var page = currentPage - 1;
			if (page < 1) {
				return;
			}
			setCurrent(page, true);
			return false;
		}

		function rightClick() {
			if ($rightArrow.hasClass('disabled')) {
				return;
			}
			var page = currentPage + 1;
			if (page > pagesTotal) {
				return;
			}
			setCurrent(page, true);
			return false;
		}

		function pageClick(event)
		{
			var page = intval($(event.target).children('input').val(), 1);
			if (page === currentPage) {
				return;
			}
			setCurrent(page, true);
			return false;
		}

		function setCurrent(page, trigger)
		{
			var showLeft = showAround + 1,
				showRight = showAround;
			if ((page - showLeft) < 0) {
				showRight += showLeft - page;
			} else if (page + showRight > pagesTotal) {
				showLeft += (page + showRight) - pagesTotal;
			}
			$radioGroup.children().each(function(index) {
				if ((index + 1) === page) {
					this.className = activeClassName;
				} else {
					this.className = defaultClassName;
				}
				if (index < page - showLeft || index >= page + showRight) {
					$(this).hide();
				} else {
					$(this).show();
				}
			});

			if (page === 1) {
				$firstBtn[0].className = disabledClassName;
				$leftArrow[0].className = disabledClassName;
			} else {
				$firstBtn[0].className = arrowClassName;
				$leftArrow[0].className = arrowClassName;
			}
			
			if (page === pagesTotal) {
				$lastBtn[0].className = disabledClassName;
				$rightArrow[0].className = disabledClassName;
			} else {
				$lastBtn[0].className = arrowClassName;
				$rightArrow[0].className = arrowClassName;
			}

			if (trigger) {
				settings.callback(page);
			}

			currentPage = page;
		}

		return function(newSettings) {
			$.extend(true, settings, newSettings);
			applySettings();
		};
	};

	$.fn.bs3paginator = function(settings) {
		return this.each(function() {
			if (this.bs3paginator === undefined) {
				this.bs3paginator = new Bs3Paginator(this, settings);
			} else {
				this.bs3paginator(settings);
			}
		});
	};
})(jQuery);