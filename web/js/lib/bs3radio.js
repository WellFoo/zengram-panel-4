(function($){
	'use strict';

	if ($ === undefined) {
		console.error('Bs3Radio reqires jQuery');
		return;
	} else if ($.fn.button === undefined) {
		console.error('Bs3Radio reqires Bootstrap 3 Button.js');
		return;
	}

	var nameIterator = 0;

	var defaultSettings = {
		callback: function() {},
		classes: {
			activeBtn: 'default',
			defaultBtn: 'default'
		},
		name: null,
		list: [],
		current: 0,
	};

	var Bs3Radio = function(el, newSettings) {
		var settings = {},
			currentPos = 0,
			defaultClassName = '',
			activeClassName = '',
			namePostfix = nameIterator++,
			$container = $(el),
			$radioGroup = $('<div class="btn-group" data-toggle="buttons">'),
			$radioBtn = $('<label>'),
			$radioBtnInput = $('<input type="radio" autocomplete="off">');

		$radioGroup
			.attr('data-toggle', 'buttons')
			.on('click.bs3radio', '.btn', buttonClick)
			.appendTo($container);

		$.extend(true, settings, defaultSettings, newSettings);

		function applySettings(newSettings)
		{
			defaultClassName = 'btn btn-' + settings.classes.defaultBtn;
			activeClassName = 'btn btn-' + settings.classes.activeBtn + ' active';

			render();
		}

		function render()
		{
			var name = settings.name || 'bs3radio_' + namePostfix;

			$radioGroup.html('');
			for (var i = 0; i < settings.list.length; i++) {
				var $input = $radioBtnInput.clone();
				$input.attr({
					name: name,
					value: i
				});

				var $btn = $radioBtn.clone();
				$btn.data({
						pos: i,
						name: settings.list[i]
					})
					.append($input)
					.append(settings.list[i])
					.appendTo($radioGroup);
			}

			toggle(settings.current);
		}

		function buttonClick(event)
		{
			var data = $(this).data();
			toggle(data.pos);
			settings.callback(data.pos, data.name);
			return false;
		}

		function toggle(pos)
		{
			$radioGroup.children().each(function(index, el){
				if (index === pos) {
					this.className = activeClassName;
					this.checked = true;
				} else {
					this.className = defaultClassName;
					this.checked = false;
				}
			});
			currentPos = pos;
		}

		applySettings();

		return function(input) {
			if (typeof input === 'number') {
				toggle(input);
			} else {
				$.extend(true, settings, input);
				applySettings();
			}
		};
	};

	$.fn.bs3radio = function(settings) {
		return this.each(function() {
			if (this.bs3radio === undefined) {
				this.bs3radio = new Bs3Radio(this, settings);
			} else {
				this.bs3radio(settings);
			}
		});
	};
})(jQuery);