//(function($){ 'use strict';

var account_elements = {},
	submitting = true;

/** @namespace window.Zengram */
function updateAccount()
{
	var url = "/site/stats",
		elements;
	url += '?' + jQuery.param(yii.getQueryParams(window.location.toString()));
	$.getJSON(url, function (data)
	{
		// TODO необходимо сделать так, чтобы ответы от всех запросов имели одинаковый формат
		// (не просто "ok", а полная инфа о статусе аккаунта, как тут)
		// тогда можно будет все скармливать единому обработчику
		$.each(data, function (account, val)
		{
			elements = getAccountElements(account);

			elements['account_media'].html(val.account_media);
			elements['account_followers'].html(val.account_followers);
			elements['account_follows'].html(val.account_follows);
			elements['likes'].html(val.likes);
			var comanimico, fanimico, unfanimico;
			if (val.comment_status === 0) {
				elements['comments'].removeClass('blocked').removeData('toggle').popover('destroy');
				comanimico = '';
			} else {
				elements['comments'].addClass('blocked').data('data', {
					'toggle': 'popover',
					'title': window.Zengram.t('Action paused'),
					'content': val.comment_status
				}).popover();
				comanimico = '<span class="glyphicon glyphicon-info-sign anim-ico" aria-hidden="true"></span> ';
			}

			if (val.follow_status === 0) {
				elements['follow'].removeClass('blocked').removeData('toggle').popover('destroy');
				fanimico = '';
			} else {
				elements['follow'].addClass('blocked');

				//elements['follow'].attr('data-title', 		window.Zengram.t('Action paused'));
				elements['follow'].attr('data-content', 	val.follow_status);
				
				fanimico = '<span class="glyphicon glyphicon-info-sign anim-ico" aria-hidden="true"></span> ';
			}

			if (val.unfollow_status === 0) {
				elements['unfollow'].removeClass('blocked').removeData('toggle').popover('destroy');
				unfanimico = '';
			} else {
				elements['unfollow'].addClass('blocked').data('data', {
					'toggle': 'popover',
					'title': window.Zengram.t('Action paused'),
					'content': val.unfollow_status
				}).popover({placement: 'top'});
				unfanimico = '<span class="glyphicon glyphicon-info-sign anim-ico" aria-hidden="true"></span> ';
			}

			elements['comments'].html(comanimico + val.comments);
			elements['follow'].html(fanimico + val.follow);

			elements['newcomments'].html(window.Zengram.t('New comments') + ': ' + val.new_comments);
			elements['unfollow'].html(unfanimico + val.unfollow);
			elements['lastfollowers'].html(val.lastfollowers);


			if (val.status == 0) {
				elements['projectStart'].show();
				elements['projectStop'].hide();
				elements['timerButtonDisabled'].hide();
				elements['timerButton'].show();
			} else {
				elements['projectStart'].hide();
				elements['projectStop'].show();
				elements['timerButtonDisabled'].show();
				elements['timerButton'].hide();
			}

			if (val.message) {
				 updateAccountError(account, val.message, false);
			}

			$('input[data-account=' + account + ']').each(function ()
			{
				 var $this = $(this),
				     option = 'option_' + $this.data('option');
				 if ($this.prop('checked') != val[option]) {
					 $this.off('change', optionChange);
					 $this.prop('checked', val[option]).change();
					 $this.on('change', optionChange);
				 }
			});
		});
	});
}

function getAccountElements(account)
{
	if (account_elements[account] !== undefined) {
		return account_elements[account];
	}

	var container = $('[data-account-container = ' + account + ']'),
		data = {

		'account_media'      : container.find('[id ^= account_media]'),
		'account_followers'  : container.find('[id ^= account_followers]'),
		'account_follows'    : container.find('[id ^= account_follows]'),
		'likes'              : container.find('[id ^= likes]'),
		'follow'             : container.find('[id ^= follow]'),
		'comments'           : container.find('[id ^= comments]'),
		'newcomments'        : container.find('[id ^= newcomments]'),
		'unfollow'           : container.find('[id ^= unfollow]'),
		'lastfollowers'      : container.find('[id ^= lastfollowers]'),
		'timerButtonDisabled': container.find('[id ^= timerButtonDisabled]'),
		'timerButton'        : container.find('[id ^= timerButton_]'),
		'projectStart'       : container.find('.projectStart'),
		'projectStop'        : container.find('.projectStop')

	};

	account_elements[account] = data;

	return data;
}

function updateBalance()
{
	var url = "/site/balance";
	url+= '?' + jQuery.param(yii.getQueryParams(window.location.toString()));
	$.getJSON(url, function (data)
	{
		$('#balance_d').text(data['d']);
		$('#balance_H').text(data['H']);
		$('#balance_i').text(data['i']);
		if (data['d'] == '00' && data['H'] == '00' && data['i'] == '00') {
			//$('.projectStart').addClass('disabled');
			$('.btnew.btn-comment').on('click.low-balance', function ()
			{
				$('#low-balance').modal();
				return false;
			});
		} else {
			$('.projectStart').removeClass('disabled');
			$('.btnew.btn-comment').off('.low-balance');
		}
		if (data['d'] == '00' && parseInt(data['H']) < 5) {
			$('.btnew[data-target=\\#photo-upload]').attr('data-target', '#low-balance');
		} else {
			$('.btnew[data-target=\\#low-balance]').attr('data-target', '#photo-upload');
		}
	});
}

function accountRevision(account, callback) {
	var url = "/site/revision/" + account;

	$.get(url, callback, 'JSON');
}

function accountStart(account)
{
	var url = "/site/start/" + account;
	url += '?' + jQuery.param(yii.getQueryParams(window.location.toString()));
	$.get(url, function (data)
	{
		var elements = getAccountElements(account),
			$startBtn = elements['projectStart'],
			$stopBtn = elements['projectStop'],
			$tmrBtnDsbl = elements['timerButtonDisabled'],
			$tmrBtn = elements['timerButton'];

		if (data === 'ok') {
			$startBtn.hide();
			$stopBtn.show();
			$startBtn.find('span').hide();
			$tmrBtn.show();
			$tmrBtnDsbl.hide();
			setTimeout((function ()
			{
				updateStatus(account)
			}).bind(this), 3000);
			return true;
		}
		updateAccountError(account, data, true);

		$startBtn.show();
		$stopBtn.hide();

		$tmrBtn.hide();
		$tmrBtnDsbl.show();

		return false;
	});
}

function updateAccountError(accountId, error, showModal)
{
	var account = $('#start_' + accountId).data('login'),
		text = '',
		info = '';

	switch (error) {
		case 'passwordError':
		case 'password_error':
		case 'Invalid username/password':
			text = window.Zengram.t('Account @{account} have incorrect username or password.', {account: account});
			info = window.Zengram.t('Wrong username or password.');
			break;

		case 'checkpointError':
		case 'checkpoint_error':
			text = '<div style="overflow: hidden; line-height: 30px; text-align: left; color: #333333;">' +
				'<img class="pull-right" src="' + window.Zengram.t('/img/telefon.jpg') + '" style="width: 250px;">' +
				window.Zengram.t('It is necessary to verify @{account} Instagram account. Make this action on <a target="_blank" href="https://instagram.com">Instagram.com</a> or in your Instagram app and we will start work.', {account: account}) +
				'</div>';
			info = window.Zengram.t('It is necessary to pass verification.');
			break;

		case 'followersLimitExceeded':
			text = window.Zengram.t('A limit of followings of the account @{account} is exceeded. It is necessary to unfollow.', {account: account});
			info = window.Zengram.t('The limit of followings is exceeded. It is necessary to unfollow.');
			break;

		case 'unfollowersLimitExceeded':
			text = window.Zengram.t('A limit of unfollowings of the account{account} is exceeded. It is necessary to follow.', {account: account});
			info = window.Zengram.t('Work of unfollowings is complete');
			break;

		case 'emptyGeo':
			text = window.Zengram.t('The account @{account} has no points for work. It is necessary to adjust geography of spread.', {account: account});
			info = window.Zengram.t('There are no points for work. It is necessary to adjust geography of spread.');
			break;

		case 'badGeo':
			text = window.Zengram.t('The chosen points of the account @{account} contain few publications. It is necessary to choose other geopoint, or to expand the promoting area.', {account: account});
			info = window.Zengram.t('The chosen by you points contain few publications. It is necessary to choose other geopoint, or to expand the promoting area.');
			break;

		case 'emptyHashtags':
			text = window.Zengram.t('The account @{account} has no hashtags for work. It is necessary to add hashtags.', {account: account});
			info = window.Zengram.t('There are no hashtags for work. It is necessary to add hashtags.');
			break;

		case 'emptyCompetitors':
			text = window.Zengram.t('The account @{account} has no accounts for work. It is necessary to add accounts.', {account: account});
			info = window.Zengram.t('There are no accounts for work. It is necessary to add accounts.');
			break;

		case 'badHashtags':
			text = window.Zengram.t('The chosen hashtags of the account @{account} contain few publications. It is necessary to choose other hashtags.', {account: account});
			info = window.Zengram.t('The chosen by you hashtags contain few publications. It is necessary to choose other hashtags.');
			break;

		case 'emptyComments':
			text = window.Zengram.t('The account @{account} has no list of comments for adding. It is necessary to add comments, or to disconnect commenting.', {account: account});
			info = window.Zengram.t('There is no list of comments for additing. It is necessary to add comments, or to disconnect commenting.');
			break;

		case 'spamComments':
			text = window.Zengram.t('The account @{account} has all comments marked as Spam. It is necessary to add comments, or to disconnect commenting.', {account: account});
			info = window.Zengram.t('There is no list of comments for additing. All current comments are marked as Spam. It is necessary to add comments, or to disconnect commenting.');
			break;

		case 'instagramError':
			text = window.Zengram.t('There were error while uploading media to account @{account}. Try uploading another one after making some actions.', {account: account});
			break;

		case 'problemRequest':
			text = window.Zengram.t('On 10/07/2016 there are some problems of accounts authorization from instagram (5% of cases). Currently instagram denied authorization for your account {account}. This can happen on any device or service that you are using. We found out that the problem is solved by itself during the day. We offer you is to set the settings of your project and get familiar with our service. Once your account is authorized, the project will automatically start, which we will inform you by email.',
				{account: account}
			);
			break;
		default :
			text = window.Zengram.t('An error occurred while processing your request. Please try again later.');
	}

	if (showModal && text.length) {
		alertModal(text);
	}

	if (info.length) {
		$('#start_' + accountId + ' span')
			.attr('data-content', info)
			.show();
	}
}

function updateStatus(account)
{
	var url = "/site/stats/" + account;
	url += '?' + jQuery.param(yii.getQueryParams(window.location.toString()));
	$.get(url, function (data)
	{
		if (data === 'ok') {
			return;
		}
		updateAccountError(account, data, true);
	});
}

var optionChange;

$(document).ready(function ()
{
	updateAccount();
	setInterval(updateAccount, 60000);
	setInterval(updateBalance, 60000);

	var onResetActionClick = function()
	{
		var account = $(this).data('id'),
			elements = getAccountElements(account),
			url = "/site/reset/" + account;

		url += '?' + jQuery.param(yii.getQueryParams(window.location.toString()));

		$.get(url, function (data)
		{
			if (data == 'ok') {
				if (elements['likes'].text() !== '-') {
					elements['likes'].html('0');
				}
				if (elements['comments'].text() !== '-') {
					elements['comments'].html('0');
				}
				if (elements['follow'].text() !== '-') {
					elements['follow'].html('0');
				}
				if (elements['unfollow'].text() !== '-') {
					elements['unfollow'].html('0');
				}
			}
		});
		return false;
	};
	$('.resetAction').on("click", onResetActionClick);

	var onProjectStartClick = function()
	{
		var account = $(this).data('account');
		var error = '';
		// Получаем текущее значение баланса
		$.ajax({
			url: '/site/is-valid-targeting/?account_id=' + account,
			async: false,
			type: 'GET',
			success: function(data){
				error = data;
			}
		});

		var targeting = '';
		if (error != ''){
			switch (error){
				case 'no_places':
					var targeting = window.Zengram.t('Search by cities');
					break;
				case 'no_hashtags':
					var targeting = window.Zengram.t('Search by hashtags');
					break;
				case 'no_competitors':
					var targeting = window.Zengram.t('Search by accounts');
					break;
				case 'no_money':
					$("#low-balance").modal('show');
					return;
			}
			var textNoTargeting = window.Zengram.t('Dear Customer! The tinctures of the project you have chosen to target {target}, but does not have data to work with. Enter information or select a different target and run the project', {target: targeting});

			$("#emptyGeoModal").find('p').text(textNoTargeting);
			$("#emptyGeoModal").find('h2').text(window.Zengram.t('No points for work'));
			$("#emptyGeoModal").modal('show');
			return;
		}


		if ($('.projectStart').hasClass("goHome") == true) {
			$.post($('#options-form').attr('action'), $('#options-form').serialize()).done(function()
			{
				if (accountStart(account)) {
					window.location.href = "/"
				}
			});
		}
		else {
			accountStart(account);
		}

		return false;
	};
	$('.projectStart').on("click", onProjectStartClick);

	var onProjectStopClick = function()
	{
		var account = $(this).data('account'),
			elements = getAccountElements(account);

		elements['projectStart'].show();
		elements['projectStop'].hide();
		elements['timerButtonDisabled'].show();
		elements['timerButton'].hide();

		$.get("/site/stop/" + account, function (data)
		{
			if (data == 'ok') {

			}
			else {
				//alert("ERROR:" + data);
			}
		});

		return false;
	};
	$('.projectStop').on("click", onProjectStopClick);

	optionChange = function ()
	{
		var account = $(this).data('account');
		var option = $(this).data('option');
		$.post(
			'/site/options/' + account,
			{'option': option, 'value': this.checked ? '1' : '0'},
			function (response) {
				if (response.status !== 'ok') {
					return;
				}

				// быстрый костыль для проблемы с черточками :D
				updateAccount();

				for (var key in response.data) {
					if (response.data.hasOwnProperty(key)) {
						updateOption(account, key, response.data[key]);
					}
				}
			}
		);
	};

	function updateOption(acc_id, name, data)
	{
		var $el = $('input[data-account='+acc_id+'][data-option='+name+']');
		if (!$el.length) {
			return;
		}
		$el.off('change', optionChange);
		$el.prop('checked', data.checked | false);
		$el.prop('disabled', data.disabled | false);
		$el.change();
		$el.on('change', optionChange);


	}

	$('.projectOption').on('change', optionChange);

	$('.start-all').on("click", function ()
	{
		var balance = null;
		// Получаем текущее значение баланса
		$.ajax({
			url: '/site/current-balance/',
			async: false,
			type: 'GET',
			success: function(data){
				balance = data;
			}
		});
		if (balance != null && balance <= 0){
			$("#low-balance").modal('show');
			return;
		}
		$('.projectStart').hide();
		$('.projectStop').show();
		$('.timerButton').show();
		$('.timerButtonDisabled').hide();
		$.get("/site/start/all", function (data)
		{
			if (data == 'ok') {
				//$('.projectStart').hide();
				//$('.projectStop').show();
				//$('.timerButton').addClass('disabled');
			}
			else {
				if (data == 'passwordError') {
					$("#loginError").modal('show');
				}
				else {
					alert("ERROR:" + data);
				}
			}
		});
		return false;
	});

	$('.stop-all').on("click", function ()
	{
		$('.lastfollowers').html('0');
		$('.lastfollows').html('0');
		$('.projectStop').hide();
		$('.projectStart').show();
		$('.timerButton').hide();
		$('.timerButtonDisabled').show();
		$.get("/site/stop/all", function (data)
		{
		});
		return false;
	});

	var onTimerClick = function ()
	{

		var id = $(this).data("id");
		var value = $(this).data("value");

		$('#timerButton_' + id + ' > a').html($(this).text() + ' <i class="dd fa fa-angle-down"></i>');

		if (id > 0) {
			$.post("/site/timer/" + id, {'timer': value}, function (data)
			{
				if (data == 'ok') {
				}
			});
		}

		$(this).parent().parent().parent().removeClass('open');
		return false;
	};
	$('.setTimer').on("click", onTimerClick);

	var onSpeedClick = function()
	{
		var id = $(this).data("id");
		var value = $(this).data("value");

		$('#speedButton_' + id).html(speeds[value] + ' <i class="dd fa fa-angle-down"></i>');

		if (id > 0) {
			$.post("/site/speed/" + id, {'speed': value}, function (data)
			{
				if (data == 'ok') {
				}
			});
		}

		$(this).parent().parent().parent().removeClass('open');
		return false;
	};
	$('.setSpeed').on("click", onSpeedClick);
	var onChangeActionClick = function()
	{
		var id = $(this).data('id');

		$("#changeAccount .modal-body").html('<p class="text-center"><img src="/img/ajax-loader.gif"></p>');
		$("#changeAccount").modal('show');
		
		$.get("/account/change-form/" + id, function (data)
		{
			$("#changeAccount .modal-body").html(data);

			$('#changeAccountForm').on('submit', function (e)
			{
				e.preventDefault();
				e.stopImmediatePropagation();
				var form = $('#changeAccountForm');
				$.ajax({
					url: '/account/change/' + id,
					data: form.serialize(),
					type: 'post',
					beforeSend: function ()
					{
						submitting = true;
						form.find('.alert').hide();
					},
					success: function (data)
					{
						if (data.errors !== undefined) {
							form.find('.alert').html('').show();
							if (data.errors.hasOwnProperty('error') && data.errors.error[0] == 'checkpoint_required') {
								var text = '<div style="overflow: hidden; line-height: 30px; text-align: left; color: #333333;">' +
									'<img class="pull-right" src="' + window.Zengram.t('/img/telefon.jpg') + '" style="width: 250px;">' +
									window.Zengram.t('It is necessary to verify @{account} Instagram account. Make this action on <a target="_blank" href="https://instagram.com">Instagram.com</a> or in your Instagram app and we will start work.', {account: form.find('input[name*=login]').val()}) +
									'</div>';
								$('#alertModalContent').html(text);
								$('#changeAccount').modal('hide');
								$('#alertModal').modal();
								delete data.errors.error;
							}
							for (var input in data.errors) {
								if (data.errors.hasOwnProperty(input)) {
									form.find('.alert').append(data.errors[input] + '<br>');
								}
							}
						}
						form.modal('hide');
					},

					dataType: 'json'
				});
				return false;
			});
		});

		return false;
	};
	$('.changeAccountAction').on("click", onChangeActionClick);

	$('.addNewAccount').on('click', function ()
	{

		//$("#changeAccount .modal-content").html('<p class="text-center"><img src="/img/ajax-loader.gif"></p>');

		//$('#addAccount').modal('show');
		/*$.get("/account/add", function (data) {
		 $("#addAccount .modal-content").html(data);
		 });*/
		//return false;
	});

	$('#addAccount-form').on('submit', function (e)
	{
		console.log('on submit');
		$('#addAccount-form button').addClass('disabled');
		e.preventDefault();
		e.stopImmediatePropagation();
		console.log('get form');
		var form = $('#addAccount-form');
		/*$.post("/account/add", form.serialize(), function (data) {
		 $("#addAccount .modal-content").html(data);
		 });*/
		var params = form.serializeArray();
		try{
			var city = ymaps.geolocation.city;
			if (city.toString().length <= 0){
				city = window.Zengram.t('Default city');
			}
			
			console.log('city', city);
			geocoderDataSource(city, function(points){
				console.log('geocoderDataSource');
				placeChanged(points[0],function(place_func){
					console.log('params push');
					params.push({'name': 'place' , 'value': JSON.stringify(place_func)});
					console.log('sendAddAccount');
					sendAddAccount(form, params);
					//сделано для ре-инициализации триггеров
					$('.projectStart').off("click", onProjectStartClick);
					$('.projectStart').on("click", onProjectStartClick);
					$('.projectOption').off('change', optionChange);
					$('.projectOption').on('change', optionChange);
					$('.setTimer').off("click", onTimerClick);
					$('.setTimer').on("click", onTimerClick);
					$('.setSpeed').off("click", onSpeedClick);
					$('.setSpeed').on("click", onSpeedClick);
					$('.changeAccountAction').off("click", onChangeActionClick);
					$('.changeAccountAction').on("click", onChangeActionClick);
					$('.projectStop').off("click", onProjectStopClick);
					$('.projectStop').on("click", onProjectStopClick);
					$('.resetAction').off("click", onResetActionClick);
					$('.resetAction').on("click", onResetActionClick);
					$("[data-toggle='toggle']").bootstrapToggle('destroy');
					$("[data-toggle='toggle']").bootstrapToggle();
					$('.helper').popover('destroy');
					$('.helper').popover({placement: 'top'}).unbind('click').on('click', function (e) {
						$(this).popover('toggle');
					});
					$('.account-login').dotdotdot('destroy');
					$('.account-login').dotdotdot();
				});
			})
		} catch (e){
			console.log('send add account ', form, params);
			sendAddAccount(form, params);
			$('#addAccount-form button').removeClass('disabled');
 			console.error(e);
		}
	});

	var url = "/site/errors";
	url+= '?' + jQuery.param(yii.getQueryParams(window.location.toString()));
	$.getJSON(url, function (data)
	{
		if (data['error'] !== undefined){
			updateAccountError(0, data['error'], true)
		}
	});
});

window.tryRevision = 0;
window.showedProblemRequestMessage = 0;
window.showedEtcErrortMessage = 0;

function revisionHandler(response)
{
	if (response.status == 'ok') {
		accountStart(response.account_id);
		$('#accountAdded').modal('show');
	} else if (response.status == 'error' && window.tryRevision <= 6)
	{
		window.tryRevision++;
		if (response.message == 'problemRequest' && window.showedProblemRequestMessage < 1) {
			updateAccountError(response.account_id, 'problemRequest', true);
			window.showedProblemRequestMessage = 1;
		} else {
			if (window.showedEtcErrortMessage < 1) {
				updateAccountError(response.account_id, response.message, true);
				window.showedEtcErrortMessage = 1;
			}
		}

		setTimeout(function () {
			accountRevision(response.account_id, revisionHandler);
		}, 10);
	}
}

function sendAddAccount(form, params){
	console.log('sendAddAccount');
	$.ajax({
		url: '/account/add',
		data: params,
		async: false,
		type: 'post',
		beforeSend: function ()
		{
			submitting = true;
			form.find('.alert').hide();
		},
		success: function (data)
		{
			if (data.result !== undefined){
				//showModal
				if (data.account !== undefined) {
					$('#accounts-list').append(data.account);
					setTimeout(function(){
						updateAccount();
					}, 300);
					$('.addAccount').modal('hide');
					$('#accountAdded').find('#login').text(data.login);
					if (data.city !== undefined){
						$('#accountAdded').find('#city-block').show();
						$('#accountAdded').find('#city').text(data.city);
					}
					if (data.settings !== undefined){
						$('#accountAdded').find('#settings-button').attr('href', data.settings);
					}

					accountRevision(data.account_id, revisionHandler);
				}
			}
			if (data.errors !== undefined) {
				form.find('.alert').html('').show();
				if (data.errors.hasOwnProperty('error') && data.errors.error[0] == 'checkpoint_required') {
					var text = '<div style="overflow: hidden; line-height: 30px; text-align: left; color: #333333;">' +
						'<img class="pull-right" src="' + window.Zengram.t('/img/telefon.jpg') + '" style="width: 250px;">' +
						window.Zengram.t('It is necessary to verify @{account} Instagram account. Make this action on <a target="_blank" href="https://instagram.com">Instagram.com</a> or in your Instagram app and we will start work.', {account: form.find('input[name*=login]').val()}) +
						'</div>';
					$('#alertModalContent').html(text);
					$('#addAccount').modal('hide');
					$('#alertModal').modal();
					delete data.errors.error;
				}
				for (var input in data.errors) {
					if (data.errors.hasOwnProperty(input)) {
						form.find('.alert').append(data.errors[input] + '<br>');
					}
				}
			}
		},
		complete: function ()
		{
			$('#addAccount-form button').removeClass('disabled');
		},
		dataType: 'json'
	});
}

function updateTextInput(id, val)
{
	$('.value_' + id).html(val);
}
function setTimer(timerId)
{
	$('#settimer_' + timerId).modal('hide');
	var timerVal = $('#setTimer_' + timerId).val();
	if (timerVal != 0) {
		$('#timer_' + timerId).html(timerVal + ' ' + window.Zengram.t('days'));
	} else {
		$('#timer_' + timerId).html('');
	}
}

function clearNewComments(id)
{
	$.get("/account/clearcomments/" + id, function ()
	{
		$("#newcomments_" + id).html(window.Zengram.t('New comments') + ': 0');
	});
}

//})(jQuery);