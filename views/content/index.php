<?php
use app\components\Counters;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this yii\web\View
 * @var $medias View
 * @var app\models\Account $account
 * @var string $filter
 */

$this->registerJsFile('@web/js/account.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/content.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.dotdotdot.min.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.cropit.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.fileupload.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jMinEmoji.min.js', ['position' => View::POS_BEGIN]);
$this->registerCssFile('@web/css/minEmoji.css', ['position' => View::POS_BEGIN]);

$this->title = Yii::t('views', 'Account {login} content', ['login' => $account->login]);
//$this->params['breadcrumbs'][] = $this->title;

$status = $account->getStatus();
?>

<section class="row">
	<?php echo $this->render(
		'//account/_account_inside', [
		'speeds'  => [Yii::t('views', 'slow'), Yii::t('views', 'medium'), Yii::t('views', 'fast')],
		'account' => $account,
		'status'  => $status,
	]); ?>
</section>
<!-- Setting panels list -->
<section class="row">
	<h2 class="content-title col-md-12 text-center" id="comment"><?= Yii::t('views', 'My publications') ?></h2>

	<div id="media_filter" class="btn-group" role="group" style="margin-bottom: 20px;" data-filter="<?= $filter ?>">
		<?php
		echo Html::a(Yii::t('views', 'With new comments'), ['index', 'id' => $account->id, 'filter' => 'new'], [
			'class' => 'btn btn-' . ($filter === 'new' ? 'success' : 'default')
		]);
		echo Html::a(Yii::t('views', 'All'), ['index', 'id' => $account->id, 'filter' => 'all'], [
			'class' => 'btn btn-' . ($filter !== 'new' ? 'success' : 'default')
		]);
		?>
	</div>

	<!-- Actions panel end -->
	<div class="set-panel" id="media-list" data-user-id="<?= $account->id ?>">
		<?= $this->render('medias', ['account' => $account, 'prev' => false]) ?>
	</div>
</section>

<!-- Modal alert -->
<div id="alertModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-lg accountErrorModal">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<p id="alertModalContent" class="text-center"
			   style="color: #ff6666; margin: 25px auto 45px auto; font-size: 18px; line-height: 25px; font-family: 'Open Sans', sans-serif;"></p>
		</div>
	</div>
</div>
<!-- Modal alert end -->
<script>
	$(document).ready(function () {
		$('[data-toggle="popover"]').popover(
			{
				placement: 'top',
				trigger: 'hover',
				container: 'body'
			}
		);

		$('.helper').popover({placement: 'top'}).unbind('click').on('click', function (e) {
			$(this).popover('toggle');
		});

		function updatePhotosHeight()
		{
			var photoWidth = $(".instagram-photo").first().width();
			$('.media-grid-item').css({
				height: photoWidth + 'px',
				width: photoWidth + 'px'
			});
		}

		$(window).on('resize, media-list-loaded', updatePhotosHeight);
		updatePhotosHeight();
		setTimeout(function ()
		{
			<?= Counters::renderAction('content') ?>
		}, 2000);
	});
</script>
