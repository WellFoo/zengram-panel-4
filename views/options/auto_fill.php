<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\Account $account
 */

// Получаем массив донноров текущего аккаунта
// *TODO* перенести потом получение данных из БД в модель/контроллер
$query = 'SELECT * FROM auto_fill_dorons WHERE account_id='.$account->id;
$donors = Yii::$app->db->createCommand($query)->queryAll();
?>
<!-- New block -->
<div class="panel panel-settings" id="auto-fill-settings">

	<div class="panel-header container-fluid">
		<div class="row">
			<div class="col-xs-3 col-sm-2">Прокачка</div>
			<div class="col-xs-9 col-sm-10">
				<span class="visible-md visible-lg">
					<?php if ($account->options->auto_fill_start_date != NULL): ?>
						Прокачка запущена
						<?= $account->options->auto_fill_start_date ?>
						<span style="margin:20px;">|</span> В работе
						<?= floor(((time() - strtotime($account->options->auto_fill_start_date)) / 60 / 60 / 24))  ?> дней
					<?php endif; ?>
				</span>
				<span class="hidden-md hidden-lg">
					Прокачка
					<i class="fa fa-question-circle description" data-toggle="popover" data-title="Автопрокачка"
					   data-content="Автоматическая прокачка аккаунта"></i>
				</span>
			</div>
		</div>
	</div>

	<div class="panel-body" id="actions">
		<div class="row">
			<div class="pull-left" style="width:450px; padding-left: 20px;">
				<table class="table" style="width:405px;">
					<tr>
						<td colspan="3">
							<?php
							if ($account->options->auto_fill_mode == 0){
								$modeId = 'autoFillStart';
								$modeClass = 'btn btn-success';
								$modeText = 'Включить режим автопрокачки';
								$mode = 1;
							}else{
								$modeId = 'autoFillStop';
								$modeClass = 'btn btn-danger';
								$modeText = 'Выключить режим автопрокачки‘';
								$mode = 0;
							}
							?>
							<a class="<?= $modeClass ?>" style="width:100%;" onclick="changeAutoFillMode('<?= $mode ?>');" id="<?= $modeId ?>"><?= $modeText ?></a>
						</td>
					</tr>
					<tr>
						<td style="padding-top:12px">Постить фотографии раз в</td>
						<td style="width:100px;">
							<div class="input-group" style="width:100%;">
								<input type="search" placeholder="" id="autfreq" class="form-control"  value="<?= $account->options->auto_fill_freq ?>">
								<span class="input-group-btn">
									<button class="btn btn-success" type="button" onclick="saveAutoFillFreq();">
										<span title="Сохранить" class="glyphicon glyphicon-floppy-disk"></span>
									</button>
								</span>
							</div>
						</td>
						<td style="padding-top:12px">час(ов)</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="input-group" style="width:100%;">
								<input class="form-control"  type="search" id="new-auto-fill-donor" placeholder="Добавьте аккаунт донора">
								<span class="input-group-btn">
									<button class="btn btn-success" type="button" onclick="addAccount();">
										<span title="Добавить" class="glyphicon glyphicon-plus"></span>
									</button>
								</span>
							</div>

							<div class="donors-list" style="margin-top:20px">
								<?php foreach($donors as $donor):  ?>
									<div class="balloon balloon-float">
										<span class="button-holder">
											<span title="Удалить" class="delete glyphicon glyphicon-trash" onclick="deleteDonors(<?= $donor['id'] ?>)"></span>
										</span>
										<span class="content">
											@<?= $donor['donor_login'] ?>
										</span>
									</div>
								<?php endforeach; ?>
							</div>

						</td>
					<tr>
				</table>
			</div>
			<div class="pull-left">
				<table class="table" style="width:235px; margin-left:20px;">
					<tr>
						<td colspan="4" style="padding-top:14px; padding-bottom:12px;">Делать подписок в сутки</td>
					</tr>
					<tr>
						<td style="padding-top:14px;">От</td>
						<td><input class="form-control" style="width:50px;" value="<?= $account->options->auto_fill_follows_min_during_period ?>" type="text" id="follows_min_during_period"  placeholder=""></td>
						<td style="padding-top:14px;">До</td>
						<td><input class="form-control" style="width:50px;" value="<?= $account->options->auto_fill_follows_max_during_period ?>" type="text" id="follows_max_during_period"  placeholder=""></td>
					</tr>
					<tr>
						<td colspan="4" style="padding-top:14px; padding-bottom:12px;">Лайкать после подписки</td>
					</tr>
					<tr>
						<td style="padding-top:14px;">От</td>
						<td><input class="form-control" style="width:50px;" value="<?= $account->options->auto_fill_likes_after_follow_min ?>" type="text" id="likes_after_follow_min"  placeholder=""></td>
						<td style="padding-top:14px;">До</td>
						<td><input class="form-control" style="width:50px;" value="<?= $account->options->auto_fill_likes_after_follow_max ?>" type="text" id="likes_after_follow_max"  placeholder=""></td>
					</tr>
				</table>
			</div>
			<div class="pull-left">
				<table class="table" style="margin-left:45px;">
					<tr>
						<td colspan="4" style="padding-top:14px; padding-bottom:12px;">Распределение подписок (%)</td>
					</tr>
					<tr>
						<td colspan="3" style="padding-top:12px;">Интересные люди:</td>
						<td><input class="form-control" style="width:50px;" value="<?= 100 - $account->options->auto_fill_follow_percent_clients ?>" type="text" id="follow_percent_popular" placeholder=""></td>
					</tr>
					<tr>
						<td colspan="3" style="padding-top:12px;">Клиенты Zengram:</td>
						<td><input class="form-control" style="width:50px;" value="<?= $account->options->auto_fill_follow_percent_clients ?>" type="text" id="follow_percent_clients" placeholder=""></td>
					</tr>
					<tr>
						<td colspan="4">
							<a style="width:100%;" class="btn btn-success col-md-12 col-sm-12 col-xs-12" onclick="changeAutoFillSettings();">Сохранить настройки</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
	/* TODO сделать потом через ajax */

	function deleteDonors(donorId)
	{
		document.location.href = '/options/delete-auto-fill-donors/?donor_id=' + donorId + '&account_id=' + <?=  $account->id ?>;
	}

	function addAccount()
	{
		var donorLogin = $('#new-auto-fill-donor').val();
		document.location.href = '/options/add-auto-fill-donors/?donor_login=' + donorLogin + '&account_id=' + <?=  $account->id ?>;
	}

	function saveAutoFillFreq()
	{
		var freq = $('#autfreq').val();
		document.location.href = '/options/save-auto-fill-donors-freq/?freq=' + freq + '&account_id=' + <?=  $account->id ?>;
	}

	function changeAutoFillMode(mode)
	{
		//alert (mode);
		document.location.href = '/options/change-auto-fill-mode/?mode=' + mode + '&account_id=' + <?=  $account->id ?>;
	}

	function changeAutoFillSettings()
	{
		// Получаем данные
		var follows_max_during_period = $('#follows_max_during_period').val();
		var follows_min_during_period = $('#follows_min_during_period').val();
		var likes_after_follow_min = $('#likes_after_follow_min').val();
		var likes_after_follow_max = $('#likes_after_follow_max').val();
		var follow_percent_clients = $('#follow_percent_clients').val();
		document.location.href = '/options/change-settings-auto-fill/?account_id=' + <?=  $account->id ?> + '&follows_max_during_period=' + follows_max_during_period + '&follows_min_during_period=' + follows_min_during_period + '&likes_after_follow_min=' + likes_after_follow_min + '&likes_after_follow_max=' + likes_after_follow_max + '&follow_percent_clients=' + follow_percent_clients;
	}
</script>