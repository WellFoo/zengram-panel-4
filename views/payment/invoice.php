<?php
/* @var $this yii\web\View */
/* @var $model app\models\Invoice */
/* @var $price app\models\Prices */
/* @var $form yii\bootstrap\ActiveForm */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('views', 'Invoice');
?>

<div class="row">
	<?php
	$form = ActiveForm::begin([
		'id'          => 'invoice-form',
		'options'     => ['class' => 'form'],
		'fieldConfig' => [
			'template'     => "<div class=\"col-xs-6\">{label}</div><div class=\"col-xs-2\">{input}</div><div class=\"col-lg-8\">{error}</div>",
			'labelOptions' => ['class' => 'control-label', 'style' => 'text-align: left;'],
		],
	]); ?>
	<?php foreach ($model->attributeLabels() as $attributeId => $attribute): ?>
		<?= $form->field($model, $attributeId)->textInput(['disabled' => 'disabled']); ?>
	<?php endforeach; ?>
	<?= Html::hiddenInput('Invoice[price_id]', $model->price_id); ?>
	<div class="clearfix"></div>
	<div class="form-group"><?= Yii::t('views', 'Purchase info:') ?></div>
	<div class="table-price">
		<?= $form->field($price, 'value')->textInput(['disabled' => 'disabled']); ?>
		<?= $form->field($price, 'price')->textInput(['disabled' => 'disabled']); ?> <i class="fa <?= Yii::t('views', 'fa-usd') ?>"></i>
		<?= $form->field($price, 'bonus')->textInput(['disabled' => 'disabled']); ?>%
	</div>
	<div class="clearfix"></div>
	<div class="form-group">
		<?= Html::submitInput('', ['class' => 'robokassa-button']) ?>
		<a href="/page/price/" class="btn btn-warning"><?= Yii::t('app', 'Cancel') ?></a>
	</div>
	<?php ActiveForm::end(); ?>
</div>