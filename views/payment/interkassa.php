<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

/* @var $invoice app\models\Invoice */
/* @var $price app\models\Prices */
$params = [
	'ik_co_id' => Yii::$app->params['interkassaId'],
	'ik_pm_no' => $invoice->id,
	'ik_am' => $invoice->sum,
	'ik_suc_u' => Url::to(['success-payment', 'id' => $invoice->id], true),
	'ik_pnd_u' => Url::to(['/'], true),
	'ik_desc' => Yii::t('app', 'Balance recharge on Zengram.net for #{user}', ['user' => Yii::$app->user->identity->getId()])
];

if ($price->id == 9) {
	$params['ik_desc'] = 'Оплата услуг для анализа instagram пользователей';
}

ksort($params, SORT_STRING);

if (YII_ENV_DEV){
	$params['ik_pw_via'] = 'test_interkassa_test_xts';
	$params['ik_ia_u'] =  Url::to(['interkassa-callback'], true);
	ksort($params, SORT_STRING);
}
array_push($params, Yii::$app->params['interkassaSecretKey']); // добавляем в конец массива "секретный ключ"

$signString = implode(':', $params);
$sign = base64_encode(md5($signString, true));
$params['ik_sign'] = $sign;
?>
<form id="interkassa" method="post" action="https://sci.interkassa.com/" accept-charset="UTF-8">
	<?php foreach ($params as $name=>$value): ?>
		<input type="hidden" name="<?= $name ?>" value="<?= $value; ?>">
	<?php endforeach; ?>
	<input type="submit" id="submit" value="<?= Yii::t('app', 'Click here if you are not redirected automatically') ?>" style="display: none" />
</form>
<script type="text/javascript">
	$(document).ready(function() {
		setTimeout(function(){$('#submit').click();}, 100);
		setTimeout(function(){$('#submit').show();}, 4000);
	});
</script>