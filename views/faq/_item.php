<?php
/**
 * @var \yii\web\View   $this
 * @var \app\models\FAQ $item
 * @var array           $rates
 * @var integer         $maxRate
 */
$rate = \yii\helpers\ArrayHelper::getValue($rates, $item->id, 0);

if ($maxRate == 0) {
	$rating = 0;
} else {
	$rating = round(5 * $item->rating / $maxRate);
}

if ($rating < 0) {
	$rating = 0;
}
?>

<div class="faq-item" data-id="<?= $item->id ?>">
	<div class="clearfix">
		<div class="faq-rating pull-right">
			<?= Yii::t('views', 'Rating:') ?>
			<span class="rating">
				<?php for ($i = 1; $i < 6; $i++) { ?>
					<i class="fa fa-star<?= $i > $rating ? '-o' : '' ?>"></i>
				<?php } ?>
			</span>
		</div>
		<div class="faq-question"><?= $item->question ?></div>
	</div>
	<div class="faq-answer"><?= $item->answer ?></div>
	<?php if (!Yii::$app->user->isGuest) : ?>
	<div class="clearfix">
		<div class="pull-right">
			<span class="radio-inline">
				<?= Yii::t('views', 'Is article helped?') ?>
			</span>
			<label class="radio-inline text-success">
				<input type="radio" name="rating_<?= $item->id ?>" value="yes" <?= ($rate > 0 ? 'checked' : '') ?>> <?= Yii::t('views', 'Yes') ?>
			</label>
			<label class="radio-inline text-danger">
				<input type="radio" name="rating_<?= $item->id ?>" value="no" <?= ($rate < 0 ? 'checked' : '') ?>> <?= Yii::t('views', 'No') ?>
			</label>
		</div>
	</div>
	<?php endif; ?>
</div>
