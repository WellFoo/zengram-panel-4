<?php
/**
 * @var \yii\web\View        $this
 * @var \app\models\FAQ[]    $to_top
 * @var \app\models\FAQ[]    $items
 * @var array                $rates
 * @var integer              $maxRate
 * @var \app\models\FAQCats  $currentCategory
 * @var \yii\data\Pagination $paginator
 */
use yii\helpers\Html;

if (is_null($currentCategory)) {
	$currentCategory = new \app\models\FAQCats();
	$currentCategory->name = Yii::t('views', 'All questions');
}

$this->title = $currentCategory->name;
$this->registerMetaTag([
	'name' => 'description',
	'content' => Yii::t('views', 'How to Get More Followers and Likes on Instagram? If you have any query related to Zengram app or functioning, prices etc, feel free to contact us!!')
], 'description');
/*$this->registerMetaTag([
	'name' => 'keywords',
	'content' => Yii::t('views', 'How to Get Followers on Instagram, How To Get Many Likes On Instagram, How To Get More Followers On Instagram, How To Get More Likes On Instagram')
], 'keywords');*/

$paginator->route = '/page/faq';

?>
<style>
	.panel-faq .panel-heading {
		background-color: #00BF51;
		color: #F6F6F6;
		font-weight: bold;
	}

	.panel-faq .list-group-item {
		color: #555;
	}
	.panel-faq .list-group-item.active {
		background-color: #00BF51;
		border-color: #00B14B;
		color: #FFF;
	}
	.panel-faq .list-group-item {

	}
	.panel-faq .pagination {
		margin: 0;
	}

	.faq-frequency {
		margin-bottom: 15px;
	}

	.faq-item {
		margin: 10px 0;
	}

	.faq-item .faq-question {
		font-weight: bold;
		color: #D9534F;
	}

	.faq-item .faq-answer {
		line-height: 120%;
		margin: 10px 0;
	}
	.faq-item .faq-question:first-letter,
	.faq-item .faq-answer > *:first-letter {
		margin-left: 1em;
	}
	.faq-item .faq-answer li:first-letter {
		margin-left: 0;
	}
	.faq-item .faq-answer li {
		margin-left: 1em;
	}
	.faq-item .faq-answer p {
		margin: 10px 0;
	}

	.faq-item .radio-inline input {
		margin-top: 0;
	}
	.faq-item .pull-right {
		color: #A1A19F;
		font-size: 12px;
		white-space: nowrap;
	}
	.faq-item .rating {
		color: #00BF51;
	}

	.pagination > .active > a:hover {
		background-color: #3C763D;
		border-color: #3C763D;
	}
</style>

<h1 class="text-center" style="font-size: 32px; font-weight: normal; margin-bottom: 20px;"><?= Yii::t('views', 'Help') ?></h1>

<?php if (count($to_top)) : ?>
<div class="panel panel-default panel-faq">
	<div class="panel-heading text-center"><?= Yii::t('views', 'Frequently asked questions') ?></div>
	<div class="panel-body" style="padding-bottom: 0;">
		<?php foreach ($to_top as $item) { ?>
			<div class="faq-frequency col-sm-6">
				<a data-toggle="popover" data-trigger="hover" data-placement="bottom" data-id="<?= $item->id ?>"
				   data-content="<?= Html::encode($item->answer_short) ?>">
					<?= $item->question ?>
				</a>
			</div>
		<?php } ?>
	</div>
</div>
<?php endif; ?>

<div class="row">
	<div class="col-sm-3">
		<div class="panel panel-default panel-faq">
			<div class="panel-heading text-center"><?= Yii::t('views', 'Topics') ?></div>
			<div class="list-group">
				<?php
				foreach (\app\models\FAQCats::find()->all() as $category) {
					/** @var \app\models\FAQCats|null $category */
					echo Html::a(
						$category->name,
						['/faq', 'cat_id' => $category->id],
						['class' => 'list-group-item'.($category->id == $currentCategory->id ? ' active' : '')]
					);
				}
				?>
			</div>
		</div>
	</div>

	<div class="col-sm-9">
		<div class="panel panel-default panel-faq">
			<div class="panel-heading text-center"><?= $currentCategory->name ?></div>
			<ul class="list-group" id="faq-list">
				<?php
				foreach ($items as $item) {
					echo Html::beginTag('li', ['class' => 'list-group-item']);
					echo $this->render('_item', [
						'item'    => $item,
						'rates'   => $rates,
						'maxRate' => $maxRate
					]);
					echo Html::endTag('li');
				}
				?>
			</ul>
			<div class="panel-body">
				<div class="col-sm-6">
					<?= \yii\widgets\LinkPager::widget([
						'pagination' => $paginator,
						'options' => [
							'class' => 'pagination pagination-sm'
						]
					]) ?>
				</div>
				<div class="col-sm-6 text-right">
					<form method="get">
						<?php if ($currentCategory->id) : ?>
							<input type="hidden" name="cat_id" value="<?= $currentCategory->id ?>">
						<?php endif; ?>
						<?php if ($paginator->page > 1) : ?>
							<input type="hidden" name="page" value="<?= $paginator->page ?>">
						<?php endif; ?>
						<?= Yii::t('views', 'Show by') ?>
						<div class="btn-group btn-group-sm" role="group" aria-label="<?= Yii::t('views', 'Show by') ?>">
							<button class="btn btn-<?= $paginator->pageSize == 5 ? 'success' : 'default' ?>"
							        type="submit" name="per-page" value="5">5</button>
							<button class="btn btn-<?= $paginator->pageSize == 15 ? 'success' : 'default' ?>"
							        type="submit" name="per-page" value="15">15</button>
							<button class="btn btn-<?= $paginator->pageSize == 1000 ? 'success' : 'default' ?>"
							        type="submit" name="per-page" value="all"><?= Yii::t('views', 'All') ?></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="answer-modal" class="modal zen-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"></h4>
			<div class="modal-body"></div>
		</div>
	</div>
</div>

<script id="frequency-data" type="application/json"><?=
	json_encode(\yii\helpers\ArrayHelper::map($to_top, 'id', 'answer'))
?></script>

<script type="text/javascript">
jQuery('document').ready(function() {
	var frequencyAnswers = JSON.parse($('#frequency-data').text());
	var $modal = $('#answer-modal');

	$('a[data-toggle=popover]')
		.on('click', function() {
			var $this = $(this);
			$modal.find('.modal-title').text($this.text());
			$modal.find('.modal-body').html(
				frequencyAnswers[$(this).data('id')]
			);
			$modal.modal('show');
		}).popover();

	$('#faq-list').find('input').on('change', function() {
		if (!this.checked) {
			return;
		}

		var $this = $(this);
		var $item = $this.parents('.faq-item').first();
		$.post(
			'<?= \yii\helpers\Url::to(['/faq/rating']) ?>',
			{
				id: $item.data('id'),
				rating: $this.val()
			}
		);

		if ($this.val() == 'no') {
			$('#support').modal('show');
		}

	});
});
</script>