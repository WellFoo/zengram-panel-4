<?php
/**
 * @var app\components\FluidableView $this
 * @var \app\models\Account[] $accounts
 * @var integer $count
 * @var integer $price
 * @var \app\models\ActionsSliders[] $images
 */
$this->registerJsFile('@web/js/jquery.flexslider-min.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerJsFile('@web/js/actions.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerCssFile('@web/css/actions.css');
$this->title = Yii::t('views', 'Акция');
$this->fluid = true;
?>
<div class="clearfix"></div>
<div class="action-index">
	<div class="slide row">
		<div class="wfix text">
			<div>Продвигайте свой аккаунт бесплатно!</div>
			<span>Разместите пост с рекомендацией zengram в своем аккаунте</span>
			<small>и получите до 7 дней бесплатного продвижения в сервисе!</small>
		</div>
	</div>
	<div class="container action-wrapper">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-lg-push-8">

				<div class="full-text visible-xs">
					Больше подписчиков - больше бесплатного продвижения!<br>
					За каждые 100 подписчиков на аккаунте,<br>
					где Вы разместите пост о зенграм,<br>
					мы подарим Вам 8 часов.
				</div>

				<div class="circle visible-xs"><?= $count + 57 + 7 * (floor(abs(time() - strtotime('2016-02-05')) / 86400)) ?></div>
				<div class="rec-text visible-xs">Пользователей уже рекомендуют нас</div>

				<div class="iphone center-block">
					<div class="top">Выберите любую картинку, которая Вам нравится<span class="visible-xs"> свайпом</span></div>
					<div class="bottom">Мы добавим описание к посту
						- "<?= Yii::t('app', 'I am promoting my account in Instagram using zengram.net. I was glad for 3 free days, which is presenting to everyone') ?>"
					</div>
					<div class="flexslider">
						<ul class="slides">
							<?php foreach ($images as $image): ?>
								<li>
									<img data-id="<?= $image->id; ?>" src="<?= $image->path; ?>"/>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<div class="action-description-link center-block hidden-xs">
					<a href="javascript:void(0)"
					   onclick="alertModal('<?= Yii::t('app', 'Вы размещаете в своем аккаунте пост с любой картинкой.<br>Мы добавляем в его описание фразу – «Раскручиваю свой аккаунт с помощью @zengram_russia Порадовали 5 бесплатных дней, которые дарят всем!»<br>За каждых 100 подписчиков вы получите 8 часов продвижения в сервисе бесплатно. Минимальное начисление 24 часа, если количество Ваших подписчиков не достигает 100.<br>Максимальное начисление - 7 бесплатных дней, даже если число подписчиков на Вашем аккаунте превышает 9000.<br>В акции могут участвовать только открытые аккаунты. (Если аккаунт закрыт настройками приватности, пост будет считаться удаленным и все бесплатное время, Вам начисленное - будет удаленно)<br>Если пост с рекомендацией ZENGRAM будет удален Вами менее чем через 7 дней, все начисления автоматически будут сняты с Вашего счета. Исключение НЕ составит даже тот случай, если подобное действие приведет к отрицательному балансу. Об этом Вас обязательно оповестит наша система и предложит разместить пост заново.'); ?>')">
						<?= Yii::t('app', 'Action description'); ?>
					</a>
				</div>
				<div class="visible-sm visible-xs" style="min-height: 35px"></div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-lg-pull-4 text-center">
				<div class="action-accounts-panel">
					<div class="full-text hidden-xs">
						Больше подписчиков - больше бесплатного продвижения!<br>
						За каждые 100 подписчиков на аккаунте,<br>
						где Вы разместите пост о зенграм,<br>
						мы подарим Вам 8 часов.
					</div>
					<div class="circle hidden-xs"><?= $count + 57 + 7 * (floor(abs(time() - strtotime('2016-02-05')) / 86400)) ?></div>
					<div class="rec-text hidden-xs">Пользователей уже рекомендуют нас</div>
					<?php if (!empty($accounts)) { ?>
						<div class="full-text">
							Разместите пост на следующих аккаунтах<br/>
							и Вам будет начислено - <span class="free-text"><span id="free-sum"><?= $price ? \app\models\Account::getStaticTimerText($price, true) : 0 ?></span> бесплатно</span>
						</div>
						<div class="action-result"></div>
						<div class="checkbox-list center-block" id="accounts">
							<?php foreach ($accounts as $account) { ?>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
										<input class="account-checkbox" type="checkbox" id="action-checkbox-<?= $account->id ?>" data-id="<?= $account->id ?>" checked/>
										<label for="action-checkbox-<?= $account->id ?>"><span></span>@<?= $account->login ?></label>
										<div class="clearfix"></div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div id="upload-message-<?= $account->id; ?>"></div>
									</div>
								</div>
							<?php } ?>
						</div>
						<div class="clearfix"></div>
						<button type="button" class="add-link" onclick="sendImage()">Разместить</button>
						<button type="button" class="add-link-wait hidden">Разместить <i class="fa fa-spinner fa-pulse"></i></button>
						<div class="action-description-link center-block visible-xs">
							<a href="javascript:void(0)"
							   onclick="alertModal('<?= Yii::t('app', 'Вы размещаете в своем аккаунте пост с любой картинкой.<br>Мы добавляем в его описание фразу – «Раскручиваю свой аккаунт с помощью @zengram_russia Порадовали 5 бесплатных дней, которые дарят всем!»<br>За каждых 100 подписчиков вы получите 8 часов продвижения в сервисе бесплатно. Минимальное начисление 24 часа, если количество Ваших подписчиков не достигает 100.<br>Максимальное начисление - 7 бесплатных дней, даже если число подписчиков на Вашем аккаунте превышает 9000.<br>В акции могут участвовать только открытые аккаунты. (Если аккаунт закрыт настройками приватности, пост будет считаться удаленным и все бесплатное время, Вам начисленное - будет удаленно)<br>Если пост с рекомендацией ZENGRAM будет удален Вами менее чем через 7 дней, все начисления автоматически будут сняты с Вашего счета. Исключение НЕ составит даже тот случай, если подобное действие приведет к отрицательному балансу. Об этом Вас обязательно оповестит наша система и предложит разместить пост заново.'); ?>')">
								<?= Yii::t('app', 'Action description'); ?>
							</a>
						</div>

					<?php } else { ?>
						<div class="full-text">
							К сожалению, у вас нету аккаунтов для размещения. Если вы уже размещали пост, вам необходимо подождать 7 дней для размещения следующего<br/>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>