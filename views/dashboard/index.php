<?php
use app\models\Account;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\bootstrap\ActiveForm;

$this->registerJsFile('@web/js/jquery.dotdotdot.min.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.cropit.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jMinEmoji.min.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.Jcrop.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.ui.widget.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.iframe-transport.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.fileupload.js', ['position' => View::POS_BEGIN]);
$this->registerCssFile('@web/css/jquery.Jcrop.min.css', ['position' => View::POS_BEGIN]);

$this->registerJsFile('@web/js/place.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/content.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/lib/can.custom.js', ['position' => View::POS_BEGIN]);
//$this->registerJsFile('@web/js/account.js', ['position' => View::POS_BEGIN]);

$this->registerJsFile('https://api-maps.yandex.ru/2.0-stable/?load=package.standard,package.geoObjects&lang=' . (Yii::$app->language == 'ru' ? 'ru-RU' : 'en-US'));

/** @var \app\models\Account $account */
?>
<?=$this->render('_modals')?>

<!--suppress ALL -->
<script id='account-view' type='text/x-handlebars'>
    <?=$this->render('account.mustache')?>
</script>

<script id='accounts-list-view' type='text/x-handlebars'>
    <?=$this->render('accounts-list.mustache')?>
</script>

<script id='toggle-option-view' type='text/x-handlebars'>
    <?=$this->render('toggle-option.mustache')?>
</script>

<script id='dropdown-list-view' type='text/x-handlebars'>
    <?=$this->render('dropdown-list.mustache')?>
</script>

<?=$this->render('_header', compact('user', 'prices', 'accounts'))?>


<div class = "row" id = "accounts-list"></div>


<?php if ($user->demo) : ?>
	<div id="addAccountFirst" class="modal fade bs-example-modal-lg addAccount" tabindex="-1" role="dialog"
	     aria-labelledby="myLargeModalLabel"
	     aria-hidden="true">
		<div class="modal-dialog changePasswordModal">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<div class="modal-content text-warning">
				<p style="color: #ff6666; margin: 0 auto 25px auto; font-size: 18px; line-height: 22px;">
					<?= Yii::t('views', 'Zengram&nbsp;recommends you to check your first account before start:') ?>
				</p>
				<ul style="margin-bottom: 25px;">
					<li><?= Yii::t('views', 'Age of your account is more than 2 weeks') ?></li>
					<li><?= Yii::t('views', 'You already have some followers') ?></li>
					<li><?= Yii::t('views', 'The account connected with your Facebook') ?></li>
					<li><?= Yii::t('views', 'The account contains more than 10 publications') ?></li>
					<li><?= Yii::t('views', 'You don&#39;t use other services of Instagram promotion') ?></li>
				</ul>
				<p style="margin-bottom: 25px; line-height: 22px;"><?= Yii::t('views', 'According our experience, completing all these requirements makes promotion faster and more reliable.') ?></p>

				<p style="margin-bottom: 25px; line-height: 22px;"><?= Yii::t('views', 'Press OK, and the project will start. By adding other accounts you won&#39;t see this message again.') ?></p>

				<div style="overflow: hidden;">
					<button class="btn btn-danger btn-lg" data-dismiss="modal" style="width: 45%; float: left;">
						<?= Yii::t('app', 'Cancel') ?>
					</button>

					<button class="btn btn-success" data-dismiss="modal" style="width: 45%; float: right;"
					        onclick="$('#addAccount').modal(); $('#addAccount-form').submit();">OK
					</button>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php if (count($accounts) > 0 && !$user->demo) : ?>
	<div id="addAccountMoreOne" class="modal fade bs-example-modal-lg addAccount" tabindex="-1" role="dialog"
	     aria-labelledby="myLargeModalLabel"
	     aria-hidden="true">
		<div class="modal-dialog changePasswordModal">

			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>

			<div class="modal-content text-warning">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<p style="color: #ff6666; margin: 0 auto 25px auto; font-size: 18px; line-height: 22px; text-align: center;">
					<?= Yii::t('views', '<strong style="font-weight: bold;">Dear client.</strong><br>We remind that withdrawal of time from your account for direct using of service supposed to be multiplied by number of accounts. For example, if you have 2 Instagram accounts, withdrawal for one working hour would be 2 hours. And if you have one Instagram account, withdrawal for one working hour would be one hour.') ?>
				</p>

				<div style="overflow: hidden;">
					<button class="btn btn-danger btn-lg" data-dismiss="modal" style="width: 45%; float: left;">
						<?= Yii::t('app', 'Cancel') ?>
					</button>

					<button class="btn btn-success" data-dismiss="modal" style="width: 45%; float: right;"
					        onclick="$('#addAccount').modal();">OK
					</button>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<!-- Modal Add Account -->
<div id="addAccount" class="modal fade bs-example-modal-lg addAccount" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
	<div class="modal-dialog changePasswordModal">
		<button style="display: none;" type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span></button>

			<h2 class="text-center" style="margin: 0; line-height: 1.428571429; font-size: 26px; color: #ff6666;">
				<?= Yii::t('views', 'Add Instagram account') ?>
			</h2>

			<p class="text-center"
			   style="color: #ff6666; margin: 25px auto 45px auto; font-size: 18px; line-height: 25px; font-family: 'Open Sans', sans-serif;">
				<?= Yii::t('views', 'Your privacy is very important for us!<br>That&#39;s why we don&#39;t store your password,<br>it will be used only for getting necessary API data for Instagram') ?>
			</p>
			<?php $model = new \app\forms\AccountCreateForm(); ?>
			<?php $form = ActiveForm::begin([
				'id' => 'addAccount-form',
				'options' => [
					'class' => 'form-horizontal',
					'onSubmit' => \app\components\Counters::renderAction('account_create')
				],
				'fieldConfig' => [
					'template' => "<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
					'labelOptions' => ['class' => 'col-lg-3 control-label'],
				],
				'validateOnBlur' => false,
				'validateOnType' => false,
				'validateOnChange' => false,
				'validateOnSubmit' => true,
				'enableAjaxValidation' => true,
				'validationUrl' => ['account/ajax-validate'],
			]); ?>

			<p class="alert alert-danger" style="display: none"></p>

			<?= $form->field($model, 'login', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('login')]]) ?>

			<?= $form->field($model, 'password', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('password')]])->passwordInput() ?>

			<div class="form-group">
				<div class="col-lg-12">
					<?= Html::submitButton(Yii::t('views', 'Add account'), [
						'class' => 'btn btn-danger btn-lg',
						'name' => 'login-button',
						'onClick' => $user->demo ? '$("#addAccountFirst").modal(); $("#addAccount").modal("hide"); return false;' : ''
					]) ?>
				</div>
			</div>

			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
<?php if ($user->allowImages): ?>
	<!-- Modal Add Account Error end -->
	<div class="modal fade" id="addQueue" tabindex="-1" role="dialog" aria-labelledby="addQueue">
		<div class="modal-dialog" role="document">

			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<div class="modal-body">
					<div id="upload-image-panel">
						<div id="container_image">
							<h2 class="text-center"><?= Yii::t('views', 'Add photo') ?></h2>

							<div class="row">
								<div class="col-md-12">
									<?= Html::a(Yii::t('views', 'Log'), ['account/pubimages'], [
										'class' => 'btn btn-success',
									]); ?>
								</div>
								<div class="col-md-8">
									<div class="upload-description">
										<p><?= Yii::t('views', '1. Select image for upload') ?></p>
										<input type="file" class="cropit-image-input">
									</div>

									<div class="upload-image-wrapper">
										<!-- .cropit-image-preview-container is needed for background image to work -->
										<div class="cropit-image-preview-container">
											<div class="cropit-image-preview"
											     style="width: 235px; height: 235px"></div>
										</div>
									</div>

									<div class="upload-description">
										<p><?= Yii::t('views', '2. Change zoom settings (if necessary)') ?></p>
										<input type="range" class="cropit-image-zoom-input">
									</div>
									<div class="upload-description">
										<p><?= Yii::t('views', '3. Enter description (if necessary). Spintax allowed') ?></p>
										<textarea class="form-control image-upload-input"
										          id="image-upload-input"></textarea>
									</div>

									<div id="upload-image-alert" class="alert" style="display: none"></div>

								</div>
								<div class="col-md-4">
									<p><?= Yii::t('views', '4. Enter pauses') ?></p>
									<?= Yii::t('views', 'From') ?>
									<input type="number" id="pauseFrom" value="10">
									<?= Yii::t('views', 'to') ?>
									<input type="number" id="pauseTo" value="30">
								</div>
								<div class="col-md-4">
									<p><?= Yii::t('views', '5. Select projects to proceed') ?></p>

									<div class="imageProfiles">
										<input type="checkbox" checked
										       onclick="$('input[name=imagesProfiles]').prop('checked', this.checked)">
										<?= Yii::t('views', 'Select all') ?>
									</div>
									<hr>
									<?php foreach ($accounts as $project): ?>
										<div class="imageProfiles">
											<input name="imagesProfiles" type="checkbox"
											       data-id="<?= $project['id'] ?>"
											       checked>
											@<?= $project['login'] ?>
										</div>
									<?php endforeach; ?>
									<div class="clear"></div>
									<button class="btn btn-success" onclick="sendpubImage()">
										<?= Yii::t('app', 'Send') ?>
									</button>
									<p style="display: none" class="loading-send text-center">
										<img src="/img/ajax-loader.gif">
									</p>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<script src = "/js/dashboard/helpers.js" type="application/javascript"></script>

<script src = "/js/dashboard/components/dropdown.js" type="application/javascript"></script>
<script src = "/js/dashboard/components/toggle-option.js" type="application/javascript"></script>
<script src = "/js/dashboard/components/account.js" type="application/javascript"></script>
<script src = "/js/dashboard/components/account-list.js" type="application/javascript"></script>

<script src = "/js/dashboard/models/account.js" type="application/javascript"></script>
<script src = "/js/dashboard.js" type="application/javascript"></script>

<script type="application/javascript">
	jQuery(function($) {
		var $addAccForm = $('#addAccount-form');
		$addAccForm.on('submit place', function (e) {
			e.preventDefault();
			e.stopImmediatePropagation();

			var params = $addAccForm.serializeArray();
			var $btn = $addAccForm.find('button');
			$btn.addClass('disabled');

			console.log('send add account ', $addAccForm, params);
			try{
				var city = ymaps.geolocation.city;
				if (city.toString().length <= 0){
					city = window.Zengram.t('Default city');
				}

				console.log('city', city);
				geocoderDataSource(city, function(points){
					console.log('geocoderDataSource');
					placeChanged(points[0],function(place_func){
						console.log('params push');
						params.push({'name': 'place' , 'value': JSON.stringify(place_func)});
						console.log('sendAddAccount');
						sendAddAccount($addAccForm, params);
						reInitPopovers(document);
					});
				})
			} catch (e){
				sendAddAccount($addAccForm, params);
				$btn.removeClass('disabled');
				console.error(e);
			}
		});
	});

	Account.prototype.speed_items = [
		{ label: '<?= Yii::t('views', 'Slow') ?>', value: 0 },
		{ label: '<?= Yii::t('views', 'Medium') ?>', value: 1 },
		{ label: '<?= Yii::t('views', 'Fast') ?>', value: 2 }
	];

	window.accounts = [];
	window.demo = null;

	<?php if (!$user->demo) { ?>
	<?php
	$account_statuses = Account::getStatusesByInstagramIdsList(ArrayHelper::getColumn($accounts, 'instagram_id'));
	if ($account_statuses === null) {
		$account_statuses = [];
	}
	?>
	<?php foreach ($accounts as $account) { ?>
	<?php
	$follow_status = $account->follow_status;
	$options = $account->options;

	if (is_null($options)) {
		$options = new \app\models\Options([
			'account_id' => $account->id
		]);
		$options->save();
	}

	if ($account->comment_status && $account->follow_status) {
		$hours = floor(($account->pause_until - time())/3600);
		if ($hours > 0) {
			$account->comment_status =
			$account->follow_status = Yii::t('views', 'There are some temporary restrictions by Instagram on using follows and comments and they are paused on {hours} hours. The actions will start automatically when pause will be ended.', ['hours' => $hours]) . ' ' . Yii::t('views', 'Time is not subs on that period.');
		} else {
			$account->comment_status = $account->follow_status = 0;
		}
	} elseif($account->comment_status){
		$pause_until = $account->comments_block_date + $account->comments_block_expire * 3600 ;
		if (!$options->follow){
			if ($pause_until < $account->pause_until){
				$pause_until = $account->pause_until;
			}
		}
		$hours = ceil(($pause_until-time())/3600);
		if ($hours > 0) {
			$account->comment_status = Yii::t('views', 'There are some restrictions on using of the action by Instagram and it is paused for {hours} hours. The actions will start automatically when pause will be ended.', ['hours' => $hours]);
			if ($options->follow == false) {
				$account->comment_status .= ' ' . Yii::t('views', 'Time is not subs on that period.');
			}
		} else {
			$account->comment_status = 0;
		}
	} elseif($account->follow_status){
		$pause_until = $account->follow_block_date + $account->follow_block_expire * 3600 ;
		if (!$options->comment){
			if ($pause_until < $account->pause_until){
				$pause_until = $account->pause_until;
			}
		}
		$hours = ceil(($pause_until- time())/3600);
		if ($hours > 0) {
			$account->follow_status = Yii::t('views', 'There are some restrictions on using of the action by Instagram and it is paused for {hours} hours. The actions will start automatically when pause will be ended.', ['hours' => $hours]);
			if ($options->comment == false) {
				$account->follow_status .= ' ' . Yii::t('views', 'Time is not subs on that period.');
			}
		} else {
			$account->follow_status = 0;
		}
	}

	$statusRaw = ArrayHelper::getValue($account_statuses, intval($account->instagram_id), false);
	$status = ($statusRaw === true ? 'true' : 'false');
	?>

	window.accounts[<?=$account->id?>] = new Account({
		id:                 <?=$account->id?>,
		login:              '<?=$account->login?>',
		likesOption:        <?=($options->likes) ? 'true' : 'false'?>,
		commentsOption:     <?=($options->comment) ? 'true' : 'false'?>,
		followsOption:      <?=($options->follow) ? 'true' : 'false'?>,
		unfollowsOption:    <?=($options->unfollow) ? 'true' : 'false'?>,
		avatar:             '<?=str_replace('http://', '//', $account->account_avatar)?>',
		newComments:        <?=$account->getNewComments()?>,
		info: {
			followers:      <?=$account->account_followers?>,
			newFollowers:   <?=$account->getLastFollowers()?>,
			followings:     <?=$account->account_follows?>
		},
		isActive:           <?=$status?>,
		hideTimer:          <?=(!$account->user->use_timer) ? 'true' : 'false'?>,
		timerText:          '<?=$account->getTimerText()?>',
		counters: {
			likes:          <?=$account->likes?>,
			comments:       <?=$account->comments?>,
			follows:        <?=$account->follow?>,
			unfollows:      <?=$account->unfollow?>
		},
		speed:              (<?=$options->speed?> || 0),
		photos:             <?=$account->account_media?>,
	show: true
	});

	<?php if (!empty($account->message)) { ?>
	window.accounts[<?=$account->id?>].addError('common', '<?=$account->getErrorMessage()?>');
	<?php } ?>

	<?php if (!empty($account->follow_status)) { ?>
	window.accounts[<?=$account->id?>].addError('follows', '<?=$account->follow_status?>');
	<?php } ?>

	<?php if (!empty($account->comment_status)) { ?>
	console.log('<?=$account->comment_status?>');
	window.accounts[<?=$account->id?>].addError('comments', '<?=$account->comment_status?>');
	<?php } ?>

	<?php if ($account->unfollow_status) { ?>
	window.accounts[<?=$account->id?>].addError('unfollows', '<?=$account->getUnfollowsStatusMessage()?>');
	<?php } ?>

	<?php } ?>

	<?php } else { ?>
	<?php
	/*
	 * Если аккаунты отсутствуют, или пользователь не добавлял аккаунтов ранее.
	 * Добавляем в список проектов демонстрационный аккаунт.
	 */
	$account = new \app\models\DemoAccount();
	?>

	window.demo = new Account({
		id:                 parseInt('<?=(($account->id == 'DEMO') ? 0 : $account->id)?>'),
		login:              '<?=$account->login?>',
		likesOption:        <?=($account->options->likes) ? 'true' : 'false'?>,
		commentsOption:     <?=($account->options->comment) ? 'true' : 'false'?>,
		followsOption:      <?=($account->options->follow) ? 'true' : 'false'?>,
		unfollowsOption:    <?=($account->options->unfollow) ? 'true' : 'false'?>,
		avatar:             '<?=$account->account_avatar?>',
		newComments:        15,
		info: {
			followers:      <?=$account->account_followers?>,
			newFollowers:   (<?=$account->account_followers?> - 2),
			followings:     <?=$account->account_follows?>
		},
		counters: {
			likes:          <?=$account->likes?>,
			comments:       <?=$account->comments?>,
			follows:        <?=$account->follow?>,
			unfollows:      <?=$account->unfollow?>
		},
		speed:              (<?=$account->options->speed?> || 0),
		photos:             <?=$account->account_media?>,
	show:               true,
		demo:               true,
	});

	window.accounts[0] = window.demo;
	<?php } ?>

	var Accounts = can.List();

	Accounts.bind('add', function(type, account) {
		setTimeout(function () {
			var $toggles = $(document)
				.find('[data-account-container="' + account[0].id + '"]')
				.find("[data-toggle='toggle']");

			$toggles.bootstrapToggle('destroy');
			$toggles.bootstrapToggle();
		}, 500);
	});

	for (var i = 0; i < window.accounts.length; i++) {
		if (window.accounts[i]) {
			Accounts.push(window.accounts[i]);
		}
	}

	var list = new AccountList('#accounts-list', {
		accounts: Accounts
	});

	$(document).ready(function () {
		reInitPopovers(document);
	});

	setInterval(function () { updateAccounts() },120000);
	setInterval(function () { updateBalance() }, 120000);
</script>