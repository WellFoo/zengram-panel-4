<?php
use app\models\Users;
use yii\helpers\Html;
?>
<section class="header container" <?=((!$show_border) ? 'style = "border-bottom: none;"' : '')?>>
	<div class="row">

		<i class="toggle-menu fa fa-bars col-xs-3 visible-xs"></i>

		<p class="logo col-xs-6 col-sm-3 col-md-2 col-lg-3">
			<a href="/" title="<?=Yii::t('app', 'Zengram')?>">
				<img src="/img/logo.png" alt="<?=Yii::t('app', 'Zengram')?>"/><?= Html::encode(Yii::$app->params['company']) ?>
			</a>
		</p>

		<?php if (!Yii::$app->user->isGuest) {?>
		<nav class="nav-top col-xs-12 col-sm-8 col-md-9 col-lg-7 hidden-xs">
			<?php }else{ ?>
			<nav class="nav-top col-xs-12 col-sm-6 col-md-7 col-lg-6 hidden-xs">
				<?php }?>

				<?php
				if (!Yii::$app->user->isGuest) {
					echo Yii::$app->language !== 'en' ? Html::a(Yii::t('app', 'My accounts'), ['/']) :
						Html::a(Yii::t('app', 'My accounts'), ['/'], ['title' => Yii::t('app', 'Zengram App')]);
				}

				foreach ($pages as $page) {
					/** @var \app\models\Page $page */
					if (LANGUAGE === 'rf' && $page->alias === 'price') {
						continue;
					}
					echo empty($page->linkTitle) ? Html::a(Yii::t('app', $page->title), ['/page/' . $page->alias]):
						Html::a(Yii::t('app', $page->title), ['/page/' . $page->alias], ['title' => $page->linkTitle]);
				}
				echo Yii::$app->language !== 'en' ? Html::a(Yii::t('app', 'Blog'), ['/site/blog']) :
					Html::a(Yii::t('app', 'Blog'), ['/site/blog'], ['title' => Yii::t('app', 'Our Blog')]);
				if (!Yii::$app->user->isGuest) {
					echo Html::a(Yii::t('app', 'Support'), ['#support'], [
						'data-toggle' => 'modal', 'data-target' => '#support'
					]);
					echo Html::a(Yii::t('app', 'Settings'), '/site/#user-settings', [
						'data' => ['toggle' => 'modal', 'target' => '#user-settings'],
						'class' => 'visible-xs'
					]);
					echo Html::a(Yii::t('app', 'Log out'), ['/site/logout'], [
						'data-method' => 'post',
						'class' => 'visible-xs'
					]);
				}
				?>
			</nav>

			<?php if (Yii::$app->user->isGuest): ?>
				<div class="auth-reg-panel col-xs-3 col-sm-1 col-lg-2">
					<a class="registration" data-toggle="modal" data-target="#regModal" title="<?= Yii::t('app', 'Sign up') ?>">
						<i class="fa fa-user"></i> <span class="hidden-xs"><?= Yii::t('app', 'Sign up') ?></span>
					</a>
					<a class="sign-in" data-toggle="modal" data-target="#loginModal" title="<?= Yii::t('app', 'Log in') ?>">
						<i class="fa fa-sign-in"></i> <span class="hidden-xs"><?= Yii::t('app', 'Log in') ?></span>
					</a>
				</div>
			<?php else: ?>
				<div class="user-name col-xs-3 col-sm-1 col-lg-2 hidden-xs">
					<button type="button" class="btn btn-default dropdown-toggle pull-right" data-toggle="dropdown"
					        title="<?= $user->mail; ?>">
						<?= $user->mail ?>
						<!-- <img class="select-img" src="/img/select_btn.png" alt=""/>-->
					</button>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a href="/site/#user-settings" data-toggle="modal" data-target="#user-settings"><?= Yii::t('app', 'Settings') ?></a>
						</li>
						<?php
						if (isset($user->isAdmin) and $user->isAdmin) {
							echo '<li>' . Html::a(Yii::t('app', 'Admin panel'), ['/admin']) . '</li>';
						}
						if ($user->role === Users::ROLE_ADMIN || $user->role === Users::ROLE_MANAGER) {
							echo '<li>' . Html::a(Yii::t('app', 'Manager panel'), ['/management']) . '</li>';
						}
						?>
						<li class="divider"></li>
						<li><?= Html::a(Yii::t('app', 'Log out'), ['/site/logout'], ['data-method' => 'post']); ?></li>
					</ul>
				</div>
			<?php endif; ?>

	</div>
</section>