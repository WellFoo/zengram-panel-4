<?php
/* @var $this yii\web\View */
/* @var $accounts app\models\Account[] */
/* @var $prices app\models\Prices[] */
/* @var $user app\models\Users */

use app\models\Prices;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\View;
use app\models\Account;

$this->registerJsFile('@web/js/jquery.dotdotdot.min.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.cropit.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jMinEmoji.min.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.Jcrop.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.ui.widget.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.iframe-transport.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.fileupload.js', ['position' => View::POS_BEGIN]);
$this->registerCssFile('@web/css/jquery.Jcrop.min.css', ['position' => View::POS_BEGIN]);

$this->registerJsFile('@web/js/account.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/place.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/content.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.cookie.js', ['position' => View::POS_BEGIN]);

if ($user->allowImages) {
	$this->registerJsFile('@web/js/pubimages.js', ['position' => View::POS_BEGIN]);
}
$this->registerJsFile('https://api-maps.yandex.ru/2.0-stable/?load=package.standard,package.geoObjects&lang=' . (Yii::$app->language == 'ru' ? 'ru-RU' : 'en-US'));

$this->title = Yii::t('app', 'Zengram');

if (LANGUAGE == 'en') {
	$this->registerMetaTag([
		'name' => 'description',
		'content' => Yii::t('main', 'Buy Zengram app free and get more real Instagram followers, likes and comments fast. Know how to get followers and promote your Instagram account with Zengram!')
	]);
}
?>
<script>
	var mapsApiKey = '<?= Yii::$app->params['yandex_maps_api_key'] ?>';
</script>
<div class="site-index">
	<?php if (Yii::$app->session->hasFlash('AccountDeletedError')): ?>
		<div class="alert alert-danger">
			<?= Yii::t('views', 'There was an error deleting your account') ?>
		</div>
	<?php endif; ?>

	<?php if (Yii::$app->session->hasFlash('AccountDeleted')): ?>
		<div class="alert alert-success">
			<?= Yii::t('views', 'Your account has successfully been deleted') ?>
		</div>
	<?php endif; ?>
	<?php if (Yii::$app->session->hasFlash('AccountLoginError')): ?>
		<div class="alert alert-warning">
			<?= Yii::$app->session->getFlash('AccountLoginError') ?>
		</div>
	<?php endif; ?>

	<!-- Sub Header -->
	<section class="row sub-header">
		<!-- Btn-cont -->
		<div class="col-xs-6 col-sm-3 va-bottom">

			<?php
			if (!empty($user->allowImages)) {
				echo Html::a(
					Yii::t('views', 'Add photo'),
					['#'],
					[
						'class' => 'add-massive-images bot-bor btn btn-success col-xs-12 col-sm-12 col-md-12',
						'id' => 'addMassiveImages',
						'data-toggle' => "modal",
						'data-target' => "#addQueue"
					]
				);
			}
			echo Html::a(
				Yii::t('views', 'Add account'),
				['#'],
				[
					'class' => 'addNewAccount add-acount bot-bor btn btn-success col-xs-12 col-sm-12 col-md-12',
					'id' => 'addNewAccount',
					'onClick' => 'return false;',
					'data-toggle' => 'modal',
					'data-target' => count($accounts) > 0 ? '#addAccountMoreOne' : '#addAccount'
				]
			);

			if (count($accounts) > 1) {
				?>
				<div class="col-xs-12 col-md-6">
					<div class="row pad-3 first"><?php
						echo Html::a(
							Yii::t('views', 'Start all'),
							['#startall'],
							['class' => 'start-all btn btn-success hidden-xs col-sm-12 col-md-12 ']
						);
						?></div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="row pad-3 last"><?php
						echo Html::a(
							Yii::t('views', 'Stop all'),
							['#stopall'],
							['class' => 'stop-all btn btn-danger hidden-xs col-sm-12 col-md-12']
						);
						?></div>
				</div>
			<?php } ?>

		</div>

		<!-- Counter -->
		<div class="user-balance col-xs-6 col-sm-4 text-center va-center" style="margin: 0 -5px;">
			<p>
				<span class="hidden-xs"><?= Yii::t('views', 'Time limit:') ?></span>
				<span class="visible-xs"><?= Yii::t('views', 'Time:') ?></span>
			</p>
			<?php
			$balance = max((int)$user->balance, 0);
			?>
			<!-- <?= $balance; ?> -->
			<div class="counter-item days-last">
				<output class="btn btn-default" id="balance_d">
					<?php
					if ($balance > 86400) {
						echo sprintf("%02d", floor($balance / 86400));
					} else {
						echo '00';
					}
					?></output>
				<p><?= Yii::t('views', 'days') ?></p>
			</div>

			<div class="counter-item hours-last">
				<output class="btn btn-default" id="balance_H">
					<?php
					if ($balance > 3600) {
						$balance = $balance - (floor($balance / 86400) * 86400);
						echo sprintf("%02d", floor($balance / 3600));
					} else {
						echo '00';
					}
					?></output>
				<p><?= Yii::t('views', 'hours') ?></p>
			</div>

			<div class="counter-item minutes-last">
				<output class="btn btn-default" id="balance_i"><?php if ($balance > 0) {
						echo date("i", $balance);
					} else {
						echo '00';
					} ?></output>
				<p><?= Yii::t('views', 'minutes') ?></p>
			</div>
		</div>

		<!-- Buy packets -->
		<div class="hidden-xs col-sm-5 text-center va-center">
			<p><?= Yii::t('views', 'Buy time package') ?></p>

			<div class="buy-packets">
				<?php foreach ($prices as $price): ?>
					<div class="col-xs-4">
						<div class="row pad-3">
							<p class="days">
								<strong><?= $price->value ?></strong><?= Yii::t('app', '{n, plural, =0{ # days} one{ # day} few{ # days} many{ # days} other{ # days}}', ['n' => $price->value]) ?>
							</p>

							<p class="price">
								<strong><?= Prices::formatPrice($price->price); ?></strong>
								<span class="fa <?= Yii::t('views', 'fa-usd') ?>"></span>
							</p>
							<a href="/page/price?sel_id=<?= $price->id; ?>"
							   class="bot-bor btn btn-success"><?= Yii::t('views', 'Buy') ?></a>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
	<!-- Sub header end -->

	<!-- Accounts list -->
	<?php
	$speeds = [Yii::t('views', 'slow'), Yii::t('views', 'medium'), Yii::t('views', 'fast')];
	?>
	<script>speeds = ['<?= implode("','", $speeds) ?>'];</script>

	<div class="row" id="accounts-list">
		<?php

		if ($user->demo) {

			echo $this->render('_account', [
				'speeds' => $speeds,
				'account' => new \app\models\DemoAccount(),
				'status' => 0,
				'user' => $user,
				'newComments' => 12
			]);

		} else {
			$subQuery = (new \yii\db\Query())
				->select(['id'])
				->from('accounts')
				->where(['user_id' => $user->id]);
			$newComments = ArrayHelper::map(
				(new \yii\db\Query())
					->select(['account_id', 'sum' => 'SUM(new_comments)'])
					->from('media_count')
					->where(['in', 'account_id', $subQuery])
					->groupBy(['account_id'])
					->all(),
				'account_id',
				'sum'
			);
			$account_statuses = Account::getStatusesByInstagramIdsList(ArrayHelper::getColumn($accounts, 'instagram_id'));
			if ($account_statuses === null) {
				$account_statuses = [];
			}
			foreach ($accounts as $account) {
				$statusRaw = ArrayHelper::getValue($account_statuses, intval($account->instagram_id), false);
				$status = ($statusRaw === true ? 1 : 0);
				echo $this->render('_account', [
					'speeds' => $speeds,
					'account' => $account,
					'status' => $status,
					'user' => $user,
					'newComments' => !empty($newComments[$account->id]) ? $newComments[$account->id] : 0
				]);
			}

		}

		/* Отзыв по акции */
		if (count($accounts) > 0 && Yii::$app->language == 'en') { ?>
			<style scoped>
				.report-action {
					height: 641px;
					position: relative;
				}

				.report-action .greeting h3 {
					color: #CF0000;
					font-size: 120%;
					margin: 5px 0 15px;
					text-align: center;
				}

				.report-action .greeting p {
					line-height: 1.3em;
					margin: 10px 0;
					text-align: justify;
				}

				.report-action .greeting p:first-letter {
					margin-left: 0.5em;
				}

				.report-action #report-action-text {
					border-radius: 0;
					height: 350px;
					padding: 5px;
					resize: none;
					width: 100%;
				}

				.report-action .alert {
					box-shadow: 0 3px 5px rgba(0, 0, 0, .3);
					display: none;
					position: absolute;
					bottom: 55px;
					left: 22px;
					right: 22px;
				}

				.report-action #report-action-button {
					position: absolute;
					bottom: 17px;
					right: 15px;
				}

				@media (max-width: 768px) {
					.report-action {
						height: 606px;
					}

					.report-action #report-action-text {
						height: 315px;
					}
				}

				@media (max-width: 320px) {
					.report-action #report-action-text {
						height: 295px;
					}
				}
			</style>
			<div class="acc-pane report-action col-sm-6 col-lg-4">
				<div class="greeting">
					<?= Yii::t('views', '<h3>Dear friends!</h3><p>Our project will have first birthday soon. We want to become better for you, so there are next promotion:</p><p>Report, what in your opinion we should make better, or founded imperfection in our service. We will consider it in our work and present you free time.</p>') ?>
				</div>
				<div>
					<textarea id="report-action-text"></textarea>
				</div>
				<div>
					<button id="report-action-button" type="button" class="btn btn-success"
					        data-url="<?= yii\helpers\Url::to(['site/report-action']) ?>"><?= Yii::t('app', 'Send') ?></button>
				</div>
			</div>

			<!-- Modal Report Action -->
			<div class="modal zen-modal fade" id="report-action-success" tabindex="-1" role="dialog"
			     aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
						<div class="modal-body">
							<h4 class="modal-title"><?= Yii::t('views', 'Thanks!') ?></h4>

							<p class="text-center"
							   style="margin: 15px;"><?= Yii::t('views', 'Your feedback is important for us!') ?></p>
							<?= Html::a(Yii::t('app', 'Ok'), '#', ['class' => 'btn btn-success', 'data' => ['dismiss' => 'modal']]) ?>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal Report Action end -->

			<!-- Modal empty Balance -->
			<div class="modal zen-modal fade" id="report-empty-balance" tabindex="-1" role="dialog"
				 aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
						<div class="modal-body">
							<h4 class="modal-title"><?= Yii::t('views', 'Thanks!') ?></h4>

							<p class="text-center"
							   style="margin: 15px;"><?= Yii::t('views', 'Your feedback is important for us!') ?></p>
							<?= Html::a(Yii::t('app', 'Ok'), '#', ['class' => 'btn btn-success', 'data' => ['dismiss' => 'modal']]) ?>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal empty Balance end -->

			<script>
				var busyAction = false;
				jQuery(function ($) {
					'use strict';
					var $textArea = $('#report-action-text'),
						$button = $('#report-action-button');

					$button.on('click', function () {
						if (busyAction) {
							return;
						}
						if (!$textArea.val().length) {
							$textArea.focus();
							return;
						}
						$.ajax({
							url: $button.data('url'),
							type: 'post',
							data: {text: $textArea.val()},
							success: function (response) {
								if (response.status === 'ok') {
									$textArea.val('');
									$('#report-action-success').modal();
								}
							},
							beforeSend: function () {
								busyAction = true;
							}
						}).always(function () {
							busyAction = false;
						});
					});
				});
			</script>
		<?php } ?>
	</div>

	<?php if ($user->demo) : ?>
		<div id="addAccountFirst" class="modal fade bs-example-modal-lg addAccount" tabindex="-1" role="dialog"
		     aria-labelledby="myLargeModalLabel"
		     aria-hidden="true">
			<div class="modal-dialog changePasswordModal">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<div class="modal-content text-warning">
					<p style="color: #ff6666; margin: 0 auto 25px auto; font-size: 18px; line-height: 22px;">
						<?= Yii::t('views', 'Zengram&nbsp;recommends you to check your first account before start:') ?>
					</p>
					<ul style="margin-bottom: 25px;">
						<li><?= Yii::t('views', 'Age of your account is more than 2 weeks') ?></li>
						<li><?= Yii::t('views', 'You already have some followers') ?></li>
						<li><?= Yii::t('views', 'The account connected with your Facebook') ?></li>
						<li><?= Yii::t('views', 'The account contains more than 10 publications') ?></li>
						<li><?= Yii::t('views', 'You don&#39;t use other services of Instagram promotion') ?></li>
					</ul>
					<p style="margin-bottom: 25px; line-height: 22px;"><?= Yii::t('views', 'According our experience, completing all these requirements makes promotion faster and more reliable.') ?></p>

					<p style="margin-bottom: 25px; line-height: 22px;"><?= Yii::t('views', 'Press OK, and the project will start. By adding other accounts you won&#39;t see this message again.') ?></p>

					<div style="overflow: hidden;">
						<button class="btn btn-danger btn-lg" data-dismiss="modal" style="width: 45%; float: left;">
							<?= Yii::t('app', 'Cancel') ?>
						</button>

						<button class="btn btn-success" data-dismiss="modal" style="width: 45%; float: right;"
						        onclick="$('#addAccount').modal(); $('#addAccount-form').submit();">OK
						</button>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if (count($accounts) > 0 && !$user->demo) : ?>
		<div id="addAccountMoreOne" class="modal fade bs-example-modal-lg addAccount" tabindex="-1" role="dialog"
		     aria-labelledby="myLargeModalLabel"
		     aria-hidden="true">
			<div class="modal-dialog changePasswordModal">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<div class="modal-content text-warning">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<p style="color: #ff6666; margin: 0 auto 25px auto; font-size: 18px; line-height: 22px; text-align: center;">
						<?= Yii::t('views', '<strong style="font-weight: bold;">Dear client.</strong><br>We remind that withdrawal of time from your account for direct using of service supposed to be multiplied by number of accounts. For example, if you have 2 Instagram accounts, withdrawal for one working hour would be 2 hours. And if you have one Instagram account, withdrawal for one working hour would be one hour.') ?>
					</p>

					<div style="overflow: hidden;">
						<button class="btn btn-danger btn-lg" data-dismiss="modal" style="width: 45%; float: left;">
							<?= Yii::t('app', 'Cancel') ?>
						</button>

						<button class="btn btn-success" data-dismiss="modal" style="width: 45%; float: right;"
						        onclick="$('#addAccount').modal();">OK
						</button>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<!-- Modal Add Account -->
	<div id="addAccount" class="modal fade bs-example-modal-lg addAccount" tabindex="-1" role="dialog"
	     aria-labelledby="myLargeModalLabel"
	     aria-hidden="true">
		<div class="modal-dialog changePasswordModal">
			<button style="display: none;" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">×</span></button>

				<h2 class="text-center" style="margin: 0; line-height: 1.428571429; font-size: 26px; color: #ff6666;">
					<?= Yii::t('views', 'Add Instagram account') ?>
				</h2>

				<p class="text-center"
				   style="color: #ff6666; margin: 25px auto 45px auto; font-size: 18px; line-height: 25px; font-family: 'Open Sans', sans-serif;">
					<?= Yii::t('views', 'Your privacy is very important for us!<br>That&#39;s why we don&#39;t store your password,<br>it will be used only for getting necessary API data for Instagram') ?>
				</p>
				<?php $model = new \app\forms\AccountCreateForm(); ?>
				<?php $form = ActiveForm::begin([
					'id' => 'addAccount-form',
					'options' => [
						'class' => 'form-horizontal',
						'onSubmit' => \app\components\Counters::renderAction('account_create')
					],
					'fieldConfig' => [
						'template' => "<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
						'labelOptions' => ['class' => 'col-lg-3 control-label'],
					],
					'validateOnBlur' => false,
					'validateOnType' => false,
					'validateOnChange' => false,
					'validateOnSubmit' => true,
					'enableAjaxValidation' => true,
					'validationUrl' => ['account/ajax-validate'],
				]); ?>

				<p class="alert alert-danger" style="display: none"></p>

				<?= $form->field($model, 'login', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('login')]]) ?>

				<?= $form->field($model, 'password', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('password')]])->passwordInput() ?>

				<div class="form-group">
					<div class="col-lg-12">
						<?= Html::submitButton(Yii::t('views', 'Add account'), [
							'class' => 'btn btn-danger btn-lg',
							'name' => 'login-button',
							'onClick' => $user->demo ? '$("#addAccountFirst").modal(); $("#addAccount").modal("hide"); return false;' : ''
						]) ?>
					</div>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
	<?php if ($user->allowImages): ?>
		<!-- Modal Add Account Error end -->
		<div class="modal fade" id="addQueue" tabindex="-1" role="dialog" aria-labelledby="addQueue">
			<div class="modal-dialog" role="document">

				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
					<div class="modal-body">
						<div id="upload-image-panel">
							<div id="container_image">
								<h2 class="text-center"><?= Yii::t('views', 'Add photo') ?></h2>

								<div class="row">
									<div class="col-md-12">
										<?= Html::a(Yii::t('views', 'Log'), ['account/pubimages'], [
											'class' => 'btn btn-success',
										]); ?>
									</div>
									<div class="col-md-8">
										<div class="upload-description">
											<p><?= Yii::t('views', '1. Select image for upload') ?></p>
											<input type="file" class="cropit-image-input">
										</div>

										<div class="upload-image-wrapper">
											<!-- .cropit-image-preview-container is needed for background image to work -->
											<div class="cropit-image-preview-container">
												<div class="cropit-image-preview"
												     style="width: 235px; height: 235px"></div>
											</div>
										</div>

										<div class="upload-description">
											<p><?= Yii::t('views', '2. Change zoom settings (if necessary)') ?></p>
											<input type="range" class="cropit-image-zoom-input">
										</div>
										<div class="upload-description">
											<p><?= Yii::t('views', '3. Enter description (if necessary). Spintax allowed') ?></p>
									<textarea class="form-control image-upload-input"
									          id="image-upload-input"></textarea>
										</div>

										<div id="upload-image-alert" class="alert" style="display: none"></div>

									</div>
									<div class="col-md-4">
										<p><?= Yii::t('views', '4. Enter pauses') ?></p>
										<?= Yii::t('views', 'From') ?>
										<input type="number" id="pauseFrom" value="10">
										<?= Yii::t('views', 'to') ?>
										<input type="number" id="pauseTo" value="30">
									</div>
									<div class="col-md-4">
										<p><?= Yii::t('views', '5. Select projects to proceed') ?></p>

										<div class="imageProfiles">
											<input type="checkbox" checked
											       onclick="$('input[name=imagesProfiles]').prop('checked', this.checked)">
											<?= Yii::t('views', 'Select all') ?>
										</div>
										<hr>
										<?php foreach ($accounts as $project): ?>
											<div class="imageProfiles">
												<input name="imagesProfiles" type="checkbox"
												       data-id="<?= $project['id'] ?>"
												       checked>
												@<?= $project['login'] ?>
											</div>
										<?php endforeach; ?>
										<div class="clear"></div>
										<button class="btn btn-success"
										        onclick="sendpubImage()"><?= Yii::t('app', 'Send') ?></button>
										<p style="display: none" class="loading-send text-center"><img
												src="/img/ajax-loader.gif"></p>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<!-- Модалки -->
	<?php
	echo $this->render('_modals');
	?>
</div>
<script>
	window.showedTrap = true; // Флаг отображения сообщения о закрытии окна

	$(document).ready(function () {
		$('[data-toggle="popover"]').popover(
			{
				placement: 'top',
				trigger: 'hover',
				container: 'body'
			}
		);

		$("#nsf").on("change", function (e) {
			$.cookie('dontShowTrap', e.target.checked, { expires: 7, path: '/' });
		});

		$("#nsf").on("change", function (e) {
			$.cookie('dontShowTrap', e.target.checked, { expires: 7, path: '/' });
		});

		/*
		 *  Обработчик ловушки, возникает когда мышь попадает на элемент в верхней части экрана.
		 */
		var trapHandler = function () {
			var $modal = $("#modal-trap");

			// Показываем модальное окно
			$modal.modal('show');

			//Отключаем показ модального окна на 3 минуты
			window.showedTrap = false;
			setTimeout(function () {
				window.showedTrap = true;
			}, 180000);
		};

		$(document).on('mousemove', function (e) {
			if (e.clientY <= 70 && window.showedTrap && $.cookie('dontShowTrap') != 'true' && $("#accounts-list[data-account-id!='DEMO'] .projectStart:visible").length > 0) {
				trapHandler();
			}
		});

		$('.helper').popover({placement: 'top'}).unbind('click').on('click', function () {
			$(this).popover('toggle');
		});
		<?php if ($user->balance <= 0) : ?>
		$('.btnew.btn-comment').on('click.low-balance', function () {
			$('#low-balance').modal();
			return false;
		});
		<?php endif ?>
		$('.account-login').dotdotdot();
	});
</script>