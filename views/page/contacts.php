<?php if (Yii::$app->language === 'en') {

	$this->title = 'Instagram Liking App | Grow Instagram Followers | Zen-promo';
	$this->registerMetaTag([
		'name' => 'description',
		'content' => 'Looking for the best Instagram liking app? Zen-promo is the right choice for you to get more followers & grow your Instagram profile. Signup today to avail our services!'
	]);
	$this->registerMetaTag([
		'name' => 'keywords',
		'content' => 'Instagram Liking App, Grow Instagram, Grow Instagram Followers'
	]);
	$this->registerMetaTag([
		'name' => 'robots',
		'content' => 'index, follow'
	]);
	?>



	<h1 class="text-center">Contacts</h1>

	<p>Address: POS. BOGATOE, D 2,</p>
	<p>ZELENOGRADVSKIY R-N.,</p>
	<p>KALININGRADSKAYA OBL.,</p>
	<p>238554, RUSSIAN FEDERATION</p>

	<p>Tel: +89622535699</p>

	<p>Email: support@zen-promo.com</p>
<?php } else {

	$this->title = 'Контактная информация';

	?>
	<h1 class="text-center">Контакты</h1>

	<p>Страна: Российская Федерация</p>

	<p>Область: Калининградская область</p>

	<p>Город: Маршальское</p>

	<p>Адрес: ул. Школьная, 4/6</p>

	<p>Почтовый индекс: 238317</p>

	<p>Tel: +89622535699</p>

	<p>Email: support@zengram.ru</p>
<?php } ?>
