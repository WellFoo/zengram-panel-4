<?php
/** @var \yii\web\View $this */
use yii\helpers\Html;

$this->title = Yii::t('views', 'Sitemap | Zengram');
$this->registerMetaTag([
	'name' => 'description',
	'content' => Yii::t('views', 'Visit Zengram Sitemap to find all links and pages available on website!!')
], 'description');
/*$this->registerMetaTag([
	'name' => 'keywords',
	'content' => Yii::t('views', 'Buy Real Instagram Followers, Followers On Instagram, Get Instagram Followers, Get More Instagram Followers, Real Instagram Followers')
], 'keywords');*/


//$this->params['breadcrumbs'][] = $this->title;
?>
<h1 style="font-size: 130%; font-weight: bold; margin: 10px 20px;"><?= $this->title ?></h1>

<ul>
	<li><?= Html::a(Yii::t('views', 'Home')) ?></li>
	<?php
	foreach ($pages as $page) {
		/** @var \app\models\Page $page */
		echo Html::beginTag('li');
		echo empty($page->linkTitle) ? Html::a(Yii::t('app', $page->title), ['/page/' . $page->alias]):
			Html::a(Yii::t('app', $page->title), ['/page/' . $page->alias], ['title' => $page->linkTitle]);
		echo Html::endTag('li');
	}
	echo Html::beginTag('li');
	echo Yii::$app->language !== 'en' ? Html::a(Yii::t('app', 'Blog'), ['/site/blog']) :
		Html::a(Yii::t('app', 'Blog'), ['/site/blog'], ['title' => Yii::t('app', 'Our Blog')]);
	echo Html::endTag('li');
	?>
</ul>
