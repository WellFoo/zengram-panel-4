<?php
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $prices \app\models\Prices[]
 * @var $selected_id int
 * @var \app\models\Discounts|null $discount
 */

$this->title = Yii::t('views', 'Buy Instagram Followers App for Real & Active Instagram Followers');
$this->registerMetaTag([
	'name' => 'description',
	'content' => Yii::t('views', 'Buy Zen-promo Instagram follower app to get real and active Instagram followers. Click here to select from the packages available & increase your followers fast!')
], 'description');
if (LANGUAGE == 'en') {
	$this->registerMetaTag([
		'name' => 'keywords',
		'content' => Yii::t('views', 'Buy Instagram Followers App, Buy Active Instagram Followers, Buy Real Active Instagram Followers, Buy Real Instagram Followers')
	], 'keywords');
}
$showDiscount = !is_null($discount) && $discount->checkDate();
$discountNotInRange = !is_null($discount) && !$discount->checkDate();
?>

<?php if ($discountNotInRange) : ?>
	<div class="modal zen-modal fade" id="discount-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<p class="text-danger">
						<?= Yii::t(
							'views',
							'Action available between {from, date, dd.MM.yyyy} and {to, date, dd.MM.yyyy}',
							['from' => strtotime($discount->date_start), 'to' => strtotime($discount->date_end)]
						) ?>
					</p>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
<?php endif; ?>

<?php if ($showDiscount) : ?>
	<div class="modal zen-modal fade" id="discount-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?= Yii::t('views', 'Congratulations!') ?></h4>
				</div>
				<div class="modal-body">
					<p><?= Yii::t(
							'views',
							'Under the terms of the stock when making payment you receive a +{size}% free package',
							['size' => $discount->size]
						) ?></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<style>
		.discount-helper {
			color: #FF5151;
			font-size: 80%;
			margin-left: 5px;
			white-space: nowrap;
		}
		tr.active .discount-helper {
			color: #FFF;
			text-shadow: 2px 2px rgba(0,0,0,.3);
		}
	</style>
<?php endif; ?>

<div class="page" id="price" xmlns="http://www.w3.org/1999/html">
	<section class="container">
		<form method="get">
			<?php if ($showDiscount) {
				echo Html::hiddenInput('_discount', $discount->id);
			} ?>
			<div class="row">

				<?php if ($showDiscount) { ?>
					<h1 class="price-title text-center text-uppercase" style="color: #FF6666;">
						<?= Yii::t('views', '+{size}% free!', ['size' => $discount->size]); ?>
					</h1>

					<p class="price-subtitle text-center">
						<?= Yii::t('views', 'Especially for you a special offer:') ?>
						<br>
						<small style="font-size: 80%">
							<?= Yii::t('views', 'pay any amount of time and get a {size}% free!', ['size' => $discount->size]); ?>
						</small>
						<br>
						<small style="font-size: 70%">
							<?= Yii::t('views', 'Offer valid until {date, date, dd.MM.yyyy}.', ['date' => strtotime($discount->date_end)]); ?>
						</small>
					</p>
				<?php } else { ?>
					<h1 class="price-title"><?= Yii::t('views', 'Prices') ?></h1>

					<p class="price-subtitle col-xs-12 col-sm-12 col-md-12">
						<?= Yii::t('views', 'Here you can buy the necessary amount of time for zengram work. The bigger package you choose, the cheaper time costs for you.'); ?>
					</p>
				<?php } ?>

				<table class="table-price table-responsive col-xs-12 col-sm-12 col-md-12">

					<thead>
					<tr>
						<th class="col-xs-1 col-sm-1 col-md-1"><i class="fa fa-circle-o" style="display: none;"></i>
						</th>
						<th class="col-xs-3 col-sm-3 col-md-3"><?= Yii::t('views', 'Quantity of days') ?></th>
						<th class="col-xs-5 col-sm-5 col-md-5"><?= Yii::t('views', 'Price') ?></th>
						<th class="col-xs-3 col-sm-3 col-md-3"><?= Yii::t('views', 'Your bonus') ?></th>
					</tr>
					</thead>

					<tbody>
					<?php
					foreach ($prices as $price) {
						if ($price['id'] == 8 && (!isset(Yii::$app->user->id) || !Yii::$app->user->identity->isAdmin)) {
							break;
						}

						if ($price['id'] == 9) {
							break;
						}
						$checked = $price['id'] === $selected_id;
						?>

						<tr <?= $checked ? 'class="active"' : '' ?>>
							<td class="col-xs-1 col-sm-1 col-md-1">
								<span class="fa fa-circle-o <?= $checked ? 'fa-dot-circle-o' : ''; ?>"></span>
								<?php
								echo Html::radio(
									'price', $checked, [
									'value' => $price['id'],
								]);
								?>
							</td>
							<td class="col-xs-3 col-sm-3 col-md-3">
								<?= $price['value']
								.($showDiscount
									? '<sup class="discount-helper">+'.$discount->calc($price['value']).' '.Yii::t('views', 'free').'</super>'
									: '')
								?>
							</td>
							<td class="col-xs-5 col-sm-5 col-md-5"><?= \app\models\Prices::formatPrice($price['price']); ?>
								<span class="fa <?= Yii::t('views', 'fa-usd') ?>"></span>
							</td>
							<td class="col-xs-3 col-sm-3 col-md-3"><?= $price['bonus'] ?>
								% <?php if ($price['value'] == '30') {
									echo Html::img(['@img/most-popular.png'], ['style' => 'position: absolute; right: 0; top: 185px;']);;
								} ?></td>
						</tr>

					<?php } ?>
					</tbody>

				</table>
				<?php
				if(isset($_GET['changed_int_s'])){
					$_SESSION['select_for_metrika'] = [
						"id"=>"",
						"price"=>""
					];
					switch($_GET['changed_int_s']){
						case '1':
							$_SESSION['select_for_metrika']['id'] = "dn3";
							$_SESSION['select_for_metrika']['price'] = "99";
							break;
						case '2':
							$_SESSION['select_for_metrika']['id'] = "dn10";
							$_SESSION['select_for_metrika']['price'] = "279";
							break;
						case '3':
							$_SESSION['select_for_metrika']['id'] = "dn30";
							$_SESSION['select_for_metrika']['price'] = "699";
							break;
						case '4':
							$_SESSION['select_for_metrika']['id'] = "dn60";
							$_SESSION['select_for_metrika']['price'] = "1099";
							break;
						case '5':
							$_SESSION['select_for_metrika']['id'] = "dn90";
							$_SESSION['select_for_metrika']['price'] = "1499";
							break;
						case '6':
							$_SESSION['select_for_metrika']['id'] = "dn180";
							$_SESSION['select_for_metrika']['price'] = "2699";
							break;
						case '7':
							$_SESSION['select_for_metrika']['id'] = "dn365";
							$_SESSION['select_for_metrika']['price'] = "4999";
							break;
						case '8':
							$_SESSION['select_for_metrika']['id'] = "dn111";
							$_SESSION['select_for_metrika']['price'] = "1";
							break;
					}

				}?>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.table-price tbody tr *').click(function () {
							$('.table-price tr').removeClass('active');
							$('.fa.fa-circle-o').removeClass('fa-dot-circle-o');

							$(this).parent().addClass('active');

							$(this).parent().find('.fa.fa-circle-o').addClass('fa-dot-circle-o');
							$(this).parent().find('input[name="price"]').prop('checked', true);


							document.getElementsByClassName('table-price')[0].addEventListener('click',function(){
								var req = new XMLHttpRequest();
								req.open('GET', '/page/price/?changed_int_s='+document.getElementsByClassName('active')[0].getElementsByTagName('input')[0].value, true);
								req.send();
							});


						});
						<?php if ($discount) : ?>
						$('#discount-modal').modal();
						<?php endif; ?>
					});
				</script>
				<div class="price-subtitle col-xs-12 col-sm-12 col-md-12">
					<?= (Yii::$app->language === 'en' ? Yii::t('views', 'The acquirable package is the actual time of using zengram.net service for 1 instagram account. Balance write-offs are carried out in minutes.') : '') ?>
				</div>
				<div class="price-subtitle col-xs-12 col-sm-12 col-md-12">
					<?= Yii::t('views', 'For payment proceed, select payment and click necessary button') ?>
					<?= $this->render('_pay_methods_'.Yii::$app->language) ?>
				</div>
				<div class="clearfix"></div>
			</div>
		</form>
	</section>
</div>