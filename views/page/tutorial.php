<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = Yii::t('guide', 'Our Guidance | Zengram');
$this->registerMetaTag([
	'name' => 'description',
	'content' => Yii::t('guide', 'Find here easy guidance to gain buy Instagram followers and likes at Zengram. Visit us now for more information!!')
], 'description');
/*$this->registerMetaTag([
	'name' => 'keywords',
	'content' => Yii::t('guide', 'Buy Real Instagram Followers, Followers On Instagram, Get Instagram Followers, Get More Instagram Followers, Real Instagram Followers')
], 'keywords');*/


//$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.disordered-list strong {
	font-style: italic;
}
</style>
<div class="page instruction">
	<!--<h1><? /*= Html::encode($this->title) */ ?></h1>-->
	<h1><?= Yii::t('guide', 'Guide') ?></h1>

	<section class="wrap-cont row">
		<div class="wrap-text col-xs-12 col-sm-6 col-md-8">

			<h2><?= Yii::t('guide', 'Adding a new Instagram account.') ?></h2>

			<p>
				<?= Yii::t('guide', 'First of all you need to add your profile to our service, for that we use the button "add account". We don&#39;t store your Instagram’s data, so it is absolutely safe. For successful work we strongly recommend you to use the real Instagram accounts:') ?>
			</p>

			<ul class="order-list">
				<li><?= Yii::t('guide', 'Put a profile picture') ?></li>
				<li><?= Yii::t('guide', 'Add not less than 5 photos. Otherwise Instagram can take you as a bot.') ?></li>
				<li><?= Yii::t('guide', 'Regularly add new photos.') ?></li>
				<li><?= Yii::t('guide', 'Watch over your photos, they must be in line the Instagram politics.') ?></li>
			</ul>

		</div>

		<div class="wrap-img col-xs-12 col-sm-6 col-md-4">
			<?= Html::img(['@img/instruction/photo.png'], ['alt' => 'photo', 'class' => 'photo']) ?>
		</div>
	</section>

	<section class="wrap-cont row">

		<div class="wrap-img col-xs-12 col-sm-6 col-md-4">
			<?= Html::img(['@img/instruction/clock.png'], ['alt' => 'clock', 'class' => 'clock']) ?>
		</div>

		<div class="wrap-text col-xs-12 col-sm-6 col-md-8">
			<h2><?= Yii::t('guide', 'Use of the time') ?></h2>

			<ul class="disordered-list">
				<li>
					<?= Yii::t('guide', 'Each user can use three free days for testing Zengram. During this time you will be able to see how the growth of your followers increases. And to evaluate their quality.') ?>
				</li>

				<li>
					<?= Yii::t('guide', 'The general time is writing off for work of all connected Instagram accounts (further - projects). For example, if you connect two projects and start them, so three test days at the control panel will be spent in 1,5 days') ?>
				</li>

				<!--<li>
					<?= Yii::t('guide', 'If you want to limit work of the project for some time - use the timer.') ?>
				</li>-->

				<li>
					<?= Yii::t(
						'guide',
						'You can buy time directly at the control panel, using the main packages. Also you can get time at the page of <a href="{price_url}">price</a> where all packages and your bonuses can reach 60%! If you have a big number of projects and you want to receive special conditions, <a href="{support_url}" data-toggle="modal" data-target="{support_url}">contact us</a>.',
						['price_url' => '/page/price', 'support_url' => '#support']
					) ?>
				</li>
			</ul>
		</div>

	</section>

	<section class="wrap-cont row">

		<div class="wrap-text col-xs-12 col-sm-6 col-md-8">

			<h2><?= Yii::t('guide', 'How to set the Instagram project.') ?></h2>

			<ul class="disordered-list">
				<li>
					<?= Yii::t('guide', 'Use the button <strong>“Settings”</strong> for opening the Instagram project&#39;s settings. By default all settings are made optimally. You just need to choose the city, where you want to draw attention to your account.') ?>
				</li>

				<li>
					<?= Yii::t('guide', 'You can choose actions which Zengram will do instead of you in the tab <strong>“actions”</strong>. <strong>“To put likes”</strong> - means that Zengram will like the found photos. Choose <strong>“To comment”</strong> - and Zengram will leave comments. <strong>“To follow”</strong> - and Zengram will follow the found instagrammers. <strong>“To unfollow”</strong> - Zengram will unfollow all or only those who doesn&#39;t follow you') ?>
				</li>

				<li>
					<?= Yii::t('guide', 'You can choose one or several cities in the first tab of settings. You just need to type the first letters of the city, and the system will offer you the cities on your choice.') ?>
				</li>

				<li>
					<?= Yii::t('guide', 'In the tab of <strong>“comments”</strong> there are comments which Zengram will put down in the photo of instagrammers. By default we’ve added for you some successful comments in the spintax format. Each of them has several hundreds of comments versions. You can leave them or write your own as usual.') ?>
				</li>

				<li>
					<?= Yii::t('guide', 'You can write, for example, "cool photo" or use the spintax format. So the text {cool|excellent} {photo|photograph} will mean 4 comments at once: cool photo, excellent photo, cool photograph and excellent photograph. Try to use not less than 20 different comments for the successful work.') ?>
				</li>

				<li>
					<?= Yii::t('guide', 'Additional important requirements for the comments:') ?>
				</li>
			</ul>

			<ul class="order-list">
				<li><?= Yii::t('guide', 'The total length of the comment shouldn&#39;t be more than 300 signs.') ?></li>
				<li><?= Yii::t('guide', 'You shouldn&#39;t use  more than 4 hashtags in one comment.') ?></li>
				<li><?= Yii::t('guide', 'The comment shouldn&#39;t contain more than 1 url address.') ?></li>
				<li><?= Yii::t('guide', 'The comment shouldn&#39;t consist only of CAPITAL LETTERS.') ?></li>
				<li><?= Yii::t('guide', 'Comments must be as different as possible.') ?></li>
			</ul>

		</div>

		<div class="wrap-img col-xs-12 col-sm-6 col-md-4">
			<?= Html::img(['@img/instruction/setting.png'], ['alt' => 'setting', 'class' => 'settings']) ?>
		</div>

	</section>

	<section class="wrap-cont row">

		<div class="wrap-img col-xs-12 col-sm-6 col-md-4">
			<?= Html::img(['@img/instruction/button.png'], ['alt' => 'setting', 'button' => 'button']) ?>
		</div>

		<div class="wrap-text col-xs-12 col-sm-6 col-md-8">
			<h2><?= Yii::t('guide', 'Start of the project') ?></h2>

			<ul class="disordered-list">
				<li><?= Yii::t('guide', 'Use the button "start/stop" to start the Instagram project.') ?></li>
				<li><?= Yii::t('guide', 'At the control panel you can stop or start all projects at once.') ?></li>
				<!--<li><?= Yii::t('guide', 'It is possible to plan duration of the Instagram project&#39;s start with the help of the timer.') ?></li>
				<li><?= Yii::t('guide', 'It is possible to set the timer only when the project is stopped. After the start under a timer icon you can see how much time your Instagram project will work.') ?></li>

		-->
			</ul>

		</div>

	</section>

</div>
