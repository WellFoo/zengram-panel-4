<?php
/* @var $this yii\web\View */

$this->title = Yii::t('views', 'Real Instagram Followers App | Best Instagram Followers App');

if (LANGUAGE == 'en') {

	$this->registerMetaTag([
		'name' => 'description',
		'content' => Yii::t('views', 'Zen-promo Instagram Followers app helps in bringing attention of real Instagram users providing increased number of followers along with the best comments & likes.')
	], 'description');

	$this->registerMetaTag([
		'name' => 'keywords',
		'content' => 'Real Instagram Followers App, Best Instagram Followers App, Instagram Auto Follow App'
	], 'keywords');
}
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="landing clone row" style="background: none">

	<!-- About us -->
	<section id="aboutUs" class="container-fluid about-us">

		<section class="container">
			<div class="row">
				<h1><?= Yii::t('views', 'About us') ?></h1>

				<div class="col-xs-12 col-sm-8 col-md-5 col-xs-offset-0 col-sm-offset-4 col-md-offset-7">
					<div class="row">
						<p>
							<?= Yii::t('views', 'Zengram is not just an app to get followers on Instagram, its main purpose is to drive as much attention as possible to your account using all the available means. You will not only get more real followers, likes and comments in the most natural way possible, you will be able to use professional tools to reach your target audience. Wonder how to get likes on Instagram? App is the answer.') ?>
						</p>
						<p>
							<?= Yii::t('views', 'The service is older than you might think: it has grown from a limited-access project for a certain group of professional influencers and promoters. After the promotion algorithms were tested and approved, we have polished the interface making it easy and safe for the wide audience to work with, added a few innovative features and rolled out this ultimate Instagram automation tool.') ?>
						</p>
						<?php if (Yii::$app->language === 'en') : ?>
						<p>
							<?= Yii::t('views', 'Unlike some Instagram followers apps, we don’t offer you free followers on Instagram or thousands of free likes that would only drive up empty numbers. Zengram is aimed at finding real people interested in the same things as you, so they can truly appreciate your work. Flexible settings allow you to choose what kind of users you’re looking for, as the universal nature of the service makes it perfect to promote any type of accounts. It does not matter if you post memes or fitness techniques, with our service to get likes on Instagram is easy as pie. Just sign up and get yourself a flow of Free Followers On Instagram. App has a free tree day trial, don’t miss the deal!') ?>
						</p>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>

	</section>
	<!-- About us end -->

</div>

<?php if (Yii::$app->language === 'en') : ?>
<style>
	.advantages p {
		margin: 10px 0;
		text-align: justify;
	}
</style>
<div class="container clone">
	<section class="about-us advantages">
		<h2 class="text-center" style="font-size: 160%; margin: 20px;"><?= Yii::t('views', 'Our advantages') ?></h2>
		<p><?= Yii::t('views', 'Minimalism and efficiency - the widest set of tools and features without superfluous settings. Promotion made easy and effective.') ?></p>
		<p><?= Yii::t('views', 'Sophisticated search algorithms - find potentially interested users via hashtags, geographic location and competitors.') ?></p>
		<p><?= Yii::t('views', 'Advanced geolocation - don’t stick to specific places on the city map, cover the entire town.  Target audience is closer than you think.') ?></p>
		<p><?= Yii::t('views', 'Compete efficiently - attract your competitors’ followers and show them your advantages.') ?></p>
		<p><?= Yii::t('views', 'Fully-automated basic Instagram interactions - start liking, commenting, following and unfollowing target and existing users with one click.') ?></p>
		<p><?= Yii::t('views', 'Track new comments on your pics and comment on others’ on-the-fly - use our enormous base of unique phrases and add yours.') ?></p>
		<p><?= Yii::t('views', 'Don’t fritter away your resources - get rid of non-followers quickly and effortlessly with our branded unfollowing system as soon as you hit the limit of 7 500 followings.') ?></p>
		<p><?= Yii::t('views', 'Functional web-interface - post pictures directly from your PC.') ?></p>
		<p><?= Yii::t('views', 'Set up your goals and forget about them until they’re reached - web-based nature of the service allows usage from any stationary or mobile device and protects your safety.') ?></p>
		<p><?= Yii::t('views', 'Quality over quantity is our motto - we won’t drive up the numbers in your bio with bots, we’ll bring you real, active target audience that you’ll be able to convert into loyal followers and customers.') ?></p>
	</section>
</div>
<?php endif; ?>