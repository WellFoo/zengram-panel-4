<?php
use yii\helpers\Html;

//$this->title = 'Our Blog | Zen-promo';
$this->title = 'Our Blog | Zen-promo';
$this->title = Yii::t('app', 'Our Blog | Zen-promo');
$this->registerMetaTag([
	'name' => 'description',
	'content' => Yii::t('views', 'Visit Zen-promo blog and read our latest update and news about real instagram followers, instagram comments, instagram likes and more!')
]);
$this->registerMetaTag([
	'name' => 'keywords',
	'content' => Yii::t('views', 'Buy Real Instagram Followers, Followers On Instagram, Get Instagram Followers, Get More Instagram Followers, Real Instagram Followers')
]);
$this->registerMetaTag([
	'name' => 'robots',
	'content' => 'index, follow'
]);

?>
<div class="blog">
	<h1><?= Yii::t('app', 'Blog') ?></h1>
	<?php
	/* @var $posts app\models\Post[] */
	foreach ($posts as $post) : ?>
		<div class="row blog-wrap">
			<div class="col-sm-2 col-md-1">
				<div class="blog-date">
					<div class="day"><?= \Yii::$app->formatter->asDate($post->date, 'dd'); ?></div>
					<div class="month"><?= \Yii::$app->formatter->asDate($post->date, 'MMM'); ?></div>
					<div class="year"><?= \Yii::$app->formatter->asDate($post->date, 'yyyy'); ?></div>
				</div>
			</div>
			<div class="col-sm-10 col-md-11">
				<div class="blog-post">
					<div class="blog-title container-fluid"><?= Html::a($post->title, ['page/blog/'.$post->alias]) ?></div>
					<div class="blog-content media">
						<?php if ($post->image): ?>
							<div class="media-left">
								<?= Html::a(
									Html::img($post->imagePreview, ['alt' => $post->title, 'class' => 'blog-thumb']),
										['page/blog/'.$post->alias]
								) ?>
							</div>
						<?php endif; ?>
						<div class="media-body">
							<?= empty($post->text_short) ? $post->text : $post->text_short ?>
						</div>
						<div class="text-right">
							<?= Html::a(Yii::t('views', 'Details'), ['page/blog/'.$post->alias]) ?>...
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>