<?php
    use yii\helpers\Html;
?>
<form action = "" method = "POST">
    <?=Html :: hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), [])?>
    <div style = "margin: 0px auto; width: 580px; margin-bottom: 50px;" >
        <h1 style = "margin: 15px 0px 5px 0px; font-size: 21px; font-weight: bold; text-align: center;">Управление рассылкой статистики</h1>
        <h2 style = "margin: 10px 0px 45px 0px; font-size: 21px; font-weight: bold; text-align: center;">для аккаунта <span style = "text-decoration: underline;"><?=$user->mail?></span></h2>


        <div class = "acc-tog-set" style = "border-bottom: none; margin-bottom: 0px; padding-bottom: 0px;">
            <div style = "display: block; width: 100%; float: left;">
                <div class = "subscribe-option-toggle">
                    <dt>
                        <input
                            type="checkbox"
                            id="every_day_option"
                            class="periodOption"
                            data-on="<i class='fa fa-check'></i>"
                            data-off="<i class='fa fa-times'></i>"
                            data-toggle="toggle"
                            data-style="ios"
                            data-onstyle="success"

                            name = "every_day"

                            <?=(($user->delivery_stats_type == 1 && $user->subscribe_stats) ? 'checked' : '')?>
                        />  Хочу получать письма со статистикой каждый день
                    </dt>
                </div>
            </div>
        </div>

        <div class = "acc-tog-set" style = "border-bottom: none; margin-bottom: 0px; padding-bottom: 0px;">
            <div style = "display: block; width: 100%; float: left; z-index: 999;">
                <div class = "subscribe-option-toggle">
                    <dt>
                        <input
                            type="checkbox"
                            id="every_week_option"
                            class="periodOption"
                            data-on="<i class='fa fa-check'></i>"
                            data-off="<i class='fa fa-times'></i>"
                            data-toggle="toggle"
                            data-style="ios"
                            data-onstyle="success"

                            name = "every_week"

                            <?=(($user->delivery_stats_type == 2 && $user->subscribe_stats) ? 'checked' : '')?>
                        /> Хочу получать письма со статистикой каждую неделю
                    </dt>
                </div>
            </div>
        </div>

        <div class = "acc-tog-set" style = "border-bottom: none;">
            <div style = "display: block; width: 100%; float: left;">
                <div class = "subscribe-option-toggle">
                    <dt>
                        <input
                            type="checkbox"
                            id="statsOption"
                            class="periodOption"
                            data-on="<i class='fa fa-check'></i>"
                            data-off="<i class='fa fa-times'></i>"
                            data-toggle="toggle"
                            data-style="ios"
                            data-onstyle="success"

                            name = "stats"

                            <?=((!$user->subscribe_stats) ? 'checked' : '')?>
                        /> Я не хочу получать письма со статистикой по своим аккаунтам
                    </dt>
                </div>
            </div>
        </div>
        <input type = "submit" class = "btn btn-primary" value = "Сохранить изменения" style = "margin-top: 45px;" />
        <script>
            $(document).ready(function () {
                var options = {
                    day: $("#every_day_option"),
                    week: $("#every_week_option"),
                    none: $("#statsOption"),
                };

                $('.periodOption').parents('.acc-tog-set').on('click', function (event) {

                    target = $(event.target).find('input');
                    target.bootstrapToggle('on');

                    var option;

                    for(option in options) {
                        var element = options[option];

                        if (element.attr('id') != target.attr('id')) {
                            $(element.get(0)).bootstrapToggle('off');
                        }
                    }
                });
            });

        </script>
    </div>
</form>