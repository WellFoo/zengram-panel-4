<?php
/** @var $is_payed int */
if (!isset($bannerType)){
    $bannerType = 'standart';
}
$path = '/banners/delivery_stats-min.png';
$discount = '';
$page = 'page/price';
switch($bannerType) {
    case 'standart':

        $code = 'mailpromo-1467';
        $discount = 'discount=' . $code . '&';

        if ($is_payed) {
            $path = '/banners/delivery_stats_payed-min.png';
            $campaignInfo = 'delivery_stats_' . date('Y-m-d') . '_payed';
        } else {
            $path = '/banners/delivery_stats-min.png';
            $campaignInfo = 'delivery_stats_' . date('Y-m-d') . '_trial';
        }
        break;
    case '5daysNoMoney':
        $path = '/banners/discount10percent.png';
        $code = 'mailpromo-5677';
        $discount = 'discount=' . $code . '&';
        $campaignInfo = 'delivery_stats_' . date('Y-m-d') . '_5daysNoMoney';
        break;
    case 'changeBalanceOnFollowers':
        $path = '/banners/changeBalanceOnFollowers.png';
        $campaignInfo = 'delivery_stats_' . date('Y-m-d') . '_changeBalanceOnFollowers';
        $page = '';
        break;
}
//$content = base64_encode(file_get_contents(__DIR__ . $path));
?>
<a href = "https://zengram.ru/<?= $page ?>?<?= $discount ?>access_key=<?=$token?>&utm_source=email&utm_medium=banner&utm_campaign=<?= $campaignInfo ?>" style = "margin: 0px auto; display: inline-block; width: 580px;">
    <img src = "https://zengram.ru<?=$path?>" alt = "скидка 10%" style = "width: 100%;" />
</a>
