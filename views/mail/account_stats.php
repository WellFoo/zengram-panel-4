<table width="100%" bgcolor="#ececec" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
    <tbody>
    <tr>
        <td>
            <table bgcolor="<?=$background?>" width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style = "border-bottom: 1px solid #dadada;border-top: 1px solid #fff;">
                <tbody>
                <tr>
                    <td>
                        <table width="500" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
                            <tbody>
                            <tr>
                                <td style="font-family: 'PT Sans', Arial, sans-serif; font-size: 14px; color: #6c6c6c; text-align: left; line-height: 20px;">
                                    <br>
                                    <div style="color:rgb(101,120,134);font-family:Helvetica;font-size:14px;line-height:21px;text-align:left;margin-top:20px; margin-bottom:40px;">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td style="vertical-align:top" align="left" width="280">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" width="60">
                                                                <img src="<?=$avatar?>" style="border:none;font-size:13px;font-weight:bold;min-height:60px;line-height:13px;outline:none;text-decoration:none;text-transform:capitalize;display:inline;width:60px;border-top-left-radius:100%;border-top-right-radius:100%;border-bottom-right-radius:100%;border-bottom-left-radius:100%;margin-right:10px" height="60" width="60">
                                                            </td>
                                                            <td style="vertical-align:top" align="left">
                                                                <br>
                                                                <span style="font-weight:normal; font-size:21px"><?=$login?></span>
                                                                <br>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td style="vertical-align:middle" align="right">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left">
                                                                <div style="color:rgb(101,120,134);font-family:Helvetica;font-size:14px;line-height:21px;text-align:center">
                                                                    <span style="font-size:26px;font-weight:200;color: #31a04a;"><?=number_format($account_media, 0, '', ',')?></span><br>
                                                                    <span>Медиа</span></div>
                                                            </td>
                                                            <td align="left">
                                                                <div style="color:rgb(101,120,134);font-family:Helvetica;font-size:14px;line-height:21px;text-align:center">
                                                                    <span style="font-size:26px;font-weight:200;color: #31a04a;"><?=number_format($account_followers, 0, '', ',')?></span><br>
                                                                    <span>Подписчиков</span>
                                                                </div>
                                                            </td>
                                                            <td align="left">
                                                                <div style="color:rgb(101,120,134);font-family:Helvetica;font-size:14px;line-height:21px;text-align:center">
                                                                    <span style="font-size:26px;font-weight:200;color: #31a04a;"><?=number_format($account_follows, 0, '', ',')?></span><br>
                                                                    <span>Подписок</span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div style="color:rgb(101,120,134);font-family:arial,sans-serif;font-size:18px;line-height:27px;text-align:left;text-transform:uppercase;">Обзор</div>

                                    <table style="width:540px;margin-top:10px;border-collapse:collapse;color:#424344;font-family:arial,sans-serif;font-size:14px;line-height:21px;">
                                        <tbody>
                                        <tr style = "border-bottom: 2px solid rgb(244,244,244);">
                                            <td style="width:190px;color: #666;font-weight: bold;"></td>
                                            <td style="width: 97px;color: #666;font-weight: bold;text-align: right;"><?=(($weekly_stats) ? 'Текущая неделя' : 'Вчера')?></td>
                                            <td style="width: 97px;color: #666;font-weight: bold;text-align: right;"><?=(($weekly_stats) ? 'Прошлая неделя' : 'Позавчера')?></td>
                                            <td style="width: 97px;color: #666;font-weight: bold;text-align: right;">Динамика</td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:10px;padding-bottom:10px;text-align:left">Кол-во подписчиков</td>
                                            <td style="padding-top:10px;padding-bottom:10px;text-align: right;"><?=$follower_count?></td>
                                            <td style="padding-top:10px;padding-bottom:10px;text-align: right;"><?=$pre_follower_count?></td>
                                            <td style="padding-top:10px;padding-bottom:10px;color:<?=$follower_count_color?>;text-align: right;"><?=$evolution_follower?></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-bottom:10px;text-align:left">Размещено медиа</td>
                                            <td style="padding-bottom:10px;text-align: right;"><?=$published_media?></td>
                                            <td style="padding-bottom:10px;text-align: right;"><?=$pre_published_media?></td>
                                            <td style="padding-bottom:10px;color:<?=$published_media_color?>;text-align: right;"><?=$evolution_published_media?></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-bottom:10px;text-align:left">Новых лайков *</td>
                                            <td style="padding-bottom:10px;text-align: right;"><?=$received_likes?></td>
                                            <td style="padding-bottom:10px;text-align: right;"><?=$pre_received_likes?></td>

                                            <?php
                                                if ($pre_received_likes < 1) {
                                                    if ($received_likes > 0) {
                                                        $evolution_received_likes_color = 'green';
                                                        $evolution_received_likes_format = '>100%';
                                                    } else {
                                                        $evolution_received_likes_color = '#424344';
                                                        $evolution_received_likes_format = '0%';
                                                    }
                                                } else {
                                                    $per = ($pre_received_likes / 100);

                                                    $evolution_received_likes = round(($received_likes / $per - 100), 0);

                                                    $evolution_received_likes_color = 'red';
                                                    $evolution_received_likes_format = $evolution_received_likes . '%';

                                                    if ($evolution_received_likes > 100) {
                                                        $evolution_received_likes_color = 'green';
                                                        $evolution_received_likes_format = '>100%';
                                                    } else {
                                                        if ($evolution_received_likes > 0) {
                                                            $evolution_received_likes_color = 'green';
                                                            $evolution_received_likes_format = '+' . $evolution_received_likes . '%';
                                                        }
                                                    }
                                                }
                                            ?>

                                            <td style="padding-bottom:10px;color:<?=$evolution_received_likes_color?>;text-align: right;">
                                                <?=$evolution_received_likes_format?>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td style="padding-bottom:10px;text-align:left">Новых комментариев *</td>
                                            <td style="text-align: right;padding-bottom:10px"><?=$received_comments?></td>
                                            <td style="text-align: right;padding-bottom:10px"><?=$pre_received_comments?></td>

                                            <?php
                                            if ($pre_received_comments < 1) {
                                                if ($received_comments > 0) {
                                                    $evolution_received_comments_color = 'green';
                                                    $evolution_received_comments_format = '>100%';
                                                } else {
                                                    $evolution_received_comments_color = '#424344';
                                                    $evolution_received_comments_format = '0%';
                                                }
                                            } else {
                                                $per = ($pre_received_comments / 100);

                                                $evolution_received_comments = round(($received_comments / $per - 100), 0);

                                                $evolution_received_comments_color = 'red';
                                                $evolution_received_comments_format = $evolution_received_comments . '%';

                                                if ($evolution_received_comments > 100) {
                                                    $evolution_received_comments_color = 'green';
                                                    $evolution_received_comments_format = '>100%';
                                                } else {
                                                    if ($evolution_received_comments > 0) {
                                                        $evolution_received_comments_color = 'green';
                                                        $evolution_received_comments_format = '+' . $evolution_received_comments . '%';
                                                    }
                                                }
                                            }
                                            ?>

                                            <td style="text-align: right;padding-bottom:10px;color:<?=$received_comments_color?>"><?=$evolution_received_comments_format?></td>
                                        </tr>
                                        <tr style = "border-bottom: 2px solid rgb(244,244,244);">

                                            <td style="padding-bottom:10px;text-align:left">Коэффициент вовлеченности **</td>
                                            <td style="text-align: right;padding-bottom:10px"><?=($engagement * 100)?>%</td>
                                            <td style="text-align: right;padding-bottom:10px"><?=($pre_engagement * 100)?>%</td>

                                            <?php
                                            $evolution_engagement = round(($engagement * 100) - ($pre_engagement * 100), 0);

                                                if ($evolution_engagement > 0) {
                                                    $evolution_engagement_color = 'green';
                                                    $evolution_engagement_format = '+' . $evolution_engagement . '%';
                                                } else {
                                                    if ($evolution_engagement == 0) {
                                                        $evolution_engagement_color = '#424344';
                                                        $evolution_engagement_format = '0%';
                                                    } else {
                                                        $evolution_engagement_color = 'red';
                                                        $evolution_engagement_format = $evolution_engagement . '%';
                                                    }
                                                }
                                            ?>

                                            <td style="text-align: right;padding-bottom:10px; color: <?=$evolution_engagement_color?>;"><?=$evolution_engagement_format?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <span style = "font-size: 12px;">
                                        * - по 5 последним медиа.<br>
                                        ** - соотношение лайков+комментов последнего медиа к подписчикам.

                                    </span>
                                    <br /><br />
                                    <div style="color:rgb(101,120,134);font-family:Helvetica;font-size:14px;line-height:21px;text-align:left;padding-bottom:20px">
                                        <div style="color:rgb(101,120,134);font-family:Helvetica;font-size:18px;line-height:27px;text-align:left;text-transform:uppercase;">Подписчики</div>
                                        <table style="border-collapse:collapse;text-align:left;width:540px;margin-top:20px">
                                            <thead><tr>
                                                <td style="text-transform:uppercase;font-size:10px;font-weight:bold;text-align:center;">Пришло</td>
                                                <td style="width:40px;"></td>
                                                <td style="text-transform:uppercase;font-size:10px;font-weight:bold;text-align:center;">Ушло</td>
                                                <td style="text-transform:uppercase;font-size:10px;font-weight:bold;padding-right:10px" align="right">
                                                    Популярные новые подписчики
                                                </td>
                                            </tr>
                                            </thead><tbody>
                                            <tr>
                                                <td style="font-size:30px;padding-top:10px;padding-bottom:10px;text-align:left;"><span style="color:rgb(74,186,69)">+ <?=$came?></span></td>
                                                <td></td>
                                                <td style="font-size:30px;padding-top:10px;padding-bottom:10px;text-align:left;"><span style="color:rgb(219,6,69)">- <?=$gone?></span></td>
                                                <td style="font-size:30px;padding-top:10px;padding-bottom:10px" align="right">
                                                    <table border="0" cellpadding="5" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <?php
                                                                    $topFilled = 0;
                                                                    if (count($top_followers) > 0) {
                                                                        foreach ($top_followers as $top) {
                                                                            $topFilled++;
                                                                            ?>
                                                                                <td style="font-size:30px;padding-top:10px;padding-bottom:10px" align="right" width="40">
                                                                                    <a href = "https://instagram.com/<?=$top['username']?>">
                                                                                        <img src="<?=$top['photo']?>" style="border:none;font-size:13px;font-weight:bold;min-height:40px;line-height:13px;outline:none;text-decoration:none;text-transform:capitalize;display:inline;width:40px;border-top-left-radius:100%;border-top-right-radius:100%;border-bottom-right-radius:100%;border-bottom-left-radius:100%" height="40" width="40">
                                                                                    </a>
                                                                                </td>
                                                                            <?php
                                                                        }
                                                                    }

                                                                    for ($i = 1; $i <= (5 - $topFilled); $i++) {
                                                                        ?>
                                                                            <td style="font-size:30px;padding-top:10px;padding-bottom:10px" align="right" width="40">
                                                                                <img src="https://zengram.ru/img/demo.png" style="border:none;font-size:13px;font-weight:bold;min-height:40px;line-height:13px;outline:none;text-decoration:none;text-transform:capitalize;display:inline;width:40px;border-top-left-radius:100%;border-top-right-radius:100%;border-bottom-right-radius:100%;border-bottom-left-radius:100%" height="40" width="40">
                                                                            </td>
                                                                        <?php
                                                                    }
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<div style = "height: 10px; background-color: #ececec; width: 100%;"></div>