<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RegisterForm */

$this->title = Yii::t('promo_page', 'Zengram for instagram');

$this->registerMetaTag([
	'name' => 'description',
	'content' => Yii::t('promo_page', 'Zengram for instagram promotion'),
]);

?>
<style>
	.landing .about-us p.landing-p {
		font-size: 20px;
	}

	.landing .about-us.about-us-p {
		min-height: 900px;
		background: url(/img/landing/about-us/bg_left.jpg) 0 160px no-repeat;
		background-size: 850px;
	}

	.landing .advantages-p {
		margin-bottom: 0;
	}

	.landing .advantages-item-p p {
		font-size: 18px;
		line-height: 28px;
	}

	.landing .how-work.how-work-p h2 {
		padding: 0 0 50px 0;
	}
</style>
<div class="landing row">

	<!-- Main block -->
	<section id="landMainBlock" class="container land-main-block">
		<div class="row">

			<div class="land-main-block-text col-xs-12 col-sm-6 col-md-6">
				<div class="row">
					<h1><?= Yii::t('promo_page', 'Instagram promotion!<br>Fast and effective!') ?></h1>

					<p>
						<?= Yii::t('promo_page', 'Our service can help you to gain new followers.<br>Zengram can make likes, send<br>comments, make follow for necessary<br>followers and unfollow from unnecessary.') ?>
					</p>

					<div class="text-center col-md-7">
						<a class="btn btn-danger btn-lg" data-toggle="modal" data-target="#regModal"><?= Yii::t('app', 'Sign up') ?></a>

						<div class="clearfix"></div>
						<a class="log-in" data-toggle="modal" data-target="#loginModal"><?= Yii::t('app', 'Log in') ?></a>
					</div>
				</div>
			</div>

			<div class="land-main-block-note col-xs-12 col-sm-6 col-md-6">
				<div class="row">
					<div class="land-note">
						<?= Html::img(['@img/landing/main-block/note.png'], [
							'alt' => Yii::t('promo_page', 'Promotion in Instagram')
						]) ?>
						<a class="btn btn-danger btn-lg" data-toggle="modal" data-target="#regModal"><?= Yii::t('promo_page', 'Try on') ?></a>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>

			<div class="text-center">
				<a href="#advantages" class="land-detail btn text-uppercase btn-lg"><?= Yii::t('promo_page', 'Details') ?></a>
			</div>

		</div>
	</section>
	<!-- Main block end -->

	<!-- About us -->
	<section id="aboutUs" class="container-fluid about-us about-us-p">

		<section class="container">
			<div class="row">
				<h2><?= Yii::t('promo_page', 'About service') ?></h2>

				<div class="col-xs-12 col-sm-8 col-md-5 col-xs-offset-0 col-sm-offset-4 col-md-offset-7">
					<div class="row">
						<p class="landing-p"><?= Yii::t('promo_page', 'Zengram was creates to help interested users pay attention to account in Instagram. With our service promotion becomes a pleasant and elementary. Without any special effort, you can get more likes and comments for photos, and, of course, a lot of followers in Instagram.') ?></p>

						<p class="landing-p">
							<?= Yii::t('promo_page', 'Initially, service Zengram was created for our personal needs. We did it for themselves, because seriously want to get likes, we needed true and loyal followers in Instagram and for all that we did not want to waste time on monotone handwork. As a result, we succeeded to create a special tool for easy and effective prootion proccess<br>Zengram - is real Instagram followers from your city. Real people who like your photos and are delighted by the fact that you basically do in Instagram.<br>We work with a personal account, with brands and agencies... in general, to all who need targeted followers. With Zengram followers promotion will be fast and you will easily become popular!') ?>
						</p>
					</div>
				</div>
			</div>
		</section>

	</section>
	<!-- About us end -->

	<!-- Advantages -->
	<section id="advantages" class="container advantages advantages-p">
		<div class="row">
			<h2><?= Yii::t('promo_page', 'We provide') ?></h2>

			<figure class="advantages-item advantages-item-p col-xs-12 col-sm-6 col-md-3">
				<img src="/img/landing/advantages/1.png" alt="<?= Yii::t('promo_page', 'First 3 days for free') ?>"/>
				<p>
					<?= Yii::t('promo_page', 'Free promotion for accounts the first three days after registration. You can check the work of our service without spending a cent! Three days is enough to see how Zengram profitly different from anything you faced earlier.') ?>
				</p>
			</figure>

			<figure class="advantages-item advantages-item-p col-xs-12 col-sm-6 col-md-4 col-md-offset-1">
				<img src="/img/landing/advantages/2.png" alt="<?= Yii::t('promo_page', 'Followers from your city!') ?>"/>
				<p>
					<?= Yii::t('promo_page', 'Possibility to choose a city in which the service will collect Instagram followers to your account. Promotion can be set to one or more cities.') ?>
				</p>
			</figure>

			<figure class="advantages-item advantages-item-p col-xs-12 col-sm-6 col-md-3 col-md-offset-1">
				<img src="/img/landing/advantages/3.png" alt="<?= Yii::t('promo_page', 'Very simple') ?>"/>
				<p>
					<?= Yii::t('promo_page', 'To start promoting instagram account in Zengram, you only need a couple of clicks. Select a city, actions that should be done: likes, follows or comments. And then press start.') ?>
				</p>
			</figure>

			<figure class="advantages-item advantages-item-p col-xs-12 col-sm-6 col-md-3">
				<img src="/img/landing/advantages/4.png" alt="<?= Yii::t('promo_page', 'Start and forget') ?>"/>
				<p>
					<?= Yii::t('promo_page', 'Zengram service is running on our server. Accordingly, it is enough to run project once and the service will carry promotion until you stop it.') ?>
				</p>
			</figure>

			<figure class="advantages-item advantages-item-p col-xs-12 col-sm-6 col-md-4 col-md-offset-1">
				<img src="/img/landing/advantages/5.png" alt="<?= Yii::t('promo_page', 'Full security') ?>"/>
				<p>
					<?= Yii::t('promo_page', 'We do not keep your data, as we are working through a secured protocol.') ?>
				</p>
			</figure>

			<figure class="advantages-item advantages-item-p col-xs-12 col-sm-6 col-md-3 col-md-offset-1">
				<img src="/img/landing/advantages/6.png" alt="<?= Yii::t('promo_page', 'Fast support') ?>"/>
				<p>
					<?= Yii::t('promo_page', 'If you need help - contact on any issue. Support responds quickly and efficiently.') ?>
				</p>
			</figure>
		</div>
	</section>
	<!-- Main block end -->

	<!-- How it's work -->
	<section id="howWork" class="container-fluid how-work how-work-p">

		<section class="container">
			<div class="row">
				<h2><?= Yii::t('promo_page', 'How to work with Zengram') ?></h2>

				<figure class="how-item col-xs-12 col-sm-6 col-md-6">
					<div class="row">
						<img src="/img/landing/how-work/1.jpg" alt=""/>
						<figcaption><?= Yii::t('promo_page', '1. Add Instagram account.') ?></figcaption>
						<p>
							<?= Yii::t('promo_page', 'Add necessary instagram account on dashboard.') ?>
						</p>
					</div>
				</figure>

				<div class="clearfix"></div>

				<figure class="how-item col-xs-12 col-sm-6 col-md-6 col-md-push-6">
					<div class="row">
						<img src="/img/landing/how-work/2.jpg" alt=""/>
						<figcaption><?= Yii::t('promo_page', '2. Set your actions.') ?></figcaption>
						<p>
							<?= Yii::t('promo_page', 'Choose city, where search will be occured. Configure the actions under which will be promotion of likes and followers will be occured. Actions are: liking, following, commenting and unfollowing.') ?>
						</p>
					</div>
				</figure>

				<figure class="how-item col-xs-12 col-sm-6 col-md-6  col-md-pull-6">
					<div class="row">
						<img src="/img/landing/how-work/3.jpg" alt=""/>
						<figcaption><?= Yii::t('promo_page', '3. Press "Start" button.') ?></figcaption>
						<p>
							<?= Yii::t('promo_page', 'All the rest does not require your direct participation, Zengram works on his own. You can safely shut down your computer, and work will continue in automatic mode.') ?>
						</p>
					</div>
				</figure>

				<div class="text-center">
					<a class="btn btn-danger btn-lg" data-toggle="modal" data-target="#regModal"><?= Yii::t('app', 'Sign up') ?></a>
				</div>

			</div>
		</section>

	</section>
	<!-- How it's work end -->

</div>
