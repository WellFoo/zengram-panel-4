<?php

namespace app\controllers;

use app\components\Controller;
use app\models\Enotify;
use app\models\LoginForm;
use app\models\RegisterForm;
use app\models\Users;
use Yii;
use yii\base\Exception;
use yii\web\Response;

class UserController extends Controller
{
	public function beforeAction($action)
	{
		if ($action->id == 'auth' || $action->id == 'register') {
			$this->enableCsrfValidation = false;
		}

		return parent::beforeAction($action);
	}

	function actionNotifications(){
		if (Yii::$app->user->isGuest) {
			return $this->redirect('/');
		}
		$dsn = Yii::$app->db->dsn;
		preg_match('#.*(host=(.+);)#ui', $dsn, $values);
		$host = $values[2];
		preg_match('#.*(dbname=(.+)$)#ui', $dsn, $values);
		$dbname = $values[2];
		$enotify = new ENotify($host,Yii::$app->db->username,Yii::$app->db->password,$dbname);
		$types = [ENotify::ENOTIFY_TYPE_SPECIAl, ENotify::ENOTIFY_TYPE_NEWS, ENotify::ENOTIFY_TYPE_PAYMENTS];
		if (!empty($_POST['enotify'])){
			$values = array_fill_keys($types, 0);
			foreach ($_POST['enotify'] as $type_id => $titem) {
				$values[$type_id] = 1;
			}
			if ($enotify->changeNotifies(Yii::$app->user->id, $values)){
				Yii::$app->session->setFlash('success', Yii::t('user', 'Notifications have been saved'));
			}
		}
		$notifications = $enotify->checkNotifies(Yii::$app->user->id, $types);
		return $this->render('notify', [
			'notifications' => $notifications
		]);
	}

	public function actionApi()
	{
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		Yii::$app->response->headers->set('Content-Type', 'text/javascript');

		$secret = Yii::$app->params['auth_secret_key'];

		$user_id 	= null;
		$mail 		= null;
		$token 		= md5($secret);

		if (!Yii::$app->user->isGuest)
		{
			$user_id	= Yii::$app->user->id;

			$user 		= Users::findOne($user_id);
			$mail 		= $user->mail;
			$token		= md5($user_id . $secret);
		}

		return $this->renderPartial('/api/auth.php', [
			'is_logged_in' 	=> (!Yii::$app->user->isGuest),
			'user_id' 		=> $user_id,
			'mail' 			=> $mail,
			'token' 		=> $token
		]);
	}

	public function actionRegister()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		Yii::$app->response->headers['Access-Control-Allow-Origin'] = '*';
		Yii::$app->response->headers['Access-Control-Allow-Methods'] = 'POST, GET';
		Yii::$app->response->headers['Access-Control-Max-Age'] = '1000';
		Yii::$app->response->headers['Access-Control-Allow-Headers'] = 'Content-Type, Authorization, X-Requested-With';

		$secret = Yii::$app->params['auth_secret_key'];

		$login = Yii::$app->request->post('login');
		$password = Yii::$app->request->post('password');

		$model = new RegisterForm();

		$model->mail = $login;
		$model->password = $password;

		if (!$model->validate()) {
			$error = 'Неизвестная ошибка';

			if ($model->hasErrors()) {
				if ($model->hasErrors('mail')) {
					$error = $model->getErrors('mail')[0];
				}

				if ($model->hasErrors('password')) {
					$error = $model->getErrors('password')[0];
				}
			}

			return [
				'status' => 'error',
				'message' => $error,
			];

		} else {
			$register = $model->register();

			$user = Users::findOne(['mail' => $login]);

			if (!$register || !$user)
			{
				$error = 'Неизвестная ошибка';

				if ($model->hasErrors()) {
					if ($model->hasErrors('mail')) {
						$error = $model->getErrors('mail')[0];
					}

					if ($model->hasErrors('password')) {
						$error = $model->getErrors('password')[0];
					}
				}

				return [
					'status' => 'error',
					'message' => $error,
				];
			}

			return [
				'status' => 'ok',
				'user' => [
					'id' => $user->id,
					'mail' => $user->mail
				],
				'token' => md5($user->id . $secret),
			];
		}
	}

	public function actionAuth()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		Yii::$app->response->headers['Access-Control-Allow-Origin'] = '*';
		Yii::$app->response->headers['Access-Control-Allow-Methods'] = 'POST, GET';
		Yii::$app->response->headers['Access-Control-Max-Age'] = '1000';
		Yii::$app->response->headers['Access-Control-Allow-Headers'] = 'Content-Type, Authorization, X-Requested-With';

		$secret = Yii::$app->params['auth_secret_key'];

		$login = Yii::$app->request->post('login');
		$password = Yii::$app->request->post('password');

		$model = new LoginForm();

		$model->mail = $login;
		$model->password = $password;


		if (!$user = $model->getUser()) {
			$model->addError('mail', Yii::t('app', 'Incorrect username or password.'));
		} else {
			if (!$user->validatePassword($model->password)) {
				$model->addError('password', Yii::t('app', 'Incorrect username or password.'));
			}
		}

		$error = '';

		if ($model->hasErrors()) {
			if ($model->hasErrors('mail')) {
				$error = $model->getErrors('mail')[0];
			}

			if ($model->hasErrors('password')) {
				$error = $model->getErrors('password')[0];
			}
		}

		if ($model->hasErrors()) {
			return [
				'status' 	=> 'error',
				'message' 	=> $error,
				'token' 	=> md5($secret),
			];
		} else {
			if (Yii::$app->user->isGuest) {
				$model->login();
			}

			return [
				'status' 	=> 'ok',
				'user'		=> [
					'id'	=> $user->id,
					'mail'	=> $user->mail
				],
				'token' 	=> md5($user->id . $secret),
			];
		}
	}
}
