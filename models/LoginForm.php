<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
	public $mail;
	public $password;

	private $_user = false;


	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			// username and password are both required
			[['mail', 'password'], 'required'],
			['mail', 'email', 'message' => Yii::t('app', 'Enter correct e-mail.')],
			// password is validated by validatePassword()
//            ['password', 'validatePassword'],
		];
	}

	/**
	 * Validates the password.
	 * This method serves as the inline validation for password.
	 *
	 * @param string $attribute the attribute currently being validated
	 * @param array $params the additional name-value pairs given in the rule
	 */
	public function validatePassword($attribute, $params)
	{
		if (!$this->hasErrors()) {
			$user = $this->getUser();
			
			if (!$user || !$user->validatePassword($this->password)) {
				$this->addError($attribute, Yii::t('app', 'Incorrect username or password.'));
			}
		}
	}

	/**
	 * Logs in a user using the provided username and password.
	 * @return boolean whether the user is logged in successfully
	 */
	public function login()
	{
		$user = $this->getUser();

		if ($user && $user->validatePassword($this->password)) {
			
			if (empty($user->continent)) {
				/** @var Users $users */
				$users = Users::findOne($user->id);
				$users->continent = Account::ipInfo("continent_code");
				$users->save();
			}
			return Yii::$app->user->login($user, 2592000);

		} else {
			$this->addError('password', Yii::t('app', 'Incorrect username or password.'));
			return false;
		}
	}

	/**
	 * Finds user by [[username]]
	 *
	 * @return Users|null
	 */
	public function getUser()
	{
		if ($this->_user === false) {
			$this->_user = Users::findByMail($this->mail);
		}

		return $this->_user;
	}
}
