<?php

namespace app\models;

use app\components\RPCHelper;
use app\components\GearmanClientSingleton;
use app\components\GearManTrait;
use app\components\UHelper;
use core\instagram\exceptions\CheckpointReqiredException;
use core\storage\MemoryStorage;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * This is the model class for table "accounts".
 *
 * @property integer $id
 * @property integer $date
 * @property string $source
 */
class InstashpionHint extends ActiveRecord
{
	const STATUS_PENDING = 0;
	const STATUS_FAIL = 1;
	const STATUS_ACCEPTED = 2;
	const STATUS_SUCCESS = 3;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'instashpion_hits';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['date'], 'integer'],
			['source', 'string'],
		];
	}
}
