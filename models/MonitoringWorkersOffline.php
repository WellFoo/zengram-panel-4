<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "monitoring_workers_offline".
 *
 * @property integer $id
 * @property string $worker
 * @property integer $monitoring_id
 * @property integer $checked
 * @property integer $complete
 * @property integer $offline_time
 */
class MonitoringWorkersOffline extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'monitoring_workers_offline';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['worker', 'monitoring_id', 'checked', 'complete', 'offline_time'], 'required'],
			[['monitoring_id', 'checked', 'complete', 'offline_time'], 'integer'],
			[['worker'], 'string', 'max' => 20],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'            => 'ID',
			'worker'        => 'Сервер',
			'monitoring_id' => 'Monitoring ID',
			'checked'       => 'Учтен в мониторинге',
			'complete'      => 'Поднялся',
			'offline_time'  => 'Время оффлайн',
			'starttime'     => 'Время падения',
		];
	}
}