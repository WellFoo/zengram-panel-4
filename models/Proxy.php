<?php

namespace app\models;

use app\components\RPCHelper;
use Yii;
use yii\db\Query;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "proxy".
 *
 * @property integer $id
 * @property string  $proxy
 * @property integer $cat_id
 * @property string  $continent
 * @property string  $status
 */
class Proxy extends ActiveRecord
{
	const UPDATE_STATUS_TRIES = 4;

	const STATUS_UNDEFINED = 'undefined';
	const STATUS_ERROR     = 'error';
	const STATUS_WARNING   = 'warning';
	const STATUS_OK        = 'ok';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'proxy';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['proxy', 'cat_id'], 'required'],
			['proxy', 'string', 'max' => 70],
			['continent', 'string'],
			['status', 'in', 'range' => [self::STATUS_UNDEFINED, self::STATUS_ERROR, self::STATUS_WARNING, self::STATUS_OK]],
			['status', 'default', 'value' => self::STATUS_UNDEFINED],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'     => 'ID',
			'proxy'  => 'Прокси',
			'cat_id' => 'Категория'
		];
	}

	static function getRandom()
	{
		return self::getFree()->toArray();
	}

	public function getCatalog()
	{
		return self::hasOne(ProxyCat::className(), ['id' => 'cat_id']);
	}

	/**
	 * @param integer|null $cat_id
	 * @param string|null  $continent
	 *
	 * @return $this|null
	 */
	public static function getFree($cat_id = null, $continent = null)
	{
		if (!empty(Yii::$app->params['use_continents'])) {
			if (empty($continent)){
				$continent = 'EU';
			}
			$where = sprintf('continent = \'%s\'', $continent);
		} else {
			if ($cat_id === null) {
				$cat = ProxyCat::findOne(['default' => 1]);
				$cat_id = $cat->id;
			}
			$where = sprintf('cat_id = %d', $cat_id);
		}

		$started = Account::getActiveAccounts();
		$andWhere = ($started) ? 'AND instagram_id IN ('.implode(',', $started).') ' : ' AND false';
		$row = Yii::$app->db->createCommand(sprintf(
			"SELECT
				p.id,
				(SELECT COUNT(*) FROM accounts WHERE proxy_id = p.id ".$andWhere.") AS c_run,
				(SELECT COUNT(*) FROM accounts WHERE proxy_id = p.id) AS c_all
			FROM proxy p
			WHERE %s AND status != '%s' AND status != '%s'
			ORDER BY c_run, c_all LIMIT 1",
			$where,
			self::STATUS_ERROR,
			self::STATUS_UNDEFINED
		))->queryOne();
		return self::findOne(['id' => $row['id']]);
	}

	public function getProxys()
	{
		// TODO: проверить использование и удалить в случае необходимости
		$started = Account::getActiveAccounts();
		$andWhere = ($started) ? 'AND instagram_id IN ('.implode(',', $started).') ' : ' AND false';
		$row = Yii::$app->db->createCommand("SELECT p.*, count(a.id) AS countpr
				FROM proxy AS p
				LEFT JOIN accounts AS a
				ON a.proxy_id=p.id ".$andWhere."
				GROUP BY p.id
				ORDER BY countpr ASC"
		)->queryAll();

		return $row;
	}

	public static function getProxies($cat_id = null)
	{
		$started = Account::getActiveAccounts();
		$andWhere = ($started) ? 'AND instagram_id IN ('.implode(',', $started).') ' : ' AND false';
		$proxies = (new Query())
			->select(['p.id', 'p.proxy', 'p.cat_id', 'COUNT(a.id) AS online', 'p.status'])
			->from(['p' => self::tableName()])
			->leftJoin(['a' => 'accounts'], 'a.proxy_id = p.id '.$andWhere);

		if ($cat_id !== null) {
			$proxies->where(['p.cat_id' => intval($cat_id)]);
		}

		return $proxies
			->groupBy('p.id')
			->orderBy(['online' => SORT_ASC])
			->all();
	}

	public function checkStatus()
	{
		$string = Yii::$app->security->generateRandomString();
		$ip = @file_get_contents(Yii::getAlias('@app/config/server_ip.raw'));
		$oldValue = Yii::$app->urlManager->baseUrl;
		if (!empty($ip)) {
			Yii::$app->urlManager->baseUrl = 'http://' . $ip . '/';
		}
		$url = Url::to([
			'/site/revert',
			'secret' => 'terces',
			'string' => $string
		], true);
		if (!empty($ip)) {
			Yii::$app->urlManager->baseUrl = $oldValue;
		}

		$ch = curl_init($url);
		curl_setopt_array($ch, [
			CURLOPT_PROXY => $this->proxy,
			CURLOPT_FOLLOWLOCATION => 1,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_TIMEOUT        => 10,
		]);

		$response = curl_exec($ch);

		return $string === strrev($response);
	}

	public function updateStatus()
	{
		$errors = 0;
		$success = 0;
		for ($i = 0; $i < self::UPDATE_STATUS_TRIES; $i++) {
			if ($this->checkStatus()) {
				$success++;
			} else {
				$errors++;
			}
		}
		if ($errors > 0 && $success > 0) {
			$this->status = self::STATUS_WARNING;
		} else if ($errors > 0) {
			$this->status = self::STATUS_ERROR;
		} else if ($success > 0) {
			$this->status = self::STATUS_OK;
		} else { // ну а вдруг :D
			$this->status = self::STATUS_UNDEFINED;
		}
		$this->save();
	}

	public static function spreadProxy($proxy)
	{
		$query = Account::find()
			->where(['proxy_id' => $proxy->id]);

		foreach ($query->each() as $account) {
			/** @var $account Account */

			$free = self::getFree($proxy->cat_id);
			if ($free === null) {
				return false;
			}

			$account->proxy_id = $free->id;
			$account->save();
			if ($account->monitoring_status === Account::STATUS_WORK) {
				RPCHelper::stopAccount($account, 'spreadProxy');
				RPCHelper::startAccount($account, 'spreadProxy');
			}
		}
		return true;
	}
}
