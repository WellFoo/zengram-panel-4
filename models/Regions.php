<?php

namespace app\models;

use app\components\TreeTrait;
use \yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%geo}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string  $name
 * Relations
 * @property Regions     $parent
 */
class Regions extends ActiveRecord
{
	use TreeTrait;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'regions';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['parent_id'], 'integer'],
			[['name'], 'required'],
			[['name'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'parent_id' => 'Родительская категория',
			'name' => 'Название',
		];
	}

	public function getParent()
	{
		return $this->hasOne(self::className(), ['id' => 'parent_id']);
	}
}
