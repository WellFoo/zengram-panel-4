<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "monitoring_log".
 *
 * @property integer $id
 * @property integer $monitoring_id
 * @property string $time
 * @property string $result
 * @property integer $success
 * @property integer $notify_sms
 * @property integer $notify_email
 */
class MonitoringLog extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'monitoring_log';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['monitoring_id', 'result', 'notify_sms', 'notify_email'], 'required'],
			[['monitoring_id'], 'integer'],
			[['notify_sms', 'notify_email', 'success'], 'boolean'],
			[['time'], 'safe'],
			[['result'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'monitoring_id' => 'Id отслеживания',
			'time'          => 'Время подсчета',
			'result'        => 'Результат',
			'notify_sms'    => 'Оповещение по смс',
			'notify_email'  => 'Оповещение по Email',
			'success'       => 'Норма',
		];
	}
}
