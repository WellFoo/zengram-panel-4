<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "actions".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $mail
 * @property string $datetime
 * @property string $data
 */

class Actions extends ActiveRecord
{
	const MAX_DAYS = 7;
	const CHECK_DAYS = 7;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'mail', 'datetime', 'data'], 'safe'],
		];
	}


	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'actions';
	}

}