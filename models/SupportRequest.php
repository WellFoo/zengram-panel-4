<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "pages".
*
	* @property integer $id
	* @property string $title
	* @property string $alias
	* @property string $text
	* @property string $subject
	* @property string $added
	* @property integer $weight
*/
class SupportRequest extends ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'support_request';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['user_id', 'text'], 'required'],
            [['text', 'added', 'subject'], 'string'],
            [['user_id'], 'integer']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'alias' => 'Alias',
            'text' => 'Text',
            'weight' => 'Weight',
        ];
    }
}
