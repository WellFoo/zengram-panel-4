<?php

namespace app\models;

use app\components\RPCHelper;
use yii\db\ActiveRecord;
use Yii;

/**
 * @property integer $id
 * @property integer $account_id
 * @property integer $insta_id
 * @property string  $login
 * @property string  $max_id
 * @property boolean $complete
 *
 * @property Account $account
 *
 * This is the model class for table "account_competitors".
 */

class AccountCompetitors extends ActiveRecord
{
	public $account;

	public static function tableName()
	{
		return 'account_competitors';
	}

	public function rules()
	{
		return [
			[['account_id', 'login'], 'required'],
			[['insta_id', 'max_id'], 'number'],
			['complete', 'boolean'],
			['login', 'checkLogin'],
		];
	}

//	public function checkLogin($attribute, $value)
//	{
//		if ($this->hasErrors()) {
//			return;
//		}
//
//		if ($this->account === null || !$this->account->findUser($this->login)) {
//			$this->addError($attribute, Yii::t('app', 'Unable to check account, please try again later'));
//			return;
//		}
//
//		$result = $this->account->findUserResult;
//
//		if ($result['result'] !== 'success') {
//			$this->addError($attribute, Yii::t('app', 'Unable to check account, please try again later'));
//			return;
//		}
//
//		if (empty($result['data'])) {
//			$this->addError($attribute, Yii::t('app', 'Account @{login} not found', ['login' => $this->login]));
//			return;
//		}
//		if (!empty($result['data']['users'][0])){
//			$userdata = $result['data']['users'][0];
//		} else {
//			$userdata = $result['data'];
//		}
//		if ($userdata['is_private']) {
//			$this->addError($attribute, Yii::t('app', 'Account @{login} is private', ['login' => $this->login]));
//			return;
//		}
//
//		$competitor = self::findOne(['account_id' => $this->account_id, 'insta_id' => $userdata['pk']]);
//		if (!is_null($competitor)) {
//			$this->addError($attribute, Yii::t('app', 'Account @{login} already exists', ['login' => $this->login]));
//			return;
//		}
//
//		$this->insta_id = $userdata['pk'];
//	}
	public function checkLogin($attribute, /** @noinspection PhpUnusedParameterInspection */$value)
	{
		if ($this->hasErrors()) {
			return;
		}

		Yii::error($this->account);

		if ($this->account === null) {
			$this->addError($attribute, Yii::t('app', 'Unable to check account, please try again later'));
			return;
		}

		$result = RPCHelper::findUser($this->account, $this->login);

		Yii::error($result);

		if ($result['status'] !== 'ok') {
			$this->addError($attribute, Yii::t('app', 'Unable to check account, please try again later'));
			return;
		}

		if (empty($result['data'])) {
			$this->addError($attribute, Yii::t('app', 'Account @{login} not found', ['login' => $this->login]));
			return;
		}

		if (!empty($result['data']['users'][0])){
			$userdata = $result['data']['users'][0];
		} else {
			$userdata = $result['data'];
		}
		if ($userdata['is_private']) {
			$this->addError($attribute, Yii::t('app', 'Account @{login} is private', ['login' => $this->login]));
			return;
		}

		$competitor = self::findOne(['account_id' => $this->account_id, 'insta_id' => $userdata['pk']]);
		if (!is_null($competitor)) {
			$this->addError($attribute, Yii::t('app', 'Account @{login} already exists', ['login' => $this->login]));
			return;
		}

		$this->insta_id = $userdata['pk'];
	}

}