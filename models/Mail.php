<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "mail".
 *
 * @property integer $id
 * @property string  $mail
 * @property string  $instagram_id
 * @property string  $type
 */
class Mail extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'mail';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['mail', 'type'], 'required'],
			[['mail', 'type'], 'string', 'max' => 150],
			['instagram_id', 'number'],
			[['type', 'mail', 'instagram_id'], 'unique',
				'targetAttribute' => ['type', 'mail', 'instagram_id'],
				'message' => 'The combination of Mail, Instagram ID and Type has already been taken.'
			]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'   => Yii::t('app', 'ID'),
			'mail' => Yii::t('app', 'Mail'),
			'type' => Yii::t('app', 'Type'),
		];
	}
}
