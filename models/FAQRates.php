<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "faq_rates".
 *
 * @property integer $faq_id
 * @property integer $user_id
 * @property integer $rating
 */
class FAQRates extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'faq_rates';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['faq_id', 'user_id', 'rating'], 'required'],
			[['faq_id', 'user_id', 'rating'], 'integer'],
			[['faq_id', 'user_id'], 'unique', 'targetAttribute' => ['faq_id', 'user_id'], 'message' => 'The combination of Faq ID and User ID has already been taken.']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'faq_id' => Yii::t('app', 'Faq ID'),
			'user_id' => Yii::t('app', 'User ID'),
			'rating' => Yii::t('app', 'Rating'),
		];
	}
}
