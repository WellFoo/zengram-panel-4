<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "pages".
*
	* @property integer $id
	* @property string $title
	* @property string $alias
	* @property string $text
	* @property string $linkTitle
	* @property integer $weight
*/
class Page extends ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'pages';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['title', 'alias'], 'required'],
            [['text', 'linkTitle'], 'string'],
            [['weight'], 'integer'],
            [['title', 'alias'], 'string', 'max' => 255]
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'alias' => 'Alias',
            'text' => 'Text',
            'weight' => 'Weight',
        ];
    }
}
