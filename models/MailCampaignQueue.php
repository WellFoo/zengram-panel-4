<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "mail_campaign_queue".
 *
 * @property integer $id
 * @property integer $campaign
 * @property string  $provider
 * @property string  $message
 * @property integer $date
 * @property integer $status
 */
class MailCampaignQueue extends ActiveRecord
{

	const STATUS_CREATED = 0;
	const STATUS_SENDED = 1;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'mail_campaign_queue';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'campaign', 'date', 'status'], 'integer'],
			[['provider', 'message'], 'string'],
		];
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {

			if ($insert) {

				if (empty($this->date)) {
					$this->date = time();
				}

				if (empty($this->status)) {
					$this->status = self::STATUS_CREATED;
				}

			}

			return true;

		}

		return false;
	}
}
