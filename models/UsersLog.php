<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "userts_log".
 * @property integer $id
 * @property string $date
 * @property string $text
 */
class UsersLog extends ActiveRecord
{
	public static function tableName()
	{
		return 'users_log';
	}
}