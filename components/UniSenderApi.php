<?php

namespace app\components;

class UniSenderApiException extends \Exception {} 

/**
 * API UniSender
 *
 * @see http://www.unisender.com/ru/help/api/
 * @version 1.3
 */
class UniSenderApi
{
	/**
	 * @var string
	 */
	private $_api_key;

	/**
	 * @var string
	 */
	private $_lang = 'en';

	/**
	 * @var string
	 */
	private $_encoding = 'UTF8';

	/**
	 * @var int
	 */
	private $_retryCount = 0;

	/**
	 * @var float
	 */
	private $_timeout;

	/**
	 * @var bool
	 */
	private $_compression = false;

	/**
	 * @param string $apiKey
	 * @param string $lang
	 * @param string $encoding
	 * @param integer $retryCount
	 * @param integer|null $timeout
	 * @param bool $compression
	 */
	public function __construct($apiKey, $lang = 'en', $encoding = 'UTF8', $retryCount = 4, $timeout = null, $compression = false)
	{
		$this->_api_key = $apiKey;

		if (in_array($lang, ['en', 'ru', 'it'])) {
			$this->_lang = $lang;
		}

		if (!empty($encoding)) {
			$this->_encoding = $encoding;
		}

		if (!empty($retryCount)) {
			$this->_retryCount = $retryCount;
		}

		if (!empty($timeout)) {
			$this->_timeout = $timeout;
		}

		if ($compression) {
			$this->_compression = $compression;
		}
	}

	/**
	 * @param string $name
	 * @param array $arguments
	 * @return string
	 */
	public function __call($name, $arguments)
	{
		if (!is_array($arguments) || empty($arguments)) {
			$params = array();
		} else {
			$params = $arguments[0];
		}

		return $this->callMethod($name, $params);
	}

	/**
	 * @return array
	 * @throws UniSenderApiException
	 */
	public function getLists()
	{
		return $this->callMethod('getLists')['result'];
	}

	public function createList($title)
	{
		$response = $this->callMethod('createList', [
			'title' => $title
		]);
		return $response['result']['id'];
	}

	/**
	 * @param array $fields
	 * @param array $data
	 * @return array
	 * @throws UniSenderApiException
	 */
	public function importContacts($fields, $data)
	{
		return $this->callMethod('importContacts', [
			'field_names' => $fields,
			'data' => $data
		]);
	}

	public function createField($name, $type = 'string')
	{
		return $this->callMethod('createField', [
			'name' => $name,
			'type' => $type
		]);
	}

	/**
	 * @param integer $list_id
	 * @param string  $sender_name
	 * @param string  $sender_email
	 * @param string  $subject
	 * @param string  $html_body
	 * @param string|null $text_body
	 * @return integer
	 * @throws UniSenderApiException
	 */
	public function createEmailMessage($list_id, $sender_name, $sender_email, $subject, $html_body, $text_body = null)
	{
		$params = [
			'sender_name'  => $sender_name,
			'sender_email' => $sender_email,
			'subject'      => $subject,
			'body'         => $html_body,
			'list_id'      => $list_id,
			'lang'         => 'ru'
		];

		if (is_null($text_body)) {
			$params['generate_text'] = 1;
		} else {
			$params['text_body'] = $text_body;
		}

		$result = $this->callMethod('createEmailMessage', $params)['result'];
		return $result['message_id'];
	}

	/**
	 * @param $message_id
	 * @param bool $track_read
	 * @param bool $track_links
	 * @return array
	 * @throws UniSenderApiException
	 */
	public function createCampaign($message_id, $track_read = false, $track_links = false)
	{
		return $this->callMethod('createCampaign', [
			'message_id'  => $message_id,
			'track_read'  => intval(!!$track_read),
			'track_links' => intval(!!$track_links),
			'defer'       => 1,
		])['result'];
	}

	/**
	 * @param array $data
	 * @return array
	 * @throws UniSenderApiException
	 */
	public function subscribe($data)
	{
		return $this->callMethod('subscribe', $data);
	}

	/**
	 * @param string $value
	 * @param string $Key
	 */
	private function iconv(&$value, $Key)
	{
		$value = iconv($this->_encoding, 'UTF8//IGNORE', $value);
	}

	/**
	 * @param string $value
	 * @param string $Key
	 */
	private function mb_convert_encoding(&$value, $Key)
	{
		$value = mb_convert_encoding($value, 'UTF8', $this->_encoding);
	}

	/**
	 * @param string $methodName
	 * @param array $params
	 * @return array
	 * @throws UniSenderApiException
	 */
	private function callMethod($methodName, $params = array())
	{
		if ($this->_encoding != 'UTF8') {
			if (function_exists('iconv')) {
				array_walk_recursive($params, array($this, 'iconv'));
			} else if (function_exists('mb_convert_encoding')) {
				array_walk_recursive($params, array($this, 'mb_convert_encoding'));
			}
		}

		$url = $methodName . '?format=json';

		if ($methodName == 'importContacts') {
			$url .= '&overwrite_tags=1';
		}


		if ($this->_compression) {
			$url .= '&api_key=' . $this->_api_key . '&request_compression=bzip2';
			$content = bzcompress(http_build_query($params));
		} else {
			$params = array_merge((array)$params, array('api_key' => $this->_api_key));
			$content = http_build_query($params);
		}

		$contextOptions = array(
			'http' => array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $content,
			)
		);

		if ($this->_timeout) {
			$contextOptions['http']['timeout'] = $this->_timeout;
		}

		$retryCount = 0;
		$context = stream_context_create($contextOptions);

		do {
			$Host = $this->getApiHost($retryCount);
			$result = $this->checkResponse($t = file_get_contents($Host . $url, false, $context));

			var_dump($t);

			$retryCount++;
		} while ($result === false && $retryCount < $this->_retryCount);

		if ($result === false) {
			throw new UniSenderApiException();
		}

		return $result;
	}

	/**
	 * @param int $retryCount
	 * @return string
	 */
	private function getApiHost($retryCount = 0)
	{
		if ($retryCount % 2 == 0) {
			return 'http://api.unisender.com/'.$this->_lang.'/api/';
		} else {
			return 'http://www.api.unisender.com/'.$this->_lang.'/api/';
		}
	}

	/**
	 * @param $response
	 * @return bool|array
	 */
	private function checkResponse($response)
	{
		$data = json_decode($response, JSON_OBJECT_AS_ARRAY);

		if (!empty($data['error']) && strrpos($data['error'], 'is not unique')) {
			// Уже добавлен
			return true;
		}

		if (is_null($data) || !isset($data['result'])) {
			return false;
		}
		return $data;
	}
}