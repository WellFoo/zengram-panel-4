<?php

namespace app\components;

class GearmanClientSingleton
{

	private static $_instance = null;

	public static function getInstance(){

		if (self::$_instance == null) {
			self::$_instance = new \GearmanClient();
			self::$_instance->addServer(\Yii::$app->params['gearmanServer'], '4730');
		}

		return self::$_instance;
	}

	private function __construct() {}
	private function __clone() {}

}