<?php

namespace app\components;

use app\models\MailCampaignQueue;
use app\models\MailTriggers;
use app\models\Users;
use app\models\UserTokens;
use app\modules\management\models\UniSenderCampaign;
use app\modules\management\models\UniSenderEmail;
use app\modules\management\models\UniSenderList;
use Exception;
use Yii;
use yii\base\Model;


class Mail extends Model
{
	const PROVIDER_UNISENDER = 'unisender';
	const PROVIDER_PECHKIN   = 'pechkin';

	// Используемый поставщик рассылок
	public $provider = self::PROVIDER_PECHKIN;
	public $prepare = true;
	public $send = true;

	public $users;
	public $list_name;

	public $fields = [];

	public $subject;
	public $body;

	public $tpl;

	public function __construct($data)
	{
		parent::__construct();

		$this->users     = $data['users'];
		$this->list_name = $data['list_name'];
		$this->subject   = $data['subject'];

		$this->tpl = new TemplateParser(
			LANGUAGE . '-mails',
			Yii::getAlias('@app/templates/')
		);
	}

	/**
	 * @param $task MailTriggers
	 * @return bool
	 * @internal param array $data
	 */
	public static function testSendMails($task)
	{
		$mail = new Mail([
			'users' => [
				['email' => 'personal.vitaly@gmail.com', 'ip' => '89.22.247.50', 'id' => '54'],
				['email' => 'personal.vitaly@gmail.com', 'ip' => '89.22.247.50', 'id' => '54'],
				['email' => 'dev.vitaly@gmail.com', 'ip' => '89.22.247.156', 'id' => '56']
			],
			'list_name' => self::getListName($task),
			'subject' => 'Zengram.ru - test',
		]);

		$mail->body = $mail->tpl->fillTemplate('main', [
			'CONTENT' => 'test',
			'BLOCK' => '',
			'FOOTER' => '',
		]);

		return $mail->send();
	}

	/**
	 * @param $task MailTriggers
	 * @param $data array
	 * @return bool
	 */
	public static function sendServiceRemind($task, $data)
	{
		$mail = new Mail([
			'users' => $data,
			'list_name' => self::getListName($task),
			'subject' => self::getSubject($task),
		]);

		$mail->body = $mail->tpl->fillTemplate('main', [
			'CONTENT' => $mail->tpl->fillTemplate('service-remind-'.$task->interval),
			'BLOCK' => '',
			'FOOTER' => $mail->tpl->fillTemplate('service-remind-footer'),
		]);

		return $mail->send();
	}

	/**
	 * @param $task MailTriggers
	 * @param $data
	 * @return bool
	 */
	public static function sendTestEndNeedMoney($task, $data)
	{
		$mail = new Mail([
			'users' => $data,
			'list_name' => self::getListName($task),
			'subject' => self::getSubject($task),
		]);

		$mail->body = $mail->tpl->fillTemplate('main', [
			'CONTENT' => $mail->tpl->fillTemplate('test-period-end-'.$task->interval),
			'BLOCK' => '',
			'FOOTER' => $mail->tpl->fillTemplate('test-period-end-footer-'.$task->interval),
		]);

		return $mail->send();
	}

	public static function sendNoMoneyNoHoney($task, $data)
	{
		$mail = new Mail([
			'users' => $data,
			'list_name' => self::getListName($task),
			'subject' => self::getSubject($task),
		]);

		$mail->body = $mail->tpl->fillTemplate('main', [
			'CONTENT' => $mail->tpl->fillTemplate('no-money-no-honey-'.$task->interval),
			'BLOCK' => (LANGUAGE == 'ru') ?
				$mail->tpl->fillTemplate('no-money-no-honey-add-block-'.$task->interval) : '',
			'FOOTER' => $mail->tpl->fillTemplate('no-money-no-honey-footer-'.$task->interval),
		]);

		return $mail->send();
	}

	public static function sendStatistic($task, $data)
	{
		$mail = new Mail([
			'users' 		=> $data['users'],
			'list_name' 	=> 'Рассылка для пользователей у которых ' . $data['accounts_count'] . ' аккаунтов (' . date("d.m.Y / H:i:s") . ').', // self::getListName($task),
			'subject' 		=> 'Zengram.ru - Статистика по Вашим аккаунтам!',
		]);

		// Отправляем через unisender
		$mail->provider = self::PROVIDER_UNISENDER;

		$content = '';

		for ($i = 1; $i <= $data['accounts_count']; $i++) {
			$content .= '{{account-content-'.$i.'}}';
		}

		$mail->body 	=  $mail->tpl->fillTemplate('main-nopaddings', [
			'CONTENT' 	=> $mail->tpl->fillTemplate((($data['weekly']) ? 'header-for-weekly-stats' : 'header-for-stats'), ['DATE' => $data['date']]),
			'BLOCK' 	=> $content,
			'FOOTER' 	=> '',
		]);


		foreach ($data['users'] as $key => $user) {
			$body = $mail->body;

			foreach($user['additional_fields'] as $field => $value) {
				$body = str_replace('{{' . $field . '}}', $value, $body);
			}

			//file_put_contents(__DIR__ . '/mails/mail_prev_from_'.$data['accounts_count'] . (($data['weekly']) ? '_weekly' : '') .'_' . $user['id'] . '.html', $body);
		}

		$mail->fields = $data['fields'];

		return $mail->send();
	}

	public static function sendNoActiveProjects($task, $data)
	{
		$mail = new Mail([
			'users' => $data,
			'list_name' => self::getListName($task),
			'subject' => self::getSubject($task),
		]);

		$mail->body = $mail->tpl->fillTemplate('main', [
			'CONTENT' => (LANGUAGE == 'ru') ?
				$mail->tpl->fillTemplate('no-active-projects') :
				$mail->tpl->fillTemplate('no-active-projects-'.$task->interval),
			'BLOCK' => '',
			'FOOTER' => $mail->tpl->fillTemplate('no-active-projects-footer'),
		]);

		return $mail->send();
	}

	public static function getSubject($task)
	{
		switch ($task->action) {
			case MailTriggers::ACTION_NO_PAYMENT:

				if (LANGUAGE == 'ru') {
					return ($task->interval == 20) ?
						'Zengram.ru - Не время сбавлять темп! Подписчики Instagram жду именно тебя!' :
						'Zengram.ru - Подарок внутри! Получи подписчиков в Instagram вместе с нами!';
				} else {
					return ($task->interval == 20) ?
						'This is no time to slow down the pace! Subscribers Instagram waiting just for you!' :
						'Gift inside! Get subscribers Instagram with us!';
				}

				break;
			case MailTriggers::ACTION_NO_USE_BALANCE:

				if (LANGUAGE == 'ru') {
					return 'Помогите вашим подписчикам Instagram найти Вас, возобновив работы!';
				} else {
					return ($task->interval == 40) ?
						'Start your projects and get new subscribers now!' :
						'Notice that your Instagram account is waiting for the resumption of work';
				}

				break;
			case MailTriggers::ACTION_AFTER_REG:

				if (LANGUAGE == 'ru') {
					return 'Zengram.ru - Получите подписчиков в Instagram и завершите регистрацию, добавив свой аккаунт';
				} else {
					return ($task->interval == 5) ?
						'Get Instagram subscribers and complete the registration by adding your account' :
						'Want more subscribers to Instagram? Get 3 free day promotion.';
				}

				break;
			case MailTriggers::ACTION_FREE_DAYS:

				if (LANGUAGE == 'ru') {
					return ($task->interval == 5) ?
						'Zengram.ru - Получи подписчиков в Instagram со скидкой 10%. Скидка действует только 5 дней.' :
						'Zengram.ru - Получи подписчиков в Instagram с финальной скидкой 20%. Скидка действует 7 дней.';
				} else {
					return ($task->interval == 5) ?
						'Get subscribers to Instagram with 10% discount. The discount is valid only 5 days.' :
						'Get subscribers to Instagram with the final 20% discount. Discount is valid for 7 days.';
				}

				break;
			default:
				return Yii::$app->params['host'];
				break;
		}
	}

	public static function getListName($task)
	{
		return Yii::$app->params['host']
		. ': '.$task->name
		. ' ('.$task->interval.') '
		. ' от '.date('Y-m-d H:i:s');
	}

	public function send()
	{
		if (!$this->prepare) {
			var_dump('Подготовка к отправке сообщений отключена');
			return false;
			
		}

		if (count($this->users) == 0) {
			var_dump('Отсутствуют пользователи для отправки');
			return false;
		}

		switch ($this->provider) {
			case self::PROVIDER_PECHKIN:

				$api = new PechkinApi(Yii::$app->params['pechkin_login'], Yii::$app->params['pechkin_password']);

				$list_result = $api->lists_add($this->list_name);

				if (empty($list_result['list_id'])) {
					var_dump('херня');
					return false;
				}

				$list_id = $list_result['list_id'];

				$api->lists_add_merge($list_id, 'text', ['req' => 'on', 'var' => 'ATOKEN', 'title' => 'accesskey']);

				if (count($this->fields) > 0) {
					foreach ($this->fields as $field => $title) {
						$api->lists_add_merge($list_id, 'text', ['req' => 'on', 'var' => $field, 'title' => $title]);
					}
				}

				foreach ($this->users as $user) {

					$token = new UserTokens();
					$token->generate($user['id']);
					$token->save();

					$data = [
						'merge_1' => '',
						'merge_2' => '',
						'merge_3' => $token->token,
					];

					if (!empty($user['additional_fields'])) {
						$i = 4;
						foreach ($user['additional_fields'] as $value) {
							$data['merge_' . $i++] = $value;
						}
					}

					try {
						$api->lists_add_member($list_id, $user['email'], $data);
					} catch (Exception $e){

						var_dump($e->getMessage());

						if ($e->getMessage() !== 'Error code 6' && $e->getMessage() !== 'Error code 10') {
							continue;
						}

						$bad_user = Users::findOne([
							'mail' => $user['email']
						]);

						if (!is_null($bad_user)) {
							$bad_user->email_confirmed = false;
							$bad_user->save();
						}
					}
				}

				$unsubscribe_link = (LANGUAGE == 'ru') ?
					'<a id="unsub_link" href="%ОТПИСАТЬСЯ%">отписаться</a>' :
					'<a id="unsub_link" href="%ОТПИСАТЬСЯ%">unsubscribe</a>';

				// Рассылка
				$campaign = $api->campaigns_create([
					0 => $list_id
				], [
					'list_id' => $list_id,
					'name' => $this->list_name,
					'from_name' => Yii::$app->params['company'],
					'from_email' => Yii::$app->params['adminEmail'],
					'subject' => $this->subject,
					'html' => $this->body.$unsubscribe_link,
				]);

				if (!$this->send) {
					var_dump('Отправка сообщений отключена');
					return false;
				}

				$campaignQueue = new MailCampaignQueue([
					'campaign' => $campaign['campaign_id'],
					'provider' => $this->provider
				]);

				$campaignQueue->save();

				return self::startCampaign($campaignQueue, $api);

				break;
			case self::PROVIDER_UNISENDER:

				// Генерация листа
				$list = UniSenderList::create($this->list_name);

				$list->addField('ACCESS_TOKEN');

				if (count($this->fields) > 0) {
					foreach ($this->fields as $field => $title) {
						$list->addField($field);
					}
				}

				//$list->createFields();

				foreach ($this->users as $user) {
					$data = [
						'email' => $user['email'],
						'email_request_ip' => $user['ip']
					];

					$token = new UserTokens();
					$token->generate($user['id']);
					$token->save();

					$data['ACCESS_TOKEN'] = $token->token;

					if (!empty($user['additional_fields'])) {
						foreach ($user['additional_fields'] as $key => $value) {
							$data[$key] = $value;
						}
					}

					$list->addUser($data);
				}

				$list->importUsers();

				// Инициализация рассылки
				$message = new UniSenderEmail([
					'list_id' => $list->id,
					'from_name' => Yii::$app->params['company'],
					'from_address' => Yii::$app->params['adminEmail'],
					'subject' => $this->subject,
					'html_body' => $this->body,
				]);

				if (!$message->save()) {
					return false;
				}

				$campaignQueue = new MailCampaignQueue([
					'campaign' => $message->id,
					'provider' => $this->provider
				]);

				$campaignQueue->save();

				if (!$this->send) { // TODO: не уверен
					var_dump('Отправка сообщений отключена');
					return false;
				}

				return self::startCampaign($campaignQueue);

				break;
			default:
				return false;
				break;
		}
	}


	/**
	 * @param $campaign MailCampaignQueue
	 * @param $api | null PechkinApi
	 * @return bool
	 */
	public static function startCampaign($campaign, $api = null)
	{

		switch ($campaign->provider) {
			case self::PROVIDER_PECHKIN:

				if (is_null($api)) {
					$api = new PechkinApi(Yii::$app->params['pechkin_login'], Yii::$app->params['pechkin_password']);
				}

				try {

					$api->campaigns_update($campaign->campaign, [
						'status' => 'MODERATING'
					]);

					$campaign->status = MailCampaignQueue::STATUS_SENDED;

				} catch (\yii\base\Exception $e) {

					Yii::$app->mailer->compose()
						->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['company']])
						->setTo(Yii::$app->params['adminEmail'])
						->setSubject('Печкин, проблема с рассылкой')
						->setHtmlBody(
							'При попытке запуска рассылки произошла ошибка: <br>'.$e->getMessage()
						)->send();

					$campaign->message = $e->getMessage();
				}

				break;
			case self::PROVIDER_UNISENDER:

				$model = new UniSenderCampaign([
					'message_id' => $campaign->campaign
				]);

				if (!$model->create()) {
					// TODO: Добавить описание ошибки
					return false;
				}

				$campaign->status = MailCampaignQueue::STATUS_SENDED;

				break;
			default:
				return false;
				break;
		}

		$campaign->save();

		return true;
	}
}