<?php

namespace app\components;

trait UniSenderApiTrait
{
	private static $_api;

	private static function getApi()
	{
		if (is_null(self::$_api)) {
			self::$_api = new UniSenderApi(\Yii::$app->params['unisender_api_key']);
		}
		return self::$_api;
	}
}