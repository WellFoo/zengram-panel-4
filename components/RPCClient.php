<?php namespace app\components;

use PhpAmqpLib\Message\AMQPMessage;

class RPCClient
{
	private $_channel;
	private $_queue_name;
	private $_callback_queue_name;
	private $_correlation_id;
	private $_response;

	/**
	 * RPCClient constructor.
	 * @param string|null $queue_name
	 */
	public function __construct($queue_name = null)
	{
		if (empty($queue_name)) {
			$queue_name = \Yii::$app->params['rabbit_server']['queue_name'];
		}
		$this->_queue_name = $queue_name;

		$this->_channel = AMQPChannelSingleton::instance();
		list($this->_callback_queue_name, ,) = $this->_channel->queue_declare('', false, false, true, true);
		$this->_correlation_id = uniqid('AMQP_RPC_RESPONSE_');
		$this->_channel->basic_consume($this->_callback_queue_name, '', false, false, false, false, [$this, 'onResponse']);
	}

	/**
	 * @param string $function
	 * @param mixed $data
	 * @param int $timeout
	 * @return array|null
	 */
	public function call($function, $data, $timeout = 30)
	{
		$message = new AMQPMessage(
			json_encode([
				'function' => $function,
				'payload' => $data
			]), [
				'correlation_id' => $this->_correlation_id,
				'reply_to' => $this->_callback_queue_name
			]
		);

		$this->_channel->basic_publish($message, '', $this->_queue_name);
		try {
			while (is_null($this->_response)) {
				$this->_channel->wait(null, false, $timeout);
			}
		} catch (\Exception $e) {
			return [
				'status' => 'error',
				'message' => $e->getMessage()
			];
		}

		$resp = json_decode($this->_response, true);
		$this->_response = null;

		return $resp;
	}

	/**
	 * @param AMQPMessage $message
	 */
	public function onResponse($message)
	{
		if ($message->get('correlation_id') === $this->_correlation_id) {
			$this->_response = $message->body;
		}
		$this->_channel->close();
	}
}