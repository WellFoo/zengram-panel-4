<?php

class DB {
	// Конфигурация соединения
	/** @var  PDO */
	private $connection;
	public $query_count;
	private $config = array();

	public $error = '';

	public function __construct($config) {

		if (!is_array($config)) {
			$config = json_decode($config, true);
		}

		$this->config = $config;
	}

	// Соединение с базой данных
	private function connect() {

		try {
			$this -> connection = new PDO(
				$this->config['type'] . ':host=' . $this->config['host'] . ';dbname=' . $this->config['base'],
				$this->config['user'],
				$this->config['pass']
			);
			return true;
		} catch (PDOException $e) {
			$this -> error = $e -> getMessage();
			return false;
		}
	}

	// Запрос к базе
	public function query($sql, $parameters = '', $mode = 'assoc') {

		if (!$this -> connect()) return false;

		$this -> query_count++;

		if ($parameters === '') {
			$result = $this -> connection -> query($sql);
			if ($result === false) return false;
			$result -> execute();
		} else {
			$result = $this -> connection -> prepare($sql);
			if ($result === false) return false;
			$result -> execute($parameters);
		}

		switch($mode) {
			case 'assoc': // Полный ассоциативный массив
				$result = $result -> fetchAll(PDO::FETCH_ASSOC);
				break;
			case 'num': // Полный индексированный массив
				$result = $result -> fetchAll(PDO::FETCH_NUM);
				break;
			case 'bool': // Просто результат выполнения
				$result = true;
				break;
			case 'count': // Количество строк
				$result = $result -> rowCount();
				break;
			case 'id': // Последний присвоенный ID
				$result = $this -> connection -> lastInsertId();
				break;
			default:
				$result = $result -> fetchAll(PDO::FETCH_ASSOC);
				break;
		}

		// Закрывает соединение
		$this -> close();

		return $result;
	}

	/* Экраниорование */
	public function escapeSymbols($escaped_string) {

		if (!$this -> connect()) return $escaped_string;

		$escaped = $this -> connection -> quote($escaped_string);

		$this -> close();

		return $escaped;
	}

	// Закрытие соединения с базой
	private function close() {
		$this -> connection = null;
	}
} 