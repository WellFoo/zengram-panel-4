<?php namespace app\components;

use app\models\Task;
use Yii;
use yii\console\Controller;

class TaskController extends Controller
{
	/** @var Task */
	public $task = false;
	public $test = false;
	public $argv = [];

	public function beforeAction($action)
	{
		global $argv;

		$this->argv = $argv;

		if (!parent::beforeAction($action)) {
			return false;
		}
		
		if (!empty($this->argv[2]) && $this->argv[2] == 'test') {
			$this->test = true;
			return true;
		}
		
		$this->task = Task::getTask(Yii::$app->controller->route);
				
		if ($this->task === false) {
			self::log('Задача не найдена, выполнение завершается');
			return false;
		}

		self::log('Приступаем к выполнению задачи №'.$this->task->id);

		return true;
	}

	public function __destruct()
	{
		if ($this->task !== false) {
			self::log('Задача №'.$this->task->id.' завершена');
			$this->task->delete();
		}
	}

	public static function log($text, $end = true)
	{
		echo date('Y-m-d H:i:s').' - '.$text.($end ? ';' : '')."\r\n";
	}
}