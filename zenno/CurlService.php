<?php

class CurlService
{
	private $cookiefile = '';
	private $cookie = '';
	private $proxy = '';
	private $url = 'https://instagram.com/api/v1/';
	private $error = '';
	private $guid = '';
	private $useragent = '';
	public $endUrl = '';

	private $debug = false;

	/**
	 * @param       $url
	 * @param       $send_data
	 * @param array $duplicate
	 *
	 * @return mixed
	 */
	public function manualRequest($url, $send_data = [], $duplicate = [])
	{
		$ch = curl_init();
		if (!$this->useragent) {
			$this->useragent = $this->GenerateUserAgent();
		}
		if (!$this->guid) {
			$this->guid = $this->GenerateGuid();
		}
		$data = [
			'device_id'    => "android-".$this->guid,
			'guid'         => $this->guid,
			'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
		];
		foreach ($send_data as $key => $val) {
			$data[$key] = $val;
		}
		$data = json_encode($data);
		$post = 'signed_body='.$this->GenerateSignature($data).'.'.urlencode($data).'&ig_sig_key_version=4';
		if ($duplicate) {
			foreach ($duplicate as $key => $duplicates) {
				foreach ($duplicates as $val) {
					$post .= '&'.$key.'='.$val;
				}
			}
		}
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_URL, $this->url.$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->useragent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		if ($this->proxy) {
			curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
		}
		if ($this->debug) {
//			curl_setopt($ch, CURLINFO_HEADER_OUT, true);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 30);
			curl_setopt($ch, CURLOPT_HEADER, true);
		}
		$headers = ["Accept-Language: en-US;q=0.6,en;q=0.4"];
		if ($this->cookie){
			curl_setopt ($ch, CURLOPT_COOKIE, $this->cookie);
		}
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		if ($this->cookiefile) {
			curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiefile);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiefile);
		}

		$response     = curl_exec($ch);
		$this->endUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
		if ($this->debug) {
			if (!$response) {
				$this->error = curl_error($ch);
			}
			echo
			'Proxy: ', $this->proxy, '<br>',
			'Cookie: ', $this->cookie, '<br>',
			'CURLOPT_URL: ', $this->url.$url, '<br>',
			'POST: ', $post, '<br>',
			'curl_info: <pre>';
			print_r(curl_getinfo($ch));
			echo '</pre><br>';
		}
		return $response;
	}

	public function login($user, $password)
	{
		return $this->manualRequest('accounts/login/', ['username' => $user, 'password' => $password]);
	}

	public function setPublic($user_id)
	{
		return $this->manualRequest('accounts/set_public/', ['user_id' => $user_id]);
	}

	public function getFriendship($user_id)
	{
		return $this->manualRequest('friendships/show/'.$user_id.'/', ['user_id' => $user_id]);
	}

	public function getFollows($user_id, $max_id)
	{
		$url = 'friendships/'.$user_id.'/following/';

		$params = [];
		if ($max_id) {
			$params['max_id'] = $max_id;
			$url .= '?max_id='.$max_id;
		}
		return $this->manualRequest($url, $params);
	}

	public function getMany($user_ids, $max_id = 0)
	{
		$url = 'friendships/show_many/';

		$params = [];
		if ($max_id) {
			$params['max_id'] = $max_id;
			$url .= '?max_id='.$max_id;
		}
		return $this->manualRequest($url, [], ['user_id' => $user_ids]);
	}

	public function makeUnfollow($user_id)
	{
		return $this->manualRequest('friendships/destroy/'.$user_id.'/'
			, ['user_id' => $user_id]
		);
	}

	public function makeFollow($user_id)
	{
		return $this->manualRequest('friendships/create/'.$user_id.'/'
			, ['user_id' => $user_id]
		);
	}

	public function makeLike($media_id)
	{
		return $this->manualRequest('media/'.$media_id.'/like/'
//			,['media_id' => $media_id]
		);
	}

	public function makeComment($media_id, $comment)
	{
		return $this->manualRequest('media/'.$media_id.'/comment/', [
//			'media_id' => $media_id,
			'comment_text' => $comment
		]);
	}

	private function GenerateGuid()
	{
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			mt_rand(0, 65535),
			mt_rand(0, 65535),
			mt_rand(0, 65535),
			mt_rand(16384, 20479),
			mt_rand(32768, 49151),
			mt_rand(0, 65535),
			mt_rand(0, 65535),
			mt_rand(0, 65535));
	}

	private function GenerateUserAgent()
	{
		$resolutions = array('720x1280', '320x480', '480x800', '1024x768', '1280x720', '768x1024', '480x320');
		$versions    = array('GT-N7000', 'SM-N9000', 'GT-I9220', 'GT-I9100');
		$dpis        = array('120', '160', '320', '240');

		$ver = $versions[array_rand($versions)];
		$dpi = $dpis[array_rand($dpis)];
		$res = $resolutions[array_rand($resolutions)];

		return
			'Instagram 4.'.mt_rand(1, 2).'.'.mt_rand(0, 2).' Android ('.mt_rand(10, 11).'/'.mt_rand(1, 3).'.'
			.mt_rand(3, 5).'.'.mt_rand(0, 5).'; '.$dpi.'; '.$res.'; samsung; '.$ver.'; '.$ver.'; smdkc210; en_US)';
	}

	private function GenerateSignature($data)
	{
		return hash_hmac('sha256', $data, 'b4a23f5e39b5929e0666ac5de94c89d1618a2916');
	}

	/**
	 * @param string $proxy
	 */
	public function setProxy($proxy)
	{
		$this->proxy = $proxy;
	}

	/**
	 * @param string $cookiefile
	 */
	public function setCookiefile($cookiefile)
	{
		$this->cookiefile = $cookiefile;
	}

	public static function init($settings)
	{
		$res = new static();

		if (isset($settings['proxy']) && $settings['proxy']) {
			$res->setProxy($settings['proxy']);
		}
		if (isset($settings['useragent']) && $settings['useragent']) {
			$res->setUseragent($settings['useragent']);
		}
		if (isset($settings['guid']) && $settings['guid']) {
			$res->setGuid($settings['guid']);
		}
		if (isset($settings['cookie']) && $settings['cookie']) {
			$res->setCookie($settings['cookie']);
		}

		return $res;
	}

	public function getStats($user_id)
	{
		return $this->manualRequest('users/'.$user_id.'/info/', ['user_id' => $user_id]);
	}

	/**
	 * @return string
	 */
	public function getGuid()
	{
		return $this->guid;
	}

	/**
	 * @return string
	 */
	public function getUseragent()
	{
		return $this->useragent;
	}

	/**
	 * @param string $guid
	 */
	public function setGuid($guid)
	{
		$this->guid = $guid;
	}

	/**
	 * @param string $useragent
	 */
	public function setUseragent($useragent)
	{
		$this->useragent = $useragent;
	}

	/**
	 * @param boolean $debug
	 */
	public function setDebug($debug)
	{
		$this->debug = $debug;
	}

	/**
	 * @return string
	 */
	public function getError()
	{
		return $this->error;
	}

	/**
	 * @param string $cookie
	 */
	public function setCookie($cookie)
	{
		$this->cookie = $cookie;
	}
}