<?php
error_reporting(E_ALL | E_WARNING | E_PARSE);
//error_reporting(0);
include_once('CurlService.php');
if ($_COOKIE) {
	$cookiesStringToPass = '';
	foreach ($_COOKIE as $name => $value) {
		if ($cookiesStringToPass) {
			$cookiesStringToPass .= ';';
		}
		$cookiesStringToPass .= $name.'='.addslashes($value);
	}
	$_POST['cookie'] .= $cookiesStringToPass;
}
unset($_POST['cookie']);
$curl = CurlService::init($_POST);
$curl->setCookiefile($cookieFile = dirname(__FILE__).'/tmp/'.$_POST['login'].'.txt');
$relogin = false;
if (!file_exists($cookieFile)) {
	$relogin = true;
} else {
	$cookie = file_get_contents($cookieFile);
	if (strpos($cookie, 'ds_user_id') === false) {
		$relogin = true;
	}
}
if ($relogin) {
	$result = $curl->login($_POST['login'], $_POST['password']);
	if ($json = json_decode($result, true)) {
		if ($json['status'] != 'ok') {
			echo '{"status":"fail", "message":"login_required_script"}';
			exit;
		}
	} else {
		echo '{"status":"fail", "message":"login_required_script"}';
		exit;
	}
}
if (!isset($_GET['id'])) {
	echo '{"status":"fail", "message":"error_id"}';
	exit;
}
switch ($_GET['type']) {
	case 'like':
		$res = $curl->makeLike($_GET['id']);
		break;
	case 'follow':
		$res = $curl->makeFollow($_GET['id']);
		break;
	case 'unfollow':
		$res = $curl->makeUnfollow($_GET['id']);
		break;
	case 'comment':
		$res = $curl->makeComment($_GET['id'], $_POST['comment']);
		break;
	default:
		$res = '{"status":"fail", "message":"Error type"}';
}
if (strpos($res, 'login_required') !== false){
	@unlink($cookieFile);
}
echo $res;