<?php

namespace app\commands;

use app\components\TaskController;
use app\models\AccountComments;
use app\modules\admin\models\CommentingProject;
use app\modules\admin\models\PopularAccounts;
use app\modules\admin\models\PopularAccountsComments;
use app\modules\admin\models\PopularAccountsCommentsLog;
use app\modules\admin\models\PopularLog;
use yii\console\Controller;
use Yii;

class CommentController extends TaskController
{

	//const SERVER = 'fast-unfollow-worker.local/';
	const SERVER = '188.165.127.151/';

	public function actionCommentStar()
	{
		self::log('Скрипт комментирования запущен');

		$this->deleteComments();

		$hour = date('H');

		$projects = CommentingProject::find()
			->where(['cron_status' => CommentingProject::STATUS_ACTIVE])
			->andWhere(['<=', 'time_from', $hour])
			->andWhere(['>=', 'time_until', $hour]);

		self::log('Начинаем работу по проектам, всего: '.$projects->count());

		if ($projects->count() == 0) return false;

		/* @var $project CommentingProject */
		foreach ($projects->all() as $project) {
			$this->checkComments($project);
			$this->addComments($project);
		}

		self::log('Скрипт комментирования отработал');

		return true;
	}

	public function actionTestConnection()
	{
		self::log('Тест соединения с '.self::SERVER.'/commentPopular.php');
		$api_url = self::SERVER.'/commentPopular.php';

		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $api_url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($curl, CURLOPT_TIMEOUT, 60);

		$result = curl_exec($curl);

		var_dump($result);
	}

	public function getMultiResponse($requests, $script)
	{
		$api_key = 'lkm5yn2lv8727lkv57923875g2ov39';
		$api_url = self::SERVER.$script;

		self::log('Зпрос на адрес: '.$api_url);

		$mh = curl_multi_init();

		$response = [];
		$channels = [];

		self::log('Запрос отправлен, всего потоков: '.count($requests));
		
		foreach ($requests as $key => $data) {
			$data['key'] = $api_key;

			$ch = curl_init();
			curl_setopt_array($ch, [
				CURLOPT_URL              => $api_url,
				CURLOPT_POST             => true,
				CURLOPT_HEADER           => 0,
				CURLOPT_RETURNTRANSFER   => true,
				CURLOPT_POSTFIELDS       => $data,
				CURLOPT_CONNECTTIMEOUT   => 30,
				CURLOPT_TIMEOUT_MS       => 300 * 1000,
			]);

			curl_multi_add_handle($mh, $ch);

			$channels[$key] = $ch;
		}
		
		$still_running = null;

		do {
			curl_multi_select($mh);

			do {
				$rv = curl_multi_exec($mh, $still_running);
			} while ($rv == CURLM_CALL_MULTI_PERFORM);


		} while ($still_running);

		self::log('Получен ответ, обработка');

		foreach ($channels as $key => $channel) {
			$result_json = curl_multi_getcontent($channel);

			if ($result_json === false) {
				$response[$key] = false;
				continue;
			}

			$result = json_decode($result_json, true, 512, JSON_BIGINT_AS_STRING);

			if (!is_array($result) || empty($result['status']) || $result['status'] != 'ok') {
				self::log('Ошибка при выполнении запроса, ответ сервера: "'.$result_json.'"');
				$response[$key] = false;
				continue;
			}

			//self::log('Успех! Ответ сервера: "'.$result_json.'"');

			$response[$key] = $result['data'];

			curl_multi_remove_handle($mh, $channel);
		}

		curl_multi_close($mh);

		self::log('Обработка завершена');

		return $response;
	}

	private function deleteComments()
	{

		self::log('Запускается удаление комментариев');

		$comments = PopularAccountsComments::find()
			->where('comment_id IS NOT NULL AND expire_date IS NOT NULL AND expire_date <= '.time());

		self::log('Выбрано для удаления: '.$comments->count());

		if ($comments->count() == 0) return false;

		self::log('Начинаем удаление');

		$comments = $comments->all();

		$requests = [];

		/* @var $comment PopularAccountsComments */
		foreach ($comments as $comment) {

			/* @var $settings CommentingProject */
			$settings = CommentingProject::findOne($comment->project_id);

			$requests[$comment->id] = [
				'action' => 'delete',
				'settings' => json_encode($settings->toArray()),
				'stars_data' => json_encode([
					$comment->id => [
						'media_id'   => $comment->media_id,
						'comment_id' => $comment->comment_id,
						'user_id'    => $comment->user_id,
					]
				])
			];
		}

		$responses = $this->getMultiResponse($requests, 'commentPopular.php');

		self::log('Работаем с полученными данными');

		foreach ($responses as $id => $response_array) {
			if (!is_array($response_array)){
				var_dump($response_array);
				continue;
			}
			$response = array_shift($response_array);

			/* @var $comment PopularAccountsComments */
			$comment = PopularAccountsComments::findOne($id);

			if (is_null($comment)) continue;

			/* @var $settings CommentingProject */
			$settings = CommentingProject::findOne($comment->project_id);

			if (is_null($settings)) continue;

			if ($response['status'] !== 'success') {
				PopularAccountsCommentsLog::log([
					'code' => PopularAccountsCommentsLog::CODE_DELETE_FAILED,
					'instagram_id' => $comment->popular_id,
					'description' => [
						'media_id'   => $comment->media_id,
						'comment_id' => $comment->comment_id,
						'user_id'    => $comment->user_id,
					],
					'project_id' => $settings->id
				]);

				$comment->actionTry();

				continue;
			}

			PopularAccountsCommentsLog::log([
				'code' => PopularAccountsCommentsLog::CODE_DELETE_SUCCESS,
				'instagram_id' => $comment->popular_id,
				'description' => [
					'media_id'   => $comment->media_id,
					'user_id'    => $comment->user_id,
				],
				'project_id' => $settings->id
			]);

			$comment->delete();
		}

		self::log('Удаление завершено');

		return true;

	}

	private function checkComments($project)
	{

		self::log('Проверка наличия необработанных звёзд в базе по проекту: '.$project->name);

		/* @var $project CommentingProject */

		// Есть ли звёзды в базе
		$stars = PopularAccountsComments::find()
			->where('comment_id IS NULL')
			->andWhere(['project_id' => $project->id]);

		self::log('Найдено: '.$stars->count());

		if ($stars->count()) return false;

		// Необходимо заполнить базу
		$query = CommentingProject::getAccountQuery($project->popular_category);

		self::log('Добавляем в базу записи: '.$query->count());

		if (!$query->count()) return false;

		$popular_accounts = $query->all();

		$counter = 0;

		/* @var $account PopularAccounts */
		foreach ($popular_accounts as $account) {

			$comment = new PopularAccountsComments([
				'popular_id' => (string) $account->instagram_id,
				'try_count' => PopularAccountsComments::DEFAULT_TRY_COUNT,
				'project_id' => $project->id
			]);

			if (!$comment->save()) {
				var_dump($comment->getErrors());
			} else {
				$counter++;
			}
		}

		self::log('По проекту '.$project->name.' было добавлено записей: '.$counter);

		PopularAccountsCommentsLog::log([
			'code' => PopularAccountsCommentsLog::CODE_ADDED_ROWS,
			'description' => 'Добавлено аккаунтов для комментирования: '.$counter,
			'project_id' => $project->id
		]);

		return true;
	}

	private function addComments($project)
	{
		self::log('Приступаем к добавлению комментариев по проекту: '.$project->name);

		/** @var CommentingProject $project */

		$comments = PopularAccountsComments::find()
			->where('comment_id IS NULL')
			->limit($project->count_per_iteration);

		$count = $comments->count();

		if ($count > $project->count_per_iteration - 1) {
			$offset = mt_rand(0, $count - $project->count_per_iteration - 1);
		} else {
			$offset = 0;
		}

		$comments = $comments->offset($offset)->all();

		shuffle($comments);
		$requests = [];

		self::log('Выбрано для комментирования: '.count($comments));

		if (!count($comments)) return false;

		/* @var $comment PopularAccountsComments */
		foreach ($comments as $comment) {
			$texts = AccountComments::getRandomComments($project->comment_account, 10, $project->obfuscation);

			if ($texts === false) {
				PopularAccountsCommentsLog::log([
					'code' => PopularAccountsCommentsLog::CODE_EMPTY_COMMENT,
					'project_id' => $project->id
				]);
				return false;
			}

			$requests[$comment->id] = [
				'action' => 'comment',
				'settings' => json_encode($project->toArray()),
				'stars_data' => json_encode([
					$comment->id => [
						'instagram_id' => $comment->popular_id,
						'comments'    => $texts
					]
				])
			];
		}

		$responses = $this->getMultiResponse($requests, 'commentPopular.php');

		self::log('Работаем с полученными данными');

		//return false;

		foreach ($responses as $id => $response_array) {

			/* @var $comment PopularAccountsComments */
			$comment = PopularAccountsComments::findOne($id);

			if (is_null($comment)) continue;

			if (!is_array($response_array)) {
				PopularAccountsCommentsLog::log([
					'code' => PopularAccountsCommentsLog::CODE_ERROR,
					'instagram_id' => $comment->popular_id,
					'description' => 'Ошибка при выполнении запроса.',
					'project_id' => $project->id
				]);

				$comment->actionTry();

				continue;
			}
			foreach ($response_array as $response) {
				if (empty($response['status']) || $response['status'] !== 'success') {
					PopularAccountsCommentsLog::log([
						'code' => PopularAccountsCommentsLog::CODE_COMMENT_FAILED,
						'instagram_id' => $comment->popular_id,
						'description' => [
							'media_id'    => $response['media_id'],
							'user_id'    => $response['commentator_id'],
							'text'       => $response['text'],
							'media_code' => $response['media_code'],
							'error' => empty($response['error']['message']) ? $response['error'] : $response['error']['message']
						],
						'project_id' => $project->id
					]);

					$comment->actionTry();

					continue;
				}

				$comment->setAttributes([
					'media_id'    => (string)$response['media_id'],
					'comment_id'  => (string)$response['comment_id'],
					'user_id'     => (string)$response['commentator_id'],
					'expire_date' => time() + $project->expired_time * rand(50, 70),
				]);

				if (!$comment->save()) {
					var_dump($response);
					var_dump($comment->getErrors());
				} else {
					PopularAccountsCommentsLog::log([
						'code' => PopularAccountsCommentsLog::CODE_COMMENT_SUCCESS,
						'instagram_id' => $comment->popular_id,
						'description' => [
							'media_id'   => $response['media_id'],
							'comment_id' => $response['comment_id'],
							'user_id'    => $response['commentator_id'],
							'media_code' => $response['media_code'],
							'text'       => $response['text']
						],
						'project_id' => $project->id
					]);
				}

				// Логи
				$log = new PopularLog();
				$log->setAttributes([
					'instagram_id' => $comment->popular_id,
					'date' => date('Y-m-d'),
					'comments' => 1,
					'media_update' => 0,
					'project_id' => $project->id,
				]);

				$log->save();


			}
		}

		self::log('Добавление завершено');

		return true;
	}
}
