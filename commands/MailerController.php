<?php

namespace app\commands;

use app\components\Mail;
use app\components\TaskController;
use app\models\Account;
use app\models\AccountStats;
use app\models\BalanceFlow;
use app\models\Invoice;
use app\models\MailCampaignQueue;
use app\models\MailCheck;
use app\models\MailTriggers;
use app\models\MailUserTriggers;
use app\models\Users;
use app\models\UserTokens;
use DateTime;
use IntlDateFormatter;
use MongoDB\Client;
use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\db\Expression;

class MailerController extends TaskController
{

	const DAY = 86400;
	const FIVE_DAYS = 432000;
	public $weeklyStats = false;
	private $sendedIds = [];
	public $taskMailerStats = null;
	private $usersSendCounter = 0;
	private $accountSendCounter = 0;
	private $summarySendMailStats = 0;

	public function actionSend()
	{
		self::log('Запуск рассылок');
		$triggers = MailTriggers::find()
			->orderBy(['interval' => SORT_DESC])->all();

		if (count($triggers) == 0) {
			self::log('Рассылок не найдено');
			return false;
		}

		/** @var MailTriggers $trigger */
		foreach ($triggers as $trigger) {
			self::log('Запуск действия: ' . $trigger->name);
			self::log('Интервал: ' . $trigger->interval);
			$this->makeAction($trigger);

			/*if (!$trigger->permanent) {
				$this->checkAction($trigger);
			}*/
		}

		$this->campaingStart();

		self::log('Рассылки отработаны');
		return true;
	}

	private function campaingStart()
	{
		// Попытка повторно отправить рассылки
		$campaigns = MailCampaignQueue::findAll([
			'status' => MailCampaignQueue::STATUS_CREATED
		]);

		if (count($campaigns) == 0) {
			self::log('Незапущенных рассылок не обнаружено');
			return false;
		}

		self::log('Запуск неотработанных рассылок: ' . count($campaigns));

		foreach ($campaigns as $campaign) {
			Mail::startCampaign($campaign);
		}

		return true;
	}

	/**
	 * @param $task MailTriggers
	 * @return bool
	 */
	private function makeAction($task)
	{
		switch ($task->action) {
			case MailTriggers::ACTION_NO_PAYMENT:
				return $this->noMoneyNoHoney($task);
				break;
			case MailTriggers::ACTION_NO_USE_BALANCE:
				return $this->noActiveProjects($task);
				break;
			case MailTriggers::ACTION_AFTER_REG:
				return $this->serviceRemind($task);
				break;
			case MailTriggers::ACTION_FREE_DAYS:
				return $this->testEndNeedMoney($task);
				break;
			/*case MailTriggers::ACTION_STATS:
				return $this->sendUserStats($task);
				break;*/
			default:
				return false;
				break;
		}
	}

	/**
	 * @param $task MailTriggers
	 * @return bool
	 */
	private function checkAction($task)
	{
		switch ($task->action) {
			case MailTriggers::ACTION_NO_PAYMENT:
				$users = $this->noMoneyNoHoneyUsers($task);
				break;
			case MailTriggers::ACTION_NO_USE_BALANCE:
				$users = $this->noActiveProjectsUsers($task);
				break;
			default:
				return false;
				break;
		}

		if (count($users) == 0) {
			self::log('Нет пользователей для сброса триггера');
			return false;
		}

		self::log('Выбрано пользователей для сброса триггера: ' . count($users));

		foreach ($users as $user_id) {
			MailUserTriggers::reset($user_id, $task->action);
		}

		return true;
	}

	/**
	 * @return array
	 */


	private function getAccountsForStats()
	{
		self::log('Получаем пачку аккаунтов для рассылки статистики по ним');

		// Получаем пользователей, у которых есть аккаунты, соответствующие нашим критериям
		$resultIds = (new \yii\db\Query())
			->select([
				new Expression('distinct accounts.user_id'),
			])
			->from('accounts')
			->join('LEFT JOIN', 'users', 'accounts.user_id = users.id')
			->andWhere(['not in', 'accounts.user_id', $this->sendedIds])// Исключаем уже обработанных
			->andWhere(['not in', 'accounts.message', ['passwordError', 'checkpointError']])
			->andWhere('accounts.auth_block = 0')
			->andWhere('users.subscribe_stats = true')
			->andWhere('users.delivery_stats_type = 1')
			->limit(1000)->all();


		if (empty($resultIds)) {
			self::log('Рассылка закончена. Обработано ' . $this->usersSendCounter . ' пользователей их аккаунтов: ' . $this->accountSendCounter);
			self::log('Реально рассылка произведена ' . $this->summarySendMailStats . ' пользователям');
			exit();
		}
		$userIds = array_unique(array_column($resultIds, 'user_id'));
		// Добавляем к обработанным сразу


		// Получаем список аккаунтов, принадлежащих пользователям
		$result = (new \yii\db\Query())
			->select([
				new Expression('distinct accounts.instagram_id'),
				'user_id',
				'accounts.instagram_id',
				'users.balance',
				'accounts.account_avatar',
				'accounts.login',
				'users.is_payed',
				'users.reg_ip',
				'accounts.account_media',
				'accounts.account_follows',
				'accounts.account_followers',
				'users.mail',
				new Expression(
					"
					(select date from account_stats
					where account_stats.user_id = accounts.user_id
					AND (
						likes > 0
						OR comments > 0
						OR follows > 0
						OR unfollows > 0)
					ORDER BY date DESC LIMIT 1 OFFSET 0) as last_user_action_date
					")
			])
			->from('accounts')
			->join('LEFT JOIN', 'users', 'accounts.user_id = users.id')
			->andWhere(['not in', 'accounts.message', ['passwordError', 'checkpointError']])
			->andWhere(['in', 'accounts.user_id', $userIds])
			->andWhere('accounts.auth_block = 0')
			->andWhere('users.subscribe_stats = true')
			->andWhere('users.delivery_stats_type = 1')
			->all();

		self::log('Получена пачка пользователей для рассылки: '
			. count($userIds)
			. ' у которых '
			. count($result)
			. ' аккаунтов');
		$this->usersSendCounter += count($resultIds);
		$this->accountSendCounter += count($result);


		$ids = array_column($result, 'instagram_id');

		$mongo = new Client('mongodb://zengram:4g8rrk1@83.220.168.147:27017,zengram:4g8rrk1@188.165.127.151:27017?replicaSet=zengram&connectTimeoutMS=300000');

		$map = <<<JS
			function() {
				var doc = this.value;
				
				var followersCount = (Object.keys(doc.followers)).length;
				var followersCameCount = (Object.keys(doc.camefollowers || {})).length;
				var followersGoneCount = (Object.keys(doc.gonefollowers || {})).length;
				var newMediaCount = (Object.keys(doc.newMedia)).length;
				var likes = 0;
				var newLikesCount = 0;
				var comments = 0;
				var newCommentsCount = 0;
				var engagement = 0;
			
				for (media_id in doc.newLikes) {
						newLikesCount = newLikesCount + doc.newLikes[media_id].length;
				}
			
				for (media_id in doc.newComments) {
						newCommentsCount = newCommentsCount + doc.newComments[media_id].length;
				}
			
				for (media in doc.lastMedia) {
						likes = likes + doc.lastMedia[media].activity.likes.length;
				}
			
				for (media in doc.lastMedia) {
						comments = comments + doc.lastMedia[media].activity.comments.length;
				}
			
				if (likes > 0) {
					engagement = (((likes + comments) / 5) / followersCount);
				}
			
				emit(doc.instagram_id, {
					followers: {
						total: followersCount,
						came: followersCameCount,
						gone: followersGoneCount
					},
					newLikes: newLikesCount,
					newComments: newCommentsCount,
					newMedia: newMediaCount,
					engagement: engagement,
					date: doc.date,
					top: doc.topFollowers
				});
			}
JS;

		$reduce = <<<JS
			function(instagram_id, map) { 
				var data = map[map.length - 1],
						previos_peroiod = map[map.length - 2];
						
				if (previos_peroiod.periods) {
						previos_peroiod = previos_peroiod.periods.current
				}
				
				var periods = {
						current: map[map.length - 1],
						previos: previos_peroiod
				};
				
				return {
						periods:periods
				}
			}
JS;
		if (empty($this->sendedIds)) {
			$mongo->zengram->command([
				'mapreduce' => 'out',
				'map' => $map,
				'reduce' => $reduce,
				'query' => [
					'value.date' => [
						'$gte' => new \MongoDB\BSON\Timestamp(0, strtotime(gmdate("Y-m-d") . " 00:00:00") - ((60 * 60 * 24 * 3) - (60 * 60 * 3 - 20))),
						'$lt' => new \MongoDB\BSON\Timestamp(0, strtotime(gmdate("Y-m-d") . " 00:00:00") - ((60 * 60 * 3)))
					],
					'value.followersCountProblem' => ['$ne' => true],
				],
				'out' => 'dailyStats'
			]);
		}


		$this->sendedIds = array_merge($this->sendedIds, $userIds);
		if ($this->weeklyStats) {
			$collection = $mongo->zengram->weeklyStats;
		} else {
			$collection = $mongo->zengram->dailyStats;
		}

		$cursor = $collection->find(
			[
				'_id' => ['$in' => $ids],
				'value.periods' => ['$exists' => true],
			]
		);


		$accounts = [];
		$dataCount = 0;
		foreach ($cursor as $document) {
			$dataCount++;
			if ($document['value']['periods'] && !empty($document['value']['periods'])) {
				$accounts[$document['_id']]['pre'] = $document['value']['periods']['previos'];
				$accounts[$document['_id']]['current'] = $document['value']['periods']['current'];
			} else {

				print 'Нет данных за прошлый период ' . $document['_id'] . "\r\n";

				$accounts[$document['_id']]['pre'] = [
					'followers' => [
						'total' => 0,
						'came' => 0,
						'gone' => 0,
					],
					'newMedia' => 0,
					'newLikes' => 0,
					'newComments' => 0,
					'engagement' => 0
				];
				$accounts[$document['_id']]['current'] = $document['value'];
			}
		}

		self::log('Извлечена статистика для ' . $dataCount . ' аккаунтов');

		foreach ($result as $index => $user) {
			if (!isset($accounts[$user['instagram_id']])) {
				unset($accounts[$user['instagram_id']], $result[$index]);
			}
		}


		return [$result, $accounts];
	}

	static function evolutionCalc($current, $prev)
	{
		if ($prev < 1) {
			if ($current > 0) {
				return 100;
			}
			return 0;
		}

		$per = ($prev / 100);

		$evolution = round(($current / $per - 100), 1);

		if ($evolution > 100) {
			return 100;
		}

		return $evolution;
	}

	static function formatEvolution($evolution, $percents_concat = true)
	{
		$color = 'red';

		if ($evolution == 0) {
			return ['inherit', ($evolution . (($percents_concat) ? '%' : ''))];
		}

		if ($evolution > 0) {
			$evolution = '+' . $evolution;
			$color = 'green';
		}

		return [$color, ($evolution . (($percents_concat) ? '%' : ''))];
	}

	public function actionStats()
	{
		if (is_null($this->taskMailerStats)) {
			$this->taskMailerStats = MailTriggers::findOne(43);
		}

		list($accounts, $accounts_stats) = $this->getAccountsForStats();


		$users = [];
		$groups = [];
		/*
		 * Сгрупируем аккаунты по своим пользователям
		 */
		foreach ($accounts as $account) {
			$users[$account['user_id']][] = $account;
		}

		/*
		 * Разобьем пользователей на группы по количеству аккаунов (каждая группа отдельная рассылка)
		 */
		foreach ($users as $user_id => $accounts) {
			//self::log('У пользователя '. $user_id . ' - '.count($accounts).' аккаунтов.');

			$groups[count($accounts)][$user_id] = [
				'accounts' => $accounts,
				'mail' => $accounts[0]['mail'],
				'id' => $accounts[0]['user_id'],
				'ip' => long2ip($accounts[0]['reg_ip']),
				'is_payed' => $accounts[0]['is_payed'],
				// добавить поле из селекта для определения критериев для рассылки
			];
		}

		self::log('Запускаем ' . count($groups) . ' рассылок.');

		/*
		 * Обходим все группы (рассылки) и формируем для них используемые поля.
		 * Для каждой рассыки, с разным кол. пользователей формируются разные поля разные поля.
		 * А так же заполняем созданные поля данными из аккаунтов пользователя.
		 */

		foreach ($groups as $accounts_count => $users) {
			$formatter = new \IntlDateFormatter('ru_RU', \IntlDateFormatter::FULL, \IntlDateFormatter::FULL);
			$formatter->setPattern('d MMMM');

			$date = $formatter->format(new DateTime(date('Y-m-d', time() - (60 * 60 * 24))));

			if ($this->weeklyStats) {
				$date = $formatter->format(new DateTime(date('Y-m-d', time() - (60 * 60 * 24 * 7)))) . ' - ' . $formatter->format(new DateTime(date('Y-m-d', time() - (60 * 60 * 24))));
			}

			$data = [
				'users' => [],
				'fields' => [],
				'date' => $date,
				'weekly' => $this->weeklyStats
			];

			/*
			 * Проходим пользователей группы для создания и заполнения полей полей рассылки
			 * для каждого аккаунта данного пользователя
			 */
			foreach ($users as $user) {
				/* Указываем данные пользователя необходимые для рассылки*/
				$item = [
					'email' => $user['mail'], //'personal.vitaly@gmail.com',
					'id' => $user['id'],
					'ip' => $user['ip'],
					'additional_fields' => []
				];

				$user['accounts'] = array_slice($user['accounts'], 0, 30);
				$data['accounts_count'] = count($user['accounts']);

				$item['additional_fields']['display_name'] = explode('@', $user['mail'])[0];
				$data['fields']['display_name'] = 'display username';

				foreach ($user['accounts'] as $iteration => $account) {
					$iteration = $iteration + 1;

					$stats = $accounts_stats[$account['instagram_id']]['current'];
					$pre = $accounts_stats[$account['instagram_id']]['pre'];

					$follower_count = $stats['followers']['total'];
					$pre_follower_count = $pre['followers']['total'];

					list($follower_count_color1, $evolution_follower1) = self::formatEvolution($follower_count - $pre_follower_count, false);

					$published_media1 = $stats['newMedia'];
					$pre_published_media1 = $pre['newMedia'];

					list($published_media_color1, $evolution_published_media1) = self::formatEvolution(($published_media1 - $pre_published_media1), false);

					$received_likes1 = $stats['newLikes'];
					$pre_received_likes1 = $pre['newLikes'];

					list($received_likes_color1, $evolution_received_likes1) = self::formatEvolution(self::evolutionCalc($received_likes1, $pre_received_likes1));

					$received_comments1 = $stats['newComments'];
					$pre_received_comments1 = $pre['newComments'];

					list($received_comments_color1, $evolution_received_comments1) = self::formatEvolution(self::evolutionCalc($received_comments1, $pre_received_comments1));

					$engagement = round($stats['engagement'], 2);
					$pre_engagement = round($pre['engagement'], 2);

					list($engagement_color, $evolution_engagement) = self::formatEvolution(self::evolutionCalc($engagement, $pre_engagement));

					$evolution_engagement = $engagement - $pre_engagement;

					$item['additional_fields']['account-content-' . $iteration] = $this->renderPartial('/mail/account_stats.php', [
						'login' => $account['login'],
						'avatar' => $account['account_avatar'],
						'account_media' => $account['account_media'],
						'account_follows' => $account['account_follows'],
						'account_followers' => $account['account_followers'],
						'follower_count' => $follower_count,
						'pre_follower_count' => $pre_follower_count,
						'evolution_follower' => $evolution_follower1,
						'published_media' => $published_media1,
						'pre_published_media' => $pre_published_media1,
						'evolution_published_media' => $evolution_published_media1,
						'received_likes' => $received_likes1,
						'pre_received_likes' => $pre_received_likes1,
						'evolution_received_likes' => $evolution_received_likes1,
						'received_comments' => $received_comments1,
						'pre_received_comments' => $pre_received_comments1,
						'evolution_received_comments' => $evolution_received_comments1,
						'engagement' => $engagement,
						'pre_engagement' => $pre_engagement,
						'evolution_engagement' => $evolution_engagement,
						'follower_count_color' => $follower_count_color1,
						'published_media_color' => $published_media_color1,
						'received_likes_color' => $received_likes_color1,
						'received_comments_color' => $received_comments_color1,
						'top_followers' => $stats['top'],
						'came' => $stats['followers']['came'],
						'gone' => $stats['followers']['gone'],
						'background' => (($iteration % 2) ? '#fff' : '#f7f7f7'),
						'weekly_stats' => $this->weeklyStats
					]);

					$data['fields']['account-content-' . $iteration] = 'content for account';
				}

				//print_r($user);
				// Получаем баланс пользователя
				$userBalance = $user['accounts'][0]['balance'];
				$userLastActionDate = $user['accounts'][0]['last_user_action_date'];
				//echo $userBalance . ' | ' . $userLastActionDate;
				$token = new UserTokens();
				$token->generate($user['id']);
				$token->save();


				$bannerType = 'standart';
				// Определяем, какой тип баннера отсылать
				// Те у кого нет денег более 5 дней, и не возобновил работу.
				if ($userBalance <= 0 && (time() - strtotime($userLastActionDate) > self::FIVE_DAYS)) {
					$bannerType = '5daysNoMoney';
				}

				// Те у кого на балансе остались бесплатные деньги, и нет активности
				if ($userBalance >= 0 && (time() - strtotime($userLastActionDate) > self::FIVE_DAYS)) {
					$bannerType = 'changeBalanceOnFollowers';
				}


				$item['additional_fields']['banner'] = $this->renderPartial('/mail/banner.php', [
					'is_payed' => $user['is_payed'],
					'token' => $token->token,
					'bannerType' => $bannerType
				]);

				$data['fields']['banner'] = 'banner';

				$data['users'][] = $item;
			}

			self::log('Создаем рассылку для ' . count($data['users']) . ' пользователей у которых ' . $accounts_count . ' аккаунтов');
			$this->summarySendMailStats += count($data['users']);

			Mail::sendStatistic($this->taskMailerStats, $data);
		}
		// Чистим переменные, чтобы освободить память
		unset($data);
		unset($item);
		// Рекурсия
		$this->actionStats();
	}

	private function noMoneyNoHoneyUsers($task)
	{
		// Ищем тех кто оплачивал наш сервис, но нет последующей оплаты
		$users = Users::find()
			->select([
				Users::tableName() . '.id',
				Users::tableName() . '.mail',
				Users::tableName() . '.reg_ip',
				MailUserTriggers::tableName() . '.date',
			])
			->leftJoin(MailUserTriggers::tableName(),
				Users::tableName() . '.id = ' . MailUserTriggers::tableName() . '.user_id AND action = ' . $task->action)
			->where(['and',
				['is_payed' => 1],
				['email_confirmed' => true],
				['<=', 'balance', 0]
			])
			->asArray();

		$users->addSelect(['stats' => sprintf(
			'(SELECT COUNT(*) FROM %s WHERE %s = %s AND %s >= \'%s\')',
			AccountStats::tableName(),
			AccountStats::tableName() . '.user_id',
			Users::tableName() . '.id',
			AccountStats::tableName() . '.date',
			date('Y-m-d', time() - self::DAY * $task->interval)
		)]);

		$data = [];

		foreach ($users->each() as $user) {
			if (empty($user['date'])) continue;
			if ((int)$user['stats'] <= 0) continue;

			$data[] = $user['id'];
		}

		return $data;
	}

	private function noActiveProjectsUsers($task)
	{
		// Ищем тех, у кого есть положительный ОПЛАЧЕННЫЙ БАЛАНС, но не запускаются проекты
		$users = Users::find()
			->select([
				Users::tableName() . '.id',
				Users::tableName() . '.mail',
				Users::tableName() . '.reg_ip',
				MailUserTriggers::tableName() . '.date',
			])
			->leftJoin(MailUserTriggers::tableName(),
				Users::tableName() . '.id = ' . MailUserTriggers::tableName() . '.user_id AND action = ' . $task->action)
			->where(['and',
				['is_payed' => 1],
				['>', 'balance', 0],
				['email_confirmed' => true],
			])
			->asArray();

		$users->addSelect(['stats' => sprintf(
			'(SELECT COUNT(*) FROM %s WHERE %s = %s AND %s >= \'%s\')',
			AccountStats::tableName(),
			AccountStats::tableName() . '.user_id',
			Users::tableName() . '.id',
			AccountStats::tableName() . '.date',
			date('Y-m-d', time() - self::DAY * $task->interval)
		)]);

		$data = [];

		foreach ($users->each() as $user) {
			if (empty($user['date'])) continue;
			if ((int)$user['stats'] <= 0) continue;

			$data[] = $user['id'];
		}

		return $data;
	}

	/**
	 * @param $task MailTriggers
	 * @return bool
	 */
	private function serviceRemind($task)
	{
		// Ищем тех, кто не добавил свой аккаунт на сервис
		$users = Users::find()
			->select([
				Users::tableName() . '.id',
				Users::tableName() . '.mail',
				Users::tableName() . '.reg_ip',
				MailUserTriggers::tableName() . '.date',
			])
			->leftJoin(MailUserTriggers::tableName(),
				Users::tableName() . '.id = ' . MailUserTriggers::tableName() . '.user_id AND action = ' . $task->action)
			->where([
				'demo' => 1,
				'email_confirmed' => true,
			])
			->andWhere(['<=', 'regdate', date('Y-m-d H:i:s', time() - self::DAY * $task->interval)])
			->asArray()
			->all();

		self::log('Выбрано пользователей: ' . count($users));

		$data = [];

		foreach ($users as $user) {

			if (strpos($user['mail'], 'hotmail') !== false) continue;
			if (strpos($user['mail'], 'outlook.com') !== false) continue;

			if (!empty($user['date'])) continue;

			$data[] = [
				'email' => $user['mail'],
				'ip' => long2ip($user['reg_ip']),
				'id' => $user['id']
			];

			MailUserTriggers::sended([
				'user_id' => $user['id'],
				'action' => $task->action,
				'date' => time()
			]);
		}

		self::log('Отправляется: ' . count($data));
		return Mail::sendServiceRemind($task, $data);
	}

	/**
	 * @param $task MailTriggers
	 * @return bool|void
	 */
	private function testEndNeedMoney($task)
	{
		// Ищем тех, у кого закончился тестовый период, но не было оплат
		$users = Users::find()
			->select([
				Users::tableName() . '.id',
				Users::tableName() . '.mail',
				Users::tableName() . '.reg_ip',
				BalanceFlow::tableName() . '.date',
			])
			->leftJoin(MailUserTriggers::tableName(),
				Users::tableName() . '.id = ' . MailUserTriggers::tableName() . '.user_id AND action = ' . $task->action)
			->leftJoin('(SELECT balance_flow.user_id UserID, MAX(balance_flow.id) MaxId FROM balance_flow GROUP BY UserID) BalanceFlow',
				'users.id = BalanceFlow.UserID ')
			->innerJoin('balance_flow', 'BalanceFlow.MaxId = balance_flow.id')
			->where([
				'demo' => 0,
				'is_payed' => 0,
				'email_confirmed' => true,
			])
			->andWhere(['<=', 'balance', 0])
			->andWhere(['is', MailUserTriggers::tableName() . '.date', null])
			->asArray()->all();

		$data = [];

		foreach ($users as $user) {

			if (strpos($user['mail'], 'hotmail') !== false) continue;
			if (strpos($user['mail'], 'outlook.com') !== false) continue;

			if (time() - strtotime($user['date']) < self::DAY * $task->interval) {
				continue;
			}

			$data[] = [
				'email' => $user['mail'],
				'ip' => long2ip($user['reg_ip']),
				'id' => $user['id']
			];

			MailUserTriggers::sended([
				'user_id' => $user['id'],
				'action' => $task->action,
				'date' => time()
			]);
		}

		self::log('Отправляется: ' . count($data));
		return Mail::sendTestEndNeedMoney($task, $data);
	}

	/**
	 * @param $task MailTriggers
	 * @return bool
	 */
	private function noMoneyNoHoney($task)
	{
		// Ищем тех кто оплачивал наш сервис, но нет последующей оплаты
		$users = Users::find()
			->select([
				Users::tableName() . '.id',
				Users::tableName() . '.mail',
				Users::tableName() . '.reg_ip',
				MailUserTriggers::tableName() . '.date',
			])
			->leftJoin(MailUserTriggers::tableName(),
				Users::tableName() . '.id = ' . MailUserTriggers::tableName() . '.user_id AND action = ' . $task->action)
			->where(['and',
				['is_payed' => 1],
				['email_confirmed' => true],
				['<=', 'balance', 0]
			])
			->asArray();

		$users->addSelect(['stats' => sprintf(
			'(SELECT COUNT(*) FROM %s WHERE %s = %s AND %s >= \'%s\')',
			AccountStats::tableName(),
			AccountStats::tableName() . '.user_id',
			Users::tableName() . '.id',
			AccountStats::tableName() . '.date',
			date('Y-m-d', time() - self::DAY * $task->interval)
		)]);

		self::log('Выбрано пользователей для отправки: ' . $users->count());

		$data = [];

		foreach ($users->each() as $user) {

			if (strpos($user['mail'], 'hotmail') !== false) continue;
			if (strpos($user['mail'], 'outlook.com') !== false) continue;

			if ((int)$user['stats'] > 0) continue;
			if (!empty($user['date'])) continue;

			$data[] = [
				'email' => $user['mail'],
				'ip' => long2ip($user['reg_ip']),
				'id' => $user['id']
			];

			MailUserTriggers::sended([
				'user_id' => $user['id'],
				'action' => $task->action,
				'date' => time()
			]);
		}

		self::log('Отправляется: ' . count($data));
		return Mail::sendNoMoneyNoHoney($task, $data);
	}

	/**
	 * @param $task MailTriggers
	 * @return bool
	 */
	private function noActiveProjects($task)
	{
		// Ищем тех, у кого есть положительный ОПЛАЧЕННЫЙ БАЛАНС, но не запускаются проекты
		$users = Users::find()
			->select([
				Users::tableName() . '.id',
				Users::tableName() . '.mail',
				Users::tableName() . '.reg_ip',
				MailUserTriggers::tableName() . '.date',
			])
			->leftJoin(MailUserTriggers::tableName(),
				Users::tableName() . '.id = ' . MailUserTriggers::tableName() . '.user_id AND action = ' . $task->action)
			->where(['and',
				['is_payed' => 1],
				['>', 'balance', 0],
				['email_confirmed' => true],
			])
			->asArray();

		$users->addSelect(['stats' => sprintf(
			'(SELECT COUNT(*) FROM %s WHERE %s = %s AND %s >= \'%s\')',
			AccountStats::tableName(),
			AccountStats::tableName() . '.user_id',
			Users::tableName() . '.id',
			AccountStats::tableName() . '.date',
			date('Y-m-d', time() - self::DAY * $task->interval)
		)]);

		self::log('Выбрано пользователей для отправки: ' . $users->count());

		$data = [];

		foreach ($users->each() as $user) {

			if (strpos($user['mail'], 'hotmail') !== false) continue;
			if (strpos($user['mail'], 'outlook.com') !== false) continue;

			if ((int)$user['stats'] > 0) continue;
			if (!empty($user['date'])) continue;

			$data[] = [
				'email' => $user['mail'],
				'ip' => long2ip($user['reg_ip']),
				'id' => $user['id']
			];

			MailUserTriggers::sended([
				'user_id' => $user['id'],
				'action' => $task->action,
				'date' => time()
			]);
		}

		self::log('Отправляется: ' . count($data));
		return Mail::sendNoActiveProjects($task, $data);
	}

	public function actionCheckEmails()
	{
		self::log('Запуск проверки e-mail адресов');

		$users = Users::find()
			->leftJoin(MailCheck::tableName(),
				Users::tableName() . '.id = ' . MailCheck::tableName() . '.user_id')
			->where(['or',
				[MailCheck::tableName() . '.mx_checked' => MailCheck::MX_NO_CHECKED],
				//[MailCheck::tableName().'.mx_checked' => MailCheck::MX_CHECKED_FALSE],
				['is', MailCheck::tableName() . '.mx_checked', null]
			]);

		if ($users->count() == 0) {
			self::log('Не найдено пользователей с неподтверждённым адресом');
			return false;
		}

		self::log('Выбрано пользователей для проверки: ' . $users->count());

		$success = 0;
		$fail = 0;

		/** @var Users $user */
		foreach ($users->each() as $user) {

			self::log('Пользователь: ' . $user->mail);

			try {
				$result = $this->checkEmail($user->mail);
			} catch (ErrorException $e) {
				$result = false;
			}

			self::log('Результат: ' . ($result ? 'успешно' : 'ошибка'));

			$check = MailCheck::findOne([
				'user_id' => $user->id
			]);

			if (is_null($check)) {
				$check = new MailCheck([
					'user_id' => $user->id,
					'user_email' => $user->mail,
				]);
			}

			$check->mx_checked = $result ? MailCheck::MX_CHECKED_TRUE : MailCheck::MX_CHECKED_FALSE;

			$check->save();

			if ($result) {
				$user->email_confirmed = true;
				$user->save();
				$success++;
			} else {
				$fail++;
			}
		}

		/*
		
		$message = Yii::$app->mailer->compose(...);
		$message->getSwiftMessage()->getHeaders()->addTextHeader('name', 'value');
		
		*/

		self::log('Проверка завершена. Из ' . $users->count() . ' было подтверждено ' . $success . ' и отбраковано ' . $fail);
		return true;
	}

	public function actionGetData()
	{

		$checks = MailCheck::find()->all();

		$result = [];

		$suspicious = [];

		$success = 0;
		$fail = 0;

		/** @var MailCheck $check */
		foreach ($checks as $check) {

			$email = substr($check->user_email, mb_strpos($check->user_email, '@') + 1);

			if (empty($result[$email])) {
				$result[$email] = [
					'success' => 0,
					'fail' => 0,
				];
			}

			if ($check->mx_checked === MailCheck::MX_CHECKED_TRUE) {
				$result[$email]['success']++;
				$success++;
			} else {
				$result[$email]['fail']++;
				$fail++;
			}

		}

		foreach ($result as $key => $item) {

			if ($item['success'] + $item['fail'] == 1) {
				unset($result[$key]);
				continue;
			}

			if ($item['success'] == 0 || $item['fail'] == 0) {
				unset($result[$key]);

				if ($item['success'] > 100 || $item['fail'] > 100) {
					$suspicious[$key] = $item;
				}

				continue;
			}
		}

		print_r($result);
		print_r($suspicious);
		var_dump('++ ' . $success);
		var_dump('-- ' . $fail);

	}

	public function actionAction()
	{
		$users = Users::find()
			->select([
				Users::tableName() . '.id',
				Users::tableName() . '.mail',
				Users::tableName() . '.balance'
			])->addSelect(['invoice' => sprintf(
				"(SELECT COUNT(*) FROM %s WHERE user_id = %s AND status = 3)",
				Invoice::tableName(),
				Users::tableName() . '.id'
			)])->orderBy(['id' => SORT_ASC])->asArray()->all();

		foreach ($users as $key => $value) {

			if ($value['invoice'] < 2) {
				continue;
			}

			/** @var Invoice $inv */
			$inv = Invoice::find()->where([
				'user_id' => $value['id'],
				'status' => 3
			])->orderBy([
				'id' => SORT_DESC
			])->one();

			$accs = Account::findAll([
				'user_id' => $value['id']
			]);

			$users_accs = '';

			foreach ($accs as $acc) {
				$users_accs .= ($users_accs == '' ? '' : ', ') . $acc->login;
			}

			echo $value['mail'] . ' | ' .
				$value['invoice'] . ' | ' .
				(round($value['balance'] / 86400, 1)) . ' | ' .
				count($accs) . ' | ' .
				$users_accs . ' | ' .
				mb_strcut($inv->date, 0, 10) .
				PHP_EOL;

		}

	}

	private function checkEmail($email)
	{
		// получаем данные об MX-записи домена, указанного в email
		$path = explode('@', $email);
		$hostname = $path[1];
		$mx = dns_get_record($hostname, DNS_MX);
		$hostname = empty($mx) ? $hostname : $mx[0]['target'];

		// открываем сокет и создаем поток
		$socket = $this->connectToServer($hostname, 25, 10);
		if (!$socket) {
			return false;
		} else {
			// отправляем пустую строку, чтобы получить приветствие сервера
			$this->sWrite($socket, '');
			// представляемся сами
			$this->sWrite($socket, "EHLO sitename.ru\r\n");
			$ehlo = '<no-reply@sitename.ru>';
			$rcpt = '<' . $email . '>';

			$response = $this->sWrite($socket, "MAIL FROM: $ehlo\r\n");
			$response = $this->sWrite($socket, "RCPT TO: $rcpt\r\n");

			// закрываем соединение
			$this->sWrite($socket, "QUIT\r\n");
			fclose($socket);

			// ниже идет простейшая обработка полученного ответа
			if (substr_count($response, '550') > 0) {
				return false;
			} else {
				if (substr_count($response, '250') > 0) {
					return true;
				}
			}
		}
	}

	private function connectToServer($hostname, $port, $timeout)
	{
		$socket = fsockopen($hostname, $port, $errno, $errstr, $timeout);
		if (!$socket) {
			return false;
		}
		return $socket;
	}

	private function sWrite($socket, $data)
	{
		// отправляем команду в сокет
		fputs($socket, $data);
		// получаем первый байт ответа от сервера
		$answer = fread($socket, 1);
		// узнаем информацию о состоянии потока
		$remains = socket_get_status($socket);
		// и получаем оставшиеся байты ответа от сервера
		if ($remains-- > 0) $answer .= fread($socket, $remains['unread_bytes']);
		// функция возвращает ответ от сервера на переданную команду
		return $answer;
	}
}